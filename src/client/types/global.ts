import { Router } from 'next/router';
import { ReactNode } from 'react';

export interface IAppState {
  isLogined: boolean;
}

export interface IAppMsg {
  messageText: string;
  severity: 'error' | 'warning' | 'info' | 'success';
  open: boolean;
}

export interface IGlobalMessagePayload {
  messageText: string;
  severity: 'error' | 'warning' | 'info' | 'success';
}

export interface IGlobalState {
  isWechatWeb: boolean;
  router: Router;
  alertMessage: IGlobalMessagePayload & { open: boolean };
}

export interface IGlobalAction {
  type: string;
  payload?: any;
}

export interface IGlobalContainerProps {
  children: ReactNode;
}

export interface IColumnType {
  label: string;
  sortable?: boolean;
  field: string;
}
