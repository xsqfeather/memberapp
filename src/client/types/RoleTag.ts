export interface IRoleTag {
  name: string;
  createdAt: string;
  updatedAt: string;
}
