import { GetServerSideProps } from 'next';

export interface IMenuItem {
  icon: any;
  label: string;
  href: string;
}

export interface ILayoutProps {
  menuItems?: IMenuItem[];
  isLogined: boolean;
}
export interface IPageProps {
  layoutProps: ILayoutProps;
  mainProps: any;
}
