export interface IUser {
  username: string;
  password: string;
}

export interface IUserList {
  loading: boolean;
  list: IUser[];
}
