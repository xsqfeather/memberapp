export interface IProfile {
  username: string;
  nikename: string;
  gender: string;
}
