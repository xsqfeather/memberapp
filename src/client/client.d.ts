declare module 'ra-data-graphql';
declare module 'ra-data-graphql-simple';

declare module '*.module.scss' {
  const classes: { [key: string]: string };
  export default classes;
}

declare module '*.jpg' {
  let _: string;
  export = _;
}

declare module '*.jpeg' {
  let _: string;
  export = _;
}

declare module '*.png' {
  let _: string;
  export = _;
}

declare module '*.svg' {
  let _: string;
  export = _;
}

declare module 'react-image-picker';

declare module 'react-area-linkage';
declare module 'area-data';
declare module 'ra-language-chinese';
declare module 'socket.io-stream';
