export const REST_API =
  process.env.NODE_ENV === 'production'
    ? 'http://hk.beingred.net/api'
    : 'http://127.0.0.1:3000/api';

export const HOST =
  process.env.NODE_ENV === 'production'
    ? 'https://hk.beingred.net'
    : 'http://127.0.0.1:3000';
export const APP_KEY = 'ABgBJUAzbaJYoudW8hlqfUaoIKKh2aA8';
