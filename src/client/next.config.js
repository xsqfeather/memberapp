const withImages = require('next-images');
module.exports = withImages({
  webpack5: false,
  distDir: '../../client_build',
  compress: true,
  webpack(config, options) {
    return config;
  },
});
