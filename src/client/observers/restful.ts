import { stringify } from 'query-string';
import { REST_API } from '../constants/endpoints';
import { ajax } from 'rxjs/ajax';

interface IGetListParams {
  pagination: {
    page: number;
    perPage: number;
  };
  sort: {
    field: string;
    order: string;
  };
  filter:
    | {
        [x: string]: string;
      }
    | {};
}

interface ICreateParams {
  data: {
    [x: string]: any;
  };
}

interface IUpdateParams {
  id: number;
  data: {
    [x: string]: any;
  };
}

export function getOneSubject(resource: string, id: number, token?: string) {
  const url = `${REST_API}/${resource}/${id.toString()}`;
  return ajax({
    url,
    method: 'GET',
    headers: token
      ? {
          Authorization: 'bearer ' + token,
        }
      : {},
  });
}

export function getListSubject(
  resource: string,
  params: IGetListParams,
  token?: string,
) {
  const { page, perPage } = params.pagination;
  const { field, order } = params.sort;
  const query = {
    sort: JSON.stringify([field, order]),
    range: JSON.stringify([(page - 1) * perPage, page * perPage - 1]),
    filter: JSON.stringify(params.filter),
  };
  const url = `${REST_API}/${resource}?${stringify(query)}`;
  return ajax({
    url,
    method: 'GET',
    headers: token
      ? {
          Authorization: 'bearer ' + token,
        }
      : {},
  });
}

export function createOneSubject(
  resource: string,
  body: object,
  token?: string,
) {
  const url = `${REST_API}/${resource}`;
  return ajax({
    url,
    method: 'POST',
    body,
    headers: token
      ? {
          Authorization: 'bearer ' + token,
        }
      : {},
  });
}

export function deleteOneSubject(
  resource: string,
  params: {
    id: number;
  },
  token?: string,
) {
  const url = `${REST_API}/${resource}/${params.id}`;
  return ajax({
    url,
    method: 'DELETE',
    headers: token
      ? {
          Authorization: 'bearer ' + token,
        }
      : {},
  });
}

export function deleteManySubject(
  resource: string,
  params: object,
  token?: string,
) {
  const query = {
    filter: JSON.stringify(params),
  };
  const url = `${REST_API}/${resource}/?${stringify(query)}`;
  return ajax({
    url,
    method: 'DELETE',
    headers: token
      ? {
          Authorization: 'bearer ' + token,
        }
      : {},
  });
}
