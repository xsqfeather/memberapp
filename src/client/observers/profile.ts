import { bind } from '@react-rxjs/core';
import { createSignal } from '@react-rxjs/utils';
import { IProfile } from '../types/profile';

const [profileChange$, setProfile] = createSignal<IProfile>();
const [useProfile, profile$] = bind<IProfile>(profileChange$, {
  username: '',
  nikename: '',
  gender: '',
});

export { setProfile, useProfile, profile$ };
