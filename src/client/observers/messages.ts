import { bind } from '@react-rxjs/core';
import { createSignal } from '@react-rxjs/utils';

const [ChatOpenChange$, setChatOpen] = createSignal<boolean>();
const [useChatOpen, _chatOpen$] = bind<boolean>(ChatOpenChange$, false);

const [CloseUrlChange$, setCloseUrl] = createSignal<string>();
const [useCloseUrl, _closeUrl$] = bind<string>(CloseUrlChange$, '/messages');

const [ChattingUserIdChange$, setChattingUserId] = createSignal<number>();
const [useChattingUserId, _chattingUserId] = bind<number>(
  ChattingUserIdChange$,
  0,
);

export {
  useChatOpen,
  setChatOpen,
  setCloseUrl,
  useCloseUrl,
  useChattingUserId,
  setChattingUserId,
};
