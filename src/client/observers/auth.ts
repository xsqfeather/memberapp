import { REST_API } from '../constants/endpoints';
import { ajax } from 'rxjs/ajax';

export function getSMSSubject(phone: string) {
  const url = `${REST_API}/sms`;
  return ajax({
    url,
    method: 'POST',
    headers: {
      /*some headers*/
    },
    body: {
      phone,
    },
  });
}

interface ILoginParams {
  loginType: 'PHONE_SMS' | 'ANY_PASSWORD';
  phone?: string;
  password?: string;
  sms?: string;
}

export function getLoginSubject({ loginType, phone, sms }: ILoginParams) {
  const url = `${REST_API}/sessions`;

  switch (loginType) {
    case 'PHONE_SMS':
      return ajax({
        url,
        method: 'POST',
        headers: {
          /*some headers*/
        },
        body: {
          phone,
          sms,
          loginType,
        },
      });

    default:
      throw new Error('NO_LOGIN_TYPE_PASS');
  }
}
