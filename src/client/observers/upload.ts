import { ajax } from 'rxjs/ajax';

const ClientId = '9b1dea8a1fdcb97';
// const ClientSecret = "1f2735cadc3d91e1f3bf2d6eba0c64e7d15ef61d";

export function uploadToWoogemeSubject(body: FormData) {
  const url = `https://bianhong.qingmiao168.com/api/distributed-images`;
  const uploadHeaders = new Headers();
  return ajax({
    url,
    method: 'POST',
    body,
    headers: uploadHeaders,
  });
}
