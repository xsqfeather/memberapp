import { bind } from '@react-rxjs/core';
import { createSignal } from '@react-rxjs/utils';

import { IAppMsg, IAppState } from '../types/global';

const [RouteChanging$, setRouteChanging] = createSignal<boolean>();
const [userRouteChanging, _routeChanging$] = bind<boolean>(
  RouteChanging$,
  false,
);

const [AppStateChange$, setAppState] = createSignal<IAppState>();
const [useAppState, appState$] = bind<IAppState>(AppStateChange$, {
  isLogined: false,
});

const [AppMsgChange$, setAppMsg] = createSignal<IAppMsg>();
const [useAppMsg, appMsg$] = bind<IAppMsg>(AppMsgChange$, {
  open: false,
  messageText: '',
  severity: 'info',
});

appMsg$.subscribe((msg) => {
  if (msg.open === true) {
    setTimeout(() => {
      setAppMsg({
        ...msg,
        open: false,
      });
    }, 2000);
  }
});

export {
  useAppState,
  setAppState,
  appState$,
  setRouteChanging,
  userRouteChanging,
  setAppMsg,
  useAppMsg,
};
