import { Typography } from '@material-ui/core';
import React, { FC } from 'react';
const Dashboard: FC = () => {
  return (
    <div>
      <Typography
        variant="h4"
        style={{
          marginTop: 200,
          textAlign: 'center',
        }}
      >
        欢迎进入变红平台管理
      </Typography>
    </div>
  );
};

export default Dashboard;
