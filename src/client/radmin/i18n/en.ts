import { TranslationMessages } from 'react-admin';
import englishMessages from 'ra-language-english';

const EnglishMessage: TranslationMessages = {
  ...englishMessages,
};

export default EnglishMessage;
