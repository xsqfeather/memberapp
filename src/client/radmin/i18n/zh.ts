import { TranslationMessages } from 'ra-core';
import chineseMessages from 'ra-language-chinese';

const ChineseMessage: TranslationMessages = {
  ...chineseMessages,
  'Request failed with status code 401': '用户名或者密码错误',
  pos: {
    search: '搜索',
    menu: {
      sales: '销售',
      tags: '标签管理',
      blogs: '各种帖子管理',
      planets: '星球管理',
      firms: '企业应用',
      videos: '视频管理',
      menuCourses: '课程管理',
      users: '用户管理',
    },
  },
  resources: {
    'role-tags': {
      name: '角色标签',
      fields: {
        tagName: '角色标签名',
        description: '描述',
      },
    },
    blogs: {
      name: '当红管理',
      fields: {
        createdAt: '创建时间',
        character: '人物',
        coverImage: '封面',
        title: '标题',
        body: '正文',
        videoId: '讲解视频',
      },
    },
    topics: {
      name: '话题管理',
      fields: {
        title: '标题',
        des: '详情',
        cover: '封面',
        status: '审核状态',
        publisherId: '上传者',
        createdAt: '创建时间',
      },
    },
    courses: {
      name: '课程系列',
      fields: {
        title: '系列标题',
        description: '描述',
        coverId: '封面',
        coverUrl: '封面',
      },
    },
    lessons: {
      name: '课程',
      fields: {
        title: '课程名',
        body: '课程内容',
        courseId: '所属系列',
        coverUrl: '封面',
        isPublished: '是否发布',
        planetId: '所属星球',
        roleIds: '角色',
        tagIds: '标签',
      },
    },
    'lesson-nodes': {
      name: '课节',
      fields: {
        title: '标题',
        body: '内容',
        lessonId: '所属课程',
        coverId: '封面',
        'cover.originUrl': '封面',
        isPublished: '是否发布',
      },
    },
    exams: {
      name: '测验管理',
      fields: {
        name: '题目',
        type: '题型',
        score: '分值',
        order: '序号',
        options: '选项',
        answer: '答案',
        'lessonNode.lessonId': '所属课程',
        lessonNodeId: '所属章节',
        optionValue: '选项值',
        optionName: '选项名称',
        actions: '操作',
      },
    },
    firms: {
      name: '应用',
      fields: {
        name: '应用名',
        title: '应用标题',
        slogan: '标语',
        appKey: '密钥',
        appUrls: '应用链接',
        description: '描述',
      },
    },

    users: {
      name: '用户',
    },

    'fake-users': {
      name: '机器人 |||| 机器人',
      fields: {
        avatar: '头像',
        nickname: '昵称',
        phone: '电话',
        email: '邮箱',
        personalNote: '个性签名',
        postFreq: '发帖频率',
        followFreq: '粉人频率',
        updatedAt: '最后更新',
        username: '用户名',
        gender: '性别',
        profile: {
          avatar: '头像',
        },
      },
    },

    'perfer-tags': {
      name: '偏好标签',
      fields: {
        tagName: '标签名',
        description: '描述',
      },
    },
    videos: {
      name: '视频',
      fields: {},
    },
    planets: {
      name: '星球',
      fields: {
        name: '星球名称',
        level: '等级',
        limit: '所需分数',
      },
    },
    slides: {
      name: '轮播图',
      fields: {
        position: '位置',
      },
    },
  },
};

export default ChineseMessage;
