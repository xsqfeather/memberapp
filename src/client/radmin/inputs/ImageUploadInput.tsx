import { InputProps, useInput } from 'ra-core';
import { ImageUpload } from '../../components/SliceImageUploader';
import {
  Card,
  CardContent,
  Paper,
  Typography,
  IconButton,
} from '@material-ui/core';
import { makeStyles, createStyles, Theme } from '@material-ui/core/styles';
import ClearRoundedIcon from '@material-ui/icons/ClearRounded';

const useStyles = makeStyles((theme: Theme) =>
  createStyles({
    paper: {
      marigin: 20,
      display: 'flex',
      flexDirection: 'column',
      textAlign: 'center',
      justifyContent: 'center',
      alignContent: 'center',
      height: 200,
      width: 300,
      overflow: 'hidden',
      position: 'relative',
      '&:hover': {
        '&>span': {
          display: 'block',
        },
      },
    },
    button: {
      position: 'absolute',
      left: '50%',
      display: 'none',
      transform: 'translateX(-50%)',
      cursor: 'pointer',
      transition: '.5s',
      '&:hover': {
        boxShadow: '0 0 3px rgba(216, 58, 58, 0.9)',
      },
    },
  }),
);

const ImageUploadInput = (
  props: InputProps & { Description?: string; style?: any },
) => {
  const { Description, style: outerStyle } = props;

  const classes = useStyles();

  const {
    input: { onChange, value },
  } = useInput(props);
  return (
    <Card>
      <CardContent>
        <div style={{ display: 'flex', alignItems: 'center' }}>
          <Typography>
            {Description ? Description : '上传图片'} &nbsp;&nbsp;&nbsp;
          </Typography>
          {value === '' ? (
            <ImageUpload
              getURL={(url: string) => {
                console.log(url);
                onChange(url);
              }}
            />
          ) : (
            <Paper className={classes.paper} style={outerStyle}>
              <img style={{ transition: '.5s' }} src={value} />
              {
                <IconButton
                  className={classes.button}
                  color="primary"
                  aria-label="clean up picture"
                  component="span"
                  onClick={() => {
                    onChange('');
                  }}
                >
                  <ClearRoundedIcon fontSize="large" />
                </IconButton>
              }
            </Paper>
          )}
        </div>
      </CardContent>
    </Card>
  );
};

export default ImageUploadInput;
