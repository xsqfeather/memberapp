import React, { memo, useEffect, useRef, useState } from 'react';
import { InputProps, useInput } from 'ra-core';
import VideoUpload from '../../components/VideoUpload';
import { REST_API } from '../../constants/endpoints';
import { Card, CardContent, Typography } from '@material-ui/core';
import Button from '@material-ui/core/Button';
import axios from 'axios';
import hlsjs from 'hls.js';

export default memo(function VideoUploadInput(
  props: InputProps & { size?: 'small' | 'default' },
) {
  const { size } = props;
  const {
    input: { onChange, value },
  } = useInput(props);
  const [previewSrc, setPreviewSrc] = useState('');
  const [hlsSrc, sethlsSrc] = useState('');
  useEffect(() => {
    if (value) {
      axios.get(`${REST_API}/lesson-node-videos/${value}`).then((res) => {
        const { isLive, liveUrl, downloadUrl } = res.data;
        if (isLive) {
          sethlsSrc(liveUrl);
        } else {
          setPreviewSrc(downloadUrl);
        }
      });
    }
  }, [value]);
  return (
    <Card>
      <CardContent
        style={{
          position: 'relative',
          display: 'flex',
          flexDirection: 'column',
          textAlign: 'center',
          justifyContent: 'center',
          alignContent: 'center',
        }}
      >
        {value ? (
          <VideoPreview
            nativeSrc={previewSrc}
            hlsSrc={hlsSrc}
            onChange={(ev) => {
              onChange(ev);
            }}
          />
        ) : (
          <VideoUpload
            uploadUrl={`${REST_API}/lesson-node-videos`}
            size={size ?? 'default'}
            getIds={(ids, records) => {
              onChange(ids[0]);
            }}
            onFileChange={(file) => {
              setPreviewSrc(URL.createObjectURL(file));
            }}
          />
        )}
      </CardContent>
    </Card>
  );
});

export function VideoPreview(props: {
  hlsSrc?: string;
  nativeSrc?: string;
  onChange: (event: any) => void;
}) {
  const { nativeSrc, hlsSrc, onChange } = props;
  const videoRef = useRef<HTMLVideoElement>();
  useEffect(() => {
    if (hlsSrc) {
      const video = videoRef.current;
      const hls = new hlsjs();
      hls.loadSource(hlsSrc);
      hls.attachMedia(videoRef.current);
      return () => {
        video.pause();
        hls.destroy();
        console.log('已销毁HLS');
      };
    }
  }, [hlsSrc]);
  return (
    <>
      {hlsSrc !== '' ? (
        <>
          <video controls ref={videoRef}></video>
        </>
      ) : (
        <>
          <Typography
            style={{
              position: 'absolute',
              width: '94%',
              color: 'white',
              fontSize: '22px',
              fontWeight: 900,
              background: 'rgba(0,0,0,.4)',
            }}
          >
            后台正在转码,当前预览非流媒体...
          </Typography>
          <video
            controlsList={'nodownload'}
            src={nativeSrc}
            style={{
              height: '100%',
              width: '100%',
            }}
            controls
          ></video>
        </>
      )}
      <Button
        variant="contained"
        color="primary"
        component="span"
        onClick={() => {
          onChange('');
        }}
      >
        更换视频
      </Button>
    </>
  );
}
