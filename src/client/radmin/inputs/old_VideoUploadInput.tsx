import { FilePond, registerPlugin } from 'react-filepond';

import 'filepond/dist/filepond.min.css';

import FilePondPluginImageExifOrientation from 'filepond-plugin-image-exif-orientation';
import FilePondPluginImagePreview from 'filepond-plugin-image-preview';
import 'filepond-plugin-image-preview/dist/filepond-plugin-image-preview.css';
import { useState } from 'react';
import FilePondPluginFileValidateType from 'filepond-plugin-file-validate-type';
import { InputProps, useInput } from 'ra-core';
registerPlugin(
  FilePondPluginImageExifOrientation,
  FilePondPluginFileValidateType,
  FilePondPluginImagePreview,
);

const VideoUploadInput = (props: InputProps) => {
  const [files, setFiles] = useState([]);
  const [imgUUID, setImgUUID] = useState('xxxxxx');
  const {
    input: { onChange },
  } = useInput(props);
  return (
    <FilePond
      files={files}
      onupdatefiles={setFiles}
      allowMultiple={false}
      maxFiles={3}
      acceptedFileTypes={['video/*']}
      server={{
        url: '/api/',
        process: {
          url: 'videos',
          ondata: (formData: any) => {
            console.log(formData);
            return formData;
          },
          onload: (response) => {
            const video = JSON.parse(response);
            setImgUUID(video.uuid);
            onChange(video.id);
            return video.originUrl;
          },
        },
        revert: {
          url: 'videos' + '/' + imgUUID,
          method: 'DELETE',
        },
      }}
      name="file"
      labelIdle={'点击或者拖拽上传视频'}
      labelFileLoading="载入中"
      labelFileProcessing="上传中"
      labelFileProcessingComplete="上传完成"
      labelFileProcessingAborted="上传取消"
      labelFileProcessingError="上传出错"
      labelFileProcessingRevertError="转换出错"
      labelFileRemoveError="删除出错"
    />
  );
};

export default VideoUploadInput;
