import * as React from 'react';
import { FC, useState } from 'react';
import { useSelector } from 'react-redux';
import { useMediaQuery, Theme, Box } from '@material-ui/core';
import {
  useTranslate,
  DashboardMenuItem,
  MenuItemLink,
  MenuProps,
} from 'react-admin';

import { AppState } from '../types';
import SubMenu from './SubMenu';
import {
  EditAttributesSharp,
  PersonPin,
  PlaceRounded,
  RoomRounded,
  TagFaces,
  VideoCall,
  VideocamOff,
  VideocamOffRounded,
  VideoLabel,
} from '@material-ui/icons';

type MenuName =
  | 'menuPlanets'
  | 'menuSales'
  | 'menuCustomers'
  | 'menuPlanets'
  | 'menuFirms'
  | 'menuVideos'
  | 'menuCourses'
  | 'menuPosts'
  | 'menuUsers'
  | 'menuBlogs';

const Menu: FC<MenuProps> = ({ onMenuClick, logout, dense = false }) => {
  const [state, setState] = useState({
    menuCatalog: true,
    menuSales: true,
    menuCustomers: true,
    menuBlogs: true,
    menuPlanets: true,
    menuFirms: true,
    menuVideos: true,
    menuCourses: true,
    menuPosts: true,
  });
  const translate = useTranslate();
  const isXSmall = useMediaQuery((theme: Theme) =>
    theme.breakpoints.down('xs'),
  );
  const open = useSelector((state: AppState) => state.admin.ui.sidebarOpen);
  useSelector((state: AppState) => state.theme);

  const handleToggle = (menu: MenuName) => {
    setState((state) => ({ ...state, [menu]: !state[menu] }));
  };

  return (
    <Box mt={1}>
      <DashboardMenuItem onClick={onMenuClick} sidebarIsOpen={open} />
      <MenuItemLink
        to="/slides"
        primaryText={translate(`resources.slides.name`, {
          smart_count: 2,
        })}
        onClick={onMenuClick}
        sidebarIsOpen={open}
        dense={dense}
      />

      <SubMenu
        handleToggle={() => handleToggle('menuSales')}
        isOpen={state.menuSales}
        sidebarIsOpen={open}
        name="pos.menu.tags"
        icon={<TagFaces />}
        dense={dense}
      >
        <MenuItemLink
          to={`/role-tags`}
          primaryText={translate(`resources.role-tags.name`, {
            smart_count: 2,
          })}
          leftIcon={<PersonPin />}
          onClick={onMenuClick}
          sidebarIsOpen={open}
          dense={dense}
        />
        <MenuItemLink
          to={`/perfer-tags`}
          primaryText={translate(`resources.perfer-tags.name`, {
            smart_count: 2,
          })}
          leftIcon={<RoomRounded />}
          onClick={onMenuClick}
          sidebarIsOpen={open}
          dense={dense}
        />
      </SubMenu>
      <SubMenu
        handleToggle={() => handleToggle('menuBlogs')}
        isOpen={state.menuBlogs}
        sidebarIsOpen={open}
        name="pos.menu.blogs"
        icon={<PlaceRounded />}
        dense={dense}
      >
        <MenuItemLink
          to={`/blogs`}
          primaryText={translate(`resources.blogs.name`, {
            smart_count: 2,
          })}
          leftIcon={<EditAttributesSharp />}
          onClick={onMenuClick}
          sidebarIsOpen={open}
          dense={dense}
        />
        <MenuItemLink
          to={`/topics`}
          primaryText={translate(`resources.topics.name`, {
            smart_count: 2,
          })}
          leftIcon={<EditAttributesSharp />}
          onClick={onMenuClick}
          sidebarIsOpen={open}
          dense={dense}
        />
      </SubMenu>

      <SubMenu
        handleToggle={() => handleToggle('menuPlanets')}
        isOpen={state.menuPlanets}
        sidebarIsOpen={open}
        name="pos.menu.planets"
        icon={<PlaceRounded />}
        dense={dense}
      >
        <MenuItemLink
          to={`/planets`}
          primaryText={translate(`resources.planets.name`, {
            smart_count: 2,
          })}
          leftIcon={<EditAttributesSharp />}
          onClick={onMenuClick}
          sidebarIsOpen={open}
          dense={dense}
        />
      </SubMenu>

      <SubMenu
        handleToggle={() => handleToggle('menuCourses')}
        isOpen={state.menuCourses}
        sidebarIsOpen={open}
        name="pos.menu.menuCourses"
        icon={<PlaceRounded />}
        dense={dense}
      >
        <MenuItemLink
          to={`/courses`}
          primaryText={translate(`resources.courses.name`, {
            smart_count: 2,
          })}
          leftIcon={<EditAttributesSharp />}
          onClick={onMenuClick}
          sidebarIsOpen={open}
          dense={dense}
        />
        <MenuItemLink
          to={`/lessons`}
          primaryText={translate(`resources.lessons.name`, {
            smart_count: 2,
          })}
          leftIcon={<EditAttributesSharp />}
          onClick={onMenuClick}
          sidebarIsOpen={open}
          dense={dense}
        />
      </SubMenu>
      <SubMenu
        handleToggle={() => handleToggle('menuUsers')}
        isOpen={state.menuFirms}
        sidebarIsOpen={open}
        name="pos.menu.users"
        icon={<PlaceRounded />}
        dense={dense}
      >
        <MenuItemLink
          to={`/users`}
          primaryText={translate(`resources.users.name`, {
            smart_count: 2,
          })}
          leftIcon={<EditAttributesSharp />}
          onClick={onMenuClick}
          sidebarIsOpen={open}
          dense={dense}
        />
        <MenuItemLink
          to={`/fake-users`}
          primaryText={translate(`resources.fake-users.name`, {
            smart_count: 2,
          })}
          leftIcon={<EditAttributesSharp />}
          onClick={onMenuClick}
          sidebarIsOpen={open}
          dense={dense}
        />
      </SubMenu>
      <SubMenu
        handleToggle={() => handleToggle('menuFirms')}
        isOpen={state.menuFirms}
        sidebarIsOpen={open}
        name="pos.menu.firms"
        icon={<PlaceRounded />}
        dense={dense}
      >
        <MenuItemLink
          to={`/firms`}
          primaryText={translate(`resources.firms.name`, {
            smart_count: 2,
          })}
          leftIcon={<EditAttributesSharp />}
          onClick={onMenuClick}
          sidebarIsOpen={open}
          dense={dense}
        />
      </SubMenu>
      <SubMenu
        handleToggle={() => handleToggle('menuVideos')}
        isOpen={state.menuVideos}
        sidebarIsOpen={open}
        name="pos.menu.videos"
        icon={<VideoLabel />}
        dense={dense}
      >
        <MenuItemLink
          to={`/videos`}
          primaryText={translate(`resources.videos.name`, {
            smart_count: 2,
          })}
          leftIcon={<VideoLabel />}
          onClick={onMenuClick}
          sidebarIsOpen={open}
          dense={dense}
        />
      </SubMenu>
      {/* <SubMenu
                handleToggle={() => handleToggle('menuPosts')}
                isOpen={state.menuPosts}
                sidebarIsOpen={open}
                name="pos.menu.posts"
                icon={<VideoLabel />}
                dense={dense}
            >
                <MenuItemLink
                    to={`/posts`}
                    primaryText={translate(`resources.posts.name`, {
                        smart_count: 2,
                    })}
                    leftIcon={<VideoLabel />}
                    onClick={onMenuClick}
                    sidebarIsOpen={open}
                    dense={dense}
                />
              
            </SubMenu> */}
      {isXSmall && logout}
    </Box>
  );
};

export default Menu;
