import { Children, cloneElement, FC, memo, ReactElement, useMemo } from 'react';
import {
  CreateContextProvider,
  FormInput,
  FormWithRedirect,
  FormWithRedirectProps,
  useCreateController,
  useEditController,
  EditContextProvider,
} from 'react-admin';

export interface FormContentProps
  extends Omit<FormWithRedirectProps, 'render'> {
  state: {
    visible: boolean;
    id: string | boolean;
  };
  mode: 'edit' | 'create';
  onClose: () => void;
}

export const FormCreateLayer: FC<{
  children: ReactElement<FormContentProps>;
  resource: string;
  initialValues?: Record<string, any>;
}> = memo(({ children, resource, initialValues }) => {
  const createControllerProps = useCreateController({
    resource,
    basePath: `/${resource}`,
  });
  const { basePath, save, saving, version } = createControllerProps;
  return (
    <CreateContextProvider value={createControllerProps}>
      <FormWithRedirect
        basePath={basePath}
        redirect={null}
        resource={resource}
        save={save}
        saving={saving}
        initialValues={initialValues}
        version={version}
        render={(formProps) =>
          cloneElement(children, { ...formProps, ...children.props })
        }
      />
    </CreateContextProvider>
  );
});
export const FormEditLayer: FC<{
  children: ReactElement<FormContentProps>;
  resource: string;
  id: string;
  initialValues?: Record<string, any>;
}> = memo(({ children, resource, id, initialValues }) => {
  const editControllerProps = useEditController({
    resource,
    basePath: `/${resource}`,
    mutationMode: 'optimistic',
    id,
  });
  const { basePath, save, saving, version, record } = editControllerProps;
  return (
    <EditContextProvider value={editControllerProps}>
      <FormWithRedirect
        basePath={basePath}
        redirect={null}
        resource={resource}
        save={save}
        saving={saving}
        version={version}
        record={record}
        initialValues={initialValues}
        render={(formProps) =>
          cloneElement(children, { ...formProps, ...children.props })
        }
      />
    </EditContextProvider>
  );
});
export const FormInjectLayer: FC<{
  variant?: string;
  margin?: string;
  context: {
    resource?: string;
    basePath?: string;
    record?: Record<string, any>;
  };
  children: ReactElement[];
}> = memo(({ children, margin, variant, context }) => {
  const itemProps = useMemo(() => {
    return {
      basePath: context.basePath,
      record: context.record,
      resource: context.resource,
    };
  }, [context.basePath, context.record, context.resource]);
  return (
    <form>
      {Children.map(children, (child) => (
        <FormInput
          {...itemProps}
          input={child}
          variant={child.props.variant || variant}
          margin={child.props.margin || margin}
        />
      ))}
    </form>
  );
});
