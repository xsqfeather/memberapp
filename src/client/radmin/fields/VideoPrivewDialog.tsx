import { Dialog, DialogContent, DialogTitle } from '@material-ui/core';
import DPlayer from 'dplayer';
import Hls from 'hls.js';
import { useEffect, useState } from 'react';

const VideoPrivewDialog = ({
  preview,
  handlePreviewClose,
  previewRecord,
}: any) => {
  const [dp, setDp] = useState(null);
  const [hl, setHl] = useState(null);
  const handleOnEnter = () => {
    const container = document.getElementById(
      previewRecord.uuid + '-video-player',
    );
    const newdp = new DPlayer({
      container: container,
      autoplay: true,
      video: {
        url: previewRecord.liveUrl,
        type: 'customHls',
        customType: {
          customHls: function (video: any, player: any) {
            const hls = new Hls();
            setHl(hls);
            hls.loadSource(video.src);
            hls.attachMedia(video);
          },
        },
      },
    });
    setDp(newdp);
  };
  useEffect(() => {
    return () => {
      if (dp) {
        console.log('毁灭吧播放器');

        dp.destroy();
        setDp(null);
      }
      if (hl) {
        hl.destroy();
      }
    };
  }, []);
  return (
    <Dialog
      onExited={() => {
        if (dp) {
          console.log('毁灭吧播放器');
          dp.destroy();
          setDp(null);
        }
        if (hl) {
          hl.destroy();
        }
      }}
      onEntered={handleOnEnter}
      open={preview}
      onClose={() => {
        if (dp) {
          console.log('毁灭吧播放器');
          dp.destroy();
          setDp(null);
        }
        if (hl) {
          hl.destroy();
        }
        handlePreviewClose();
      }}
    >
      <DialogTitle>{previewRecord.label}</DialogTitle>
      <DialogContent>
        <div id={previewRecord.uuid + '-video-player'}></div>
      </DialogContent>
    </Dialog>
  );
};

export default VideoPrivewDialog;
