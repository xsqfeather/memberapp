import { Chip } from '@material-ui/core';

export function TagListField({ record, source }: any) {
  return (
    <>
      {record && Array.isArray(record[source])
        ? record[source].map((t: string, i: number) => (
            <Chip key={i} label={t} />
          ))
        : '-'}
    </>
  );
}
