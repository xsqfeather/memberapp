import * as React from 'react';
import { Admin, Resource } from 'react-admin';
import polyglotI18nProvider from 'ra-i18n-polyglot';
import blogs from './resources/blogs';
import RestdataProvider from '../dataProvider/rest';
import ChineseMessage from './i18n/zh';
import EnglishMessage from './i18n/en';
import MyLayout from './layouts/Layout';
import Dashboard from './dashboard/Dashboard';
import { RestAuthProvider } from '../authProvider';
import { Login } from './layouts';
import role_tags from './resources/role_tags';
import planets from './resources/planets';
import perfer_tags from './resources/perfer_tags';
import firms from './resources/firms';
import videos from './resources/videos';
import courses from './resources/courses';
import lessons from './resources/lessons';
import lesson_nodes from './resources/lesson_nodes';
import posts from './resources/posts';
import fake_users from './resources/fake_users';
import users from './resources/users';
import exams from './resources/exams';
import slides from './resources/slides';
import topics from './resources/topics';

const AdminApp = () => {
  const [dataProvider, setDataPrider] = React.useState(null);
  const [i18nProvider, setI18nProvider] = React.useState(null);
  const [authProvider, setAuthProvider] = React.useState(null);

  React.useEffect(() => {
    if (process.browser) {
      const token = localStorage.getItem('token');
      const dataPrivider = new RestdataProvider(token);
      setDataPrider(dataPrivider);
      const authProvider = new RestAuthProvider(token);
      setAuthProvider(authProvider);
      const i18nProvider = polyglotI18nProvider((locale: string) => {
        if (locale === 'en') {
          return EnglishMessage;
        }
        return ChineseMessage;
      }, 'zh');
      setI18nProvider(i18nProvider);
    }
  }, []);

  if (!dataProvider) {
    return (
      <div className="loader-container">
        <div className="loader">Loading...</div>
      </div>
    );
  }
  if (!i18nProvider) {
    return (
      <div className="loader-container">
        <div className="loader">Loading...</div>
      </div>
    );
  }
  if (!authProvider) {
    return (
      <div className="loader-container">
        <div className="loader">Loading...</div>
      </div>
    );
  }

  return (
    <Admin
      dataProvider={dataProvider}
      i18nProvider={i18nProvider}
      layout={MyLayout}
      dashboard={Dashboard}
      authProvider={authProvider}
      loginPage={Login}
    >
      <Resource name="blogs" {...blogs} /> 
      <Resource name="topics" {...topics} />
      <Resource name="role-tags" {...role_tags} />
      <Resource name="planets" {...planets} />
      <Resource name="perfer-tags" {...perfer_tags} />
      <Resource name="firms" {...firms} />
      <Resource name="videos" {...videos} />
      <Resource name="courses" {...courses} />
      <Resource name="lessons" {...lessons} />
      <Resource name="lesson-nodes" {...lesson_nodes} />
      <Resource name="exams" {...exams} />
      <Resource name="posts" {...posts} />
      <Resource name="fake-users" {...fake_users} />
      <Resource name="images" />
      <Resource name="users" {...users} />
      <Resource name="slides" {...slides} />
    </Admin>
  );
};

export default AdminApp;
