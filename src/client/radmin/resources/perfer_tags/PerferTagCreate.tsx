import { Create, CreateProps, SimpleForm, TextInput } from 'ra-ui-materialui';

export default function PerferTagCreate(props: CreateProps) {
  return (
    <Create {...props}>
      <SimpleForm redirect="list">
        <TextInput source="tagName" />
        <TextInput resettable source="description" multiline rows={4} />
      </SimpleForm>
    </Create>
  );
}
