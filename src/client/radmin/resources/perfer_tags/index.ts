import PerferTagCreate from './PerferTagCreate';
import PerferTagEdit from './PerferTagEdit';
import PerferTagList from './PerferTagList';

const perfer_tags = {
  create: PerferTagCreate,
  list: PerferTagList,
  edit: PerferTagEdit,
};

export default perfer_tags;
