import { Edit, CreateProps, SimpleForm, TextInput } from 'ra-ui-materialui';

export default function PerferTagEdit(props: CreateProps) {
  return (
    <Edit {...props}>
      <SimpleForm redirect="list">
        <TextInput source="tagName" />
        <TextInput source="description" multiline rows={4} />
      </SimpleForm>
    </Edit>
  );
}
