import {
  Filter,
  Datagrid,
  List,
  ListProps,
  TextField,
  TextInput,
} from 'react-admin';

const PlanetTagFilter = (props) => (
  <Filter {...props}>
    <TextInput label="pos.search" resettable source="q" alwaysOn />
  </Filter>
);

export default function PerferTagList(props: ListProps) {
  return (
    <List {...props} filters={<PlanetTagFilter />}>
      <Datagrid rowClick="edit">
        <TextField source="tagName" />
        <TextField source="description" />
      </Datagrid>
    </List>
  );
}
