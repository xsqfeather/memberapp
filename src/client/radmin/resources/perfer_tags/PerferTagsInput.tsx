import { useDataProvider } from 'ra-core';
import { AutocompleteArrayInput } from 'ra-ui-materialui';
import { useEffect, useState } from 'react';
import { useFormState } from 'react-final-form';

export const PerferTagsInput = (props: any) => {
  const { values } = useFormState();
  const [choices, setChoices] = useState(
    [] as {
      id: string;
      name: string;
    }[],
  );
  const dataProvider = useDataProvider();
  useEffect(() => {
    if (values.roleTagId) {
      dataProvider
        .getList('perfer-tags', {
          pagination: {
            page: 1,
            perPage: 100,
          },
          sort: {
            field: 'createdAt',
            order: 'DESC',
          },
          filter: {
            roleTagId: values.roleTagId,
          },
        })
        .then(({ data }: any) => {
          setChoices(
            data
              .filter((tag: any) => tag.tagName !== values.tagName)
              .map((tag: any) => {
                return {
                  id: tag.tagName,
                  name: tag.tagName,
                };
              }),
          );
        });
    }
  }, [values.roleTagId]);
  console.log({ choices });

  return (
    <>
      {choices.length > 0 && (
        <AutocompleteArrayInput {...props} choices={choices} fullwidth />
      )}
    </>
  );
};
