import { useState } from 'react';
import {
  Filter,
  Datagrid,
  List,
  ListProps,
  TextField,
  TextInput,
  FormDataConsumer,
  ReferenceField,
  ImageField,
} from 'react-admin';
import VideoPrivewDialog from '../../fields/VideoPrivewDialog';
import IsLiveField from '../videos/IsLiveField';

const CourseFilter = (props) => (
  <Filter {...props}>
    <TextInput label="pos.search" resettable source="q" alwaysOn />
  </Filter>
);

export default function LessonNodeList(props: ListProps) {
  const [preview, setPreview] = useState(false);
  const [previewRecord, setPreviewRecord] = useState({} as any);
  const handlePreview = (record: any) => {
    setPreviewRecord(record);
    setPreview(true);
  };

  const handlePreviewClose = () => {
    setPreviewRecord({});
    setPreview(false);
  };
  return (
    <>
      <VideoPrivewDialog
        preview={preview}
        handlePreviewClose={handlePreviewClose}
        handlePreview={handlePreview}
        previewRecord={previewRecord}
      />
      <List
        {...props}
        filters={<CourseFilter />}
        sort={{
          field: 'createdAt',
          order: 'DESC',
        }}
      >
        <Datagrid rowClick="show">
          <ImageField source="cover.originUrl" />
          <TextField source="title" />
          <TextField source="body" />
          <ReferenceField source="lessonId" reference="lessons">
            <TextField source="title" />
          </ReferenceField>
          <ReferenceField link={false} source="videoId" reference="videos">
            <IsLiveField source="isLive" onPreview={handlePreview} />
          </ReferenceField>
        </Datagrid>
      </List>
    </>
  );
}
