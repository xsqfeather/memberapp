import LessonNodeCreate from './LessonNodeCreate';
import LessonNodeEdit from './LessonNodeEdit';
import LessonNodeList from './LessonNodeList';
import LessonNodeShow from './LessonNodeShow';

const lesson_nodes = {
  create: LessonNodeCreate,
  list: LessonNodeList,
  show: LessonNodeShow,
  edit: LessonNodeEdit,
};

export default lesson_nodes;
