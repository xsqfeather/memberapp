import {
  AutocompleteInput,
  Create,
  CreateProps,
  ReferenceInput,
  SimpleForm,
  TextInput,
} from 'ra-ui-materialui';
import VideoUploadInput from '../../inputs/VideoUploadInput';
import { REST_API } from '../../../constants/endpoints';

export default function LessonNodeCreate(props: CreateProps) {
  return (
    <Create {...props}>
      <SimpleForm>
        <VideoUploadInput
          source="videoId"
          uploadUrl={`${REST_API}/lesson-node-videos`}
        />
        <TextInput source="title" fullWidth resettable />
        <TextInput resettable fullWidth source="body" multiline rows={4} />
        <ReferenceInput fullWidth source="lessonId" reference="lessons">
          <AutocompleteInput optionText="title" />
        </ReferenceInput>
      </SimpleForm>
    </Create>
  );
}
