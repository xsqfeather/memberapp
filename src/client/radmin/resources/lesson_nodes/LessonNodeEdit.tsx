import { Button } from '@material-ui/core';
import {
  AutocompleteInput,
  CreateProps,
  Edit,
  EditProps,
  ImageField,
  ReferenceField,
  ReferenceInput,
  SimpleForm,
  TextInput,
} from 'ra-ui-materialui';
import { useState } from 'react';
import VideoPrivewDialog from '../../fields/VideoPrivewDialog';
import VideoUploadInput from '../../inputs/VideoUploadInput';
import IsLiveField from '../videos/IsLiveField';
import { REST_API } from '../../../constants/endpoints';

export default function LessonNodeEdit(props: EditProps) {
  const [videoEditable, setVideoEditable] = useState(false);
  const [preview, setPreview] = useState(false);
  const [previewRecord, setPreviewRecord] = useState({} as any);
  const handlePreview = (record: any) => {
    setPreviewRecord(record);
    setPreview(true);
  };

  const handlePreviewClose = () => {
    setPreviewRecord({});
    setPreview(false);
  };
  return (
    <>
      <VideoPrivewDialog
        preview={preview}
        handlePreviewClose={handlePreviewClose}
        handlePreview={handlePreview}
        previewRecord={previewRecord}
      />
      <Edit {...props}>
        <SimpleForm redirect="list">
          {videoEditable && (
            <VideoUploadInput
              source="videoId"
              uploadUrl={`${REST_API}/lesson-node-videos`}
            />
          )}
          {!videoEditable && (
            <div
              style={{
                display: 'flex',
                alignItems: 'center',
              }}
            >
              <ReferenceField link={false} source="videoId" reference="videos">
                <IsLiveField source="isLive" onPreview={handlePreview} />
              </ReferenceField>
              <Button
                onClick={() => {
                  setVideoEditable(true);
                }}
                color="primary"
                variant="contained"
              >
                更改视频
              </Button>
            </div>
          )}

          <TextInput source="title" fullWidth resettable />
          <TextInput resettable fullWidth source="body" multiline rows={4} />
          <ReferenceInput fullWidth source="lessonId" reference="lessons">
            <AutocompleteInput optionText="title" />
          </ReferenceInput>
        </SimpleForm>
      </Edit>
    </>
  );
}
