import {
  ImageField,
  ReferenceField,
  TextField,
  Show,
  ShowProps,
  SimpleShowLayout,
} from 'ra-ui-materialui';

export default function LessonNodeShow(props: ShowProps) {
  return (
    <Show {...props}>
      <SimpleShowLayout>
        <TextField source="title" />
        <TextField source="body" />
        <ReferenceField source="coverId" reference="images">
          <ImageField source="originUrl" />
        </ReferenceField>
      </SimpleShowLayout>
    </Show>
  );
}
