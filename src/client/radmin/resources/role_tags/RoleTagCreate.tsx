import {
  BooleanInput,
  Create,
  CreateProps,
  SimpleForm,
  TextInput,
} from 'ra-ui-materialui';

export default function RoleTagCreate(props: CreateProps) {
  return (
    <Create {...props}>
      <SimpleForm redirect="list">
        <TextInput source="tagName" />
        <TextInput source="description" multiline rows={4} />
      </SimpleForm>
    </Create>
  );
}
