import {
  BooleanInput,
  Edit,
  CreateProps,
  SimpleForm,
  TextInput,
} from 'ra-ui-materialui';

export default function RoleTagEdit(props: CreateProps) {
  return (
    <Edit {...props}>
      <SimpleForm redirect="list">
        <TextInput source="tagName" />
        <TextInput source="description" multiline rows={4} />
      </SimpleForm>
    </Edit>
  );
}
