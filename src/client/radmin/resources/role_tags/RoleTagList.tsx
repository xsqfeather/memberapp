import {
  Datagrid,
  Filter,
  List,
  ListProps,
  ReferenceInput,
  SelectInput,
  TextField,
  TextInput,
} from 'react-admin';

const RoleTagsFilter = (props) => (
  <Filter {...props}>
    <TextInput resettable label="搜索" source="q" alwaysOn />
  </Filter>
);

export default function RoleTagList(props: ListProps) {
  return (
    <List {...props} filters={<RoleTagsFilter />}>
      <Datagrid rowClick="edit">
        <TextField source="tagName" />
        <TextField source="description" />
      </Datagrid>
    </List>
  );
}
