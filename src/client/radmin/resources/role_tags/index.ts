import RoleTagCreate from './RoleTagCreate';
import RoleTagEdit from './RoleTagEdit';
import RoleTagList from './RoleTagList';

const role_tags = {
  create: RoleTagCreate,
  list: RoleTagList,
  edit: RoleTagEdit,
};

export default role_tags;
