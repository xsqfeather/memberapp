import {
  FC,
  Fragment,
  memo,
  ReactComponentElement,
  useCallback,
  useEffect,
  useMemo,
  useRef,
  useState,
} from 'react';
import {
  Dialog,
  DialogTitle,
  DialogContent,
  DialogActions,
  Button,
  CircularProgress,
  makeStyles,
  createStyles,
  Theme,
  Typography,
  IconButton,
} from '@material-ui/core';
import {
  FormWithRedirect,
  useDelete,
  useCreateController,
  useEditController,
  CreateContextProvider,
  EditContextProvider,
  FormWithRedirectProps,
  FormInput,
  useRefresh,
} from 'react-admin';
import { AutocompleteInput, ReferenceInput, TextInput } from 'ra-ui-materialui';
import VideoUpload from '../../../inputs/VideoUploadInput';
import { Save as SaveIcon, Add as IconContentAdd } from '@material-ui/icons';
import CloseIcon from '@material-ui/icons/Close';
import { REST_API } from '../../../../constants/endpoints';
import { useForm } from 'react-final-form';
import axios from 'axios';
import hlsjs from 'hls.js';

const FormItem: FC<{
  basePath: string;
  record: any;
  resource: string;
  variant: string;
  margin: string;
  children: ReactComponentElement<any>;
}> = ({ children, margin, variant, ...props }) => {
  return (
    <FormInput
      {...props}
      input={children}
      variant={children.props.variant || variant}
      margin={children.props.margin || margin}
    />
  );
};
const useStyles = makeStyles((theme: Theme) =>
  createStyles({
    button: {
      margin: theme.spacing(1),
      boxShadow: 'none',
    },
    modalTitle: {
      margin: 0,
      padding: theme.spacing(2),
    },
    modalContent: {
      padding: theme.spacing(2),
      minHeight: '240px',
    },
    modalActions: {
      margin: 0,
      padding: theme.spacing(1),
    },
    closeButton: {
      position: 'absolute',
      right: theme.spacing(1),
      top: theme.spacing(1),
      color: theme.palette.grey[500],
    },
    loadingWrap: {
      display: 'flex',
      alignItems: 'center',
      justifyContent: 'center',
      width: '100%',
      height: '100%',
    },
    video: {
      width: '100%',
    },
  }),
);
const ModalForm: FC<
  { visible: boolean; onClose: () => void; isCreate: boolean } & Omit<
    FormWithRedirectProps,
    'render'
  >
> = ({
  visible,
  basePath,
  children,
  className,
  isCreate,
  component: Component,
  handleSubmit,
  invalid,
  margin,
  mutationMode,
  pristine,
  record,
  redirect,
  resource,
  saving,
  submitOnEnter,
  toolbar,
  undoable,
  variant,
  validating,
  onClose,
  ...props
}) => {
  const itemProps = useMemo(() => {
    return {
      basePath,
      record,
      resource,
      variant,
      margin,
    };
  }, [basePath, record, resource, variant, margin]);
  const styles = useStyles();
  const onSaveAndClose = useCallback(
    (e) => {
      handleSubmit(e);
      onClose();
    },
    [handleSubmit, onClose],
  );
  const form = useForm();
  const label = useMemo(() => (isCreate ? '创建课节' : '更新课节'), [isCreate]);
  useEffect(() => {
    if (visible) form.reset();
  }, [form, visible]);
  return (
    <Dialog open={visible} onClose={onClose} fullWidth aria-label={label}>
      <DialogTitle>{label}</DialogTitle>
      <DialogContent>
        <form className="modal-form" {...sanitizeRestProps(props)}>
          <FormItem {...itemProps}>
            <VideoUpload
              size="small"
              source="videoId"
              uploadUrl={`${REST_API}/lesson-node-videos`}
            />
          </FormItem>
          <FormItem {...itemProps}>
            <TextInput source="title" fullWidth resettable />
          </FormItem>
          <FormItem {...itemProps}>
            <TextInput resettable fullWidth source="body" multiline rows={4} />
          </FormItem>
          <FormItem {...itemProps}>
            <ReferenceInput fullWidth source="lessonId" reference="lessons">
              <AutocompleteInput optionText="title" />
            </ReferenceInput>
          </FormItem>
        </form>
      </DialogContent>
      <DialogActions>
        <Button onClick={onClose}>取消</Button>
        {isCreate ? (
          <>
            <Button
              variant="contained"
              color="primary"
              className={styles.button}
              onClick={onSaveAndClose}
              disabled={saving}
            >
              保存并关闭
            </Button>
            <Button
              variant="contained"
              color="primary"
              className={styles.button}
              startIcon={<SaveIcon />}
              disabled={saving}
              onClick={handleSubmit}
            >
              保存并新建
            </Button>
          </>
        ) : (
          <Button
            variant="contained"
            color="primary"
            className={styles.button}
            onClick={onSaveAndClose}
            disabled={saving}
          >
            保存
          </Button>
        )}
      </DialogActions>
    </Dialog>
  );
};

export namespace LessonNodeDialog {
  export interface BehaviorProps {
    state: {
      visible: boolean;
      id: string | null;
    };
    onClose(): void;
  }
  export const Create: FC = memo(() => {
    const [visible, setVisible] = useState(false);
    const createControllerProps = useCreateController({
      resource: 'lesson-nodes',
      basePath: '/lesson-nodes',
    });
    const { basePath, resource, save, saving, version } = createControllerProps;
    const onClick = useCallback(() => {
      setVisible(true);
    }, []);
    const onClose = useCallback(() => {
      setVisible(false);
    }, []);
    return (
      <Fragment>
        <Button variant="contained" color="primary" onClick={onClick}>
          <IconContentAdd />
          创建课节
        </Button>
        <CreateContextProvider value={createControllerProps}>
          <FormWithRedirect
            basePath={basePath}
            redirect={null}
            resource={resource}
            save={save}
            saving={saving}
            version={version}
            render={(props) => (
              <ModalForm
                isCreate
                onClose={onClose}
                visible={visible}
                {...props}
              />
            )}
          />
        </CreateContextProvider>
      </Fragment>
    );
  });
  export const Update: FC<BehaviorProps> = memo(({ state, onClose }) => {
    const editControllerProps = useEditController({
      resource: 'lesson-nodes',
      basePath: '/lesson-nodes',
      mutationMode: 'optimistic',
      id: state.id,
    });
    const { basePath, resource, save, saving, version, record } =
      editControllerProps;
    return (
      <EditContextProvider value={editControllerProps}>
        <FormWithRedirect
          basePath={basePath}
          redirect={null}
          resource={resource}
          save={save}
          saving={saving}
          version={version}
          record={record}
          render={(props) => (
            <ModalForm
              isCreate={false}
              onClose={onClose}
              visible={state.visible}
              {...props}
            />
          )}
        />
      </EditContextProvider>
    );
  });
  export const Delete: FC<BehaviorProps> = memo(({ state, onClose }) => {
    const [deleteOne, { loading }] = useDelete();
    const refresh = useRefresh();
    const onClick = useCallback(async () => {
      try {
        await deleteOne('lesson-nodes', state.id!);
        refresh();
        onClose();
      } catch (e) {
        console.error(e);
      }
    }, [deleteOne, state.id, onClose, refresh]);
    return (
      <Dialog open={state.visible} fullWidth aria-label="删除确认">
        <DialogTitle>删除确认</DialogTitle>
        <DialogContent>你确认要删除该课节吗？</DialogContent>
        <DialogActions>
          {!loading && <Button onClick={onClose}>取消</Button>}
          <Button variant="contained" color="primary" onClick={onClick}>
            {loading && <CircularProgress color="secondary" />}
            <span>确认</span>
          </Button>
        </DialogActions>
      </Dialog>
    );
  });
  export const Preview: FC<BehaviorProps> = memo(({ state, onClose }) => {
    const styles = useStyles();
    const [liveUrl, setLiveUrl] = useState<string | null>(null);
    const [isEmpty, setIsEmpty] = useState(false);
    const [loading, setLoading] = useState(true);
    const [isTranscoding, setIsTranscoding] = useState(false);
    const videoRef = useRef<HTMLVideoElement>();
    useEffect(() => {
      if (!state.id) {
        setIsEmpty(true);
        setLoading(false);
        return void 0;
      }
      setLoading(true);
      setIsEmpty(false);
      axios.get(`${REST_API}/lesson-node-videos/${state.id}`).then((res) => {
        const { isLive, liveUrl } = res.data;
        if (!isLive || !liveUrl) setIsTranscoding(true);
        else setLiveUrl(liveUrl);
        setLoading(false);
      });
    }, [state.id]);
    useEffect(() => {
      if (!liveUrl || !videoRef.current) return void 0;
      if (!hlsjs.isSupported()) {
        console.error('不支持HLS播放');
        return void 0;
      }
      const hls = new hlsjs();
      hls.loadSource(liveUrl);
      hls.attachMedia(videoRef.current);
      const video = videoRef.current;
      video.play();
      return () => {
        video.pause();
        hls.destroy();
        console.log('已销毁HLS');
      };
    }, [liveUrl, state.visible]);
    return (
      <Dialog
        open={state.visible}
        onClose={onClose}
        fullWidth
        aria-label="预览视频"
      >
        <DialogTitle disableTypography className={styles.modalTitle}>
          <Typography variant="h6">预览视频</Typography>
          <IconButton
            aria-label="close"
            className={styles.closeButton}
            onClick={onClose}
          >
            <CloseIcon />
          </IconButton>
        </DialogTitle>
        <DialogContent dividers className={styles.modalContent}>
          {isTranscoding && <Typography>视频转码中</Typography>}
          {isEmpty && <Typography>未上传视频</Typography>}
          {loading && (
            <div className={styles.loadingWrap}>
              <CircularProgress />
            </div>
          )}
          {liveUrl && (
            <video controls className={styles.video} ref={videoRef}>
              {liveUrl}
            </video>
          )}
        </DialogContent>
      </Dialog>
    );
  });
  export const useModalBehavior = () => {
    const [state, setState] = useState<BehaviorProps['state']>({
      visible: false,
      id: null,
    });
    const onClose = useCallback(() => {
      setState({
        visible: false,
        id: null,
      });
    }, []);
    return {
      model: {
        state,
        onClose,
      },
      open: useCallback((id: string) => {
        setState({
          visible: true,
          id,
        });
      }, []),
      close: onClose,
      visible: state.visible,
    };
  };
}

const sanitizeRestProps = ({
  active,
  dirty,
  dirtyFields,
  dirtyFieldsSinceLastSubmit,
  dirtySinceLastSubmit,
  error,
  errors,
  form,
  hasSubmitErrors,
  hasValidationErrors,
  initialValues,
  modified = null,
  modifiedSinceLastSubmit,
  save = null,
  submitError,
  submitErrors,
  submitFailed,
  submitSucceeded,
  submitting,
  touched = null,
  valid,
  values,
  visited = null,
  __versions = null,
  ...props
}: any) => props;
