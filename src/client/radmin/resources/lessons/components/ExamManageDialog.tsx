import {
  FC,
  memo,
  useCallback,
  useMemo,
  useState,
  MouseEvent,
  useRef,
  useEffect,
} from 'react';
import {
  Dialog,
  DialogTitle,
  DialogContent,
  DialogActions,
  Button,
  Drawer,
  Typography,
  IconButton,
  CircularProgress,
  List,
  ListItem,
  ListItemText,
  ButtonGroup,
  Theme,
  createStyles,
  Popper,
  Fade,
  Paper,
  ClickAwayListener,
} from '@material-ui/core';
import {
  Datagrid,
  FunctionField,
  ListBase,
  TextField,
  useGetOne,
  useCreateContext,
  useDelete,
  useRefresh,
  useEditContext,
} from 'react-admin';
import { EXAM_TYPE_MAP, SCORE_CHOICES } from '../../exams/constants';
import { makeStyles } from '@material-ui/core/styles';
import {
  Close as CloseIcon,
  Visibility as VisibilityIcon,
  Edit as EditIcon,
  Delete as DeleteIcon,
} from '@material-ui/icons';
import {
  FormContentProps,
  FormCreateLayer,
  FormEditLayer,
  FormInjectLayer,
} from '../../../layouts/FormModal';
import { NumberInput, SelectInput, TextInput } from 'ra-ui-materialui';
import { ExamInput } from '../../exams/ExamCreate';
import axios from 'axios';

interface ExamDeleteConfirmProps {
  model: {
    id: string;
    anchor: HTMLElement;
    visible: boolean;
  };
  onClose(): void;
}

const answerParse = (type: any, answer: string) => {
  if (type === 'judgment') return answer === 'F' ? '错误' : '正确';
  return answer;
};
const useExamNodePreviewStyles = makeStyles((theme) => ({
  modalTitle: {
    margin: 0,
    padding: theme.spacing(2),
  },
  modalContent: {
    padding: theme.spacing(2),
    height: '400px',
    overflow: 'auto',
    position: 'relative',
  },
  closeButton: {
    position: 'absolute',
    right: theme.spacing(1),
    top: theme.spacing(1),
    color: theme.palette.grey[500],
  },
  optionList: {
    padding: 0,
    '& > li': {
      padding: '0',
    },
  },
  optionItem: {
    display: 'flex',
    gap: '8px',
    alignItems: 'center',
  },
  loadingWrap: {
    display: 'block',
    height: '100%',
    width: '100%',
    position: 'absolute',
    left: 0,
    top: 0,
    zIndex: 9,
    background: '#ffffff',
  },
}));
const useListStyles = makeStyles({
  content: {
    width: '60vw',
  },
  name: {
    display: 'block',
    overflow: 'hidden',
    whiteSpace: 'nowrap',
    textOverflow: 'ellipsis',
    maxWidth: '400px',
  },
  list: {
    marginTop: '16px',
  },
});
const useDialogStyles = makeStyles((theme: Theme) =>
  createStyles({
    button: {
      margin: theme.spacing(1),
      boxShadow: 'none',
    },
    modalTitle: {
      margin: 0,
      padding: theme.spacing(2),
    },
    modalContent: {
      padding: theme.spacing(2),
      minHeight: '240px',
    },
    modalActions: {
      margin: 0,
      padding: theme.spacing(1),
    },
    closeButton: {
      position: 'absolute',
      right: theme.spacing(1),
      top: theme.spacing(1),
      color: theme.palette.grey[500],
    },
    loadingWrap: {
      display: 'flex',
      alignItems: 'center',
      justifyContent: 'center',
      width: '100%',
      height: '100%',
    },
    video: {
      width: '100%',
    },
    popper: {
      zIndex: theme.zIndex.modal,
      '&[x-placement*="bottom"] $arrow': {
        top: 0,
        left: 0,
        marginTop: '-0.9em',
        width: '3em',
        height: '1em',
        '&::before': {
          borderWidth: '0 1em 1em 1em',
          borderColor: `transparent transparent ${theme.palette.background.paper} transparent`,
        },
      },
      '&[x-placement*="top"] $arrow': {
        bottom: 0,
        left: 0,
        marginBottom: '-0.9em',
        width: '3em',
        height: '1em',
        '&::before': {
          borderWidth: '1em 1em 0 1em',
          borderColor: `${theme.palette.background.paper} transparent transparent transparent`,
        },
      },
      '&[x-placement*="right"] $arrow': {
        left: 0,
        marginLeft: '-0.9em',
        height: '3em',
        width: '1em',
        '&::before': {
          borderWidth: '1em 1em 1em 0',
          borderColor: `transparent ${theme.palette.background.paper} transparent transparent`,
        },
      },
      '&[x-placement*="left"] $arrow': {
        right: 0,
        marginRight: '-0.9em',
        height: '3em',
        width: '1em',
        '&::before': {
          borderWidth: '1em 0 1em 1em',
          borderColor: `transparent transparent transparent ${theme.palette.background.paper}`,
        },
      },
    },
    arrow: {
      position: 'absolute',
      fontSize: 7,
      width: '3em',
      top: '50%',
      transform: 'translateY(-50%)',
      height: '3em',
      '&::before': {
        content: '""',
        margin: 'auto',
        display: 'block',
        width: 0,
        height: 0,
        borderStyle: 'solid',
      },
    },
  }),
);
const useDeleteConfirm = () => {
  const [state, setState] = useState<ExamDeleteConfirmProps['model']>({
    visible: false,
    id: null,
    anchor: null,
  });
  // 解决点击两次popper触发按钮
  const metadataRef = useRef({
    opening: false,
  });
  return {
    model: state,
    visible: state.visible,
    open: useCallback((id: string, event: MouseEvent<HTMLElement>) => {
      metadataRef.current.opening = true;
      setState({
        id,
        anchor: event.currentTarget,
        visible: true,
      });
      setTimeout(() => {
        metadataRef.current.opening = false;
      }, 0);
    }, []),
    onClose: useCallback(() => {
      if (metadataRef.current.opening) return void 0;
      setState({
        id: null,
        anchor: null,
        visible: false,
      });
    }, []),
  };
};
const useExamNodePreview = () => {
  const [state, setState] = useState<{ visible: boolean; id: string | null }>({
    visible: false,
    id: null,
  });
  return [
    useMemo(() => {
      return {
        ...state,
        onClose: () => {
          setState({
            id: null,
            visible: false,
          });
        },
      };
    }, [state]),
    useCallback((id: string) => {
      setState({
        id,
        visible: true,
      });
    }, []),
  ] as const;
};

const ExamNodePreview: FC<{
  id: string | null;
  visible: boolean;
  onClose: () => void;
}> = memo(({ id, visible, onClose }) => {
  const styles = useExamNodePreviewStyles();
  const { data, loading } = useGetOne('exams', id!, {
    enabled: visible,
  });
  const hasOptions = useMemo(
    () => Array.isArray(data?.options),
    [data?.options],
  );
  const answer = useMemo(() => {
    if (!data) return null;
    return answerParse(data.type, data.answer);
  }, [data?.answer, data?.type]);
  return (
    <Dialog open={visible} fullWidth onClose={onClose} aria-label="测验查看">
      <DialogTitle disableTypography className={styles.modalTitle}>
        <Typography variant="h6">测验查看</Typography>
        <IconButton
          aria-label="close"
          className={styles.closeButton}
          onClick={onClose}
        >
          <CloseIcon />
        </IconButton>
      </DialogTitle>
      <DialogContent dividers className={styles.modalContent}>
        {loading && (
          <div className={styles.loadingWrap}>
            <CircularProgress />
          </div>
        )}
        {data && (
          <List>
            <ListItem>
              <ListItemText
                primary="题目："
                secondary={
                  <Typography
                    component="span"
                    variant="body2"
                    color="textPrimary"
                  >
                    {data.name}
                  </Typography>
                }
              />
            </ListItem>
            {hasOptions && (
              <ListItem>
                <ListItemText
                  primary="选项："
                  secondary={
                    <List dense className={styles.optionList}>
                      {data.options.map((it, i) => (
                        <ListItem key={it.optionName + i}>
                          <ListItemText
                            primary={
                              <Typography
                                component="span"
                                variant="body2"
                                color="textPrimary"
                              >
                                {it.optionName}
                              </Typography>
                            }
                            secondary={
                              <Typography component="span">
                                {it.optionValue}
                              </Typography>
                            }
                            className={styles.optionItem}
                          />
                        </ListItem>
                      ))}
                    </List>
                  }
                  secondaryTypographyProps={{ component: 'div' }}
                />
              </ListItem>
            )}
            <ListItem>
              <ListItemText
                primary="分值："
                secondary={
                  <Typography
                    component="span"
                    variant="body2"
                    color="textPrimary"
                  >
                    {data.score}分
                  </Typography>
                }
              />
            </ListItem>
            <ListItem>
              <ListItemText
                primary="答案："
                secondary={
                  <Typography
                    component="span"
                    variant="body2"
                    color="textPrimary"
                  >
                    {answer}
                  </Typography>
                }
              />
            </ListItem>
          </List>
        )}
      </DialogContent>
    </Dialog>
  );
});
const ExamNodeListActions: FC<{
  resource: string;
  basePath: string;
  source: string;
  record?: any;
  onReview: (id: string) => void;
  onEdit: (id: string) => void;
  onDelete: (id: string, ev: MouseEvent<HTMLButtonElement>) => void;
}> = ({ children, onReview, onDelete, onEdit, ...props }) => {
  return (
    <ButtonGroup size="small">
      <Button
        onClick={() => onReview(props.record.id)}
        startIcon={<VisibilityIcon />}
      >
        查看
      </Button>
      <Button onClick={() => onEdit(props.record.id)} startIcon={<EditIcon />}>
        编辑
      </Button>
      <Button
        onClick={(ev) => onDelete(props.record.id, ev)}
        startIcon={<DeleteIcon />}
      >
        删除
      </Button>
    </ButtonGroup>
  );
};

const ExamContentDialog: FC<FormContentProps> = memo(
  ({ state, onClose, handleSubmit, saving, mode }) => {
    const styles = useDialogStyles();
    const onSaveAndClose = useCallback(
      (e) => {
        handleSubmit(e);
        onClose();
      },
      [handleSubmit, onClose],
    );
    const createContext = useCreateContext();
    const editContext = useEditContext();
    const context = useMemo(() => {
      return mode === 'create' ? createContext : editContext;
    }, [mode, createContext, editContext]);
    return (
      <Dialog
        open={state.visible}
        onClose={onClose}
        fullWidth
        aria-label="创建测验题"
      >
        <DialogTitle>创建测验题</DialogTitle>
        <DialogContent>
          <FormInjectLayer context={context}>
            <TextInput
              source="name"
              multiline
              style={{ width: '500px' }}
              required
            />
            <NumberInput source="order" required />
            <SelectInput source="score" choices={SCORE_CHOICES} required />
            <ExamInput />
          </FormInjectLayer>
        </DialogContent>
        <DialogActions>
          <Button onClick={onClose}>取消</Button>
          <Button
            variant="contained"
            color="primary"
            className={styles.button}
            onClick={onSaveAndClose}
            disabled={saving}
          >
            保存
          </Button>
        </DialogActions>
      </Dialog>
    );
  },
);
const ExamDeleteConfirm: FC<ExamDeleteConfirmProps> = memo(
  ({ model: { id, anchor, visible }, onClose }) => {
    const styles = useDialogStyles();
    const [deleteOne, { loading }] = useDelete();
    const arrowRef = useRef<HTMLSpanElement>();
    const refresh = useRefresh();
    const onClick = useCallback(async () => {
      try {
        await deleteOne('exams', id!);
        refresh();
        onClose();
      } catch (e) {
        console.error(e);
      }
    }, [deleteOne, id, onClose, refresh]);
    const modifiers = useMemo(() => {
      return {
        arrow: {
          enabled: true,
          element: arrowRef.current,
        },
      };
    }, []);
    return (
      <ClickAwayListener onClickAway={onClose}>
        <Popper
          open={visible}
          anchorEl={anchor}
          placement="right"
          transition
          className={styles.popper}
          modifiers={modifiers}
        >
          {({ TransitionProps }) => (
            <Fade {...TransitionProps} timeout={350}>
              <Paper variant="outlined">
                <span className={styles.arrow} ref={arrowRef} />
                <DialogContent>删除该测验题？</DialogContent>
                <DialogActions>
                  <Button size="small" onClick={onClose}>
                    取消
                  </Button>
                  <Button size="small" onClick={onClick} color="primary">
                    删除
                  </Button>
                </DialogActions>
              </Paper>
            </Fade>
          )}
        </Popper>
      </ClickAwayListener>
    );
  },
);

export namespace ExamManageDialog {
  export interface BehaviorProps {
    state: {
      visible: boolean;
      id: string | null;
    };
    onClose(): void;
  }
  export const useModalBehavior = () => {
    const [state, setState] = useState<BehaviorProps['state']>({
      visible: false,
      id: null,
    });
    const onClose = useCallback(() => {
      setState({
        visible: false,
        id: null,
      });
    }, []);
    return {
      model: {
        state,
        onClose,
      },
      open: useCallback((id: string) => {
        setState({
          visible: true,
          id,
        });
      }, []),
      close: onClose,
      visible: state.visible,
    };
  };
  export const List: FC<BehaviorProps> = memo(({ state, onClose }) => {
    const [scoreInfo, setScoreInfo] = useState({ total: 60, existing: 60 });
    const styles = useListStyles();
    const [dialogState, openDialog] = useExamNodePreview();
    const createBehavior = useModalBehavior();
    const editBehavior = useModalBehavior();
    const deleteConfirm = useDeleteConfirm();
    const loadScoreInfo = useCallback(async () => {
      const result = await axios.get(
        `/api/exams/score-info?lessonNodeId=${state.id}`,
      );
      setScoreInfo({
        ...result.data,
      });
    }, [state.id]);
    useEffect(() => {
      if (dialogState.visible || editBehavior.visible || deleteConfirm.visible)
        return void 0;
      loadScoreInfo();
    }, [
      loadScoreInfo,
      createBehavior.visible,
      editBehavior.visible,
      deleteConfirm.visible,
    ]);
    return (
      <Drawer
        open={state.visible}
        anchor="right"
        onClose={onClose}
        aria-label="测验列表"
      >
        {dialogState.visible && <ExamNodePreview {...dialogState} />}
        {createBehavior.visible && (
          <FormCreateLayer
            resource="exams"
            initialValues={{ lessonNodeId: state.id! }}
          >
            <ExamContentDialog mode="create" {...createBehavior.model} />
          </FormCreateLayer>
        )}
        {editBehavior.visible && (
          <FormEditLayer
            resource="exams"
            initialValues={{ lessonNodeId: state.id! }}
            id={editBehavior.model.state.id}
          >
            <ExamContentDialog mode="edit" {...editBehavior.model} />
          </FormEditLayer>
        )}
        {deleteConfirm.visible && (
          <ExamDeleteConfirm
            model={deleteConfirm.model}
            onClose={deleteConfirm.onClose}
          />
        )}
        <DialogTitle>测验列表</DialogTitle>
        <DialogContent className={styles.content} id="examList">
          <div>
            <Button
              variant="contained"
              color="primary"
              onClick={() => createBehavior.open(state.id!)}
              disabled={scoreInfo.total <= scoreInfo.existing}
            >
              创建测验题
            </Button>
            <Typography component="div" style={{ marginTop: '16px' }}>
              当前测验分数: {scoreInfo.existing} / {scoreInfo.total}
            </Typography>
          </div>
          <ListBase
            basePath="exams"
            resource="exams"
            filter={{ lessonNodeId: state.id! }}
          >
            <Datagrid
              resource="exams"
              className={styles.list}
              empty={
                <div
                  style={{
                    fontWeight: 'bold',
                    marginTop: '20%',
                    textAlign: 'center',
                    fontSize: '18px',
                  }}
                >
                  暂无数据
                </div>
              }
            >
              <TextField source="order" />
              <TextField source="name" className={styles.name} />
              <FunctionField
                source="type"
                render={(record) => EXAM_TYPE_MAP[record.type]}
              />
              <TextField source="score" />
              <FunctionField
                source="answer"
                render={(record) => answerParse(record.type, record.answer)}
              />
              <ExamNodeListActions
                source="actions"
                resource="exams"
                basePath="/exams"
                onReview={openDialog}
                onEdit={editBehavior.open}
                onDelete={deleteConfirm.open}
              />
            </Datagrid>
          </ListBase>
        </DialogContent>
        <DialogActions>
          <Button onClick={onClose} variant="contained" color="primary">
            关闭
          </Button>
        </DialogActions>
      </Drawer>
    );
  });
}
