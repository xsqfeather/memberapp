import {
  Filter,
  Datagrid,
  List,
  ListProps,
  TextField,
  TextInput,
  ReferenceField,
  ImageField,
  BooleanField,
} from 'react-admin';
import { makeStyles } from '@material-ui/core';

const PlanetTagFilter = (props) => (
  <Filter {...props}>
    <TextInput label="pos.search" resettable source="q" alwaysOn />
  </Filter>
);

const useLessonListStyle = makeStyles(() => ({
  cover: {
    '& img': {
      objectFit: 'cover',
      height: '80px',
    },
  },
  body: {
    display: 'block',
    overflow: 'hidden',
    whiteSpace: 'nowrap',
    textOverflow: 'ellipsis',
    maxWidth: '400px',
  },
}));

export default function LessonList(props: ListProps) {
  const styles = useLessonListStyle();
  return (
    <List
      {...props}
      filters={<PlanetTagFilter />}
      sort={{
        field: 'createdAt',
        order: 'DESC',
      }}
    >
      <Datagrid rowClick="edit">
        <ImageField source="coverUrl" className={styles.cover} />
        <TextField source="title" />
        <TextField source="body" className={styles.body} />
        <BooleanField source="isPublished" />
        <ReferenceField source="courseId" reference="courses">
          <TextField source="title" />
        </ReferenceField>
      </Datagrid>
    </List>
  );
}
