import {
  Button,
  Tabs,
  Tab,
  AppBar,
  Box,
  Paper,
  ButtonGroup,
  Dialog,
  DialogTitle,
  DialogContent,
  Typography,
  IconButton,
  List,
  ListItem,
  ListItemText,
  CircularProgress,
} from '@material-ui/core';
import { useGetList } from 'ra-core';
import {
  Edit,
  EditProps,
  ImageField,
  ReferenceArrayInput,
  ReferenceField,
  ReferenceInput,
  SelectArrayInput,
  SelectInput,
  TextInput,
} from 'ra-ui-materialui';
import {
  FC,
  useCallback,
  useEffect,
  useMemo,
  useRef,
  useState,
  memo,
} from 'react';
import ImageUploadInput from '../../inputs/ImageUploadInput';
import { of, lastValueFrom } from 'rxjs';
import { fromFetch } from 'rxjs/fetch';
import { switchMap, catchError } from 'rxjs/operators';
import { DndProvider } from 'react-dnd';
import { HTML5Backend } from 'react-dnd-html5-backend';
import CardSortMove from '../../../components/CardSortMove/CardSortMove';
import { SimpleForm } from 'react-admin';
import { makeStyles, Theme } from '@material-ui/core/styles';
import { LessonNodeDialog } from './components/LessonNodeDialog';
import { REST_API } from '../../../constants/endpoints';
import { ExamManageDialog } from './components/ExamManageDialog';

const TabPanel: FC<{
  index: any;
  value: any;
}> = ({ children, value, index, ...other }) => {
  return (
    <div
      role="tabpanel"
      hidden={value !== index}
      id={`tabpanel-${index}`}
      aria-labelledby={`tab-${index}`}
      {...other}
    >
      {value === index && <Box p={3}>{children}</Box>}
    </div>
  );
};

const a11yProps = (index: number) => ({
  id: `tabpanel-${index}`,
  key: `tabpanel-${index}`,
  'aria-controls': `tabpanel-${index}`,
});

const LessonNodeList: FC<{ lessonId: string }> = ({ lessonId }) => {
  const [items, setItems] = useState<Array<any>>([]);
  const prevDragEvent = useRef<string>();
  const { data } = useGetList(
    'lesson-nodes',
    {
      page: 1,
      perPage: 100,
    },
    {
      field: 'order',
      order: 'DESC',
    },
    {
      lessonId,
    },
  );
  useEffect(() => {
    if (Object.keys(data).length > 0) {
      setItems([...Object.values(data)].sort((a, b) => a.order - b.order));
    } else {
      setItems([]);
    }
  }, [data]);
  const onChange = useCallback(
    async (targetId: string, items: any[]) => {
      const index = items.findIndex((it) => it.id === targetId);
      const params = new URLSearchParams();
      params.append('lessonId', lessonId.toString());
      params.append('targetId', targetId.toString());
      items[index - 1] &&
        params.append('beforeId', items[index - 1].id.toString());
      items[index + 1] &&
        params.append('afterId', items[index + 1].id.toString());
      if (prevDragEvent.current === params.toString()) return void 0;
      else prevDragEvent.current = params.toString();
      try {
        await lastValueFrom(
          fromFetch(`${REST_API}/lesson-nodes/sort?${params}`, {
            method: 'PUT',
          }).pipe(
            switchMap((response) => {
              if (!response.ok || response.status !== 200)
                return of({ error: true, message: 'save failed' });
            }),
            catchError((err) => {
              return of({ error: true, message: err.message });
            }),
          ),
        );
        setItems(items);
      } catch (e) {
        alert(e.message);
      }
    },
    [lessonId],
  );
  const deleteBehavior = LessonNodeDialog.useModalBehavior();
  const updateBehavior = LessonNodeDialog.useModalBehavior();
  const previewBehavior = LessonNodeDialog.useModalBehavior();
  const examManageBehavior = ExamManageDialog.useModalBehavior();
  return (
    <>
      {examManageBehavior.visible && (
        <ExamManageDialog.List {...examManageBehavior.model} />
      )}
      {deleteBehavior.visible && (
        <LessonNodeDialog.Delete {...deleteBehavior.model} />
      )}
      {updateBehavior.visible && (
        <LessonNodeDialog.Update {...updateBehavior.model} />
      )}
      {previewBehavior.visible && (
        <LessonNodeDialog.Preview {...previewBehavior.model} />
      )}
      <Paper style={{ marginTop: '24px', boxShadow: 'none', padding: '0' }}>
        <DndProvider backend={HTML5Backend}>
          <CardSortMove
            items={items}
            onChange={onChange}
            onRemove={deleteBehavior.open}
            onUpdate={updateBehavior.open}
            onReviewVideo={previewBehavior.open}
            onManageExam={examManageBehavior.open}
          />
        </DndProvider>
      </Paper>
    </>
  );
};

const useLessonStyles = makeStyles((theme: Theme) => ({
  root: {
    flexGrow: 1,
    backgroundColor: theme.palette.background.paper,
  },
  'app-bar': {
    boxShadow: 'none',
    color: '#666',
    background: '#f5f5f5',
  },
  coverWrap: {
    display: 'flex',
    flexDirection: 'column',
  },
}));

export default function LessonEdit(props: EditProps) {
  const [edit, setEdit] = useState(false);
  const [tabIndex, setTabIndex] = useState(0);
  const onTabChange = useCallback((_, nv: number) => {
    setTabIndex(nv);
  }, []);
  const styles = useLessonStyles();
  return (
    <div className={styles.root}>
      <AppBar position="static" className={styles['app-bar']}>
        <Tabs value={tabIndex} onChange={onTabChange}>
          <Tab label="课程信息" {...a11yProps(0)} />
          <Tab label="课节管理" {...a11yProps(1)} />
        </Tabs>
      </AppBar>
      <TabPanel index={0} value={tabIndex}>
        <Edit {...props}>
          <SimpleForm redirect={false}>
            {!edit ? (
              <div className={styles.coverWrap}>
                <ImageField source="coverUrl" />
                <Button
                  onClick={() => {
                    setEdit(true);
                  }}
                  color="primary"
                  variant="contained"
                >
                  更改封面
                </Button>
              </div>
            ) : (
              <div
                style={{
                  display: 'flex',
                  alignItems: 'center',
                  width: '100%',
                }}
              >
                <div
                  style={{
                    width: '80%',
                  }}
                >
                  <ImageUploadInput source="coverUrl" edit />
                </div>
              </div>
            )}

            <TextInput fullWidth source="title" />
            <TextInput fullWidth resettable source="body" multiline rows={4} />
            <ReferenceInput fullWidth source="courseId" reference="courses">
              <SelectInput optionText="title" />
            </ReferenceInput>
            <ReferenceInput source="planetId" reference="planets">
              <SelectInput required optionText="name" />
            </ReferenceInput>
            <ReferenceArrayInput source="roleIds" reference="role-tags">
              <SelectArrayInput required optionText="tagName" />
            </ReferenceArrayInput>
            <ReferenceArrayInput source="tagIds" reference="perfer-tags">
              <SelectArrayInput required optionText="tagName" />
            </ReferenceArrayInput>
          </SimpleForm>
        </Edit>
      </TabPanel>
      <TabPanel index={1} value={tabIndex}>
        <LessonNodeDialog.Create />
        <LessonNodeList lessonId={props.id!} />
      </TabPanel>
    </div>
  );
}
