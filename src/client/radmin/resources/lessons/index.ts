import LessonCreate from './LessonCreate';
import LessonEdit from './LessonEdit';
import LessonList from './LessonList';

const lessons = {
  list: LessonList,
  create: LessonCreate,
  edit: LessonEdit,
};
export default lessons;
