import {
  SelectInput,
  Create,
  CreateProps,
  FormTab,
  ReferenceInput,
  TabbedForm,
  SelectArrayInput,
  ReferenceArrayInput,
  TextInput,
} from 'ra-ui-materialui';
import ImageUploadInput from '../../inputs/ImageUploadInput';

export default function LessonCreate(props: CreateProps) {
  return (
    <Create {...props}>
      <TabbedForm redirect="list">
        <FormTab label="summary">
          <ImageUploadInput source="coverUrl" />
          <TextInput required source="title" />
          <TextInput resettable source="body" multiline rows={4} />
          <ReferenceInput source="courseId" reference="courses">
            <SelectInput required optionText="title" />
          </ReferenceInput>
          <ReferenceInput source="planetId" reference="planets">
            <SelectInput required optionText="name" />
          </ReferenceInput>
          <ReferenceArrayInput source="roleIds" reference="role-tags">
            <SelectArrayInput required optionText="tagName" />
          </ReferenceArrayInput>
          <ReferenceArrayInput source="tagIds" reference="perfer-tags">
            <SelectArrayInput required optionText="tagName" />
          </ReferenceArrayInput>
        </FormTab>
      </TabbedForm>
    </Create>
  );
}
