import React from 'react';
import { Edit, SimpleForm, TextInput } from 'react-admin';
import ImageUploadInput from '../../inputs/ImageUploadInput';
import VideoUploadInput from '../../inputs/VideoUploadInput';

export const BlogEdit = (props) => (
  <Edit {...props} title="当红人物编辑">
    <SimpleForm>
      <TextInput source="title" isRequired />
      <TextInput source="character" isRequired />
      <TextInput multiline source="body" style={{ width: 500 }} isRequired />
      <div style={{ width: 500, marginBottom: 20 }}>
        <ImageUploadInput
          source="coverImage"
          Description="封面上传"
          isRequired
        />
      </div>
      <div style={{ width: 500 }}>
        <VideoUploadInput source="videoId" isRequired />
      </div>
    </SimpleForm>
  </Edit>
);
