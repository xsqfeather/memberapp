/* import {  } from "./BlogList"; */

import { BlogList } from './BlogList';
import { BlogEdit } from './BlogEdit';
import { BlogCreate } from './BlogCreate';

const blogs = {
  list: BlogList,
  edit: BlogEdit,
  create: BlogCreate,
};

export default blogs;
