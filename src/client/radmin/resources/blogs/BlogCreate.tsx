import { Create, CreateProps, SimpleForm, TextInput } from 'ra-ui-materialui';
import ImageUploadInput from '../../inputs/ImageUploadInput';
import VideoUploadInput from '../../inputs/VideoUploadInput';

export function BlogCreate(props: CreateProps) {
  return (
    <Create {...props}>
      <SimpleForm redirect="list">
        <TextInput source="title" />
        <TextInput source="character" />
        <TextInput multiline source="body" style={{ width: 500 }} />
        <div style={{ width: 500, marginBottom: 20 }}>
          <ImageUploadInput source="coverImage" Description="封面上传" />
        </div>
        <div style={{ width: 500 }}>
          <VideoUploadInput source="videoId"></VideoUploadInput>
        </div>
      </SimpleForm>
    </Create>
  );
}
