import React, { memo, useCallback, useEffect, useRef, useState } from 'react';
import {
  Datagrid,
  List,
  TextField,
  DateField,
  FieldProps,
  useRecordContext,
  Filter,
  FilterProps,
  TextInput,
} from 'react-admin';
import {
  Button,
  createStyles,
  Dialog,
  DialogContent,
  DialogTitle,
  IconButton,
  makeStyles,
  Theme,
  Typography,
} from '@material-ui/core';
import hlsjs from 'hls.js';
import { REST_API } from '../../../constants/endpoints';
import axios from 'axios';
import CloseIcon from '@material-ui/icons/Close';

const useStyles = makeStyles((theme: Theme) =>
  createStyles({
    closeButton: {
      position: 'absolute',
      right: theme.spacing(1),
      top: theme.spacing(1),
      color: theme.palette.grey[500],
    },
  }),
);

export function BlogList(props) {
  const [open, setOpen] = useState(false);
  const [videoId, setVideoId] = useState(-1);
  const [videoData, setVideoData] =
    useState<{
      isLive: boolean;
      liveUrl: string;
      downloadUrl: string;
    } | null>(null);
  useEffect(() => {
    if (videoId !== -1) {
      axios.get(`${REST_API}/lesson-node-videos/${videoId}`).then((res) => {
        const { isLive, liveUrl, downloadUrl } = res.data;
        setVideoData({ isLive, liveUrl, downloadUrl });
        setVideoId(-1);
        setOpen(true);
      });
    }
  }, [videoId]);
  const handleVideoDialogClose = useCallback(() => {
    setVideoData(null);
    setOpen(false);
  }, []);
  return (
    <>
      <VideoDialog
        open={open}
        videoData={videoData}
        onClose={handleVideoDialogClose}
      />
      <List
        {...props}
        sort={{ field: 'createdAt', order: 'DESC' }}
        filters={<BlogFilter />}
      >
        <Datagrid rowClick="edit">
          <DateField source="createdAt" showTime />
          <ImageField source="coverImage" />
          <TextField source="character" />
          <TextField source="title" />
          <BodyField source="body" />
          <ButtonField
            source="videoId"
            onClick={(videoId) => {
              setVideoId(videoId);
            }}
          />
        </Datagrid>
      </List>
    </>
  );
}

function BlogFilter(props) {
  return (
    <Filter {...props}>
      <TextInput label="搜索(人物,标题,正文关键词)" source="q" alwaysOn />
    </Filter>
  );
}
export interface VideoDialogProps {
  open: boolean;
  onClose: () => void;
  videoData: { isLive: boolean; liveUrl: string; downloadUrl: string } | null;
}
export function VideoDialog(props: VideoDialogProps) {
  const { onClose, open, videoData } = props;
  const videoRef = useRef<HTMLVideoElement>(null);
  const styles = useStyles();
  useEffect(() => {
    if (!open || !videoData) return;
    const hls = new hlsjs();
    /* setTimeout(() => {}, 0); */
    queueMicrotask(() => {
      if (videoData.isLive) {
        //hls不能直接播放Mp4
        hls.loadSource(videoData.liveUrl);
        hls.attachMedia(videoRef.current);
        videoRef.current.play();
      } else {
        videoRef.current.src = videoData.downloadUrl;
      }
      videoRef.current.play();
    });
    return () => {
      hls.destroy();
    };
  }, [videoData, open]);
  return (
    <Dialog
      open={open}
      onClose={() => {
        if (onClose) {
          onClose();
        }
      }}
    >
      <DialogTitle>
        {videoData
          ? videoData.isLive
            ? '流媒体预览'
            : '后台转码中......当前预览源文件'
          : ''}
        <IconButton
          aria-label="close"
          className={styles.closeButton}
          onClick={onClose}
        >
          <CloseIcon />
        </IconButton>
      </DialogTitle>
      <DialogContent>
        <video
          controls
          ref={videoRef}
          style={{
            width: '100%',
          }}
        ></video>
      </DialogContent>
    </Dialog>
  );
}
export function BodyField(props: FieldProps) {
  const { source } = props;
  const record = useRecordContext(props);

  return (
    <>
      <div
        style={{
          padding: 10,
          width: 300,
          height: 100,
          overflow: 'hidden',
        }}
      >
        {record[source]}
      </div>
    </>
  );
}

export function ImageField(props: FieldProps) {
  const { source } = props;
  const record = useRecordContext(props);
  return (
    <>{record ? <img src={record[source]} style={{ width: 160 }} /> : null}</>
  );
}

const ButtonField = memo(
  (
    props: FieldProps & {
      onClick: (videoId: number) => void;
    },
  ) => {
    const { source, onClick } = props;
    const record = useRecordContext(props);
    return (
      <>
        <Button
          color={'primary'}
          variant="contained"
          onClick={(e) => {
            e.stopPropagation();
            onClick(+record[source]);
          }}
        >
          预览视频
        </Button>
      </>
    );
  },
);
