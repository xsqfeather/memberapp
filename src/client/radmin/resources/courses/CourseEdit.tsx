import { Button } from '@material-ui/core';
import {
  Edit,
  SimpleForm,
  TextInput,
  ImageField,
  EditProps,
} from 'ra-ui-materialui';
import { useState } from 'react';
import ImageUploadInput from '../../inputs/ImageUploadInput';

export default function CourseEdit(props: EditProps) {
  const [edit, setEdit] = useState(false);
  return (
    <Edit {...props}>
      <SimpleForm redirect="list">
        {!edit ? (
          <div
            style={{
              display: 'flex',
              flexDirection: 'column',
            }}
          >
            <ImageField source="coverUrl" />

            <Button
              onClick={() => {
                setEdit(true);
              }}
              color="primary"
              variant="contained"
            >
              更改封面
            </Button>
          </div>
        ) : (
          <ImageUploadInput source="coverUrl" />
        )}

        <TextInput source="title" />
        <TextInput resettable source="description" multiline rows={4} />
      </SimpleForm>
    </Edit>
  );
}
