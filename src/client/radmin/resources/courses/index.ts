import CourseCreate from './CourseCreate';
import CourseEdit from './CourseEdit';
import CourseList from './CourseList';

const courses = {
  list: CourseList,
  create: CourseCreate,
  edit: CourseEdit,
};

export default courses;
