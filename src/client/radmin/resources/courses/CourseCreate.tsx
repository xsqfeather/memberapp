import { Create, CreateProps, SimpleForm, TextInput } from 'ra-ui-materialui';
import ImageUploadInput from '../../inputs/ImageUploadInput';

export default function CourseCreate(props: CreateProps) {
  return (
    <Create {...props}>
      <SimpleForm redirect="list">
        <ImageUploadInput source="coverUrl" />
        <TextInput source="title" fullWidth resettable />
        <TextInput
          resettable
          fullWidth
          source="description"
          multiline
          rows={4}
        />
      </SimpleForm>
    </Create>
  );
}
