import {
  Filter,
  Datagrid,
  List,
  ListProps,
  TextField,
  TextInput,
  ImageField,
} from 'react-admin';

const CourseFilter = (props) => (
  <Filter {...props}>
    <TextInput label="pos.search" resettable source="q" alwaysOn />
  </Filter>
);

export default function CourseList(props: ListProps) {
  return (
    <List
      {...props}
      filters={<CourseFilter />}
      sort={{
        field: 'createdAt',
        order: 'DESC',
      }}
    >
      <Datagrid rowClick="edit">
        <ImageField source="coverUrl" />
        <TextField source="title" />
        <TextField source="description" />
      </Datagrid>
    </List>
  );
}
