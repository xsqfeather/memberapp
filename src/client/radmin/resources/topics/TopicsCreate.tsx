import { Create, SelectInput, SimpleForm, TextInput } from 'react-admin';
import React from 'react';
import ImageUploadInput from '../../inputs/ImageUploadInput';

export const TopicCreate = (props) => (
  <Create {...props} title="话题编辑">
    <SimpleForm>
      <TextInput source="title" variant="outlined" isRequired />
      <TextInput source="des" multiline variant="outlined"  />
      <div
        style={{
          width: 400,
        }}
      >
        <ImageUploadInput
          source="cover"
          style={{
            width: 200,
            heigth: 200,
          }}
        />
      </div>
    </SimpleForm>
  </Create>
);
