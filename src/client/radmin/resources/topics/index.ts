import { TopicList } from './TopicsList';
import { TopicEdit } from './TopicsEdit';
import { TopicCreate } from './TopicsCreate';

const topics = {
  list: TopicList,
  edit: TopicEdit,
  create: TopicCreate,
};

export default topics;
