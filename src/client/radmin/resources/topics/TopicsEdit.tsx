import {
  Edit,
  SelectInput,
  SimpleForm,
  TextInput,
} from 'react-admin';
import React from 'react';
import ImageUploadInput from '../../inputs/ImageUploadInput';

export const TopicEdit = (props) => (
  <Edit {...props} title="话题编辑">
    <SimpleForm >
      <TextInput source="title" variant="outlined" isRequired />
      <TextInput source="des" multiline variant="outlined"  />
      <div
        style={{
          width: 400,
        }}
      >
        <ImageUploadInput
          source="cover"
          style={{
            width: 200,
            heigth: 200,
          }}
        />
      </div>  
      <SelectInput
        label="审核"
        source="status"
        choices={[
          { id: 'NotReviewed', name: '未审核' },
          { id: 'Pass', name: '审核通过' },
          { id: 'NoPass', name: '审核失败' },
        ]}
      />
    </SimpleForm>
  </Edit>
);
