import {
  createStyles,
  makeStyles,
  Theme,
  Typography,
  Button,
} from '@material-ui/core';
import React, { memo, useEffect, useState } from 'react';
import {
  List,
  Datagrid,
  TextField,
  DateField,
  FieldProps,
  useRecordContext,
  sanitizeFieldRestProps,
  Filter,
  TextInput,
  ReferenceInput,
  SelectInput,
  ReferenceField,
} from 'react-admin';
import rest from '../../../dataProvider/rest';
import axios from 'axios';
import { REST_API } from '../../../constants/endpoints';

export function TopicList(props) {
  return (
    <List
      {...props}
      sort={{ field: 'createdAt', order: 'DESC' }}
      filters={<TopicFilter />}
    >
      <Datagrid rowClick="edit">
        <DateField source="createdAt" showTime />
        <ImageFieldPlus source="cover" />
        <TextField source="title" />
        <TextField source="des" />
        <TextField source="status" />
        <UsersField source="publisherId" />
      </Datagrid>
    </List>
  );
}

const UsersField = memo(function (props: FieldProps) {
  const { className, source } = props;
  const record = useRecordContext();
  return (
    <Typography
      component="span"
      variant="body2"
      className={className}
      {...sanitizeFieldRestProps(rest)}
    >
      {record.publisherId ? (
        <ReferenceField source="publisherId" reference="users">
          <TextField source="username" />
        </ReferenceField>
      ) : (
        'Admin'
      )}
    </Typography>
  );
});

const useStyles = makeStyles((theme: Theme) =>
  createStyles({
    imageWrap: {
      width: 80,
      height: 80,
    },
    image: {
      width: '100%',
      height: '100%',
      backgroundSize: 'cover',
    },
  }),
);

function ImageFieldPlus(props: FieldProps) {
  const classes = useStyles();
  const record = useRecordContext();
  const { source } = props;
  return (
    <div className={classes.imageWrap}>
      <img src={record[source]} alt="话题封面" className={classes.image} />
    </div>
  );
}


function TopicFilter(props) {
  return (
    <Filter {...props}>
      <TextInput label="搜索(标题)" source="q" alwaysOn />
      <SelectInput
        label="内容筛选"
        source="content"
        choices={[
          { id: 'nocontent', name: '无内容话题' },
          { id: 'hascontent', name: '有内容话题' },
        ]}
        allowEmpty={true}
        emptyText={'所有话题'}
        alwaysOn
      />
      <SelectInput
        label="状态筛选"
        source="status"
        choices={[
          { id: 'NotReviewed', name: '未审核' },
          { id: 'Pass', name: '审核通过' },
          { id: 'NoPass', name: '审核失败' },
        ]}
        allowEmpty={true}
        emptyText={'所有话题'}
        alwaysOn
      />
    </Filter>
  );
}
