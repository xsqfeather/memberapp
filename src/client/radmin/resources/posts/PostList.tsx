import { Datagrid, List, ListProps, TextField } from 'react-admin';

export default function PostList(props: ListProps) {
  return (
    <List {...props}>
      <Datagrid rowClick="edit">
        <TextField source="title" />
        <TextField source="body" />
      </Datagrid>
    </List>
  );
}
