import PostCreate from './PostCreate';
import PostEdit from './PostEdit';
import PostList from './PostList';

const posts = {
  create: PostCreate,
  list: PostList,
  edit: PostEdit,
};

export default posts;
