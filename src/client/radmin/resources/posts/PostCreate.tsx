import { Create, CreateProps, SimpleForm, TextInput } from 'ra-ui-materialui';

import { RaReactPageInput } from '@react-page/react-admin';

// The rich text area plugin
import slate from '@react-page/plugins-slate';
// image
import image from '@react-page/plugins-image';

// Stylesheets for the rich text area plugin
// uncomment this
import '@react-page/plugins-slate/lib/index.css';

// Stylesheets for the imagea plugin
import '@react-page/plugins-image/lib/index.css';

import { useCallback } from 'react';

// Define which plugins we want to use.
const cellPlugins = [slate(), image];

const LANGUAGES = [
  {
    lang: 'en',
    label: 'English',
  },
  {
    lang: 'zh',
    label: 'Deutsch',
  },
];

const TRANSLATIONS: { [key: string]: string } = {
  'Edit blocks': '编辑',
  'Add blocks': '添加',
  'Move blocks': '移动',
  'Resize blocks': '调整大小',
  'Preview blocks': '预览模式',
  'Preview page': '预览模式',
};

export default function PostCreate(props: CreateProps) {
  const uiTranslator = useCallback((label?: string) => {
    if (TRANSLATIONS[label] !== undefined) {
      return TRANSLATIONS[label];
    }
    return `${label}(to translate)`;
  }, []);
  return (
    <Create {...props}>
      <SimpleForm>
        <TextInput source="title" />
        <RaReactPageInput // <---
          source="body" // <-- the fieldname on the resource.
          label="内容"
          lang="zh"
          languages={LANGUAGES}
          cellPlugins={cellPlugins}
          uiTranslator={uiTranslator}
          // ... any other Editor prop
        />
      </SimpleForm>
    </Create>
  );
}
