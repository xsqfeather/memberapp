import { ListGuesser, EditGuesser } from 'react-admin';
import { SlideCreate } from './SlideCreate';

const slides = {
  list: ListGuesser,
  edit: EditGuesser,
  create: SlideCreate,
};
export default slides;
