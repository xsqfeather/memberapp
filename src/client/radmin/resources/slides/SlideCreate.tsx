import { FC } from 'react';
import { Create, CreateProps, TabbedForm, FormTab } from 'react-admin';
import { TextInput } from 'ra-ui-materialui';

export const SlideCreate: FC<CreateProps> = (props) => {
  return (
    <Create {...props}>
      <TabbedForm>
        <FormTab label="轮播图管理">
          <TextInput source="name" required />
          <TextInput source="startTime" required />
          <TextInput source="endTime" required />
          <TextInput source="link" required />
          <TextInput source="position" required />
        </FormTab>
      </TabbedForm>
    </Create>
  );
};
