import {
  Create,
  CreateProps,
  NumberInput,
  SimpleForm,
  TextInput,
} from 'ra-ui-materialui';

export default function PlanetCreate(props: CreateProps) {
  return (
    <Create {...props}>
      <SimpleForm redirect="list">
        <TextInput source="name" />
        <NumberInput source="level" />
        <NumberInput source="limit" />
      </SimpleForm>
    </Create>
  );
}
