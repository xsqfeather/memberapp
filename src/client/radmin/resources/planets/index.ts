import PlanetCreate from './PlanetCreate';
import PlanetEdit from './PlanetEdit';
import PlanetList from './PlanetList';

const planets = {
  create: PlanetCreate,
  edit: PlanetEdit,
  list: PlanetList,
};

export default planets;
