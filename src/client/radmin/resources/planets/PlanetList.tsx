import {
  Filter,
  Datagrid,
  List,
  ListProps,
  TextField,
  TextInput,
} from 'react-admin';

const PlanetFilter = (props) => (
  <Filter {...props}>
    <TextInput resettable label="搜索 星球名" source="q" alwaysOn />
  </Filter>
);

export default function PlanetList(props: ListProps) {
  return (
    <List
      {...props}
      filters={<PlanetFilter />}
      sort={{ field: 'level', order: 'ASC' }}
    >
      <Datagrid rowClick="edit">
        <TextField source="name" sortable={false} />
        <TextField source="level" />
        <TextField source="limit" />
      </Datagrid>
    </List>
  );
}
