import {
  Edit,
  CreateProps,
  SimpleForm,
  TextInput,
  NumberInput,
  NumberField,
} from 'ra-ui-materialui';

export default function PlanetEdit(props: CreateProps) {
  return (
    <Edit {...props}>
      <SimpleForm>
        <TextInput source="name" />
        <NumberField source="level" />
        <NumberInput source="limit" />
      </SimpleForm>
    </Edit>
  );
}
