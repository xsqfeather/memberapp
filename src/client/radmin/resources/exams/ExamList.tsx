import { FC } from 'react';
import {
  ListProps,
  List,
  Datagrid,
  TextField,
  FunctionField,
  ReferenceField,
  EditButton,
  Filter,
  TextInput,
} from 'react-admin';
import { EXAM_TYPE_MAP } from './constants';

const PlanetTagFilter = (props) => (
  <Filter {...props}>
    <TextInput label="pos.search" resettable source="q" alwaysOn />
  </Filter>
);

export const ExamList: FC<ListProps> = (props) => {
  return (
    <List
      {...props}
      filters={<PlanetTagFilter />}
      sort={{
        field: 'createdAt',
        order: 'DESC',
      }}
    >
      <Datagrid>
        <TextField source="order" />
        <TextField source="name" />
        <FunctionField
          source="type"
          render={(record) => EXAM_TYPE_MAP[record.type]}
        />
        <TextField source="score" />
        <TextField source="answer" />
        <ReferenceField source="lessonId" reference="lessons">
          <TextField source="title" />
        </ReferenceField>
        <ReferenceField source="lessonNodeId" reference="lesson-nodes">
          <TextField source="title" />
        </ReferenceField>
        <EditButton basePath="/exams" />
      </Datagrid>
    </List>
  );
};
