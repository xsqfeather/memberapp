import { FC, useCallback, useState, Fragment, useEffect, useMemo } from 'react';
import {
  CreateProps,
  Create,
  TextInput,
  SelectInput,
  NumberInput,
  ReferenceInput,
  BooleanInput,
  SimpleFormIterator,
  ArrayInput,
  FormTab,
  TabbedForm,
} from 'ra-ui-materialui';
import { TYPE_CHOICES, SCORE_CHOICES } from './constants';
import { useForm } from 'react-final-form';
import { useInput } from 'ra-core';
import { Box } from '@material-ui/core';

export const LessonInput: FC = () => {
  const form = useForm();
  const [lessonNodes, setLessonNodes] = useState<any[]>([]);
  const {
    input: { value: lessonId },
  } = useInput({
    source: 'lessonNode.lessonId',
    resource: 'exams',
  });
  const loadLessonNodes = useCallback(async () => {
    if (!lessonId) return void 0;
    const filter = JSON.stringify({
      lessonId,
    });
    const result = await fetch(
      `/api/lesson-nodes?filter=${filter}&sort=["id","DESC"]&range=[0,24]`,
    ).then((res) => res.json());
    setLessonNodes(
      result.map((it) => ({
        id: it.id,
        name: it.title,
      })),
    );
  }, [lessonId]);
  useEffect(() => {
    loadLessonNodes();
  }, [loadLessonNodes]);
  const onLessonChange = useCallback(() => {
    form.change('lessonNodeId', void 0);
  }, [form]);
  return (
    <Fragment>
      <ReferenceInput
        resource="exams"
        source="lessonNode.lessonId"
        reference="lessons"
        onChange={onLessonChange}
        required
      >
        <SelectInput optionText="title" />
      </ReferenceInput>
      {lessonNodes.length > 0 && (
        <SelectInput
          resource="exams"
          source="lessonNodeId"
          choices={lessonNodes}
        />
      )}
    </Fragment>
  );
};
export const ExamInput: FC = () => {
  const {
    input: { value: type },
  } = useInput({
    resource: 'exams',
    source: 'type',
    defaultValue: 'single-choice',
  });
  const form = useForm();
  const onChange = useCallback(() => {
    form.change('answer', void 0);
  }, [form]);
  const judgmentChoices = useMemo(
    () => [
      { id: 'T', name: '正确' },
      { id: 'F', name: '错误' },
    ],
    [],
  );
  return (
    <>
      <Box>
        <SelectInput
          resource="exams"
          source="type"
          choices={TYPE_CHOICES}
          required
          value={type}
          onChange={onChange}
        />
      </Box>
      <Box>
        {(() => {
          switch (type) {
            case 'single-choice':
            case 'multiple-choice':
              return (
                <ArrayInput resource="exams" source="options" isRequired>
                  <SimpleFormIterator>
                    <TextInput source="optionName" />
                    <TextInput source="optionValue" />
                  </SimpleFormIterator>
                </ArrayInput>
              );
          }
        })()}
      </Box>
      <Box>
        {(() => {
          switch (type) {
            case 'single-choice':
            case 'multiple-choice':
            case 'filling-up':
              return (
                <TextInput
                  multiline
                  rows={6}
                  fullWidth
                  resource="exams"
                  source="answer"
                  required
                />
              );
            case 'judgment':
              return (
                <SelectInput
                  resource="exams"
                  source="answer"
                  required
                  choices={judgmentChoices}
                />
              );
            default:
              return null;
          }
        })()}
      </Box>
    </>
  );
};

export const ExamCreate: FC<CreateProps> = (props) => {
  return (
    <Create {...props}>
      <TabbedForm redirect="list">
        <FormTab label="测验管理">
          <TextInput
            source="name"
            multiline
            style={{ width: '500px' }}
            required
          />
          <LessonInput />
          <NumberInput source="order" required />
          <SelectInput source="score" choices={SCORE_CHOICES} required />
          <ExamInput />
        </FormTab>
      </TabbedForm>
    </Create>
  );
};
