import { FC } from 'react';
import {
  EditProps,
  Edit,
  TextInput,
  SelectInput,
  NumberInput,
} from 'react-admin';
import { SCORE_CHOICES } from './constants';
import { FormTab, TabbedForm } from 'ra-ui-materialui';
import { ExamInput, LessonInput } from './ExamCreate';

export const ExamEdit: FC<EditProps> = (props) => {
  return (
    <Edit {...props}>
      <TabbedForm redirect="list">
        <FormTab label="测验管理">
          <TextInput
            source="name"
            multiline
            style={{ width: '500px' }}
            required
          />
          <LessonInput />
          <NumberInput source="order" required />
          <SelectInput source="score" choices={SCORE_CHOICES} required />
          <ExamInput />
        </FormTab>
      </TabbedForm>
    </Edit>
  );
};
