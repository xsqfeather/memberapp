export const EXAM_TYPE = [
  'single-choice',
  'multiple-choice',
  'judgment',
  'filling-up',
] as const;
export type ExamType = typeof EXAM_TYPE[number];

export const EXAM_TYPE_MAP: Record<ExamType, string> = {
  'single-choice': '单选',
  'multiple-choice': '多选',
  judgment: '判断',
  'filling-up': '填空',
};

export type Choice<T extends string | number = string> = {
  name: string;
  id: T;
};

export const TYPE_CHOICES: Array<Choice<ExamType>> = Object.entries(
  EXAM_TYPE_MAP,
).map(([key, value]) => ({
  id: key as ExamType,
  name: value,
}));
export const SCORE_CHOICES: Array<Choice<number>> = [
  {
    id: 10,
    name: '10分',
  },
  {
    id: 20,
    name: '20分',
  },
  {
    id: 30,
    name: '30分',
  },
];
