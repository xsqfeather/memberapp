import { ExamCreate } from './ExamCreate';
import { ExamList } from './ExamList';
import { ExamEdit } from './ExamEdit';

const exams = {
  create: ExamCreate,
  list: ExamList,
  edit: ExamEdit,
};

export default exams;
