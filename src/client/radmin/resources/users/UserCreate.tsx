import {
  BooleanInput,
  Create,
  CreateProps,
  SimpleForm,
  TextInput,
} from 'ra-ui-materialui';

export default function UserCreate(props: CreateProps) {
  return (
    <Create {...props}>
      <SimpleForm>
        <TextInput source="username" />
        <TextInput source="password" />
        <BooleanInput source="isRobot" />
      </SimpleForm>
    </Create>
  );
}
