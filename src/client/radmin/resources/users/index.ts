import UserList from './UserList';
import UserCreate from './UserCreate';

const users = {
  create: UserCreate,
  list: UserList,
};

export default users;
