import {
  Datagrid,
  FieldProps,
  Filter,
  List,
  ListProps,
  TextField,
  TextInput,
} from 'react-admin';
import { REST_API } from '../../../constants/endpoints';

const AvatarField = ({ record }: FieldProps) => {
  return (
    <span>
      <img
        style={{
          maxWidth: 100,
        }}
        src={`${record.profile?.avatar}`}
      />
    </span>
  );
};

const UsersFilter = (props: any) => (
  <Filter {...props}>
    <TextInput
      label="手机号 ｜ 用户名 ｜ 邮箱"
      resettable
      source="q"
      alwaysOn
    />
  </Filter>
);

export default function UserList(props: ListProps) {
  return (
    <List {...props} filters={<UsersFilter />}>
      <Datagrid>
        <AvatarField source="profile.avatar" />
        <TextField source="username" />
        <TextField source="profile.nickname" />
        <TextField source="phone" />
      </Datagrid>
    </List>
  );
}
