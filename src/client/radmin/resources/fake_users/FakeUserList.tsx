import {
  Datagrid,
  DateField,
  NumberField,
  TextField,
  List,
  ImageField,
  FieldProps,
} from 'ra-ui-materialui';
import { REST_API } from '../../../constants/endpoints';

const FakeUserList = (props) => (
  <List {...props}>
    <Datagrid rowClick="edit">
      <TextField source="id" />
      <TextField source="nickname" />
      <ImageField source="profile.avatar" />
      <NumberField source="postFreq" />
      <NumberField source="followFreq" />
      <DateField source="updatedAt" />
    </Datagrid>
  </List>
);

export default FakeUserList;
