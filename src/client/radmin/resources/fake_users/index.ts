import { ListGuesser } from 'ra-ui-materialui';
import FakeUserCreate from './FakeUserCreate';
import FakeUserList from './FakeUserList';

const fake_users = {
  list: FakeUserList,
  create: FakeUserCreate,
};

export default fake_users;
