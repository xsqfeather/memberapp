import {
  SelectInput,
  Create,
  CreateProps,
  ReferenceInput,
  SimpleForm,
  TextInput,
  BooleanInput,
} from 'ra-ui-materialui';
import ImageUploadInput from '../../inputs/ImageUploadInput';
const FakeUserCreate = (props: CreateProps) => {
  return (
    <Create {...props}>
      <SimpleForm redirect="list">
        <ImageUploadInput
          source="avatar"
          style={{ height: 200, width: 200 }}
          Description="上传头像"
        />
        <TextInput variant="outlined" source="nickname" resettable />
        <TextInput
          variant="outlined"
          resettable
          source="personalNote"
          multiline
          rows={4}
        />
        <TextInput variant="outlined" source="phone" resettable />
        <SelectInput
          variant="outlined"
          source="gender"
          choices={[
            { id: 0, name: '未知/保密' },
            { id: 1, name: '女' },
            { id: 2, name: '男' },
          ]}
          resettable
        />
        <SelectInput
          variant="outlined"
          source="postFreq"
          choices={[
            { id: 1, name: '1个小时' },
            { id: 3, name: '3个小时' },
            { id: 7, name: '7个小时' },
            { id: 12, name: '12个小时' },
            { id: 24, name: '24个小时' },
          ]}
        />
        <SelectInput
          variant="outlined"
          source="followFreq"
          choices={[
            { id: 1, name: '1个小时' },
            { id: 3, name: '3个小时' },
            { id: 7, name: '7个小时' },
            { id: 12, name: '12个小时' },
            { id: 24, name: '24个小时' },
          ]}
        />
      </SimpleForm>
    </Create>
  );
};

export default FakeUserCreate;
