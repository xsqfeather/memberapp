import { Button } from '@material-ui/core';

const IsLiveField = ({ record, onPreview }: any) => {
  return (
    <div
      style={{
        display: 'flex',
        width: '100%',
        justifyContent: 'space-around',
      }}
    >
      {record && record.isLive ? (
        <Button
          variant="contained"
          color="secondary"
          onClick={(e: any) => {
            e.stopPropagation();
            onPreview(record);
          }}
        >
          预览视频
        </Button>
      ) : (
        <span>视频转码中</span>
      )}
    </div>
  );
};

export default IsLiveField;
