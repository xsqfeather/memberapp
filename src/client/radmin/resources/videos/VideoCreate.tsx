import { Toolbar } from '@material-ui/core';
import {
  Create,
  CreateProps,
  ListButton,
  ReferenceInput,
  SelectInput,
  SimpleForm,
  TextInput,
  TopToolbar,
} from 'ra-ui-materialui';
import StreamVideoInput from '../../inputs/StreamVideoInput';

const VideoCreateActions = ({ basePath, data, resource }: any) => (
  <TopToolbar>
    <ListButton basePath={basePath} />
  </TopToolbar>
);

const VideoCreateToolbar = ({ basePath, data, resource }: any) => (
  <Toolbar>
    <ListButton label="返回列表" basePath={basePath} />
  </Toolbar>
);

export default function VideoCreate(props: CreateProps) {
  return (
    <Create actions={<VideoCreateActions />} {...props}>
      <SimpleForm redirect="list" toolbar={<VideoCreateToolbar />}>
        <StreamVideoInput />
      </SimpleForm>
    </Create>
  );
}
