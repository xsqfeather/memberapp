import VideoCreate from './VideoCreate';
import VideoList from './VideoList';

const videos = {
  create: VideoCreate,
  list: VideoList,
};

export default videos;
