import { useState } from 'react';
import {
  Filter,
  Datagrid,
  List,
  ListProps,
  TextField,
  TextInput,
} from 'react-admin';
import IsLiveField from './IsLiveField';

import dynamic from 'next/dynamic';

const VideoPrivewDialog = dynamic(
  () => import('../../fields/VideoPrivewDialog'),
  {
    ssr: false,
  },
);

const PlanetTagFilter = (props: any) => (
  <Filter {...props}>
    <TextInput label="pos.search" resettable source="q" alwaysOn />
  </Filter>
);

export default function VideoList(props: ListProps) {
  const [preview, setPreview] = useState(false);
  const [previewRecord, setPreviewRecord] = useState({} as any);
  const handlePreview = (record: any) => {
    setPreviewRecord(record);
    setPreview(true);
  };

  const handlePreviewClose = () => {
    setPreviewRecord({});
    setPreview(false);
  };

  return (
    <>
      <VideoPrivewDialog
        preview={preview}
        handlePreviewClose={handlePreviewClose}
        handlePreview={handlePreview}
        previewRecord={previewRecord}
      />
      <List {...props} filters={<PlanetTagFilter />}>
        <Datagrid rowClick="edit">
          <TextField source="label" />
          <TextField source="uuid" />
          <IsLiveField source="isLive" onPreview={handlePreview} />
        </Datagrid>
      </List>
    </>
  );
}
