import {
  Filter,
  Datagrid,
  List,
  ListProps,
  TextField,
  TextInput,
} from 'react-admin';

const PlanetTagFilter = (props) => (
  <Filter {...props}>
    <TextInput label="pos.search" resettable source="q" alwaysOn />
  </Filter>
);

export default function FirmList(props: ListProps) {
  return (
    <List {...props} filters={<PlanetTagFilter />}>
      <Datagrid rowClick="edit">
        <TextField source="name" />
        <TextField source="title" />
        <TextField source="slogan" />
        <TextField source="appKey" />
        <TextField source="appUrls" />
        <TextField source="description" />
      </Datagrid>
    </List>
  );
}
