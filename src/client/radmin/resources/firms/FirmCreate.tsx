import {
  Create,
  CreateProps,
  ReferenceInput,
  SelectInput,
  SimpleForm,
  TextInput,
} from 'ra-ui-materialui';

export default function FirmCreate(props: CreateProps) {
  return (
    <Create {...props}>
      <SimpleForm redirect="list">
        <TextInput source="label" />
        <TextInput source="title" />
        <TextInput source="slogan" />
        <TextInput resettable source="description" multiline rows={4} />
      </SimpleForm>
    </Create>
  );
}
