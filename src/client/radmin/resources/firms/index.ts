import FirmCreate from './FirmCreate';
import FirmList from './FirmList';

const firms = {
  create: FirmCreate,
  list: FirmList,
};

export default firms;
