import { useContext } from 'react';
import { GlobalContext } from '../../contexts/global';

export default function LoginSimplePage() {
  const { globalState } = useContext(GlobalContext);

  return <div>{globalState.toString()}</div>;
}
