import { interval } from 'rxjs';
import { take } from 'rxjs/operators';
import { bind } from '@react-rxjs/core';

const [useFirst5SpacedNumbers] = bind(
  interval(1000).pipe(take(5)),
  0, // Default value
);

function NumberDisplay() {
  const number = useFirst5SpacedNumbers();

  return <div>{number}</div>;
}

function todoPage() {
  return <NumberDisplay />;
}

export default todoPage;
