import React from 'react';
import Card from '@material-ui/core/Card';
import Typography from '@material-ui/core/Typography';
import { makeStyles } from '@material-ui/core/styles';
import dynamic from 'next/dynamic';
import { CardContent, Divider, Grid } from '@material-ui/core';
import ulogo from '../statics/imgs/u42.png';
import u38 from '../statics/imgs/u38.jpg';
import u39 from '../statics/imgs/u39.jpg';
import u40 from '../statics/imgs/u40.jpg';
import u41 from '../statics/imgs/u41.png';
import u30 from '../statics/imgs/u30.svg';
import u29 from '../statics/imgs/u29.svg';
import u28 from '../statics/imgs/u28.svg';
import u52 from '../statics/imgs/u52.png';
import u51 from '../statics/imgs/u51.png';
import u50 from '../statics/imgs/u50.png';
import u49 from '../statics/imgs/u49.png';
import u48 from '../statics/imgs/u48.png';
import u47 from '../statics/imgs/u47.png';
import u46 from '../statics/imgs/u46.png';
import u45 from '../statics/imgs/u45.png';
import u44 from '../statics/imgs/u44.png';
import u43 from '../statics/imgs/u43.png';

const useStyles = makeStyles((theme) => ({
  icon: {
    marginRight: theme.spacing(2),
  },
  heroContent: {
    backgroundColor: theme.palette.background.paper,
    padding: theme.spacing(8, 0, 6),
  },
  heroButtons: {
    marginTop: theme.spacing(4),
  },
  cardGrid: {
    margin: '5%',
    position: 'relative',
    top: -210,
    zIndex: 888,
    padding: 10,
  },
  card: {
    height: '100%',
    display: 'flex',
    flexDirection: 'column',
  },
  cardMedia: {
    paddingTop: '56.25%', // 16:9
  },
  cardContent: {
    flexGrow: 1,
  },
  footer: {
    backgroundColor: theme.palette.background.paper,
    padding: theme.spacing(6),
  },
}));

const MainCarouselSsr = dynamic(() => import('../components/MainCarousel'), {
  ssr: false,
});

export default function Album() {
  const classes = useStyles();

  return (
    <>
      <MainCarouselSsr />
      <Card className={classes.cardGrid}>
        {/* End hero unit */}
        <Grid
          style={{
            width: '100%',
          }}
          container
          direction="row"
          alignContent="space-around"
          justify="space-around"
          alignItems="center"
        >
          <Grid item md={2} xs={4} lg={2}>
            <img
              style={{
                width: 80,
              }}
              src={ulogo}
            />
          </Grid>
          <Grid item md={10} xs={8} lg={10}>
            <Typography variant="h5">想变红，用变红</Typography>
          </Grid>
        </Grid>
        <Grid>
          <br />

          <Typography
            variant="h4"
            style={{
              padding: 20,
            }}
          >
            变红功能板块
          </Typography>
          <br />
        </Grid>
        <Grid container item justify="center" alignContent="space-around">
          <Grid item md={3} xs={6} lg={3}>
            <img style={{ width: '100%', padding: 20 }} src={u38} />
          </Grid>
          <Grid item md={3} xs={6} lg={3}>
            <img
              style={{ width: '100%', padding: 20 }}
              width="auto"
              src={u39}
            />
          </Grid>
          <Grid item md={3} xs={6} lg={3}>
            <img
              style={{ width: '100%', padding: 20 }}
              width="auto"
              src={u40}
            />
          </Grid>
          <Grid item md={3} xs={6} lg={3}>
            <img
              style={{ width: '100%', padding: 20 }}
              width="auto"
              src={u41}
            />
          </Grid>
        </Grid>
        <Divider />
        <Typography
          variant="h4"
          style={{
            padding: 20,
          }}
        >
          变红业务板块
        </Typography>
        <Divider />
        <Grid
          container
          alignItems="center"
          alignContent="center"
          direction="row"
          style={{
            textAlign: 'center',
          }}
        >
          <Grid item md={6} xs={12} lg={6}>
            <img
              style={{ width: '60%', paddingTop: '5%' }}
              width="auto"
              src={u38}
            />
          </Grid>
          <Grid
            item
            md={6}
            xs={12}
            lg={6}
            direction="column"
            container
            alignItems="center"
            alignContent="center"
          >
            <Grid
              item
              style={{
                maxWidth: 380,
              }}
            >
              <Typography variant="h4">成长</Typography>
              <br />
              <br />
              <Typography>
                AI智能推荐课程、人设、赛道和技能，以太阳系九大行星和主题色记录成长历程
                <br />
                <br />
                课件以短视频、直播和练习组成，即学即用，并智能评分
              </Typography>
            </Grid>
          </Grid>
        </Grid>
        <Divider />
        <Grid
          container
          alignItems="center"
          alignContent="center"
          direction="row"
          style={{
            textAlign: 'center',
          }}
        >
          <Grid
            item
            md={6}
            xs={12}
            lg={6}
            direction="column"
            container
            alignItems="center"
            alignContent="center"
          >
            <Grid
              item
              style={{
                maxWidth: 380,
              }}
            >
              <Typography variant="h4">当红</Typography>
              <br />
              <br />
              <Typography>
                以学习和使用APP为基础，智能分析最适合用户的网红成长方向，推荐值得借鉴的大V及数据历程
                <br />
                <br />
                相同层级同学学习交流经验
              </Typography>
            </Grid>
          </Grid>
          <Grid item md={6} xs={12} lg={6}>
            <img
              style={{ width: '60%', paddingTop: '5%' }}
              width="auto"
              src={u40}
            />
          </Grid>
        </Grid>
        <Divider />
        <Grid
          container
          alignItems="center"
          alignContent="center"
          direction="row"
          style={{
            textAlign: 'center',
          }}
        >
          <Grid item md={6} xs={12} lg={6}>
            <img
              style={{ width: '60%', paddingTop: '5%' }}
              width="auto"
              src={u39}
            />
          </Grid>
          <Grid
            item
            md={6}
            xs={12}
            lg={6}
            direction="column"
            container
            alignItems="center"
            alignContent="center"
          >
            <Grid
              item
              style={{
                maxWidth: 380,
              }}
            >
              <Typography variant="h4">跟拍</Typography>
              <br />
              <br />
              <Typography>
                丰富热门模板，一键生成大片效果的短视频，给初学者以自信和便利
                <br />
                <br />
                优秀作品一键同步至各短视频平台
              </Typography>
            </Grid>
          </Grid>
        </Grid>
        <Divider />
        <Grid
          container
          alignItems="center"
          alignContent="center"
          direction="row"
          style={{
            textAlign: 'center',
          }}
        >
          <Grid
            item
            md={6}
            xs={12}
            lg={6}
            direction="column"
            container
            alignItems="center"
            alignContent="center"
          >
            <Grid
              item
              style={{
                maxWidth: 380,
              }}
            >
              <Typography variant="h4">任务</Typography>
              <br />
              <br />
              <Typography>
                提供技能兼职
                <br />
                <br />
                MCN机构挖掘大V和招募的人才库
                <br />
                <br />
                培训机构招生渠道
                <br />
                <br />
                商演、运营推广、产品宣发、广告代言等
              </Typography>
            </Grid>
          </Grid>
          <Grid item md={6} xs={12} lg={6}>
            <img
              style={{ width: '60%', paddingTop: '5%' }}
              width="auto"
              src={u41}
            />
          </Grid>
        </Grid>
        <Divider />
        <Card>
          <CardContent>
            <Grid container direction="column">
              <Grid item container alignItems="center">
                <Grid item md={6} xs={12} lg={6}>
                  {' '}
                  <Typography
                    variant="h4"
                    style={{
                      textAlign: 'center',
                    }}
                  >
                    变红 BIANHONG
                  </Typography>{' '}
                </Grid>
                <Grid container direction="row" item md={6} xs={12} lg={6}>
                  <Grid
                    item
                    md={4}
                    xs={4}
                    lg={4}
                    style={{
                      textAlign: 'center',
                    }}
                  >
                    <img src={u30} style={{ width: '50%' }} />
                    <br />
                    专业课程
                    <br />
                    <br />
                    当红分享
                    <br />
                    <br />
                    学练一体
                    <br />
                    <br />
                  </Grid>
                  <Grid
                    style={{
                      textAlign: 'center',
                    }}
                    item
                    md={4}
                    xs={4}
                    lg={4}
                  >
                    <img src={u28} style={{ width: '50%' }} />
                    <br />
                    当红特效
                    <br />
                    <br />
                    一键生成
                    <br />
                    <br />
                    随时分享
                    <br />
                    <br />
                  </Grid>
                  <Grid
                    style={{
                      textAlign: 'center',
                    }}
                    item
                    md={4}
                    xs={4}
                    lg={4}
                  >
                    <img src={u29} style={{ width: '50%' }} />
                    <br />
                    即学即用
                    <br />
                    <br />
                    兼职赚钱
                    <br />
                    <br />
                    团队协作
                    <br />
                    <br />
                  </Grid>
                </Grid>
              </Grid>
              <Grid
                style={{
                  textAlign: 'center',
                }}
                container
                alignItems="center"
                justify="center"
                alignContent="center"
              >
                <Grid md={3} xs={4} lg={2} item>
                  <img
                    style={{
                      width: '80%',
                    }}
                    src={u43}
                  />
                </Grid>
                <Grid md={3} xs={4} lg={2} item>
                  <img
                    style={{
                      width: '80%',
                    }}
                    src={u44}
                  />
                </Grid>
                <Grid md={3} xs={4} lg={2} item>
                  <img
                    style={{
                      width: '80%',
                    }}
                    src={u45}
                  />
                </Grid>
                <Grid md={3} xs={4} lg={2} item>
                  <img
                    style={{
                      width: '80%',
                    }}
                    src={u46}
                  />
                </Grid>
                <Grid md={3} xs={4} lg={2} item>
                  <img
                    style={{
                      width: '80%',
                    }}
                    src={u47}
                  />
                </Grid>

                <Grid md={3} xs={4} lg={2} item>
                  <img
                    style={{
                      width: '80%',
                    }}
                    src={u48}
                  />
                </Grid>
                <Grid md={3} xs={4} lg={2} item>
                  <img
                    style={{
                      width: '80%',
                    }}
                    src={u49}
                  />
                </Grid>
                <Grid md={3} xs={4} lg={2} item>
                  <img
                    style={{
                      width: '80%',
                    }}
                    src={u50}
                  />
                </Grid>
                <Grid md={3} xs={4} lg={2} item>
                  <img
                    style={{
                      width: '80%',
                    }}
                    src={u51}
                  />
                </Grid>
                <Grid md={3} xs={4} lg={2} item>
                  <img
                    style={{
                      width: '80%',
                    }}
                    src={u52}
                  />
                </Grid>
              </Grid>
            </Grid>
          </CardContent>
        </Card>
      </Card>
    </>
  );
}
