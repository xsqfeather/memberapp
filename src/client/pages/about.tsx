import React, { useCallback, useState } from 'react';

// The editor core
import type { Value } from '@react-page/editor';
import Editor from '@react-page/editor';

// import the main css, uncomment this: (this is commented in the example because of https://github.com/vercel/next.js/issues/19717)
// import '@react-page/editor/lib/index.css';

// The rich text area plugin
import slate from '@react-page/plugins-slate';
// image
import image from '@react-page/plugins-image';
// import PageLayout from '../../components/PageLayout';

import { AppBar, Button, Container, Toolbar } from '@material-ui/core';

const cellPlugins = [slate(), image];

const TRANSLATIONS: { [key: string]: string } = {
  'Edit blocks': '编辑',
  'Add blocks': '添加',
  'Move blocks': '移动',
  'Resize blocks': '调整大小',
  'Preview page': '预览模式',
  TEXT: '文本编辑',
  Text: '文本编辑',
  'Write here': '在这里写',
  Image: '图片',
};

const AboutPage = () => {
  const [value, setValue] = useState<Value>(null);
  const uiTranslator = useCallback((label?: string) => {
    if (TRANSLATIONS[label] !== undefined) {
      return TRANSLATIONS[label];
    }
    return `${label}(to translate)`;
  }, []);
  return (
    <Container>
      <AppBar position="fixed">
        <Toolbar>
          <Button>保存</Button>
        </Toolbar>
      </AppBar>
      <Container
        style={{
          padding: 10,
          position: 'relative',
          top: 100,
        }}
      >
        <Editor
          // lang="de"
          // languages={LANGUAGES}
          uiTranslator={uiTranslator}
          cellPlugins={cellPlugins as any}
          value={value}
          onChange={setValue}
        />
      </Container>
    </Container>
  );
};
export default AboutPage;
