import { Container, TextField, Paper, Tab, Tabs } from '@material-ui/core';
import { useState } from 'react';
import Skeleton from '@material-ui/lab/Skeleton';
import UserInfoListContainer from '../components/UserInfoListContainer';

let timer = 0;

const LoadingBones = () => {
  return (
    <Container>
      {Array.from(new Array(20).keys()).map((_item: any, index: number) => {
        return <Skeleton height={80} key={index} animation="wave" />;
      })}
    </Container>
  );
};

const SearchPage = (props: any) => {
  const { userId } = props;
  const [text, setText] = useState('');
  const [tabValue, setTabValue] = useState(0);
  const [searchText, setSearhText] = useState('');
  const [loading, setLoading] = useState(false);

  return (
    <Container>
      <div
        style={{
          display: 'flex',
          flexDirection: 'column',
          alignItems: 'center',
        }}
      >
        <form
          onSubmit={(e: any) => {
            e.preventDefault();
          }}
          style={{
            display: 'flex',
            alignItems: 'center',
            width: '100%',
            maxWidth: 500,
            justifyContent: 'center',
          }}
        >
          <TextField
            value={text}
            onChange={(e: any) => {
              setText(e.target.value);
              setLoading(true);
              window.clearTimeout(timer);
              timer = 0;
              timer = setTimeout(() => {
                setSearhText(e.target.value);
                timer = 0;
                setLoading(false);
              }, 1234);
            }}
            autoFocus
            fullWidth
            placeholder="用户｜关键｜手机号"
            variant="outlined"
            label="搜索"
          />
        </form>
      </div>

      <Paper>
        <Tabs
          value={tabValue}
          indicatorColor="primary"
          textColor="primary"
          centered
        >
          <Tab label="用户" onClick={() => setTabValue(0)} />
          <Tab label="帖子" onClick={() => setTabValue(1)} />
          <Tab label="当红" onClick={() => setTabValue(2)} />
        </Tabs>
      </Paper>
      <Container
        style={{
          padding: 10,
        }}
      >
        {tabValue === 0 && (
          <div>
            {loading ? (
              <LoadingBones />
            ) : (
              <UserInfoListContainer
                userId={userId ? userId : null}
                filter={{
                  q: searchText,
                }}
              />
            )}
          </div>
        )}
        {tabValue === 1 && <div>帖子</div>}
        {tabValue === 2 && <div>当红</div>}
      </Container>
    </Container>
  );
};

export default SearchPage;
