import {
  Button,
  Container,
  FormControl,
  InputLabel,
  MenuItem,
  Select,
  TextField,
  Typography,
} from '@material-ui/core';
import { useRouter } from 'next/router';
import RestdataProvider from '../dataProvider/rest';

const TeamPage = (props: any) => {
  const router = useRouter();

  const getProfile = async (token: string, userId: number) => {
    const dataProvider = new RestdataProvider(token);
    const userData = await dataProvider.getOne('users', {
      id: userId,
    });
    return userData.data;
  };

  const handleSubmit = async (e: any) => {
    e.preventDefault();
    if (!props.userId) {
      return router.push('/login');
    }
    const token = props.token;
    const userId = props.userId;
    const { profile } = await getProfile(token, userId);
    if (profile.gender === undefined) {
      alert('请设置您的性别');
      return router.push('/my/profile_edit');
    }
    if (!profile.city) {
      alert('请设置您所在的城市');
      return router.push('/my/profile_edit');
    }

    if (!profile.roleName) {
      alert('请设置您的角色');
      return router.push('/my/profile_edit');
    }
    const { gender, city, roleName } = profile;
    const myCondition = {
      gender,
      city,
      roleName,
    };
  };

  return (
    <Container>
      <form
        onSubmit={handleSubmit}
        style={{
          display: 'flex',
          flexDirection: 'column',
          minHeight: 600,
          alignContent: 'space-around',
          justifyContent: 'space-around',
        }}
      >
        <FormControl>
          <InputLabel>性别</InputLabel>
          <Select>
            <MenuItem value={1}>女</MenuItem>
            <MenuItem value={2}>男</MenuItem>
            <MenuItem value={0}>未知/保密</MenuItem>
          </Select>
        </FormControl>
        <FormControl>
          <InputLabel>城市</InputLabel>
          <Select>
              <MenuItem value={'成都'}>成都</MenuItem>
              <MenuItem value={'重庆'}>重庆</MenuItem>
              <MenuItem value={'深圳'}>深圳</MenuItem>
              <MenuItem value={'其他'}>不限</MenuItem>
            </Select>
        </FormControl>
        <FormControl>
          <InputLabel>角色</InputLabel>
          <Select>
              <MenuItem value={'直播带货'}>直播带货</MenuItem>
              <MenuItem value={'摄影达人'}>摄影达人</MenuItem>
            </Select>
        </FormControl>
        <Button type="submit" variant="contained" fullWidth color="primary">
          开始组队
        </Button>
      </form>
    </Container>
  );
};

export default TeamPage;
