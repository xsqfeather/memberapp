import { useEffect, useState } from 'react';
import SocialCardDemo from '../../components/AUserCard';
import { Container } from '@material-ui/core';
import { getListSubject } from '../../observers/restful';

const MyFansPage = () => {
  const [list, setList] = useState([] as any[]);

  useEffect(() => {
    const token = localStorage.getItem('token');
    getListSubject(
      'follows/fans',
      {
        pagination: {
          page: 1,
          perPage: 10,
        },
        sort: {
          field: 'createdAt',
          order: 'DESC',
        },
        filter: {},
      },
      token,
    ).subscribe({
      next: (v: any) => {
        setList(v.response);
      },
    });
  }, []);
  return (
    <Container>
      <SocialCardDemo label="我的关注" list={list} />
    </Container>
  );
};

export default MyFansPage;
