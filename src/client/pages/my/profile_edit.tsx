import {
  Button,
  Container,
  FormControl,
  InputLabel,
  MenuItem,
  Select,
  TextField,
  Typography,
} from '@material-ui/core';
import { useEffect, useState } from 'react';
import decodeJwt from 'jwt-decode';

import AvatarUpload from '../../components/AvatarUpload';

import RestdataProvider from '../../dataProvider/rest';
import router, { useRouter } from 'next/router';

const ProfileEditPage = () => {
  const [userInfo, setUserInfo] = useState({} as any);
  const router = useRouter();


  const getUserInfo = async (token: string) => {
    const dataProvider = new RestdataProvider(token);
    const decoded: any = decodeJwt(token);
    const { userId } = decoded;
    const userData = await dataProvider.getOne('users', {
      id: userId,
    });
    setUserInfo(userData.data);
  };

  const updateUserInfo = async (token: string) => {
    const dataProvider = new RestdataProvider(token);
    const decoded: any = decodeJwt(token);
    const { userId } = decoded;
    const updateRlt = await dataProvider.update('users', {
      id: userId,
      data: {
        ...userInfo,
      },
    });
    console.log({ updateRlt });

    return updateRlt;
  };

  useEffect(() => {
    if (typeof window !== 'undefined' && userInfo.id === undefined) {
      const token = localStorage.getItem('token');
      getUserInfo(token);
    }
  }, [userInfo.id]);

  const handleSubmit = async (e: any) => {
    e.preventDefault();
    const token = localStorage.getItem('token');
    await updateUserInfo(token);
    router.back();
  };

  return (
    <Container>
      <Typography>-编辑个人资料</Typography>
      <br />
      <form
        onSubmit={handleSubmit}
        style={{
          display: 'flex',
          flexDirection: 'column',
          minHeight: 600,
          alignContent: 'space-around',
          justifyContent: 'space-around',
        }}
      >
        <div
          style={{
            display: 'flex',
            justifyContent: 'center',
            alignItems: 'center',
          }}
        >
          <InputLabel>修改头像</InputLabel>
          <AvatarUpload
            src={userInfo.profile?.avatar}
            getImages={(image: any) => {
              setUserInfo({
                ...userInfo,
                profile: {
                  ...userInfo.profile,
                  avatar: image.url,
                },
              });
            }}
          />
        </div>
        <div>
          <TextField
            onChange={(e: any) => {
              setUserInfo({
                ...userInfo,
                nickname: e.target.value,
                profile: {
                  ...userInfo.profile,
                  nickname: e.target.value,
                },
              });
            }}
            InputLabelProps={{ shrink: userInfo.profile?.nickname }}
            fullWidth
            label="我的昵称"
            value={userInfo.profile?.nickname}
          />
        </div>
        <div>
          <TextField
            InputLabelProps={{ shrink: userInfo.profile?.personalStatus }}
            onChange={(e: any) => {
              setUserInfo({
                ...userInfo,
                profile: {
                  ...userInfo.profile,
                  personalStatus: e.target.value,
                },
              });
            }}
            multiline
            rows={3}
            fullWidth
            label="个性签名"
            value={userInfo.profile?.personalStatus}
          />
        </div>
        <FormControl>
          <InputLabel>性别</InputLabel>
          {userInfo.profile && (
            <Select
              onChange={(e: any) => {
                setUserInfo({
                  ...userInfo,
                  profile: {
                    ...userInfo.profile,
                    gender: e.target.value,
                  },
                });
              }}
              value={userInfo.profile?.gender}
            >
              <MenuItem value={1}>女</MenuItem>
              <MenuItem value={2}>男</MenuItem>
              <MenuItem value={0}>未知/保密</MenuItem>
            </Select>
          )}
        </FormControl>
        <FormControl>
          <InputLabel>城市</InputLabel>
          {userInfo.profile && (
            <Select
              onChange={(e: any) => {
                setUserInfo({
                  ...userInfo,
                  profile: {
                    ...userInfo.profile,
                    city: e.target.value,
                  },
                });
              }}
              value={userInfo.profile?.city}
            >
              <MenuItem value={'成都'}>成都</MenuItem>
              <MenuItem value={'重庆'}>重庆</MenuItem>
              <MenuItem value={'深圳'}>深圳</MenuItem>
              <MenuItem value={'其他'}>其他</MenuItem>
            </Select>
          )}
        </FormControl>
        <FormControl>
          <InputLabel>角色</InputLabel>
          {userInfo.profile && (
            <Select
              onChange={(e:any) => {
                  setUserInfo({
                    ...userInfo,
                    profile: {
                      ...userInfo.profile,
                      roleName: e.target.value,
                    },
                  });
              }}
              value={userInfo.profile?.roleName}
            >
              <MenuItem value={'直播带货'}>直播带货</MenuItem>
              <MenuItem value={'摄影达人'}>摄影达人</MenuItem>
            </Select>
          )}
        </FormControl>
        <Button type="submit" fullWidth variant="contained" color="primary">
          保存
        </Button>
      </form>
    </Container>
  );
};

export default ProfileEditPage;
