import { Container } from '@material-ui/core';
import RestdataProvider from '../../dataProvider/rest';
import useScollList from '../../hooks/useScrollList';
import FriendCard from '../../components/FriendCard';

const FriendsPage = (props: any) => {
  console.log(props);
  const fetchFriends = async (page: number) => {
    const token = localStorage.getItem('token');
    const dataProvider = new RestdataProvider(token);
    const res: any = await dataProvider.getList('follows/friends', {
      pagination: {
        page,
        perPage: 20,
      },
      sort: {
        field: 'createdAt',
        order: 'DESC',
      },
      filter: {},
    });
    return res.data as any;
  };
  const [friends] = useScollList(fetchFriends);
  return (
    <Container>
      <div
        style={{
          display: 'flex',
          flexDirection: 'column',
          alignItems: 'center',
          justifyItems: 'center',
        }}
      >
        {friends.map((friend: any, index: number) => {
          const { id, profile, username } = friend;
          return (
            <FriendCard
              id={id}
              name={profile.nickname}
              username={username}
              key={index}
              src={profile.avatar}
            />
          );
        })}
      </div>
    </Container>
  );
};
export default FriendsPage;
