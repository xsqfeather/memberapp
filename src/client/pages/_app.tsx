import React, { useEffect, useState } from 'react';
import Head from 'next/head';
import { ThemeProvider } from '@material-ui/core/styles';
import CssBaseline from '@material-ui/core/CssBaseline';

import { Router, useRouter } from 'next/router';
import { lightTheme } from '../theme/theme_one';
import PcNavMenu from '../components/PcNavMenu';

import {
  setAppMsg,
  setRouteChanging,
  useAppMsg,
  userRouteChanging,
} from '../observers/app';
import {
  AppBar,
  Badge,
  Button,
  IconButton,
  LinearProgress,
  Snackbar,
  Toolbar,
} from '@material-ui/core';
import Alert from '@material-ui/lab/Alert';
import Link from 'next/link';

import decodeJwt from 'jwt-decode';

import { io } from 'socket.io-client';
import { Message, Person, Search } from '@material-ui/icons';
import ChatDialog from '../components/ChatDialog';
import RestdataProvider from '../dataProvider/rest';

const socket = io({
  query: {
    from: '_app',
  },
});

Router.events.on('routeChangeStart', () => {
  setRouteChanging(true);
  const loader = document.getElementById('routerChangeLoader');
  document.body.scrollTop = document.documentElement.scrollTop = 0;
  if (loader) {
    loader.style.display = 'block';
  }
});
Router.events.on('routeChangeComplete', () => {
  setRouteChanging(false);
  const loader = document.getElementById('routerChangeLoader');

  if (loader) {
    loader.style.display = 'none';
  }
});

interface IMyAppProps {
  Component: React.ElementType;
  pageProps: any;
  token: string;
  isLogined: boolean;
  firmId: number;
  userId: number;
  username: string;
}

const MyApp = (props: IMyAppProps) => {
  const appMsg = useAppMsg();
  const routeChanging = userRouteChanging();

  const { Component, pageProps, isLogined, firmId } = props;
  const [userId, setUserId] = useState(0);
  const [, setUsername] = useState('');
  const [user, setUser] = useState({} as any);
  const [unreadCount, setUnReadCount] = useState(0);

  const { open, messageText, severity } = appMsg;

  const router = useRouter();

  const token =
    typeof window !== 'undefined' ? localStorage.getItem('token') : null;

  const getUserInfo = async (userId) => {
    const dataProvider = new RestdataProvider(token);
    try {
      if (!userId) {
        setUser(null);
        setUsername(null);
        return;
      }
      const rlt = await dataProvider.getOne('users', {
        id: userId,
      });
      console.log({ rlt });

      setUser({ ...rlt.data });
    } catch (error) {
      if (error.response?.data?.statusCode === 401) {
        setUser(null);
        setUserId(null);
        setUsername(null);
      }
    }
  };

  useEffect(() => {
    if (token) {
      const decoded: any = decodeJwt(token);
      const { userId, username } = decoded;
      setUserId(userId);
      setUsername(username);
      getUserInfo(userId);
    } else {
      setUserId(0);
      setUsername('');
    }
  }, [token]);

  React.useEffect(() => {
    const jssStyles = document.querySelector('#jss-server-side');
    if (jssStyles) {
      jssStyles.parentElement.removeChild(jssStyles);
    }
  }, []);

  useEffect(() => {
    if (userId) {
      socket.emit('unreadCount', { userId }, (data: any) => {
        setUnReadCount(data.count ? parseInt(data.count) : 0);
        socket.on(`REFRESH_UNREAD_COUNT_${userId}`, ({ count }: any) => {
          setUnReadCount(count ? parseInt(count) : 0);
        });
      });
    }
    return () => {
      console.log('退出');
    };
  }, [userId]);

  const handleMsgClose = () => {
    setAppMsg({
      ...appMsg,
      open: false,
    });
  };

  return (
    <React.Fragment>
      <Head>
        <title>要变红，用变红</title>
      </Head>
      <ThemeProvider theme={lightTheme}>
        <CssBaseline />
        {routeChanging && <LinearProgress color="primary" />}
        {!router.pathname.includes('radmin') && (
          <>
            <AppBar
              style={{
                top: routeChanging ? 5 : 0,
              }}
              position="fixed"
              variant="elevation"
            >
              <Toolbar>
                <div
                  style={{
                    flexGrow: 1,
                  }}
                >
                  <PcNavMenu />
                </div>

                <Link href="/search" passHref>
                  <IconButton color="primary">
                    <Search />
                  </IconButton>
                </Link>
                {!userId && (
                  <Link href="/login" passHref>
                    <Button variant="outlined" color="primary">
                      登录
                    </Button>
                  </Link>
                )}
                {userId && (
                  <>
                    <Link href="/profile" passHref>
                      <IconButton color="primary">
                        <Person />
                      </IconButton>
                    </Link>
                    <Link href="/messages" passHref>
                      <IconButton color="primary">
                        <Message />
                      </IconButton>
                    </Link>
                    <div
                      style={{
                        position: 'relative',
                        left: -15,
                      }}
                    >
                      <Badge badgeContent={unreadCount} color="secondary">
                        ""
                      </Badge>
                    </div>
                  </>
                )}
              </Toolbar>
            </AppBar>
            <br />
            <br />
            <br />
            <br />
          </>
        )}

        <Component
          {...pageProps}
          socket={socket}
          router={router}
          userId={userId}
          username={props.username}
          token={token}
          isLogined={isLogined}
          firmId={firmId}
        />
      </ThemeProvider>
      <Snackbar open={open} onClose={handleMsgClose}>
        <Alert onClose={handleMsgClose} severity={severity}>
          {messageText}
        </Alert>
      </Snackbar>
      {user && <ChatDialog user={user} />}
    </React.Fragment>
  );
};

export default MyApp;
