import { Container } from '@material-ui/core';
import { useRouter } from 'next/router';
import { useEffect, useState } from 'react';
import ProfileCard from '../../components/ProfileCard';
import RestdataProvider from '../../dataProvider/rest';
const UserProfilePage = () => {
  const router = useRouter();
  const [data, setData] = useState({} as any);
  useEffect(() => {
    if (typeof window !== 'undefined' && router.query.id) {
      const token = localStorage.getItem('token');
      const dataProvider = new RestdataProvider(token);
      dataProvider
        .getOne('users', {
          id: parseInt(router.query.id as string),
        })
        .then((rlt) => {
          setData(rlt.data);
        });
    }
  }, [router.query.id]);

  const { profile, id, username } = data;
  return (
    <Container>
      <ProfileCard
        avatar={profile?.avatar}
        nickname={profile?.nickname}
        userId={id}
        username={username}
      />
    </Container>
  );
};

export default UserProfilePage;
