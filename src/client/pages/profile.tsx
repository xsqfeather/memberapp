import { GetServerSideProps } from 'next';
import useServerAuthGetOne from '../hooks/useServerAuthGetOne';

import { Container, Button } from '@material-ui/core';
import ProfileCard from '../components/ProfileCard';
import Cookies from 'js-cookie';

export const getServerSideProps: GetServerSideProps = async (context) => {
  const { req } = context;
  const { cookies } = req;

  if (!cookies.userId || cookies.userId === '') {
    return {
      redirect: {
        destination: '/login',
        permanent: false,
      },
    };
  }
  return await useServerAuthGetOne(
    'users',
    {
      id: parseInt(cookies.userId),
    },
    context,
  );
};

const UserProfilePage = (props: any) => {
  const { data, router, userId } = props;
  const { profile } = data;

  return (
    <Container>
      <ProfileCard
        avatar={profile?.avatar}
        nickname={profile?.nickname}
        userId={userId}
        username={data.username}
      />
      <Button
        variant="contained"
        onClick={() => {
          Cookies.remove('userId');
          Cookies.remove('token');
          localStorage.clear();
          router.push('/login');
        }}
        fullWidth
        color="primary"
      >
        登出当前账号
      </Button>
    </Container>
  );
};
export default UserProfilePage;
