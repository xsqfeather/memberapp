import React from 'react';
import Avatar from '@material-ui/core/Avatar';
import Paper from '@material-ui/core/Paper';
import Grid from '@material-ui/core/Grid';
import LockOutlinedIcon from '@material-ui/icons/LockOutlined';
import Typography from '@material-ui/core/Typography';
import { makeStyles } from '@material-ui/core/styles';
import SMSLoginForm from '../components/SMSLoginForm';
import { bind } from '@react-rxjs/core';
import { Observable } from 'rxjs';
import { createSignal } from '@react-rxjs/utils';
import { getLoginSubject, getSMSSubject } from '../observers/auth';
import Cookies from 'js-cookie';
import { useRouter } from 'next/router';
import { setAppMsg } from '../observers/app';
import decodeJwt from 'jwt-decode';

const useStyles = makeStyles((theme) => ({
  root: {
    height: '93vh',
    width: '100%',
    top: -20,
    position: 'relative',
  },
  image: {
    backgroundImage: 'url(https://source.unsplash.com/random)',
    backgroundRepeat: 'no-repeat',
    backgroundColor:
      theme.palette.type === 'light'
        ? theme.palette.grey[50]
        : theme.palette.grey[900],
    backgroundSize: 'cover',
    backgroundPosition: 'center',
  },
  paper: {
    margin: theme.spacing(8, 4),
    display: 'flex',
    flexDirection: 'column',
    alignItems: 'center',
    alignContent: 'center',
    justifyContent: 'center',
  },
  avatar: {
    margin: theme.spacing(1),
    backgroundColor: theme.palette.secondary.main,
  },
}));

const [sendingSmsChange$, setSendingSms]: [
  Observable<boolean>,
  (s: boolean) => void,
] = createSignal<boolean>();

const [useSendingSms] = bind<boolean>(sendingSmsChange$, false);

const [loginingChange$, setLogining]: [
  Observable<boolean>,
  (s: boolean) => void,
] = createSignal<boolean>();
const [useLogining] = bind<boolean>(loginingChange$, false);

const LoginPage = () => {
  const sendingSms = useSendingSms();
  const logining = useLogining();
  const classes = useStyles();
  const router = useRouter();

  const sendSMS = (phone: string) => {
    setSendingSms(true);

    const sendSMSSubject = getSMSSubject(phone);
    sendSMSSubject.subscribe({
      next: () => {
        setSendingSms(false);
      },
      error: (e) => {
        console.log(e.response.response.code);
      },
      complete: () => console.info('complete'),
    });
  };

  const doSMSLogin = (phone: string, sms: string) => {
    setLogining(true);
    const loginSub = getLoginSubject({
      loginType: 'PHONE_SMS',
      phone,
      sms,
    });
    loginSub.subscribe({
      next: (v) => {
        setSendingSms(false);
        const { access_token } = v.response as any;
        if (access_token) {
          const decodedToken: any = decodeJwt(access_token);
          const { userId } = decodedToken;
          localStorage.setItem('token', access_token);
          localStorage.setItem(userId, userId);
          Cookies.set('token', access_token);
          Cookies.set('userId', userId);
          router.push(`/world`);
          setAppMsg({
            open: true,
            messageText: '登录成功',
            severity: 'success',
          });
        }
      },
      error: (e) => {
        if (e.response.code === 'SMS_IS_NOT_MATCH') {
          setSendingSms(false);
          setLogining(false);
          setAppMsg({
            open: true,
            messageText: '验证码错误',
            severity: 'error',
          });
        }
      },
      complete: () => console.info('complete'),
    });
  };

  return (
    <Grid container component="main" className={classes.root}>
      <Grid item xs={false} sm={false} md={7} className={classes.image} />
      <Grid
        item
        container
        xs={12}
        sm={12}
        md={5}
        direction="column"
        justify="center"
        alignItems="center"
        component={Paper}
      >
        <Avatar className={classes.avatar}>
          <LockOutlinedIcon />
        </Avatar>
        <Typography component="h1" variant="h5">
          登录
        </Typography>
        <SMSLoginForm
          logining={logining}
          doLogin={doSMSLogin}
          sendingSms={sendingSms}
          sendSMS={sendSMS}
        />
      </Grid>
    </Grid>
  );
};

export default LoginPage;
