import React from 'react';
import DashBoardLayout from '../../components/layouts/DashBoardLayout';

import { IRoleTag } from '../../types/RoleTag';
import Datagrid from '../../components/table/Datagrid';
import { IColumnType } from '../../types/global';
import TextColumnField from '../../components/table/TextColumnField';
import { Button, TableCell } from '@material-ui/core';
import Pagination from '@material-ui/lab/Pagination';

interface IDashboardProps {
  title: string;
}

const columns: IColumnType[] = [
  {
    label: '角色名',
    field: 'name',
    sortable: true,
  },
  {
    label: '创建于',
    field: 'createdAt',
  },
  {
    label: '更新于',
    field: 'updatedAt',
    sortable: true,
  },
  {
    label: '操作',
    field: 'opera',
  },
];

const rows: IRoleTag[] = [
  {
    name: '摄影',
    createdAt: new Date().toString(),
    updatedAt: new Date().toString(),
  },
  {
    name: '所属星球',
    createdAt: new Date().toString(),
    updatedAt: new Date().toString(),
  },
];

export default function Dashboard(props: IDashboardProps) {
  return (
    <DashBoardLayout title={props.title}>
      <Datagrid<IRoleTag> columns={columns} rows={rows}>
        <TextColumnField source="name" />
        <TextColumnField source="createdAt" />
        <TextColumnField source="updatedAt" />
        <TableCell>
          <Button>编辑</Button>
          <Button>删除</Button>
        </TableCell>
      </Datagrid>
      <Pagination />
    </DashBoardLayout>
  );
}
