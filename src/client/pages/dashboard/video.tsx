import React from 'react';

import DashBoardLayout from '../../components/layouts/DashBoardLayout';
import { Card, CardContent } from '@material-ui/core';

import { IRoleTag } from '../../types/RoleTag';
import { GetServerSideProps } from 'next';
import StreamUpload from '../../components/StreamUpload';

interface IRoleTagsPageProps {
  title: string;
  rows: IRoleTag[];
}

const rows: IRoleTag[] = [
  {
    name: '摄影',
    createdAt: new Date().toString(),
    updatedAt: new Date().toString(),
  },
  {
    name: '所属星球',
    createdAt: new Date().toString(),
    updatedAt: new Date().toString(),
  },
];

export default function VideoPage(props: IRoleTagsPageProps) {
  return (
    <DashBoardLayout title={props.title}>
      <Card>
        <CardContent>
          <StreamUpload />
        </CardContent>
      </Card>
    </DashBoardLayout>
  );
}
