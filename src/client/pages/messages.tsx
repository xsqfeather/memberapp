import { Container } from '@material-ui/core';
import { useEffect, useState } from 'react';
import Messages from '../components/Messages';
import RestdataProvider from '../dataProvider/rest';
import { io } from 'socket.io-client';
const socket = io();

const MessagesPage = ({ userId }) => {
  const [list, setList] = useState([] as any);
  const fetchList = async (page: number) => {
    const token = localStorage.getItem('token');
    const dataProvider = new RestdataProvider(token);
    const rlt: any = await dataProvider.getList('messages', {
      pagination: {
        page,
        perPage: 1000,
      },
      sort: {
        field: 'updatedAt',
        order: 'DESC',
      },
      filter: {},
    });

    setList(rlt.data as any);
  };

  useEffect(() => {
    fetchList(1);
  }, []);

  useEffect(() => {
    if (userId) {
      socket.on('REFRESH_MESSAGE_LIST_' + userId, () => {
        fetchList(1);
      });
    }
  }, [userId]);

  return (
    <Container>
      <Messages list={list} />
    </Container>
  );
};

export default MessagesPage;
