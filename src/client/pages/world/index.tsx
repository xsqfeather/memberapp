import MiniBlogTimeLine, { IBlog } from '../../components/MiniBlogTimeLine';
import FloatPostButton from '../../components/FloatPostButton';
import useScrollList from '../../hooks/useScrollList';
import RestdataProvider from '../../dataProvider/rest';
import { Container, LinearProgress, CircularProgress } from '@material-ui/core';
import { SnackbarProvider } from 'notistack';
import axios from 'axios';
import { FloatBackTopButton } from '../../components/FloatBackTopButton';

axios.interceptors.request.use((config) => {
  if (!Reflect.has(config.headers, 'Authorization')) {
    const token = localStorage.getItem('token');
    token && Reflect.set(config.headers, 'Authorization', `bearer ${token}`);
  }
  return config;
});

const MyWorld = (props: any) => {
  const [blogs, toploadiing, bottomloading] = useScrollList<IBlog>(
    async (page: number) => {
      const token = localStorage.getItem('token');
      const dataProvider = new RestdataProvider(token);
      const rlt = await dataProvider.getList('mini-blog', {
        pagination: {
          page,
          perPage: 10,
        },
        sort: {
          field: 'id',
          order: 'DESC',
        },
        filter: {},
      });
      return (rlt as any).data as any;
    },
  );

  return (
    <SnackbarProvider maxSnack={3}>
      <Container>
        <div
          style={{
            width: '100%',
          }}
        >
          {toploadiing && (
            <LinearProgress
              style={{
                width: '100%',
              }}
              color="secondary"
            />
          )}
        </div>
        <MiniBlogTimeLine blogs={blogs} />
        <FloatPostButton userId={props.userId} />
        <FloatBackTopButton />
        <div
          style={{
            width: '100%',
            height: 100,
            textAlign: 'center',
            display: 'flex',
            alignItems: 'center',
            justifyContent: 'center',
          }}
        >
          {bottomloading && (
            <CircularProgress
              style={{
                width: 50,
                height: 50,
              }}
              color="secondary"
            />
          )}
        </div>
      </Container>
    </SnackbarProvider>
  );
};
export default MyWorld;
