import dynamic from 'next/dynamic';
import Head from 'next/head';
import { useEffect, useState } from 'react';
const AdminApp = dynamic(() => import('../radmin/AdminApp'), {
  ssr: false,
});
export default function RAdminPage() {
  const [loading, setLoading] = useState(true);
  useEffect(() => {
    if (process.browser) {
      if (document.body) {
        setLoading(false);
      }
    }
  }, [process.browser]);
  return (
    <>
      <Head>
        <title>管理系统</title>
        {/* <script async src="https://arc.io/widget.min.js#iYmhiArF"></script> */}
      </Head>
      {loading ? <div></div> : <AdminApp />}
    </>
  );
}
