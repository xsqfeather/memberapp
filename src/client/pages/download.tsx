import { Button, Container } from '@material-ui/core';
import { useContext, useEffect } from 'react';
import { GlobalActions, GlobalContext } from '../contexts/global';

export const DownloadPage = ({}) => {
  const { globalDispatch } = useContext(GlobalContext);
  useEffect(() => {
    if (process.browser) {
      const agent = window.navigator.userAgent.toLowerCase();
      if (agent.includes('micromessenger')) {
        globalDispatch(GlobalActions.showWeChatBackdrop());
      }
    }
    return () => {
      globalDispatch(GlobalActions.closeWechatBackGrop());
    };
  }, []);
  return (
    <Container
      style={{
        minHeight: 350,
        display: 'flex',
        justifyContent: 'center',
        alignContent: 'center',
        alignItems: 'center',
      }}
    >
      <div>
        <Button
          color="primary"
          size="large"
          variant="contained"
          component="a"
          href="/uploads/app-release.apk"
          download
        >
          点击下载Android SDK DEMO
        </Button>
      </div>
    </Container>
  );
};

export default DownloadPage;
