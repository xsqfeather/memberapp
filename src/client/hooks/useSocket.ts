import { useCallback, useEffect, useRef } from 'react';
import { io, Socket } from 'socket.io-client';

interface IOptions {
  namespace?: string;
  // 复用之前的socket链接, WS不会被重新初始化
  multiplex?: boolean;
  // 自定义查询，仅在WS初始化时可以获取
  query?: Record<string, string>;
  // 为false不加载token, 未填则从LocalStorage读取token字段
  authority?: string | false;
  // 加入房间, 仅在WS初始化时可用
  joinId?: string | number;
}

export const useSocket = (join?: string) => {
  const socketRef = useRef<Socket | null>(null);
  const connect = useCallback(
    ({ multiplex, joinId, query, authority, namespace }: IOptions = {}) => {
      socketRef.current = io(namespace, {
        query: {
          from: 'socket-hook',
          join: joinId ? `${join}:${joinId}` : join,
          np: namespace,
          ...query,
        },
        ...(authority === false
          ? {}
          : {
              transportOptions: {
                polling: {
                  extraHeaders: {
                    Authorization:
                      authority ?? `Bearer ${localStorage.getItem('token')}`,
                  },
                },
              },
            }),
        multiplex: multiplex ?? false,
      });
      socketRef.current.on('connect', (...args) => {
        socketRef.current.emit('SOCKET_ASSIGN', socketRef.current.id, () => {
          console.info('已加入房间');
        });
        console.info('链接已建立', args);
      });
      socketRef.current.on('disconnect', () => {
        console.warn('链接已断开');
      });
      socketRef.current.on('WILL_DISCONNECT', (data) => {
        console.info('链接将断开', data);
      });
      socketRef.current.on('connect_error', (error) => {
        console.error('链接错误', error);
      });
      return socketRef.current;
    },
    [],
  );
  const getSocket = () => {
    return socketRef.current;
  };
  const on: Socket['on'] = (...args) => socketRef.current.on(...args);
  const off: Socket['off'] = (...args) => socketRef.current.off(...args);
  const emit: Socket['emit'] = (...args) => socketRef.current.emit(...args);
  const disconnect: Socket['disconnect'] = () => socketRef.current.disconnect();
  useEffect(() => {
    return () => {
      const socket = socketRef.current;
      if (!socket || socket.disconnected) return void 0;
      socket.offAny();
      socket.disconnect();
    };
  }, []);
  return {
    connect,
    getSocket,
    on,
    off,
    emit,
    disconnect,
  };
};
