import { useCallback, useEffect, useReducer, useRef } from 'react';
import {
  getScrollHeight,
  getScrollTop,
  getWindowHeight,
} from '../utils/scroll';

interface IListState<T> {
  list: T[];
  topLoading: boolean;
  bottomLoading: boolean;
}

function listReducer<T extends Record<string, any>>(
  state: IListState<T>,
  action: {
    type: string;
    payload?: T[];
  },
) {
  switch (action.type) {
    case 'SHOW_BOTTOM_LOADING':
      return {
        ...state,
        bottomLoading: true,
      };
    case 'LOAD_MORE':
      return {
        ...state,
        list: [...state.list, ...action.payload],
      };
    case 'HIDE_BOTTOM_LOADING':
      return {
        ...state,
        bottomLoading: false,
      };

    case 'SHOW_TOP_LOADING':
      return {
        ...state,
        topLoading: true,
      };

    case 'LOAD_TOP':
      const merged = Array.from(
        new Map(
          action.payload
            .concat(state.list.slice(0, 10))
            .map((it) => [it.id as number, it]),
        ).values(),
      ).sort(
        (a, b) =>
          new Date(b.createdAt).getTime() - new Date(a.createdAt).getTime(),
      );
      return {
        ...state,
        list: merged.concat(state.list.slice(10)),
      };

    case 'HIDE_TOP_LOADING':
      return {
        ...state,
        topLoading: false,
      };
    default:
      return state;
  }
}

export default function useScrollList<T>(
  fetchNews?: (page: number) => Promise<T[]>,
) {
  const [listState, listDispatch] = useReducer(listReducer, {
    list: [],
    topLoading: false,
    bottomLoading: false,
  });

  const metadataRef = useRef({
    bottomTouched: false,
    topTouched: false,
    page: 1,
    timer: null as number | null,
    loadedPage: new Set<number>(),
    initialized: false,
  });

  const loadRemoteData = useCallback(
    async (page: number, behavior: 'load' | 'new') => {
      try {
        if (!metadataRef.current.loadedPage.has(page)) {
          const result = await fetchNews(page);
          metadataRef.current.loadedPage.add(page);
          if (result.length > 0)
            listDispatch({
              type: behavior === 'load' ? 'LOAD_MORE' : 'LOAD_TOP',
              payload: result,
            });
        }
        metadataRef.current.page++;
      } catch (e) {
        console.error(`Load data failed`);
        console.error(e);
      } finally {
        metadataRef.current.topTouched = false;
        metadataRef.current.bottomTouched = false;
      }
    },
    [fetchNews, listDispatch],
  );

  const initData = useCallback(async () => {
    metadataRef.current.topTouched = true;
    listDispatch({
      type: 'SHOW_TOP_LOADING',
    });
    await loadRemoteData(1, 'load');
    listDispatch({
      type: 'HIDE_TOP_LOADING',
    });
  }, [listDispatch, loadRemoteData]);

  const handleScroll = useCallback(
    async (e: any) => {
      e.preventDefault();
      if (getScrollTop() === 0) {
        if (metadataRef.current.topTouched) return void 0;
        console.log('--> 加载新数据');
        metadataRef.current.page = 1;
        metadataRef.current.topTouched = true;
        metadataRef.current.loadedPage.delete(1);
        listDispatch({
          type: 'SHOW_TOP_LOADING',
        });
        await loadRemoteData(1, 'new');
        listDispatch({
          type: 'HIDE_TOP_LOADING',
        });
        return void 0;
      }
      if (getScrollTop() + 100 + getWindowHeight() >= getScrollHeight()) {
        if (metadataRef.current.bottomTouched) return void 0;
        console.log('--> 加载更多数据');
        metadataRef.current.bottomTouched = true;
        metadataRef.current.timer = setTimeout(async () => {
          listDispatch({
            type: 'SHOW_BOTTOM_LOADING',
          });
          await loadRemoteData(metadataRef.current.page, 'load');
          listDispatch({
            type: 'HIDE_BOTTOM_LOADING',
          });
        }, 500);
      }
    },
    [loadRemoteData, listDispatch],
  );
  useEffect(() => {
    if (typeof window !== 'undefined') {
      document.addEventListener('scroll', handleScroll, false);
      return () => {
        document.removeEventListener('scroll', handleScroll);
        window.clearTimeout(metadataRef.current.timer);
      };
    }
    return void 0;
  }, [handleScroll]);

  useEffect(() => {
    if (metadataRef.current.initialized) return void 0;
    metadataRef.current.initialized = true;
    initData();
  }, [initData]);

  return [
    listState.list as T[],
    listState.topLoading,
    listState.bottomLoading,
  ] as const;
}
