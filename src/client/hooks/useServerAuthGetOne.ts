import RestdataProvider from '../dataProvider/rest';

export default async function useServerAuthGetOne(
  resourceName: string,
  params: {
    id: number;
  },
  context: any,
) {
  console.log({ params });

  const { req } = context;
  if (!req.cookies.token) {
    return {
      redirect: {
        destination: '/login',
        permanent: false,
      },
    };
  }
  const dataProvider = new RestdataProvider(req.cookies.token);
  try {
    const rlt = await dataProvider.getOne(resourceName, {
      id: params.id,
    });
    console.log({ rlt });

    return {
      props: {
        data: rlt.data,
      },
    };
  } catch (error) {
    console.error(error.response);

    return {
      props: {
        ...error.response,
      },
    };
  }
}
