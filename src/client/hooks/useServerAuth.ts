import { NextApiRequestCookies } from 'next/dist/next-server/server/api-utils';
import { IncomingMessage } from 'http';

export default function useServerAuth(
  req: IncomingMessage & {
    cookies: NextApiRequestCookies;
  },
) {
  if (!req.cookies || !req.cookies.token) {
    return {
      redirect: {
        destination: '/login',
      },
      props: null,
    };
  }
  return {
    isLogined: true,
  };
}
