import { useContext } from 'react';
import { GlobalContext } from '../contexts/global';

export default function useGlobalState() {
  const dispatch = useContext(GlobalContext);
  return {
    dispatch,
  };
}
