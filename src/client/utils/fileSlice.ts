import md5 from 'md5';
type ChunkType = 'size' | 'length';

export interface Chunk {
  position: number;
  buf: ArrayBuffer;
  md5: string;
}

export interface SliceResult {
  fileMd5: string;
  fileName: string;
  fileSize: number;
  fileType: string;
  lastModified: number;
  chunks: Chunk[];
}

/**
 * @function 文件切片
 * @param fileList 文件列表
 * @param chunkType 'length'表示chunk总长度.'size'表示chunk大小。默认为'length'
 * @param chunkValue 提供给chunkType的值,length默认4份,size默认4M
 * @returns
 */
export async function slice(
  fileList: FileList,
  chunkType: ChunkType = 'length',
  chunkValue?: number,
): Promise<SliceResult[]> {
  let result = [];
  for (let i = 0; i < fileList.length; i++) {
    const file = fileList[i];
    const fileMd5 = md5(new Uint8Array(await file.arrayBuffer()));
    const _chunkValue = chunkValue
      ? chunkValue
      : chunkType == 'length'
      ? 4
      : 4 * 1024 * 1024;
    let chunks: Chunk[];
    switch (chunkType) {
      case 'length':
        chunks = forLength(await file.arrayBuffer(), _chunkValue);
        break;
      case 'size':
        chunks = forSize(await file.arrayBuffer(), _chunkValue);
        break;
    }

    result.push({
      fileMd5: fileMd5,
      fileName: file.name,
      fileSize: file.size,
      fileType: file.type,
      lastModified: file.lastModified,
      chunks,
    });
  }
  return result;
}

/**
 * @param file
 * @param length 切片数量
 * @returns
 */
export function forLength(buf: ArrayBuffer, length: number): Chunk[] {
  const fileSize = buf.byteLength;
  const chunkSize = Math.ceil(fileSize / length);
  let chunks: Chunk[] = [];
  for (let i = 0, start = 0; i < length; i++, start += chunkSize) {
    const chunk = buf.slice(start, start + chunkSize);
    chunks.push({
      position: i,
      buf: chunk,
      md5: md5(new Uint8Array(chunk)),
    });
  }
  return chunks;
}

/**
 * @param file
 * @param chunkSize 单片大小
 * @returns
 */
export function forSize(buf: ArrayBuffer, chunkSize: number) {
  const fileSize = buf.byteLength;
  let chunks: Chunk[] = [];
  let pos = 0,
    i = 0;
  while (pos < fileSize) {
    const chunk = buf.slice(pos, (pos += chunkSize));
    chunks.push({
      position: i++,
      buf: chunk,
      md5: md5(new Uint8Array(chunk)),
    });
  }
  return chunks;
}
