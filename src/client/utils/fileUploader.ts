import axios from 'axios';
import Big from 'big.js';
import md5 from 'md5';
import { slice, SliceResult } from './fileSlice';

export interface UploadConfig {
  /**
   * @param length 按照切片数量切,默认为切片数量
   * @param size 按照切片大小来切
   */
  chunkType?: 'length' | 'size';
  /**
   * 根据chunkType确定value类型;
   * 为'length'时,代表chunk总数,默认4
   * 为'size'时,代表chunk的大小,默认1M
   */
  chunkValue?: number;
  /**
   * 只有当状态码返回正确的情况下,才会正确上传。
   * 成功状态码,默认201。
   */
  successCode?: number;
}

export interface SliceProgressEvent {
  fileName: string;
  loaded: number;
  total: number;
}

/**
 * 上传文件
 * @param url
 * @param fileList
 * @param onProgress
 * @param config
 */
export async function uploadSliceFiles(
  url: string,
  fileList: FileList,
  config: UploadConfig,
  onFulfilled?: (fileUrl: string) => void,
  onRejected?: (error: any) => void,
  onProgress?: (sliceProgressEvent: SliceProgressEvent) => void,
) {
  config.chunkType = config.chunkType ?? 'length';
  config.chunkValue = config.chunkValue
    ? config.chunkValue
    : config.chunkType == 'length'
    ? 10
    : 1024 * 1024;
  config.successCode = config.successCode ?? 201;
  const sliceResults = await slice(
    fileList,
    config.chunkType,
    config.chunkValue,
  );
  for (let i = 0; i < sliceResults.length; i++) {
    const sliceResult = sliceResults[i];
    const su = new SliceUpload(
      url,
      sliceResult,
      onProgress,
      config.successCode,
    );
    const start = await su.informStart();
    //如果已经上传,则直接返回

    if (start.status == su.successCode) {
      if (start.data !== 'ready') {
        onFulfilled(start.data);
        return;
      }
    }
    const chunks = await su.uploadChunks();

    if (!chunks) {
      onRejected('[暂无错误准确提示,错误来源于chunks上传时]');
      return;
    }
    const end = await su.informEnd();
    if (end.status == su.successCode) {
      onFulfilled(end.data);
      return;
    }
  }
}

export class SliceUpload {
  private progress = new Big(0);
  private formData: FormData;
  constructor(
    public url: string,
    public sliceResult: SliceResult,
    private onProgress?: (sliceProgressEvent: SliceProgressEvent) => void,
    public successCode = 201,
  ) {
    this.initFormData();
  }
  private initFormData() {
    this.formData = new FormData();
    this.formData.append('message', '');
    this.formData.append('fileMd5', this.sliceResult.fileMd5);
    this.formData.append('fileName', this.sliceResult.fileName);
    this.formData.append('fileSize', this.sliceResult.fileSize.toString());
    this.formData.append('fileType', this.sliceResult.fileType);
    this.formData.append(
      'chunkTotal',
      this.sliceResult.chunks.length.toString(),
    );
  }
  informStart() {
    this.formData.set('message', 'sliceStart');
    return axios.post(this.url, this.formData);
  }
  async uploadChunks() {
    this.formData.set('message', 'sliceChunk');
    this.formData.append('file', '');
    this.formData.append('chunkMd5', '');
    this.formData.append('position', '');
    for (let i = 0; i < this.sliceResult.chunks.length; i++) {
      const chunk = this.sliceResult.chunks[i];
      this.formData.set('file', new Blob([chunk.buf]));
      this.formData.set('chunkMd5', chunk.md5);
      this.formData.set('position', chunk.position.toString());
      const chunkRes = await axios.post(getUploadUrl(this.url), this.formData, {
        onUploadProgress: (e) => {
          let chunkProgress = new Big(e.loaded / e.total);
          this.progress = this.progress.plus(
            chunkProgress.times(chunk.buf.byteLength),
          );
          this.onProgress({
            fileName: this.sliceResult.fileName,
            loaded: this.progress.toNumber(),
            total: this.sliceResult.fileSize,
          });
        },
      });
      //如果块上传不成功则退出
      if (!(chunkRes.status === this.successCode)) {
        return false;
      }
    }
    return true;
  }
  informEnd() {
    this.formData.delete('file');
    this.formData.delete('chunkMd5');
    this.formData.delete('position');
    this.formData.set('message', 'sliceEnd');
    return axios.post(this.url, this.formData);
  }
}

/**
 * 获取上传url，绕过缓存
 * https://developer.mozilla.org/zh-CN/docs/Web/API/XMLHttpRequest/Using_XMLHttpRequest#%E7%BB%95%E8%BF%87%E7%BC%93%E5%AD%98
 * @returns
 */
function getUploadUrl(url: string) {
  return `${url}/?t=${Date.now()}`;
}
