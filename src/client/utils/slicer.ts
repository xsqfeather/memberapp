import EventEmitter from 'eventemitter3';

//追加ts提示,无实际意义
interface cancelFunction {
  (): void;
}

/**
 * @class 切片机
 * @class It's just a simple slicer
 * @description 不同方法都会触发事件。除了blob_simple，blob不会占用内存,只是指向文件所以他是高效的。
 * @description Each method can fire  event. With the exception of blob_simple,because blob doesn't take up memory and just points to files so it's efficient.
 */
export class Slicer extends EventEmitter {
  private blobs: Blob[] = [];
  readonly chunkTotal: number;

  /**
   * @param file
   * @param chunkSize default 1M(1x1024x1024)
   */
  constructor(file: File, private chunkSize = 1 * 1024 * 1024) {
    super();
    let offset = 0;
    while (offset < file.size) {
      this.blobs.push(file.slice(offset, offset + chunkSize, 'file/chunk'));
      offset += chunkSize;
    }
    this.chunkTotal = this.blobs.length;
  }

  /**
   * @function getBlobs 当你没有复杂需求时这是最好的选择
   * @function getBlobs This is best when you don't have complex requirements
   * @returns
   */
  getBlobs() {
    return this.blobs;
  }

  /**
   * @function slice_blob event说明切片途中会发出事件消息。适合扩展。
   * @function slice_blob A message is emitted when you slice it
   */
  slice_blob() {
    let _cancel = false;
    let _blobs = [...this.blobs];
    const cancel: cancelFunction = () => {
      _cancel = true;
      next();
    };
    let next = () => {
      if (_cancel) {
        //取消时_blobs被清空,不会在发出 chunk 事件。
        this.emit('cancel', _blobs);
        _blobs = [];
      }
      const blob = _blobs.shift();
      if (blob !== undefined) {
        this.emit('chunk', blob, next, cancel);
      } else {
        this.emit('end');
      }
    };
    next();
  }

  /**
   * @function slice_stream 某些情况下,用流切片更好,不要在不需要对buffer进行处理时使用。
   * @function slice_stream In some cases, stream slicing is better.Don't use it when you don't need to process the buffer
   */
  slice_stream() {
    let _cancel = false;
    let _blobs = [...this.blobs];
    const cancel: cancelFunction = () => {
      _cancel = true;
      next();
    };
    const next = () => {
      if (_cancel) {
        this.emit('cancel', _blobs);
        _blobs = [];
      }
      const blob = _blobs.shift();
      if (blob !== undefined) {
        let s = blob.stream() as ReadableStream<Uint8Array>;
        let r = s.getReader();
        let chunk = new Uint8Array(blob.size);
        let offset = 0;
        const readHandle = ({
          done,
          value,
        }: ReadableStreamDefaultReadResult<Uint8Array>): any => {
          if (done) {
            r.releaseLock();
            s.cancel()
              .catch((error) => {
                this.emit('error', {
                  note: 'stream.cancel failed',
                  ...error,
                });
                cancel();
              })
              .finally(() => {
                //不管流是否被正确取消,chunk都将被发出
                this.emit('chunk', new Uint8Array(chunk), next, cancel);
                //闭包比较深,手动赋值为null,告知该数据无用
                chunk = null;
                offset = null;
              });
            return;
          }
          chunk.set(value as Uint8Array, offset);
          offset += (value as Uint8Array).byteLength;
          return r.read().then(readHandle);
        };
        r.read()
          .then(readHandle)
          .catch((error) => {
            this.emit('error', {
              note: 'readStream failed',
              ...error,
            });
            cancel();
          });
      } else {
        this.emit('end');
      }
    };
    next();
  }

  on(event: 'cancel', listener: (leftover: Blob[]) => void): this;
  on(
    event: 'chunk',
    listener: (
      chunk: Uint8Array | Blob,
      next: () => void,
      cancel: () => void,
    ) => void,
  ): this;
  on(
    event: 'error',
    listener: (err: { note: string; [i: string]: any }) => void,
  ): this;
  on(event: 'end', listener: () => void): this;
  on(event: string, listener: any): this {
    super.addListener(event, listener);
    return this;
  }

  addListener(event: 'cancel', listener: (leftover: Blob[]) => void): this;
  addListener(
    event: 'chunk',
    listener: (
      chunk: Uint8Array | Blob,
      next: () => void,
      cancel: () => void,
    ) => void,
  ): this;
  addListener(
    event: 'error',
    listener: (err: { note: string; [i: string]: any }) => void,
  ): this;
  addListener(event: 'end', listener: () => void): this;
  addListener(event: string, listener: any): this {
    super.addListener(event, listener);
    return this;
  }

  once(event: 'cancel', listener: (leftover: Blob[]) => void): this;
  once(
    event: 'chunk',
    listener: (
      chunk: Uint8Array | Blob,
      next: () => void,
      cancel: () => void,
    ) => void,
  ): this;
  once(
    event: 'error',
    listener: (err: { note: string; [i: string]: any }) => void,
  ): this;
  once(event: 'end', listener: () => void): this;
  once(event: string, listener: any): this {
    super.once(event, listener);
    return this;
  }
}
