import axios from 'axios';
import { stringify } from 'query-string';
import { fetchUtils, HttpError } from 'ra-core';
import { APP_KEY, REST_API } from '../constants/endpoints';

const httpClient = (url: string, options = {} as any) => {
  if (!options.headers) {
    options.headers = new Headers({ Accept: 'application/json' });
  }
  const token = localStorage.getItem('token');
  if (token) {
    options.headers.set('Authorization', `Bearer ${token}`);
  }
  return fetchUtils.fetchJson(url, options);
};

interface IGetListParams {
  pagination: {
    page: number;
    perPage: number;
  };
  sort: {
    field: string;
    order: string;
  };
  select?: string[];
  filter:
    | {
        [x: string]: string;
      }
    | {};
}

interface ICreateParams {
  data: {
    [x: string]: any;
  };
}

interface IUpdateParams {
  id: number;
  data: {
    [x: string]: any;
  };
}

interface IDeleteParams {
  id: number;
}

interface IGetOneParams {
  id: number;
}

interface IDeleteManyParams {
  ids?: number[];
  userId?: number;
  fanUserId?: number;
}

interface IGetManyParams {
  ids: number[];
}

export function queryUrlBuilder(resource: string, params: IGetListParams) {
  const { page, perPage } = params.pagination;
  const { field, order } = params.sort;
  const query = {
    sort: JSON.stringify([field, order]),
    range: JSON.stringify([(page - 1) * perPage, page * perPage - 1]),
    filter: JSON.stringify(params.filter),
  };
  const url = `${REST_API}/${resource}?${stringify(query)}`;
  return url;
}

export default class RestdataProvider {
  private token: string;
  constructor(token: string) {
    if (token) {
      this.token = token;
      axios.defaults.headers.common = { Authorization: `bearer ${token}` };
      axios.defaults.headers.common = { 'X-APP-KEY': APP_KEY };
    }
  }

  getList = (resource: string, params: IGetListParams) => {
    const { page, perPage } = params.pagination;
    const { field, order } = params.sort;
    axios.defaults.headers.common = { Authorization: `bearer ${this.token}` };
    const query = {
      sort: JSON.stringify([field, order]),
      range: JSON.stringify([(page - 1) * perPage, page * perPage - 1]),
      filter: JSON.stringify(params.filter),
    };
    const url = `${REST_API}/${resource}?${stringify(query)}`;
    return new Promise((res: any, rej: any) => {
      axios
        .get(url)
        .then((rlt: any) => {
          const contentRangeStr = rlt.headers['content-range'];
          const contentRangeStrArr = contentRangeStr
            ? contentRangeStr.split('/')
            : [];
          const totalStr = contentRangeStrArr.pop();
          res({
            data: rlt.data,
            total: parseInt(totalStr ? totalStr : '0', 10),
          });
        })
        .catch((error: any) => {
          rej(error.response.errorData);
        });
    });
  };

  getOne = (resource: string, params: IGetOneParams): Promise<any> => {
    return new Promise((res: any, rej: any) => {
      axios
        .get(`${REST_API}/${resource}/${params.id}`, {
          headers: {
            Authorization: `bearer ${this.token}`,
          },
        })
        .then((rlt: any) => {
          res({
            data: rlt.data,
          });
        })
        .catch((err: any) => {
          rej(err);
        });
    });
  };

  getMany = (resource: string, params: IGetManyParams) => {
    const query = {
      filter: JSON.stringify({ id: params.ids }),
    };
    return new Promise((res, rej) => {
      const url = `${REST_API}/${resource}?${stringify(query)}`;
      return axios
        .get(url)
        .then((rlt: any) => {
          res({
            data: rlt.data,
          });
        })
        .catch((err: any) => {
          rej(err);
        });
    });
  };

  create = (resource: string, params: ICreateParams): Promise<any> => {
    return new Promise((res, rej) => {
      return httpClient(`${REST_API}/${resource}`, {
        method: 'POST',
        body: JSON.stringify(params.data),
      })
        .then((response: any) => {
          return res({
            data: response.json,
          });
        })
        .catch((error: any) => {
          rej(error);
        });
    });
  };

  update = (resource: string, params: IUpdateParams): Promise<any> => {
    return new Promise((res: any, rej: any) => {
      axios
        .put(`${REST_API}/${resource}/${params.id}`, params.data)
        .then((rlt: any) => {
          res({
            data: rlt.data,
            id: rlt.data.id,
          });
        })
        .catch((error: any) => {
          rej(error);
        });
    });
  };

  delete = (resource: string, params: IDeleteParams): Promise<any> => {
    return new Promise((res: any, rej: any) => {
      axios
        .delete(`${REST_API}/${resource}/${params.id}`)
        .then((rlt: any) => {
          res({
            data: rlt.data,
          });
        })
        .catch((err: any) => {
          rej(err);
        });
    });
  };

  deleteMany = (resource: string, params: IDeleteManyParams) => {
    const query = {
      filter: JSON.stringify({ ...params }),
    };
    return axios
      .delete(`${REST_API}/${resource}?${stringify(query)}`, {
        method: 'DELETE',
      })
      .then(({ data }: any) => ({ data }));
  };
}
