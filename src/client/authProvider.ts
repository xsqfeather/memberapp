import axios from 'axios';
import Cookies from 'js-cookie';
import { APP_KEY, REST_API } from './constants/endpoints';

export class RestAuthProvider {
  constructor(token?: string) {
    if (token) {
      axios.defaults.headers.common = { Authorization: `bearer ${token}` };
    }
    axios.defaults.headers.common = { 'X-APP-KEY': APP_KEY };
  }

  public login = ({
    username,
    password,
    loginType,
    phone,
    sms,
  }: {
    username?: string;
    password?: string;
    loginType: string;
    phone?: string;
    sms?: string;
  }): Promise<any> => {
    return new Promise((res, rej) => {
      return axios
        .post('/api/sessions', {
          username,
          phone,
          sms,
          password,
          loginType,
        })
        .then((response) => {
          const { data } = response;
          if (data.access_token) {
            localStorage.setItem('token', data.access_token);
            Cookies.set('token', data.access_token);
            res({
              token: data.access_token,
            });
          } else {
            rej('api_error');
          }
        })
        .catch((err) => {
          rej(err);
        });
    });
  };
  public register = (registerType: string) => {
    return axios.post('/api/register', {
      registerType,
    });
  };

  logout = () => {
    localStorage.removeItem('token');
    Cookies.remove('token');
    axios
      .delete('/sessions')
      .then()
      .catch((error) => {
        console.log(error);
      });
    return Promise.resolve();
  };

  public checkError = (error: any) => {
    return Promise.resolve();
  };

  public checkAuth = () => {
    const token = localStorage.getItem('token');
    if (!token) {
      return Promise.reject();
    } else {
      return Promise.resolve();
    }
  };

  public getPermissions = () => {
    return Promise.resolve();
  };

  public getIdentity = async (): Promise<any> => {
    return Promise.resolve();
  };

  public getLoginSMS = async (phone: string): Promise<any> => {
    return axios.post(`${REST_API}/sms`, {
      phone,
    });
  };
}
