import { Backdrop, Typography } from '@material-ui/core';
import { createContext, Dispatch, ReactNode, useReducer } from 'react';
import MessageAlert from '../components/MessageAlert';
import {
  IGlobalState,
  IGlobalAction,
  IGlobalContainerProps,
  IGlobalMessagePayload,
} from '../types/global';
import wechatbackdrop from '../statics/imgs/wechabackdrop.png';
import { Router } from 'next/router';

const GLOBAL_SHOW_MESSAGE = 'GLOBAL_SHOW_MESSAGE';
const GLOBAL_CLOSE_MESSAGE = 'GLOBAL_CLOSE_MESSAGE';
const SHOW_WECHAT_BACKGROP = 'SHOW_WECHAT_BACKGROP';
const CLOSE_WECHAT_BACKDROP = 'CLOSE_WECHAT_BACKDROP';

function reducer(state: IGlobalState, action: IGlobalAction) {
  switch (action.type) {
    case GLOBAL_SHOW_MESSAGE:
      return {
        ...state,
        alertMessage: {
          ...action.payload,
          open: true,
        },
      };
    case GLOBAL_CLOSE_MESSAGE:
      return {
        ...state,
        alertMessage: {
          ...state.alertMessage,
          open: false,
        },
      };
    case SHOW_WECHAT_BACKGROP:
      return {
        ...state,
        isWechatWeb: true,
      };
    case CLOSE_WECHAT_BACKDROP:
      return {
        ...state,
        isWechatWeb: false,
      };
    default:
      return state;
  }
}

const globalInitState: IGlobalState = {
  alertMessage: {
    open: false,
    messageText: '',
    severity: 'info',
  },
  isWechatWeb: false,
  router: null,
};

export const GlobalContext = createContext({
  globalState: {} as IGlobalState,
  globalDispatch: (action: IGlobalAction) => {},
});

export const GlobalActions = {
  //弹出通知
  globalShowMessage: (payload: IGlobalMessagePayload) => {
    return {
      type: GLOBAL_SHOW_MESSAGE,
      payload,
    };
  },
  //关闭通知
  globalCloseMessage: () => {
    return {
      type: GLOBAL_CLOSE_MESSAGE,
    };
  },
  //弹出微信浏览器提示
  showWeChatBackdrop: () => {
    return {
      type: SHOW_WECHAT_BACKGROP,
    };
  },
  closeWechatBackGrop: () => {
    return {
      type: CLOSE_WECHAT_BACKDROP,
    };
  },
};

export function GlobalContainer({ children }: IGlobalContainerProps) {
  const [state, dispatch]: [IGlobalState, Dispatch<IGlobalAction>] = useReducer(
    reducer,
    globalInitState,
  );
  return (
    <GlobalContext.Provider
      value={{
        globalState: state,
        globalDispatch: dispatch,
      }}
    >
      <Backdrop
        style={{
          zIndex: 2000,
          color: '#fff',

          textAlign: 'center',
        }}
        open={state.isWechatWeb}
      >
        <img
          src={wechatbackdrop}
          style={{
            width: '100%',
          }}
        />
      </Backdrop>
      <MessageAlert
        open={state.alertMessage.open}
        message={state.alertMessage.messageText}
        severity={state.alertMessage.severity}
        handleClose={() => {
          dispatch(GlobalActions.globalCloseMessage());
        }}
      />
      {children}
    </GlobalContext.Provider>
  );
}
