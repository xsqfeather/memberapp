import { createMuiTheme } from '@material-ui/core/styles';
import _type from '@material-ui/lab/themeAugmentation';

export const lightTheme = createMuiTheme({
  palette: {
    primary: {
      main: '#C1328E',
    },
    secondary: {
      main: '#B481BB',
    },
    error: {
      main: '#B5495B',
    },
    background: {
      default: '#FFFFFB',
    },
  },
  overrides: {
    MuiAlert: {
      filledSuccess: {
        backgroundColor: '#66BAB7',
      },
      filledError: {
        backgroundColor: '#B5495B',
      },
    },
    MuiAppBar: {
      colorSecondary: {
        backgroundColor: '#B481BB',
      },
      colorPrimary: {
        backgroundColor: 'white',
      },
    },
    RaImageField: {
      image: {
        maxWidth: 200,
        maxHeight: 200,
      },
    },
  } as any,
});

export const darkTheme = createMuiTheme({
  palette: {
    primary: {
      main: '#C1328E',
    },
    secondary: {
      main: '#B481BB',
    },
    error: {
      main: '#B5495B',
    },
    background: {
      default: '#FFFFFB',
    },
  },
  overrides: {
    MuiAlert: {
      filledSuccess: {
        backgroundColor: '#66BAB7',
      },
      filledError: {
        backgroundColor: '#B5495B',
      },
    },
    MuiAppBar: {
      colorSecondary: {
        backgroundColor: '#B481BB',
      },
      colorPrimary: {
        backgroundColor: 'rgb(193 50 142 / 10%)',
      },
    },
  },
});
