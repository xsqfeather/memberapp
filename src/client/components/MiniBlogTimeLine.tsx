import React from 'react';

import moment from 'moment';
import 'moment/locale/zh-cn';
import BlogCard from '../components/BlogCard';

moment.locale('zh-cn');

export interface IPublisher {
  nickname: string;
  username: string;
  profile: {
    avatar: string;
  };
}

export interface IBlog {
  body: string;
  images: string[];
  publisherId: number;
  createdAt: Date;
  id: number;
  nickname: string;
  username: string;
  profile?: any;
  liked: boolean;
  likeCount: number;
  commentCount: number;
  publisher: IPublisher;
}

interface MiniBlogtimeLineProps {
  blogs: IBlog[];
}

export default function MiniBlogTimeLine({ blogs }: MiniBlogtimeLineProps) {
  return (
    <div
      style={{
        display: 'flex',
        flexDirection: 'column',
        alignContent: 'center',
        alignItems: 'center',
      }}
    >
      {blogs.map((b: IBlog, index: number) => (
        <BlogCard
          createdAt={b.createdAt}
          key={index}
          postId={b.id}
          images={b.images}
          like={b.likeCount}
          liked={b.liked}
          comment={b.commentCount}
          username={b.publisher?.username}
          publisherId={b.publisherId}
          nickname={b.publisher?.nickname || b.profile?.nickname}
          body={b.body}
          avatar={b.publisher?.profile?.avatar}
        />
      ))}
    </div>
  );
}
