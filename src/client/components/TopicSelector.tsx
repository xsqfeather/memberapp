import {
  Button,
  Card,
  CardActionArea,
  CardContent,
  CardMedia,
  createStyles,
  Grid,
  Icon,
  Paper,
  IconButton,
  makeStyles,
  SvgIcon,
  TextField,
  Theme,
  Typography,
} from '@material-ui/core';
import React, { ChangeEvent, useCallback, useState } from 'react';

const useStylesA = makeStyles((theme: Theme) =>
  createStyles({
    root: {
      '& > *': {
        margin: theme.spacing(1),
      },
    },
    input: {
      display: 'none',
    },
  }),
);
interface TopicSelectorProps {
  onClick: (ev: any) => void;
}

export function TopicSelectorButton(props: TopicSelectorProps) {
  const classes = useStylesA();
  const { onClick } = props;
  return (
    <div className={classes.root} style={{ width: 60, height: 60 }}>
      <IconButton
        color="primary"
        aria-label="select topics"
        onClick={(e) => {
          onClick(e);
        }}
      >
        <TopicButton />
      </IconButton>
    </div>
  );
}

function TopicButton(props: any) {
  return (
    <SvgIcon {...props} viewBox={'0 0 1024 1024'}>
      <path
        d="M230.826667 896 261.12 725.333333 90.453333 725.333333 105.386667 640 276.053333 640 321.28 384 150.613333 384 165.546667 298.666667 336.213333 298.666667 366.506667 128 451.84 128 421.546667 298.666667 677.546667 298.666667 707.84 128 793.173333 128 762.88 298.666667 933.546667 298.666667 918.613333 384 747.946667 384 702.72 640 873.386667 640 858.453333 725.333333 687.786667 725.333333 657.493333 896 572.16 896 602.453333 725.333333 346.453333 725.333333 316.16 896 230.826667 896M406.613333 384 361.386667 640 617.386667 640 662.613333 384 406.613333 384Z"
        p-id="3170"
      ></path>
    </SvgIcon>
  );
}

const useStyles = makeStyles((theme) => ({
  root: {
    '& .MuiCardContent-root:last-child': {
      padding: theme.spacing(1),
    },
  },
  findBox: {
    display: 'flex',
    padding: 0,
    '& > *': {
      margin: theme.spacing(1),
    },
    '& .MuiTextField-root': {
      flexGrow: 1,
    },
  },
  topicGrids: {
    padding: theme.spacing(1),
    height: 300,
    overflowY: 'scroll'
  },
  topicGrid: {
    width: '100%',
    
  },
  topicCard: {},
  topicCardArea: {
    display: 'flex',
    justifyContent: 'start',
  },
  topicCardImage: {
    flex: 'none',
    width: 50,
  },
  TagWrap: {
    display: 'flex',
  },
}));
export function TopicSelector() {
  const classes = useStyles();
  return (
    <Card className={classes.root} >
      <CardContent className={classes.findBox}>
        <TextField label="搜索话题" variant="outlined" size="small" />
        <Button variant="contained" color="primary" size="small">
          取消
        </Button>
      </CardContent>
      <CardContent className={classes.findBox}>
        <Typography
          variant="h5"
          style={{
            fontSize: 14,
          }}
        >
          热门话题
        </Typography>
      </CardContent>
      <Grid
        container
        spacing={1}
        className={classes.topicGrids}
        
      >
        <Grid item zeroMinWidth className={classes.topicGrid}>
          <Card className={classes.topicCard}>
            <CardActionArea className={classes.topicCardArea}>
              <CardMedia
                className={classes.topicCardImage}
                component="img"
                image="https://tse1-mm.cn.bing.net/th?id=OIP-C.M3wKMfbrabdslnBBF1PZ0wAAAA&w=145&h=160&c=8&rs=1&qlt=90&o=6&dpr=1.25&pid=3.1&rm=2"
              />
              <CardContent>
                <Typography
                  variant="h5"
                  style={{
                    fontSize: 14,
                  }}
                >
                  欧洲杯世界联赛123
                </Typography>
              </CardContent>
            </CardActionArea>
          </Card>
        </Grid>

        <Grid item zeroMinWidth className={classes.topicGrid}>
          <Card className={classes.topicCard}>
            <CardActionArea className={classes.topicCardArea}>
              <CardMedia
                className={classes.topicCardImage}
                component="img"
                image="https://tse1-mm.cn.bing.net/th?id=OIP-C.M3wKMfbrabdslnBBF1PZ0wAAAA&w=145&h=160&c=8&rs=1&qlt=90&o=6&dpr=1.25&pid=3.1&rm=2"
              />
              <CardContent>
                <Typography
                  variant="h5"
                  style={{
                    fontSize: 14,
                  }}
                >
                  欧洲杯世界联赛
                </Typography>
              </CardContent>
            </CardActionArea>
          </Card>
        </Grid>

        <Grid item zeroMinWidth className={classes.topicGrid}>
          <Card className={classes.topicCard}>
            <CardActionArea className={classes.topicCardArea}>
              <CardMedia
                className={classes.topicCardImage}
                component="img"
                image="https://tse1-mm.cn.bing.net/th?id=OIP-C.M3wKMfbrabdslnBBF1PZ0wAAAA&w=145&h=160&c=8&rs=1&qlt=90&o=6&dpr=1.25&pid=3.1&rm=2"
              />
              <CardContent>
                <Typography
                  variant="h5"
                  style={{
                    fontSize: 14,
                  }}
                >
                  欧洲杯世界联赛
                </Typography>
              </CardContent>
            </CardActionArea>
          </Card>
        </Grid>

        <Grid item zeroMinWidth className={classes.topicGrid}>
          <Card className={classes.topicCard}>
            <CardActionArea className={classes.topicCardArea}>
              <CardMedia
                className={classes.topicCardImage}
                component="img"
                image="https://tse1-mm.cn.bing.net/th?id=OIP-C.M3wKMfbrabdslnBBF1PZ0wAAAA&w=145&h=160&c=8&rs=1&qlt=90&o=6&dpr=1.25&pid=3.1&rm=2"
              />
              <CardContent>
                <Typography
                  variant="h5"
                  style={{
                    fontSize: 14,
                  }}
                >
                  欧洲杯世界联赛
                </Typography>
              </CardContent>
            </CardActionArea>
          </Card>
        </Grid>

        <Grid item zeroMinWidth className={classes.topicGrid}>
          <Card className={classes.topicCard}>
            <CardActionArea className={classes.topicCardArea}>
              <CardMedia
                className={classes.topicCardImage}
                component="img"
                image="https://tse1-mm.cn.bing.net/th?id=OIP-C.M3wKMfbrabdslnBBF1PZ0wAAAA&w=145&h=160&c=8&rs=1&qlt=90&o=6&dpr=1.25&pid=3.1&rm=2"
              />
              <CardContent>
                <Typography
                  variant="h5"
                  style={{
                    fontSize: 14,
                  }}
                >
                  欧洲杯世界联赛
                </Typography>
              </CardContent>
            </CardActionArea>
          </Card>
        </Grid>

        <Grid item zeroMinWidth className={classes.topicGrid}>
          <Card className={classes.topicCard}>
            <CardActionArea className={classes.topicCardArea}>
              <CardMedia
                className={classes.topicCardImage}
                component="img"
                image="https://tse1-mm.cn.bing.net/th?id=OIP-C.M3wKMfbrabdslnBBF1PZ0wAAAA&w=145&h=160&c=8&rs=1&qlt=90&o=6&dpr=1.25&pid=3.1&rm=2"
              />
              <CardContent>
                <Typography
                  variant="h5"
                  style={{
                    fontSize: 14,
                  }}
                >
                  欧洲杯世界联赛
                </Typography>
              </CardContent>
            </CardActionArea>
          </Card>
        </Grid>

        <Grid item zeroMinWidth className={classes.topicGrid}>
          <Card className={classes.topicCard}>
            <CardActionArea className={classes.topicCardArea}>
              <CardMedia
                className={classes.topicCardImage}
                component="img"
                image="https://tse1-mm.cn.bing.net/th?id=OIP-C.M3wKMfbrabdslnBBF1PZ0wAAAA&w=145&h=160&c=8&rs=1&qlt=90&o=6&dpr=1.25&pid=3.1&rm=2"
              />
              <CardContent>
                <Typography
                  variant="h5"
                  style={{
                    fontSize: 14,
                  }}
                >
                  欧洲杯世界联赛
                </Typography>
              </CardContent>
            </CardActionArea>
          </Card>
        </Grid>

        <Grid item zeroMinWidth className={classes.topicGrid}>
          <Card className={classes.topicCard}>
            <CardActionArea className={classes.topicCardArea}>
              <CardMedia
                className={classes.topicCardImage}
                component="img"
                image="https://tse1-mm.cn.bing.net/th?id=OIP-C.M3wKMfbrabdslnBBF1PZ0wAAAA&w=145&h=160&c=8&rs=1&qlt=90&o=6&dpr=1.25&pid=3.1&rm=2"
              />
              <CardContent>
                <Typography
                  variant="h5"
                  style={{
                    fontSize: 14,
                  }}
                >
                  欧洲杯世界联赛
                </Typography>
              </CardContent>
            </CardActionArea>
          </Card>
        </Grid>
      </Grid>
      <CardContent>
        <ContentTags />
      </CardContent>
    </Card>
  );
}

const useContentTagStyles = makeStyles((theme) => ({
  TagWrap: {
    paddingTop: theme.spacing(1),
    display: 'flex',
    position: 'relative',
    '& textarea': {
      flexGrow: 1,
      resize: 'none',
      fontSize: 16,
      outline: 'none',
    },
  },
  hint: {
    position: 'absolute',
    top: '30%',
    right: 3,
  },
}));
function ContentTags() {
  const classes = useContentTagStyles();
  const [tags, setTags] = useState([]);
  const [inputValue, setInputValue] = useState('');
  const [alertOpen, setAlertOpen] = useState(false);
  const handleChange = useCallback((ev: ChangeEvent<HTMLTextAreaElement>) => {
    let tagValue = ev.target.value;
    setInputValue(tagValue);
  }, []);

  const handleKeyUp = useCallback((ev: any) => {
    if (ev.code === 13) {
      if (tags.length > 15) {
        alert('话题最大长度为15个字符');
      } else {
      }
    }
  }, []);

  return (
    <div className={classes.TagWrap}>
      <Paper>

      </Paper>
      <textarea
        placeholder="回车创建话题"
        onChange={handleChange}
        onKeyUp={handleKeyUp}
        value={inputValue}
      ></textarea>
      <Typography className={classes.hint} color="textSecondary">
        还可以添加{10 - tags.length}个话题
      </Typography>
    </div>
  );
}
