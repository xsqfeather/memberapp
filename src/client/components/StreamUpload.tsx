import { useEffect, useState } from 'react';
import { io } from 'socket.io-client';
import * as ss from 'socket.io-stream';
import { LinearProgress, TextField, Typography } from '@material-ui/core';

import { makeStyles, createStyles, Theme } from '@material-ui/core/styles';
import Button from '@material-ui/core/Button';
import IconButton from '@material-ui/core/IconButton';
import { RemoveCircle } from '@material-ui/icons';

const useStyles = makeStyles((theme: Theme) =>
  createStyles({
    root: {
      '& > *': {
        margin: theme.spacing(1),
      },
    },
    input: {
      display: 'none',
    },
    label: {
      position: 'relative',
    },
  }),
);

const socket = io();

interface IVideo {
  uuid: string;
  label: string;
  stream: any;
  blobStream: any;
  fileSize: number;
  file: File;
}

interface IVideoUplaodProgress {
  video: IVideo;
  onDelete: (index: number) => void;
  index: number;
}

const VideoUploadProgress = (props: IVideoUplaodProgress) => {
  const { video, onDelete, index } = props;
  const { label, uuid, fileSize, file } = video;
  const [progress, setProgress] = useState(0.0);
  const [uploading, setUploading] = useState(false);
  const [stream, setStream] = useState(null as any);
  const [blobStream, setBlobStream] = useState(null as any);

  useEffect(() => {
    if (file && (!stream || !blobStream)) {
      const s = ss.createStream();
      const b = ss.createBlobReadStream(file);
      setStream(s);
      setBlobStream(b);
    }
  }, [stream, blobStream, file]);

  useEffect(() => {
    if (stream && blobStream) {
      let chunkNumber = 0;
      ss(socket).emit('upload-video-' + uuid, stream, { label });
      blobStream.on('data', function (chunk: any) {
        chunkNumber += chunk.length;
        setProgress(chunkNumber / fileSize);
        setUploading(true);
      });
      blobStream.on('end', function () {
        console.log('end');
        setUploading(false);
      });
      blobStream.pipe(stream);
      socket.on(`video-${uuid}-uploaded`, () => {
        setUploading(false);
      });
    }

    return () => {
      const controller = new AbortController();
      controller.abort();
      if (uploading && progress > 0) {
        socket.emit('stop-upload-video-' + uuid, { uuid });
      }
      delete video.blobStream;
      delete video.stream;
      setProgress(0);
    };
  }, [stream, blobStream]);

  return (
    <div
      style={{
        display: 'flex',
        alignItems: 'center',
        flexDirection: 'row',
        margin: 10,
        flexWrap: 'wrap',
        justifyContent: 'space-around',
      }}
    >
      <div>{label}</div>
      <div
        style={{
          width: '70%',
          height: 40,
          marginTop: 10,
        }}
      >
        <LinearProgress
          color="primary"
          variant="determinate"
          value={progress * 100}
          style={{
            height: 30,
            width: '100%',
          }}
        />
        <Typography
          style={{
            position: 'relative',
            top: -25,
            color: 'white',
            width: '100%',
            textAlign: 'center',
          }}
        >
          {uploading ? (progress * 100).toFixed(2) + '%' : '上传完成'}
        </Typography>
      </div>
      <div>
        <IconButton onClick={() => onDelete(index)} color={'primary'}>
          <RemoveCircle />
        </IconButton>
      </div>
    </div>
  );
};

export default function StreamUpload() {
  const [label, setLabel] = useState('');
  const [videos, setVideos] = useState([] as IVideo[]);
  const classes = useStyles();

  const handleChange = (e: any) => {
    if (!label || label === '') {
      return alert('视频标记必须填写');
    }
    socket.emit('createVideoUUID', {}, (data: any) => {
      const uuid = data;

      setLabel('');

      const file = e.target.files[0];
      const fileSize: number = file.size;

      const stream = ss.createStream();
      const blobStream = ss.createBlobReadStream(file);

      setVideos([
        ...videos,
        {
          uuid,
          label,
          stream,
          blobStream,
          fileSize,
          file,
        },
      ]);
      e.target.value = '';
    });
  };

  return (
    <div
      style={{
        margin: 10,
        display: 'flex',
        flexDirection: 'column',
      }}
    >
      <TextField
        value={label}
        onChange={(e: any) => {
          setLabel(e.target.value);
        }}
        label="视频标记"
        variant="outlined"
      />
      <br />
      <input
        accept="video/*"
        className={classes.input}
        id={`video-${label}-input`}
        type="file"
        onChange={handleChange}
      />
      <label htmlFor={`video-${label}-input`} className={classes.label}>
        <Button
          fullWidth
          variant="contained"
          color="secondary"
          component="span"
        >
          选择文件上传
        </Button>
      </label>
      {videos.length > 0 &&
        videos.map((video: IVideo, index: number) => {
          return (
            <VideoUploadProgress
              key={index}
              video={video}
              index={index}
              onDelete={(index: number) => {
                setVideos([
                  ...videos.slice(0, index),
                  ...videos.slice(index + 1, videos.length),
                ]);
              }}
            />
          );
        })}
    </div>
  );
}
