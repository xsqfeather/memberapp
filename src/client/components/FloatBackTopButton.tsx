import { FC, useCallback } from 'react';
import { Fab, makeStyles } from '@material-ui/core';
import { ExpandLess as ExpandLessIcon } from '@material-ui/icons';

const useStyles = makeStyles(() => ({
  backTop: {
    position: 'fixed',
    right: '5%',
    zIndex: 9998,
    bottom: 120,
  },
}));

export const FloatBackTopButton: FC = () => {
  const styles = useStyles();
  const backTop = useCallback(() => {
    window.scrollTo({
      // 不触发拉取最新数据
      top: 1,
      behavior: 'smooth',
    });
  }, []);
  return (
    <Fab className={styles.backTop} color="secondary" onClick={backTop}>
      <ExpandLessIcon />
    </Fab>
  );
};
