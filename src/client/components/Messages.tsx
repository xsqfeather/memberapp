import React from 'react';
import { createStyles, Theme, makeStyles } from '@material-ui/core/styles';
import List from '@material-ui/core/List';
import ListItem from '@material-ui/core/ListItem';
import Divider from '@material-ui/core/Divider';
import ListItemText from '@material-ui/core/ListItemText';
import ListItemAvatar from '@material-ui/core/ListItemAvatar';
import Avatar from '@material-ui/core/Avatar';
import Typography from '@material-ui/core/Typography';
import ListItemSecondaryAction from '@material-ui/core/ListItemSecondaryAction';
import { Badge } from '@material-ui/core';
import {
  setChatOpen,
  setChattingUserId,
  setCloseUrl,
} from '../observers/messages';
import moment from 'moment';
import RestdataProvider from '../dataProvider/rest';

moment.locale('zh-cn');

const useStyles = makeStyles((theme: Theme) =>
  createStyles({
    root: {
      width: '100%',
      maxWidth: 550,
      backgroundColor: theme.palette.background.paper,
    },
    inline: {
      display: 'inline',
    },
  }),
);

export default function MessagesList({ list }) {
  const classes = useStyles();
  const updateUnreads = async (id: number) => {
    const token = localStorage.getItem('token');
    const dataProvider = new RestdataProvider(token);
    await dataProvider.update('messages', {
      id: id,
      data: {
        unreadCount: 0,
      },
    });
  };
  return (
    <div
      style={{
        display: 'flex',
        alignItems: 'center',
        justifyContent: 'center',
      }}
    >
      <List className={classes.root}>
        {list.map((item: any, index: number) => {
          return (
            <React.Fragment key={index}>
              <ListItem
                button
                alignItems="flex-start"
                onClick={async () => {
                  await updateUnreads(item.id);
                  setChattingUserId(item.fromUserId);
                  setChatOpen(true);
                  setCloseUrl('/messages');
                }}
              >
                <ListItemAvatar>
                  <Avatar alt={item.title} src={item.avatar} />
                </ListItemAvatar>
                <ListItemText
                  primary={item.title}
                  secondary={
                    <React.Fragment>
                      <Typography
                        component="span"
                        variant="body2"
                        className={classes.inline}
                        color="textPrimary"
                      ></Typography>
                      {item.msgType === 'image' ? '图片信息' : item.content}{' '}
                    </React.Fragment>
                  }
                />

                <ListItemSecondaryAction>
                  <Badge badgeContent={item.unreadCount} color="primary">
                    {moment(item.updatedAt).fromNow()}&nbsp;&nbsp;&nbsp;
                  </Badge>
                </ListItemSecondaryAction>
              </ListItem>
              <Divider variant="inset" component="li" />
            </React.Fragment>
          );
        })}{' '}
      </List>
    </div>
  );
}
