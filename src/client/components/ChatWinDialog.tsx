import {
  Button,
  Dialog,
  DialogActions,
  DialogContent,
  DialogTitle,
  Paper,
  TextField,
} from '@material-ui/core';
import { bind } from '@react-rxjs/core';
import { createSignal } from '@react-rxjs/utils';
import { useEffect, useReducer, useState } from 'react';
import { Observable } from 'rxjs';
import { io } from 'socket.io-client';
const socket = io();

interface ChatWinDialogProps {
  open: boolean;
  senderId: number | null;
  receiverId: number | null;
  onClose: () => void;
  socket: any;
}

interface ChatMsg {
  senderId: number;
  receiverId: number;
  content: string;
  contentType: string;
  status: string;
}

const [chatMsgsChange$]: [Observable<ChatMsg[]>, (s: ChatMsg[]) => void] =
  createSignal<ChatMsg[]>();
bind<ChatMsg[]>(chatMsgsChange$, []);

const [newMsgChange$]: [Observable<ChatMsg>, (s: ChatMsg) => void] =
  createSignal<ChatMsg>();
bind<ChatMsg>(newMsgChange$, {
  content: '',
  contentType: 'text',
  senderId: 0,
  receiverId: 0,
  status: 'unread',
});

function reducer(
  state: ChatMsg[],
  action: {
    type: string;
    payload: any;
  },
) {
  switch (action.type) {
    case 'addNew':
      return state.concat([action.payload]);

    default:
      return state;
  }
}

const ChatWinDialog = (props: ChatWinDialogProps) => {
  const { open, receiverId, onClose, senderId } = props;

  const [text, setText] = useState('');
  const [list, dispatch] = useReducer(reducer, [] as ChatMsg[]);

  const handleLoadChat = () => {
    console.log({ receiverId, senderId });
    console.log('来自', senderId);
  };
  useEffect(() => {
    socket.on(`${senderId}_chat_channel`, (data: ChatMsg) => {
      if (!list.includes(data)) {
        return dispatch({
          type: 'addNew',
          payload: data,
        });
      }
    });
    return () => {
      socket.disconnect();
    };
  }, [socket]);

  return (
    <Dialog open={open} onEntered={handleLoadChat}>
      <DialogTitle>和xxx对话中</DialogTitle>
      <DialogContent>
        <div>
          <div
            style={{
              minHeight: 150,
              width: 300,
              maxHeight: 300,
              display: 'flex',
              flexDirection: 'column',
              justifyContent: 'space-around',
              overflowY: 'scroll',
            }}
          >
            {list.map((msg, index) => {
              return (
                <div
                  key={index}
                  style={{
                    width: '100%',
                    textAlign: msg.receiverId === receiverId ? 'right' : 'left',
                  }}
                >
                  <Paper
                    component="span"
                    style={{
                      padding: 3,
                    }}
                  >
                    {msg.content}
                  </Paper>
                </div>
              );
            })}
          </div>
          <div
            style={{
              position: 'relative',
              bottom: 0,
            }}
          >
            <TextField
              value={text}
              onChange={(e) => {
                setText(e.target.value);
              }}
              variant="outlined"
              InputProps={{
                endAdornment: (
                  <Button
                    onClick={() => {
                      setText('');
                      const data: any = {
                        content: text,
                        contentType: 'text',
                        senderId,
                        receiverId,
                        status: 'unread',
                      };
                      dispatch({ type: 'addNew', payload: data });
                      socket.emit(`createChatMsg`, data);
                    }}
                    variant="contained"
                    color="primary"
                  >
                    发送
                  </Button>
                ),
              }}
            />
          </div>
        </div>
      </DialogContent>
      <DialogActions>
        <Button onClick={onClose}>关闭</Button>
      </DialogActions>
    </Dialog>
  );
};

export default ChatWinDialog;
