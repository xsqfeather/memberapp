import React from 'react';
import { makeStyles, createStyles, Theme } from '@material-ui/core/styles';
import IconButton from '@material-ui/core/IconButton';
import PhotoCamera from '@material-ui/icons/PhotoCamera';
import axios from 'axios';
import { REST_API } from '../constants/endpoints';

const useStyles = makeStyles((theme: Theme) =>
  createStyles({
    root: {
      '& > *': {
        margin: theme.spacing(1),
      },
    },
    input: {
      display: 'none',
    },
  }),
);

export default function ImageSender(props: any) {
  const classes = useStyles();
  const handleChange = async (e: any) => {
    const files: any = e.target.files;
    const formData = new FormData();
    formData.append('file', files[0]);
    const rlt = await axios.post(`${REST_API}/distributed-images`, formData);
    props.getImages(rlt.data);
  };
  return (
    <div className={classes.root}>
      <input
        accept="image/*"
        onChange={handleChange}
        className={classes.input}
        id="icon-button-file"
        type="file"
      />
      <label htmlFor="icon-button-file">
        <IconButton
          color="primary"
          aria-label="upload picture"
          component="span"
        >
          <PhotoCamera />
        </IconButton>
      </label>
    </div>
  );
}
