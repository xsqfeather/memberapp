import React from 'react';
import Box from '@material-ui/core/Box';
import { Typography } from '@material-ui/core';
import { NavMenu, NavItem } from '@mui-treasury/components/menu/navigation';
import { usePointNavigationMenuStyles } from '@mui-treasury/styles/navigationMenu/point';
import { useRouter } from 'next/router';
import Link from 'next/link';

export const PcNavMenu = React.memo(function PointNavigationMenu() {
  const router = useRouter();
  return (
    <Box height={56} display={'flex'}>
      <NavMenu useStyles={usePointNavigationMenuStyles}>
        <NavItem as={'div'} active={router.pathname === '/'}>
          <Link href="/" passHref>
            <Typography>首页</Typography>
          </Link>
        </NavItem>

        <NavItem as={'div'} active={router.pathname === '/world'}>
          <Link href="/world" passHref>
            <Typography>世界</Typography>
          </Link>
        </NavItem>
        <NavItem as={'div'} active={router.pathname === '/courses'}>
          <Link href="/team" passHref>
            <Typography>组队</Typography>
          </Link>
        </NavItem>
      </NavMenu>
    </Box>
  );
});

export default PcNavMenu;
