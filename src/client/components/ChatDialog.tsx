import React, { useEffect, useReducer, useRef, useState } from 'react';
import Button from '@material-ui/core/Button';
import Dialog from '@material-ui/core/Dialog';
import DialogActions from '@material-ui/core/DialogActions';
import DialogContent from '@material-ui/core/DialogContent';
import useMediaQuery from '@material-ui/core/useMediaQuery';
import TextField from '@material-ui/core/TextField';

import {
  createStyles,
  Theme,
  useTheme,
  WithStyles,
  withStyles,
} from '@material-ui/core/styles';
import {
  setChatOpen,
  useChatOpen,
  useChattingUserId,
  useCloseUrl,
} from '../observers/messages';
import { useRouter } from 'next/router';
import { ChevronLeft } from '@material-ui/icons';
import MuiDialogTitle from '@material-ui/core/DialogTitle';
import IconButton from '@material-ui/core/IconButton';
import CloseIcon from '@material-ui/icons/Close';
import Typography from '@material-ui/core/Typography';
import { Avatar, CircularProgress, Paper } from '@material-ui/core';
import EmojiPicker from './EmojiPicker';
import RestdataProvider from '../dataProvider/rest';

import { io } from 'socket.io-client';
import ImageSender from './ImageSender';



const socket = io({
  auth: {
    token: "abcd"
  }
}); 


const styles = (theme: Theme) =>
  createStyles({
    root: {
      margin: 0,
      padding: 2,
      display: 'flex',
      flexDirection: 'row',
      justifyContent: 'space-between',
      alignItems: 'center',
    },
    closeButton: {
      position: 'absolute',
      right: theme.spacing(1),
      top: theme.spacing(1),
      color: theme.palette.grey[500],
    },
  });
export interface DialogTitleProps extends WithStyles<typeof styles> {
  id: string;
  children: React.ReactNode;
  onClose: () => void;
}

const DialogTitle = withStyles(styles)((props: DialogTitleProps) => {
  const theme = useTheme();
  const fullScreen = useMediaQuery(theme.breakpoints.down('sm'));
  const { children, classes, onClose, ...other } = props;
  return (
    <MuiDialogTitle disableTypography className={classes.root} {...other}>
      <div>
        {fullScreen && onClose ? (
          <IconButton aria-label="close" onClick={onClose}>
            <ChevronLeft />
          </IconButton>
        ) : null}
      </div>

      <Typography
        variant="subtitle2"
        style={{
          textAlign: 'center',
        }}
      >
        {children}
      </Typography>
      <div>
        {onClose && !fullScreen ? (
          <IconButton aria-label="close" onClick={onClose}>
            <CloseIcon />
          </IconButton>
        ) : null}

        {onClose && fullScreen ? (
          // TODO: 移动端第三个
          <div
            style={{
              width: 40,
            }}
          >
            &nbsp;
          </div>
        ) : null}
      </div>
    </MuiDialogTitle>
  );
});

interface ChatMsg {
  id: number;
  senderId: number;
  receiverId: number;
  content: string;
  contentType: string;
  isSending: boolean;
  messageId: number;
}

function chatMsgReducer(
  state: ChatMsg[],
  action: {
    type: string;
    payload?: any;
  },
) {
  switch (action.type) {
    case 'ADD_MSG':
      return [
        ...state,
        {
          ...action.payload,
        },
      ];

    case 'CLEAR_MSGS':
      return [];

    case 'INIT_MSGS':
      return action.payload;

    default:
      return state;
  }
}

export default function ChatDialog({ user }) {
  const chatOpen = useChatOpen();
  const closeUrl = useCloseUrl();
  const theme = useTheme();
  const fullScreen = useMediaQuery(theme.breakpoints.down('sm'));
  const router = useRouter();
  const scrollEl = useRef(null);
  const [text, setText] = useState('');
  const chattingUserId = useChattingUserId();
  const [chatMsgs, disaptch] = useReducer(chatMsgReducer, [] as ChatMsg[]);
  const [sendingIndexes, setSendingIndexes] = useState([] as number[]);
  const [chattingUser, setChattingUser] = useState({} as any);
  const [initing, setIniting] = useState(true);
  useEffect(() => {
    return () => {
      socket.off(`NEW_CHAT_MSGS_COMING_${user.id}`);
    };
  }, []);

  const handleDiaLogOpen = () => {
    const state = {
      title: 'title',
      url: '#openChat',
    };
    history.pushState(state, 'title', '#openChat');
    window.addEventListener('popstate', handleClose, false);
  };

  const handleClose = () => {
    socket.off(`NEW_CHAT_MSGS_COMING_${user.id}`);
    setChatOpen(false);
    window.removeEventListener('popstate', handleClose, false);
    router.push(closeUrl);
  };
  const toBottom = () => {
    if (scrollEl.current) {
      scrollEl.current.scrollTop =
        scrollEl.current.scrollHeight - scrollEl.current.clientHeight;
    }
  };

  const updateUnreads = async (id: number) => {
    const token = localStorage.getItem('token');
    const dataProvider = new RestdataProvider(token);
    await dataProvider.update('messages', {
      id: id,
      data: {
        unreadCount: 0,
      },
    });
  };

  const handleEntering = async () => {
    setIniting(true);
    //载入用户信息
    disaptch({
      type: 'CLEAR_MSGS',
    });
    const token = localStorage.getItem('token');
    const dataProvider = new RestdataProvider(token);

    const chatUserRlt: any = await dataProvider.getOne('users', {
      id: chattingUserId,
    });
    setChattingUser(chatUserRlt.data);

    const chatInitData: any = await dataProvider.getList('chat-msgs', {
      pagination: {
        page: 1,
        perPage: 20,
      },
      sort: {
        field: 'createdAt',
        order: 'DESC',
      },
      filter: {
        chattingUserId,
        userId: user.id,
      },
    });

    disaptch({
      type: 'INIT_MSGS',
      payload: chatInitData.data.reverse(),
    });

    setTimeout(() => {
      toBottom();
    }, 500);

    setIniting(false);
    socket.on(`NEW_CHAT_MSGS_COMING_${user.id}`, async (data) => {
      if (user.id !== chattingUserId) {
        disaptch({
          type: 'ADD_MSG',
          payload: {
            ...data,
          },
        });
      }
      toBottom();
      if (data.messageId) {
        //更新已读
        updateUnreads(data.messageId);
      }
    });
  };
  const sendMsg = async (contentType = 'text', content = '') => {
    const token = localStorage.getItem('token');
    const dataProvider = new RestdataProvider(token);
    await dataProvider.create('chat-msgs', {
      data: {
        senderId: user.id,
        receiverId: chattingUserId,
        content,
        contentType,
      },
    });
  };

  const addMsg = async (contentType = 'text', content = '') => {
    const newIndex = chatMsgs.length;
    setSendingIndexes([...sendingIndexes, newIndex]);
    disaptch({
      type: 'ADD_MSG',
      payload: {
        senderId: user.id,
        receiverId: chattingUserId,
        content,
        contentType,
      },
    });
    setText('');
    setTimeout(() => {
      toBottom();
    }, 10);
    await sendMsg(contentType, content);
    const newSendingIndexs = sendingIndexes.filter(
      (index) => index !== newIndex,
    );
    setSendingIndexes([...newSendingIndexs]);
  };

  const handleSubmit = async (e: any) => {
    e.preventDefault();
    if (text === '') {
      return;
    }
    addMsg('text', text);
  };

  return (
    <div>
      <Dialog
        onEntered={() => {
          toBottom();
        }}
        onEntering={handleEntering}
        fullScreen={fullScreen}
        open={chatOpen}
        fullWidth={true}
        maxWidth="md"
        scroll={'paper'}
        onEnter={handleDiaLogOpen}
        aria-labelledby="responsive-dialog-title"
      >
        <DialogTitle id="customized-dialog-title" onClose={handleClose}>
          {chattingUser.profile?.nickname}
        </DialogTitle>
        <DialogContent
          style={{
            minHeight: 400,
          }}
          dividers={true}
          ref={scrollEl}
          onScroll={(e: any) => {
            if (
              e.target.scrollTop + e.target.clientHeight ===
              e.target.scrollHeight
            ) {
              console.log('到底部');
            }
            if (e.target.scrollTop === 0) {
              console.log('到ding部');
            }
          }}
        >
          {!initing ? (
            chatMsgs.map((msg: any, index: number) => {
              return (
                <div
                  key={index}
                  style={{
                    display: 'flex',
                    flexDirection:
                      msg.senderId === user.id ? 'row-reverse' : 'row',
                    alignItems: 'start',
                    minWidth: 300,
                  }}
                >
                  <div style={{ marginRight: 2, marginLeft: 2 }}>
                    <Avatar
                      alt="Remy Sharp"
                      src={
                        msg.senderId === user.id
                          ? user.profile?.avatar
                          : '/static/images/avatar/1.jpg'
                      }
                    />
                  </div>
                  <Paper
                    style={{
                      padding: msg.contentType === 'image' ? 0 : 10,
                      wordBreak: 'break-all',
                      maxWidth: '70%',
                      marginBottom: 20,
                      minHeight: 35,
                      backgroundColor:
                        msg.senderId !== user.id
                          ? '#FFFFFB'
                          : msg.contentType === 'image'
                          ? 'none'
                          : '#B1B479',
                    }}
                  >
                    {msg.contentType === 'image' && (
                      <img
                        style={{
                          maxHeight: 400,
                          width: 'auto',
                          maxWidth: 200,
                        }}
                        src={msg.content}
                      />
                    )}
                    {msg.contentType === 'text' && <span>{msg.content}</span>}
                  </Paper>
                  <div
                    style={{
                      padding: 10,
                    }}
                  >
                    {sendingIndexes.includes(index) && (
                      <CircularProgress
                        style={{
                          height: 20,
                          width: 20,
                        }}
                      />
                    )}
                  </div>
                </div>
              );
            })
          ) : (
            <div>
              <Typography>对话载入中</Typography>
            </div>
          )}
          <br />
          <br />
          <br />
        </DialogContent>
        <DialogActions
          style={{
            position: !fullScreen ? 'inherit' : 'fixed',
            bottom: !fullScreen ? 'auto' : 0,
            left: !fullScreen ? 'auto' : 0,
            width: !fullScreen ? 'auto' : '100%',
          }}
        >
          <div
            style={{
              display: 'flex',
              alignItems: 'center',
              width: '100%',
              justifyContent: 'center',
            }}
          >
            <form
              onSubmit={handleSubmit}
              style={{
                width: '100%',
              }}
            >
              <TextField
                value={text}
                onChange={(e: any) => {
                  setText(e.target.value);
                }}
                placeholder="发送消息"
                fullWidth
                variant="outlined"
                InputProps={{
                  endAdornment: (
                    <>
                      <EmojiPicker
                        onPick={(emoji: string) => setText(text + emoji)}
                      />
                      {text == '' ? (
                        <ImageSender
                          getImages={async (imgs: string[]) => {
                            for (let index = 0; index < imgs.length; index++) {
                              const img: any = imgs[index];
                              await addMsg('image', img.url);
                              setTimeout(() => {
                                toBottom();
                              }, 500);
                            }
                            setTimeout(() => {
                              toBottom();
                            }, 500);
                          }}
                        />
                      ) : (
                        <Button
                          type="submit"
                          onClick={() => {
                            scrollEl.current.scrollTop =
                              scrollEl.current.scrollHeight -
                              scrollEl.current.clientHeight;
                          }}
                          variant="contained"
                          color="primary"
                        >
                          发送
                        </Button>
                      )}
                    </>
                  ),
                }}
              />
            </form>
          </div>
        </DialogActions>
      </Dialog>
    </div>
  );
}
