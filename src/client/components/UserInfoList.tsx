import React, { useEffect, useState } from 'react';
import Divider from '@material-ui/core/Divider';
import { makeStyles } from '@material-ui/core/styles';
import { Column } from '@mui-treasury/components/flex';
import RestdataProvider from '../dataProvider/rest';
import { createOneSubject, deleteManySubject } from '../observers/restful';
import UserCard from './UserCard';

const useStyles = makeStyles(() => ({
  card: {
    width: '100%',
    borderRadius: 16,
    boxShadow: '0 8px 16px 0 #BDC9D7',
    overflow: 'hidden',
  },
  header: {
    fontFamily: 'Barlow, san-serif',
    backgroundColor: '#fff',
  },
  headline: {
    color: '#122740',
    fontSize: '1.25rem',
    fontWeight: 600,
  },
  link: {
    color: '#2281bb',
    padding: '0 0.25rem',
    fontSize: '0.875rem',
  },
  actions: {
    color: '#BDC9D7',
  },
  divider: {
    backgroundColor: '#d9e2ee',
    margin: '0 20px',
  },
}));

export const UserInfoList = React.memo(function SocialCard(props: any) {
  const { list, userId } = props;

  const [followerIds, setFollowerIds] = useState([] as number[]);

  const getFollowed = async (userId: number) => {
    const token = localStorage.getItem('token');
    const dataProvider = new RestdataProvider(token);
    const { data }: any = await dataProvider.getList('follows/followings', {
      pagination: {
        page: 1,
        perPage: 20,
      },
      sort: {
        field: 'createdAt',
        order: 'DESC',
      },
      filter: {
        fansId: userId,
        userIds: list.map((u: any) => u.id),
      },
    });
    setFollowerIds([...data.map((item: any) => item.id)]);
  };

  useEffect(() => {
    if (userId) {
      getFollowed(userId);
    }
  }, [userId, list]);

  const styles = useStyles();

  return (
    <>
      <Column p={0} gap={0} className={styles.card}>
        {list.map((item: any, index: number) => {
          const { id, nickname, avatar, username, profile } = item;
          return (
            <React.Fragment key={index}>
              <UserCard
                id={id}
                followUser={(id: number) => {
                  createOneSubject('follows/followings', {
                    userId: id,
                    fansId: userId,
                  }).subscribe({
                    next: () => {
                      getFollowed(userId);
                    },
                  });
                }}
                unFollowUser={(id: number) => {
                  deleteManySubject('follows/unfollow', {
                    userId: id,
                    fansId: userId,
                  }).subscribe({
                    next: () => {
                      getFollowed(userId);
                    },
                  });
                }}
                username={username}
                name={nickname}
                iFollowing={followerIds.includes(item.id)}
                src={avatar || profile?.avatar}
              />
              <Divider variant={'middle'} className={styles.divider} />
            </React.Fragment>
          );
        })}
      </Column>
    </>
  );
});

export default UserInfoList;
