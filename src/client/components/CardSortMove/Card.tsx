import { Card, IconButton, makeStyles, Typography } from '@material-ui/core';
import {
  Edit as EditIcon,
  Delete as DeleteIcon,
  Visibility as VisibilityIcon,
  Assignment as AssignmentIcon,
} from '@material-ui/icons';
import { CSSProperties, FC, memo } from 'react';
import { useDrag, useDrop } from 'react-dnd';
import { ItemTypes } from './ItemTypes';

const style: CSSProperties = {
  border: '1px dashed gray',
  padding: '0.5rem 1rem',
  marginBottom: '.5rem',
  backgroundColor: 'white',
  cursor: 'move',
  width: '100%',
  display: 'flex',
  alignItems: 'center',
  borderRadius: 0,
};

export interface CardItemProps {
  id: string;
  item: any;
  index: number;
  moveCard: (id: string, to: number) => void;
  findCard: (id: string) => { index: number };
  onPreview(videoId: string): void;
  onEdit(id: string): void;
  onDelete(id: string): void;
  onManageExam(id: string): void;
}

interface Item {
  id: string;
  originalIndex: number;
}

const useStyles = makeStyles({
  title: {
    width: '120px',
  },
  lessonNodeTitle: {
    flex: 1,
    display: 'flex',
    alignItems: 'center',
  },
});

export const CardItem: FC<CardItemProps> = memo(function CardItem({
  id,
  item,
  moveCard,
  findCard,
  index,
  onDelete,
  onPreview,
  onEdit,
  onManageExam,
}) {
  const originalIndex = findCard(id).index;
  const styles = useStyles();

  const [{ isDragging }, drag] = useDrag(
    () => ({
      type: ItemTypes.CARD,
      item: { id, originalIndex },
      collect: (monitor) => ({
        isDragging: monitor.isDragging(),
      }),
      end: (item, monitor) => {
        const { id: droppedId, originalIndex } = item;
        const didDrop = monitor.didDrop();
        if (!didDrop) {
          moveCard(droppedId, originalIndex);
        }
      },
    }),
    [id, originalIndex, moveCard],
  );

  const [, drop] = useDrop(
    () => ({
      accept: ItemTypes.CARD,
      canDrop: () => false,
      hover({ id: draggedId }: Item) {
        if (draggedId !== id) {
          const { index: overIndex } = findCard(id);
          moveCard(draggedId, overIndex);
        }
      },
    }),
    [findCard, moveCard],
  );

  const opacity = isDragging ? 0 : 1;
  return (
    <Card
      component="div"
      ref={(node: any) => drag(drop(node))}
      style={{ ...style, opacity }}
    >
      <div className={styles.title}>
        <Typography>第{index + 1}课</Typography>
      </div>
      <div className={styles.lessonNodeTitle}>
        <img alt="" src={item?.cover?.originUrl} />
        <div>{item.title}</div>
        {/*<div>{item.body}</div>*/}
      </div>
      <div>
        <IconButton
          title="课节详细"
          color="primary"
          onClick={() => onPreview(item.videoId)}
        >
          <VisibilityIcon />
        </IconButton>
        <IconButton
          title="课节测验"
          color="primary"
          onClick={() => onManageExam(item.id)}
        >
          <AssignmentIcon />
        </IconButton>
        <IconButton
          title="编辑课节"
          color="primary"
          onClick={() => onEdit(item.id)}
        >
          <EditIcon />
        </IconButton>
        <IconButton
          title="删除课节"
          color="primary"
          onClick={() => onDelete(item.id)}
        >
          <DeleteIcon />
        </IconButton>
      </div>
    </Card>
  );
});
