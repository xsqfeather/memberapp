import { memo, useCallback, FC } from 'react';
import { useDrop } from 'react-dnd';
import { CardItem } from './Card';
import update from 'immutability-helper';
import { ItemTypes } from './ItemTypes';
import { createStyles, Theme, makeStyles } from '@material-ui/core';

const useStyles = makeStyles((theme: Theme) =>
  createStyles({
    root: {
      width: '100%',
      margin: 0,
      padding: theme.spacing(2),
    },
    closeButton: {
      position: 'absolute',
      right: theme.spacing(1),
      top: theme.spacing(1),
      color: theme.palette.grey[500],
    },
  }),
);

const CardSortMove: FC<{
  items: Array<any>;
  onChange?(targetId: string, value: Array<any>): void;
  onRemove?(id: string): void;
  onUpdate?(id: string): void;
  onReviewVideo?(videoId: string): void;
  onManageExam?(id: string): void;
}> = ({
  items: cards,
  onChange,
  onRemove,
  onReviewVideo,
  onUpdate,
  onManageExam,
}) => {
  const styles = useStyles();
  const findCard = useCallback(
    (id: string) => {
      const card = cards.filter((c) => c.id === id)[0];
      return {
        card,
        index: cards.indexOf(card),
      };
    },
    [cards],
  );

  const moveCard = useCallback(
    (id: string, atIndex: number) => {
      const { card, index } = findCard(id);
      onChange(
        id,
        update(cards, {
          $splice: [
            [index, 1],
            [atIndex, 0, card],
          ],
        }).map((it, i) => ({ ...it, nodeIndex: i })),
      );
    },
    [findCard, cards, onChange],
  );

  const [, drop] = useDrop(() => ({ accept: ItemTypes.CARD }));
  return (
    <div ref={drop} className={styles.root}>
      {cards &&
        cards.map((card: any, index: number) => (
          <CardItem
            key={card.id}
            id={card.id}
            index={index}
            item={card}
            moveCard={moveCard}
            findCard={findCard}
            onPreview={onReviewVideo}
            onEdit={onUpdate}
            onDelete={onRemove}
            onManageExam={onManageExam}
          />
        ))}
    </div>
  );
};
export default memo(CardSortMove);
