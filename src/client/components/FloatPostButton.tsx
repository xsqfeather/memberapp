import {
  Fab,
  Dialog,
  TextField,
  DialogActions,
  DialogContent,
  Button,
  DialogTitle,
  createStyles,
  makeStyles,
  Theme,
  List,
  ListItem,
  ListItemIcon,
  ListItemText,
  Grid,
  Card,
  CardContent,
  CardMedia,
  Typography,
} from '@material-ui/core';
import { Edit } from '@material-ui/icons';
import axios from 'axios';
import { useRouter } from 'next/router';
import React, { useCallback, useEffect, useState } from 'react';
import RestdataProvider from '../dataProvider/rest';
import MiniPostImageUploader from './MiniPostImageUploader';
import { TopicSelectorButton, TopicSelector } from './TopicSelector';

interface dialogProps {
  width: number;
}

const FloatPostButton = (props: any) => {
  const [postOpen, setPostOpen] = useState(false);
  const [content, setContent] = useState('');
  const [imageUrls, setImageUrls] = useState([] as string[]);
  const router = useRouter();
  const [topicShow, setTopicShow] = useState(false);
  const handleSubmit = async (e: any) => {
    e.preventDefault();
    const token = localStorage.getItem('token');
    const dataProvider = new RestdataProvider(token);
    try {
      const rlt = await dataProvider.create('mini-blog', {
        data: {
          body: content,
          images: imageUrls,
          publisherId: props.userId,
        },
      });
      if ((rlt.data.publisherId = props.userId)) {
        setPostOpen(false);
        setImageUrls([]);
        setContent('');
        document.body.scrollTop = document.documentElement.scrollTop = 10;
        document.body.scrollTop = document.documentElement.scrollTop = 0;
      }
    } catch (error) {
      router.push('/login');
    }
  };

  const handleGetUrl = (url: string) => {
    setImageUrls([...imageUrls, url]);
  };

  const handleClose = () => {
    setTopicShow(false);
    setPostOpen(false);
  };

  return (
    <div>
      <Dialog
        onEnter={() => {
          if (!props.userId) {
            router.push('/login');
          }
          setImageUrls([]);
          setContent('');
        }}
        maxWidth="md"
        open={postOpen}
        onClose={handleClose}
      >
        <DialogTitle>发布你的动态</DialogTitle>
        <form onSubmit={handleSubmit}>
          <DialogContent>
            {topicShow ? (
              <TopicSelector />
            ) : (
              <div>
                <TextField
                  onChange={(e: any) => {
                    setContent(e.target.value);
                  }}
                  multiline
                  rows={5}
                  label="发布内容"
                  fullWidth
                  variant="outlined"
                  placeholder="说说你动态吧"
                />
                <br />
                <br />
                <div
                  style={{
                    display: 'flex',
                    flexDirection: 'row',
                  }}
                >
                  <MiniPostImageUploader getUrl={handleGetUrl} />
                  <TopicSelectorButton
                    onClick={() => {
                      setTopicShow(!topicShow);
                    }}
                  />
                </div>
                {imageUrls &&
                  imageUrls.length > 0 &&
                  imageUrls.map((url: string, index: number) => {
                    return (
                      <img
                        key={index}
                        style={{
                          width: 80,
                        }}
                        src={url}
                      />
                    );
                  })}
              </div>
            )}
          </DialogContent>
          <DialogActions>
            <Button type="button" onClick={handleClose}>
              取消
            </Button>
            <Button type="submit" variant="contained" color="secondary">
              发表
            </Button>
          </DialogActions>
        </form>
      </Dialog>
      <Fab
        onClick={() => {
          setPostOpen(true);
        }}
        style={{
          position: 'fixed',
          right: '5%',
          zIndex: 9999,
          bottom: 50,
        }}
        color="secondary"
        aria-label="edit"
      >
        <Edit />
      </Fab>
    </div>
  );
};

interface Topic {
  id: number;
  title: string;
  des: string;
  cover: string;
}
async function getTopicList(page: number, filter = {}): Promise<any> {
  const token = localStorage.getItem('token');
  const dataProvider = new RestdataProvider(token);
  const list = await dataProvider.getList('topics', {
    pagination: {
      page,
      perPage: 20,
    },
    sort: { field: 'createdAt', order: 'DESC' },
    filter,
  });
  return list;
}

function TopicList() {
  const [page, setPage] = useState(1);
  //用于判断是否已经接受完毕
  const [isOver, setOver] = useState(false);
  const [topicList, setTopicList] = useState<Topic[]>([]);
  useEffect(() => {
    getTopicList(page).then((value) => {
      setTopicList(value.data);
      if (value.total < 10) {
        setOver(true);
      }
      console.log(value);
    });
  }, []);
  return (
    <Grid container spacing={2} wrap={'nowrap'}>
      {topicList.map((topic) => {
        return (
          <Grid item key={topic.id}>
            <Card>
              <CardMedia
                style={{ width: 140, height: 100 }}
                image={topic.cover}
              />
              <CardContent style={{ height: 80 }}>
                <Typography
                  style={{
                    fontSize: 16,
                  }}
                >
                  {topic.title}
                </Typography>
                <Typography
                  style={{
                    fontSize: 14,
                  }}
                  color="textSecondary"
                >
                  {topic.des}
                </Typography>
              </CardContent>
            </Card>
          </Grid>
        );
      })}
    </Grid>
  );
}

export default FloatPostButton;
