import {
  Button,
  TableBody,
  TableCell,
  TableContainer,
  TableHead,
  TableRow,
} from '@material-ui/core';
import { createContext, ReactNode } from 'react';
import { IColumnType } from '../../types/global';

interface IDatagridProps<T> {
  children?: ReactNode;
  columns: IColumnType[];
  selectable?: boolean;
  rows: T[];
}

export const DatagridContext = createContext({});

export default function Datagrid<T>({
  children,
  columns,
  rows,
}: IDatagridProps<T>) {
  return (
    <TableContainer>
      <TableHead>
        {columns.map((column: IColumnType, index: number) => {
          return (
            <TableCell key={index} style={{ textAlign: 'center' }}>
              {column.sortable ? (
                <Button fullWidth>{column.label}</Button>
              ) : (
                column.label
              )}
            </TableCell>
          );
        })}
      </TableHead>
      <TableBody>
        {rows.map((row, index) => (
          <DatagridContext.Provider key={index} value={row}>
            <TableRow>{children}</TableRow>
          </DatagridContext.Provider>
        ))}
      </TableBody>
    </TableContainer>
  );
}
