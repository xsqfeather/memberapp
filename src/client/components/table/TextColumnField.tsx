import { TableCell } from '@material-ui/core';
import { useContext } from 'react';

import { DatagridContext } from './Datagrid';

interface ITextColumnFieldProps {
  source: string;
}

export default function TextColumnField({ source }: ITextColumnFieldProps) {
  const row = useContext(DatagridContext);
  return (
    <TableCell>
      <span>{row[source]}</span>
    </TableCell>
  );
}
