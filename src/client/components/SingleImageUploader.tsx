import React, { useEffect } from 'react';
import {
  IconButton,
  LinearProgress,
  Paper,
  Typography,
} from '@material-ui/core';
import { makeStyles, createStyles } from '@material-ui/core/styles';
import {
  Add,
  RemoveCircleOutline,
  VisibilityOutlined,
} from '@material-ui/icons';
import { v4 as uuidv4 } from 'uuid';
import { bind } from '@react-rxjs/core';
import { createSignal } from '@react-rxjs/utils';
import { createOneSubject } from '../observers/restful';
import { REST_API } from '../constants/endpoints';

const useStyles = makeStyles(() =>
  createStyles({
    root: {
      marigin: 20,
      display: 'flex',
      flexDirection: 'column',
      height: 100,
      width: 100,
      textAlign: 'center',
      justifyContent: 'center',
      alignContent: 'center',
      padding: 10,
    },
    input: {
      display: 'none',
    },
  }),
);

export interface SingleImageUploaderProps {
  /**
   * (可选)返回url
   */
  getUrl?: (url: string) => void;

  url: string;
}

const [URLChange$, setURL] = createSignal<string>();
const [useURL] = bind<string>(URLChange$, '');

const [uploadingChange$, setUploading] = createSignal<boolean>();
const [useUploading] = bind<boolean>(uploadingChange$, false);

const [uploadedChange$, setUploaded] = createSignal<boolean>();
const [useUploaded] = bind<boolean>(uploadedChange$, false);

export const SingleImageUploader: React.FC<SingleImageUploaderProps> = ({
  getUrl,
  url,
}: any) => {
  const classes = useStyles();
  const URL = useURL();
  const uploading = useUploading();
  const uploaded = useUploaded();
  const id = uuidv4();
  useEffect(() => {
    if (url && url !== '') {
      setURL(url);
    }
  }, []);

  const handleChange = async (event: React.ChangeEvent<HTMLInputElement>) => {
    setUploading(true);
    const files = event.target.files || [];
    const file = files[0];
    const formData = new FormData();
    formData.append('file', file);
    const subUploadFile = createOneSubject('images', formData);

    subUploadFile.subscribe({
      next: (v) => {
        if ((v.response as any)?.ipfsCid) {
          const url = `${REST_API}/ip-storage/${(v.response as any)?.ipfsCid}`;
          setURL(url);
          setUploaded(true);
          getUrl(`/ip-storage/${(v.response as any)?.ipfsCid}`);
        }
      },
      error: (error) => console.error(error),
      complete: () => console.log('complate'),
    });

    if (getUrl) {
      getUrl(URL || '');
    }
  };
  console.log({
    uploaded,
    uploading,
  });

  return (
    <Paper
      className={classes.root}
      style={{
        backgroundImage: `url(${URL})`,
        backgroundSize: '100%',
        backgroundRepeat: 'no-repeat',
        backgroundPosition: 'center',
      }}
    >
      {!uploading ? (
        <div>
          <input
            onChange={handleChange}
            accept="image/*"
            className={classes.input}
            id={id}
            type="file"
          />
          {!uploaded && (
            <label htmlFor={id}>
              <IconButton
                color="primary"
                aria-label="upload picture"
                component="span"
              >
                <Add fontSize="large" />
              </IconButton>
            </label>
          )}
        </div>
      ) : !uploaded ? (
        <div>
          <LinearProgress
            style={{
              width: '100%',
              height: 5,
            }}
          />
          <Typography>上传中</Typography>
        </div>
      ) : (
        <div
          style={{
            position: 'relative',
            top: 31,
            display: 'flex',
            alignContent: 'center',
            justifyContent: 'space-around',
          }}
        >
          <IconButton color="primary">
            <VisibilityOutlined />
          </IconButton>
          <IconButton
            onClick={() => {
              setUploaded(false);
              setUploading(false);
              setURL('');
            }}
            color="secondary"
          >
            <RemoveCircleOutline />
          </IconButton>
        </div>
      )}
    </Paper>
  );
};
