import React from 'react';
import Popper, { PopperPlacementType } from '@material-ui/core/Popper';
import Fade from '@material-ui/core/Fade';
import Paper from '@material-ui/core/Paper';
import { IconButton, Button } from '@material-ui/core';
import { EmojiEmotions } from '@material-ui/icons';

export default function EmojiPicker({ onPick }) {
  const [anchorEl, setAnchorEl] =
    React.useState<HTMLButtonElement | null>(null);
  const [open, setOpen] = React.useState(false);
  const [placement, setPlacement] = React.useState<PopperPlacementType>();
  const handleClick =
    (newPlacement: PopperPlacementType) =>
    (event: React.MouseEvent<HTMLButtonElement>) => {
      setAnchorEl(event.currentTarget);
      setOpen((prev) => placement !== newPlacement || !prev);
      setPlacement(newPlacement);
    };

  return (
    <>
      <Popper
        style={{
          zIndex: 999999,
          maxWidth: 300,
        }}
        open={open}
        anchorEl={anchorEl}
        placement={placement}
        transition
      >
        {({ TransitionProps }) => (
          <Fade {...TransitionProps} timeout={350} style={{ maxWidth: 300 }}>
            <Paper
              style={{
                display: 'flex',
                flexDirection: 'row',
                flexWrap: 'wrap',
                maxWidth: 300,
              }}
            >
              {[
                '😒',
                '😂',
                '❤️',
                '😍',
                '👌',
                '☺️',
                '😊',
                '😘',
                '😭',
                '😩',
                '😔',
                '😏',
                '😁',
                '😳',
                '👍',
                '✌️',
                '😉',
                '😌',
                '💕',
              ].map((emoji: string, index: number) => {
                return (
                  <div key={index}>
                    <Button
                      onClick={() => {
                        onPick(emoji);
                        setOpen(false);
                      }}
                    >
                      {emoji}
                    </Button>
                  </div>
                );
              })}{' '}
            </Paper>
          </Fade>
        )}{' '}
      </Popper>
      <IconButton color="secondary" onClick={handleClick('top')}>
        <EmojiEmotions />
      </IconButton>
    </>
  );
}
