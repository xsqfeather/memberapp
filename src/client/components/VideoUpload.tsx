import React, { useCallback, useEffect } from 'react';
import { makeStyles, createStyles, Theme } from '@material-ui/core/styles';
import BackupOutlinedIcon from '@material-ui/icons/BackupOutlined';
import Button from '@material-ui/core/Button';
import { v4 as uuidV4 } from 'uuid';
import Grid from '@material-ui/core/Grid';
import Paper from '@material-ui/core/Paper';
import Container from '@material-ui/core/Container';
import { createSignal } from '@react-rxjs/utils';
import { bind } from '@react-rxjs/core';
import LinearProgress, {
  LinearProgressProps,
} from '@material-ui/core/LinearProgress';
import Typography from '@material-ui/core/Typography';
import Box from '@material-ui/core/Box';
import { Slicer } from '../utils/slicer';
import axios from 'axios';
import md5 from 'md5';
import Big from 'big.js';

const useStyles = makeStyles((theme: Theme) =>
  createStyles({
    root: (props: { size?: 'small' | 'default' }) => ({
      userSelect: 'none',
      display: 'flex',
      flexDirection: 'column',
      textAlign: 'center',
      justifyContent: 'center',
      alignContent: 'center',
      height: props.size === 'small' ? 180 : 280,
      maxWidth: 800,
      border: '2px dashed #D3D3D3',
      borderRadius: '5px',
      overflowY: 'auto',
      overflowX: 'hidden',
    }),
    gridContainer: {
      paddingTop: theme.spacing(1),
      paddingBottom: theme.spacing(1),
      paddingLeft: '4px',
    },
    gridItem: {
      padding: theme.spacing(2),
    },
  }),
);
// 是否处于上传
const [uploadingChange$, setUploading] = createSignal<boolean>();
const [useUploading] = bind<boolean>(uploadingChange$, false);
// 是否上传成功
const [uploadedChange$, setUploaded] = createSignal<boolean>();
const [useUploaded] = bind<boolean>(uploadedChange$, false);
// 上传进度
const [progressChange$, setProgress] = createSignal<number[]>();
const [useProgress] = bind<number[]>(progressChange$, []);
// 文件列表
const [filesChange$, setFiles] = createSignal<File[]>();
const [useFiles] = bind<File[]>(filesChange$, []);
// 文件地址
const [RecordsChange$, setRecords] = createSignal<any[]>();
const [useRecords] = bind<any[]>(RecordsChange$, []);

export default function VideoUpload(props: {
  multiple?: boolean;
  getIds?: (ids: number[], records: any[]) => void;
  size?: 'small' | 'default';
  uploadUrl: string;
  onFileChange?: (file: File) => void;
}) {
  const { getIds, multiple, uploadUrl, onFileChange } = props;
  const uploading = useUploading();
  const uploaded = useUploaded();
  const progress = useProgress();
  const files = useFiles();
  const classes = useStyles({
    size: props.size ?? 'default',
  });
  const records = useRecords();
  // 监听文件变化,改变时进行切换状态进行文件上传
  useEffect(() => {
    if (files.length) {
      // 上传任务,后续在封装上传uploader模块
      files.forEach(async (file, i) => {
        onFileChange(file);
        progress[i] = 0;
        setProgress(progress);
        // 较低的块大小性能更低
        const s = new Slicer(file, 5 * 1024 * 1024);
        const fd = new FormData();
        fd.append('message', 'sliceStart');
        fd.append(
          'fileMd5',
          md5(`${file.name}${file.size}${file.lastModified}`),
        );
        fd.append('fileName', file.name);
        fd.append('fileSize', file.size.toString());
        fd.append('fileType', file.type);
        fd.append('chunkTotal', s.chunkTotal.toString());
        const start = await axios.post(uploadUrl, fd);
        if (start.status == 201) {
          // 不为ready,说明服务器存在该文件
          if (start.data !== 'ready') {
            progress[i] = 100;
            setProgress([...progress]);
            records[i] = start.data;
            setRecords([...records]);
            return;
          }
          // 为ready则开始上传任务
          let pos = 0;
          fd.append('file', '');
          fd.append('position', '');
          fd.append('chunkMd5', '');
          fd.set('message', 'sliceChunk');
          s.on('chunk', async (chunk, next, cancel) => {
            fd.set('file', new Blob([chunk]));
            fd.set('position', `${pos}`);
            fd.set('chunkMd5', md5(chunk as Uint8Array));
            const res = await axios.post(uploadUrl, fd, {
              onUploadProgress: (e) => {
                const chunkProgress = new Big(e.loaded / e.total);
                /* console.log((chunk as Uint8Array).byteLength,file.size) */
                const uploadSize = new Big(
                  chunkProgress
                    .times(new Big((chunk as Uint8Array).byteLength))
                    .toNumber(),
                );
                // 这样加很可能超过100,小数保留2位,刻意的四舍五入,因为100%应该是在服务器合并完成返回url时设定
                progress[i] +=
                  Math.ceil(
                    uploadSize.div(new Big(file.size)).toNumber() * 10000,
                  ) / 100;

                if (progress[i] >= 100) {
                  progress[i] = 99;
                }
                setProgress([...progress]);
              },
            });
            pos++;
            if (res.status !== 201) {
              cancel();
            } else {
              next();
            }
          });
          s.on('cancel', () => {
            alert('块上传服务器未返回201,待完善,上传被取消');
          });
          s.on('error', (err) => {
            console.log(err);
            alert('块处理时发生错误,' + err.note);
          });
          s.on('end', async () => {
            fd.set('message', 'sliceEnd');
            fd.delete('file');
            fd.delete('chunkMd5');
            fd.delete('position');
            const end = await axios.post(uploadUrl, fd);
            if (end.status === 201) {
              progress[i] = 100;
              setProgress([...progress]);
              records[i] = end.data;
              setRecords([...records]);
            }
          });
        }
        s.slice_stream();
      });
      setUploading(true);
    }
  }, [files]);
  // 上传任务完成时
  useEffect(() => {
    if (records.length && records.length === files.length) {
      // 上传任务完成后。
      setUploaded(true);
      setUploading(false);
      if (getIds) {
        getIds(
          records.map((record) => {
            return record.id;
          }),
          records,
        );
      }
    }
  }, [records]);
  const handleChange = useCallback(
    (ev: React.ChangeEvent<HTMLInputElement>) => {
      if (ev.target.files.length) {
        setFiles(Array.from(ev.target.files));
      }
    },
    [],
  );
  /*   console.log(`[uploading]`, uploading)
console.log(`[uploaded]`, uploaded) */
  return (
    <Container
      className={classes.root}
      disableGutters={true}
      onDragOver={(e) => {
        e.preventDefault();
      }}
      onDrop={(e: React.DragEvent<HTMLDivElement>) => {
        e.preventDefault();
        const items = e.dataTransfer.items;
        const keys = Object.keys(items);
        const files = [] as File[];
        keys.forEach((key) => {
          const item = items[key] as DataTransferItem;
          if (item.kind === 'file' && item.type.endsWith('mp4')) {
            files.push(item.getAsFile());
          }
        });
        if (!multiple && files.length > 1) {
          alert('只能上传单个视频!');
          return;
        }
        setFiles(files);
      }}
    >
      {
        // 判断是否处于上传,没有处于则显示上传按钮
        // 处于上传则显示上传列表
        !uploading && !uploaded ? (
          <UploadButton handleChange={handleChange} multiple={multiple} />
        ) : uploaded ? (
          <div>预览视频暂位</div>
        ) : (
          <UploadTask />
        )
      }{' '}
    </Container>
  );
}

interface UploadButtonProps {
  handleChange: (event: React.ChangeEvent<HTMLInputElement>) => void;
  multiple?: boolean;
}
function UploadButton(props: UploadButtonProps) {
  const multiple = props.multiple;
  const { handleChange } = props;
  const id = uuidV4();
  return (
    <div>
      <BackupOutlinedIcon color={'primary'} style={{ fontSize: 50 }} />
      <br />
      <span>拖拽视频或点击下方按钮进行上传</span>
      <br />
      <br />
      <input
        accept="video/mp4"
        style={{ display: 'none' }}
        onChange={handleChange}
        id={id}
        multiple={multiple}
        type="file"
      />
      <label htmlFor={id}>
        <Button variant="contained" color="primary" component="span">
          上传视频
        </Button>
      </label>
    </div>
  );
}

function UploadTask() {
  const files = useFiles();
  const progress = useProgress();
  const classes = useStyles({ size: 'default' });
  return (
    <Grid
      container
      spacing={1}
      className={classes.gridContainer}
      style={{
        height: '100%',
        width: '100%',
      }}
    >
      {files.map((file, idx) => {
        return (
          <Grid item xs={12} key={`${file.name}${file.size}`}>
            <Paper className={classes.gridItem}>
              <LinearProgressWithLabel value={progress[idx]} />
            </Paper>
          </Grid>
        );
      })}{' '}
    </Grid>
  );
}

function LinearProgressWithLabel(
  props: LinearProgressProps & {
    value: number;
  },
) {
  return (
    <Box display="flex" alignItems="center">
      <Box width="100%" mr={1}>
        <LinearProgress variant="determinate" {...props} />
      </Box>
      <Box minWidth={35}>
        <Typography variant="body2" color="textSecondary">
          {`${Math.round(props.value)}%`}{' '}
        </Typography>
      </Box>
    </Box>
  );
}
