import React, { useEffect, useState } from 'react';
import cx from 'clsx';
import { makeStyles } from '@material-ui/core/styles';
import Box from '@material-ui/core/Box';
import Card from '@material-ui/core/Card';
import CardContent from '@material-ui/core/CardContent';
import Avatar from '@material-ui/core/Avatar';
import Divider from '@material-ui/core/Divider';
import { useFadedShadowStyles } from '@mui-treasury/styles/shadow/faded';
import { useGutterBorderedGridStyles } from '@mui-treasury/styles/grid/gutterBordered';
import { io } from 'socket.io-client';
import Link from 'next/link';
import { Button, IconButton } from '@material-ui/core';
import { Edit } from '@material-ui/icons';

const useStyles = makeStyles(({ palette }) => ({
  card: {
    borderRadius: 12,
    minWidth: 256,
    textAlign: 'center',
  },
  avatar: {
    width: 60,
    height: 60,
    margin: 'auto',
  },
  heading: {
    fontSize: 18,
    fontWeight: 'bold',
    letterSpacing: '0.5px',
    marginTop: 8,
    marginBottom: 0,
  },
  subheader: {
    fontSize: 14,
    color: palette.grey[500],
    marginBottom: '0.875em',
  },
  statLabel: {
    fontSize: 12,
    color: palette.grey[500],
    fontWeight: 500,
    fontFamily:
      '-apple-system, BlinkMacSystemFont, "Segoe UI", Roboto, Helvetica, Arial, sans-serif, "Apple Color Emoji", "Segoe UI Emoji", "Segoe UI Symbol"',
    margin: 0,
  },
  statValue: {
    fontSize: 20,
    fontWeight: 'bold',
    marginBottom: 4,
    letterSpacing: '1px',
  },
}));

export const ProfileCard = React.memo(function ProfileCard(props: any) {
  const styles = useStyles();
  const shadowStyles = useFadedShadowStyles();
  const borderedGridStyles = useGutterBorderedGridStyles({
    borderColor: 'rgba(0, 0, 0, 0.08)',
    height: '50%',
  });
  const { nickname, username, userId, avatar } = props;
  console.log({ avatar });

  const [followersCount, setFollowersCount] = useState(0);
  const [fansCount, setFansCount] = useState(0);
  const [friendsCount, setFirendsCount] = useState(0);

  useEffect(() => {
    if (userId) {
      const socket = io({
        auth: {
          token: window.localStorage.getItem('token'),
        },
      });
      socket.emit('followingsCount', { fansId: userId }, ({ count }) => {
        setFollowersCount(count);
        console.log({ userId });

        socket.on(`FOLLOWINGS_COUNT_CHANGE_${userId}`, (data) => {
          console.log('接受关注的事件', data);

          setFollowersCount(data.count);
        });
      });

      socket.emit('fansCount', { userId }, ({ count }) => {
        setFansCount(count);
        socket.on(`FANS_COUNT_CHANGE_${userId}`, (data) => {
          setFansCount(data.count);
        });
      });

      socket.emit('friendsCount', { userId }, ({ count }) => {
        setFirendsCount(count);
        socket.on(`FRIENDS_COUNT_CHANGE_${userId}`, (data) => {
          setFirendsCount(data.count);
        });
      });
    }
  }, [userId]);

  return (
    <Card className={cx(styles.card, shadowStyles.root)}>
      <CardContent>
        <Avatar className={styles.avatar} src={avatar} />
        <h3 className={styles.heading}>{nickname}</h3>
        <span className={styles.subheader}>@{username}</span>
        <Link href="/my/profile_edit" passHref>
          <IconButton color="primary" size="small" component="span">
            <Edit />
          </IconButton>
        </Link>
      </CardContent>
      <Divider light />
      <Box display={'flex'}>
        <Box p={2} flex={'auto'} className={borderedGridStyles.item}>
          <p className={styles.statLabel}>关注</p>
          <Link href="/my/followers" passHref>
            <Button variant="text" color="secondary">
              <span className={styles.statValue}>{followersCount}</span>
            </Button>
          </Link>
        </Box>

        <Box p={2} flex={'auto'} className={borderedGridStyles.item}>
          <p className={styles.statLabel}>粉丝</p>
          <Link href="/my/fans" passHref>
            <Button variant="text" color="secondary">
              <span className={styles.statValue}>{fansCount}</span>
            </Button>
          </Link>
        </Box>
      </Box>
      <Box display={'flex'}>
        <Box p={2} flex={'auto'} className={borderedGridStyles.item}>
          <p className={styles.statLabel}>好友</p>
          <Link href="/my/friends" passHref>
            <Button variant="text" color="secondary">
              <span className={styles.statValue}>{friendsCount}</span>
            </Button>
          </Link>
        </Box>

        <Box p={2} flex={'auto'} className={borderedGridStyles.item}>
          <p className={styles.statLabel}>喜欢</p>
          <Link href="/my/likes" passHref>
            <Button variant="text" color="secondary">
              <span className={styles.statValue}>{fansCount}</span>
            </Button>
          </Link>
        </Box>
      </Box>
    </Card>
  );
});

export default ProfileCard;
