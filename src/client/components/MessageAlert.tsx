import { Snackbar } from '@material-ui/core';
import MuiAlert, { AlertProps } from '@material-ui/lab/Alert';

function Alert(props: AlertProps) {
  return <MuiAlert elevation={6} variant="filled" {...props} />;
}

interface IMessageAlertProps {
  open: boolean;
  duration?: 3000;
  handleClose: () => void;
  message: string;
  severity: 'error' | 'warning' | 'info' | 'success';
}
export default function MessageAlert(props: IMessageAlertProps) {
  const { open, handleClose, message, severity } = props;
  return (
    <Snackbar open={open} onClose={handleClose}>
      <Alert onClose={handleClose} severity={severity} color={severity}>
        {message}
      </Alert>
    </Snackbar>
  );
}
