import React from 'react';
import ListItem from '@material-ui/core/ListItem';
import ListItemIcon from '@material-ui/core/ListItemIcon';
import ListItemText from '@material-ui/core/ListItemText';
import ListSubheader from '@material-ui/core/ListSubheader';
import PeopleIcon from '@material-ui/icons/People';
import LayersIcon from '@material-ui/icons/Layers';
import AssignmentIcon from '@material-ui/icons/Assignment';
import Link from 'next/link';
import { List } from '@material-ui/core';
import { PersonalVideo } from '@material-ui/icons';

export const mainListItems = (
  <List color="primary">
    <Link href="/dashboard/courses" passHref>
      <ListItem button>
        <ListItemIcon>
          <LayersIcon />
        </ListItemIcon>
        <ListItemText primary="课程" />
      </ListItem>
    </Link>
    <Link href="/dashboard/users" passHref>
      <ListItem button>
        <ListItemIcon>
          <PeopleIcon />
        </ListItemIcon>
        <ListItemText primary="用户" />
      </ListItem>
    </Link>
    <Link href="/dashboard/role-tags" passHref>
      <ListItem button>
        <ListItemIcon>
          <PersonalVideo />
        </ListItemIcon>
        <ListItemText primary="角色标签" />
      </ListItem>
    </Link>
    <ListItem button>
      <ListItemIcon>
        <LayersIcon />
      </ListItemIcon>
      <ListItemText primary="课程" />
    </ListItem>
  </List>
);

export const secondaryListItems = (
  <List>
    <ListSubheader inset>系统设置</ListSubheader>
    <ListItem button>
      <ListItemIcon>
        <AssignmentIcon />
      </ListItemIcon>
      <ListItemText primary="首页轮播" />
    </ListItem>
    <Link href="/dashboard/home/edit" passHref>
      <ListItem button>
        <ListItemIcon>
          <AssignmentIcon />
        </ListItemIcon>
        <ListItemText primary="首页编辑" />
      </ListItem>
    </Link>

    <ListItem button>
      <ListItemIcon>
        <AssignmentIcon />
      </ListItemIcon>
      <ListItemText primary="网站设置" />
    </ListItem>
  </List>
);
