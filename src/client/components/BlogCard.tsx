import React, { FC, useCallback, useState, useEffect } from 'react';
import { makeStyles } from '@material-ui/core/styles';
import { Avatar, Box, Button, IconButton } from '@material-ui/core';
import { Column, Row, Item } from '@mui-treasury/components/flex';
import { Info, InfoSubtitle, InfoTitle } from '@mui-treasury/components/info';
import { useApexInfoStyles } from '@mui-treasury/styles/info/apex';
import { Comments } from './Comments';
import 'moment/locale/zh-cn';
import { ThumbUp } from '@material-ui/icons';
import Link from 'next/link';
import moment from 'moment';
import axios from 'axios';
import { REST_API } from '../constants/endpoints';
import { useSocket } from '../hooks/useSocket';

moment.locale('zh-cn');

const useStyles = makeStyles(() => ({
  root: {
    height: '100%',
    maxWidth: 500,
    width: '90vw',
    transition: '0.3s',
    margin: 10,
    position: 'relative',
    background: '#fff',
    '&:before': {
      transition: '0.2s',
      position: 'absolute',
      width: '100%',
      height: '100%',
      content: '""',
      display: 'block',
      backgroundColor: '#d9daf1',
      borderRadius: '1rem',
      zIndex: 0,
      bottom: 0,
    },
    '&:hover': {
      '&:before': {
        bottom: -6,
      },
      '& $card': {
        boxShadow: '-12px 12px 64px 0 #bcc3d6',
      },
    },
  },
  card: {
    zIndex: 1,
    position: 'relative',
    borderRadius: '1rem',
    boxShadow: '0 6px 20px 0 #dbdbe8',
    backgroundColor: '#fff',
    transition: '0.4s',
    padding: '16px',
    height: '100%',
  },
  logo: {
    width: 48,
    height: 48,
    borderRadius: '0.75rem',
  },
  avatar: {
    fontFamily: 'Ubuntu',
    fontSize: '0.875rem',
    backgroundColor: '#6d7efc',
  },
  join: {
    background: 'linear-gradient(to top, #638ef0, #82e7fe)',
    '& > *': {
      textTransform: 'none !important',
    },
  },
}));

const BlogCard: FC<{
  postId: number;
  avatar?: string;
  nickname?: string;
  username?: string;
  body?: string;
  images?: string[];
  createdAt?: Date;
  publisherId?: number;
  like: number;
  comment: number;
  liked: boolean;
}> = ({
  postId,
  avatar,
  nickname,
  username,
  body,
  images = [],
  createdAt,
  publisherId,
  like,
  comment,
  liked,
}) => {
  const [comments, setComments] = useState<{
    total: number;
    results: Comments.Comment[];
  }>({ total: 0, results: [] });
  const [likeCount, setLikeCount] = useState(() => like);
  const [isLike, setIsLike] = useState(() => liked);
  const [commentCount, setCommentCount] = useState(() => comment);
  const socket = useSocket('mini-blog');
  const box = Comments.useCommentBox(postId);
  const styles = useStyles();
  const onLoadComments = useCallback(async () => {
    if (!box.visible) return void 0;
    const result = await axios.get(`${REST_API}/comments?postId=${postId}`);
    setComments(result.data);
    setCommentCount(result.data.total);
  }, [box.visible, postId]);
  const onSendComment = useCallback(
    async (content: string) => {
      await axios.post(`${REST_API}/comments`, {
        postId,
        content,
      });
      // await onLoadComments();
    },
    [postId],
  );
  const onLike = useCallback(async () => {
    try {
      const result = await axios.put(`${REST_API}/likes?postId=${postId}`);
      if (!result.data?.success) return void 0;
      setLikeCount((prev) => prev + 1);
      setIsLike(true);
    } catch (e) {
      console.error(e);
    }
  }, [postId]);
  const onUnlike = useCallback(async () => {
    try {
      const result = await axios.delete(`${REST_API}/likes?postId=${postId}`);
      if (!result.data?.success) return void 0;
      setLikeCount((prev) => prev - 1);
      setIsLike(false);
    } catch (e) {
      console.error(e);
    }
  }, [postId]);
  useEffect(() => {
    if (!box.visible) return void 0;
    socket.connect({
      joinId: postId,
      namespace: '/comments',
      multiplex: false,
    });
    socket.on('REFRESH_COMMENTS', (data) => {
      console.log(`新的评论`, data);
      onLoadComments();
    });
    return () => {
      socket.off(`REFRESH_COMMENTS`);
      socket.disconnect();
    };
  }, [box.visible, postId, onLoadComments]);
  useEffect(() => {
    onLoadComments();
  }, [onLoadComments]);
  useEffect(() => {
    setLikeCount(like);
  }, [like]);
  useEffect(() => {
    setIsLike(liked);
  }, [liked]);
  useEffect(() => {
    setCommentCount(comment);
  }, [comment]);
  return (
    <div className={styles.root}>
      <Column className={styles.card}>
        <Row p={2} gap={2}>
          <Link href={`/users/${publisherId}`} passHref>
            <Avatar className={styles.logo} variant={'rounded'} src={avatar} />
          </Link>

          <Info position={'middle'} useStyles={useApexInfoStyles}>
            <InfoTitle>{nickname}</InfoTitle>
            <InfoSubtitle>{username}</InfoSubtitle>
          </Info>

          <Info position={'right'}>{moment(createdAt).fromNow()}</Info>
        </Row>
        <Box
          pb={1}
          px={2}
          color={'grey.600'}
          fontSize={'0.875rem'}
          fontFamily={'Ubuntu'}
        >
          {body}
          <div
            style={{
              display: 'flex',
              flexWrap: 'wrap',
              alignContent: 'space-around',
            }}
          >
            {images &&
              images.map((image: any) => (
                <img
                  key={image}
                  style={{
                    width: '33%',
                    maxWidth: 200,
                    maxHeight: 200,
                    height: 'auto',
                  }}
                  alt=""
                  src={image}
                />
              ))}
          </div>
        </Box>
        <Row position={'bottom'}>
          <Item position={'middle-left'} justifyItems="center">
            <IconButton
              color={isLike ? 'primary' : 'default'}
              onClick={isLike ? onUnlike : onLike}
            >
              <ThumbUp />
            </IconButton>
            <span>{likeCount}</span>
          </Item>
          <Item position={'middle-right'}>
            <Button color={'primary'} onClick={box.toggle}>
              评论（{commentCount ?? 0}）
            </Button>
          </Item>
        </Row>
        {box.visible && <Comments.CommentsList data={comments.results} />}
        {box.visible && <Comments.CommentBox onSend={onSendComment} />}
      </Column>
    </div>
  );
};

export default BlogCard;
