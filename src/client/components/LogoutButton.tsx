import { Button } from '@material-ui/core';
import Cookies from 'js-cookie';
import { useRouter } from 'next/router';
import { MouseEvent, useContext } from 'react';
import { GlobalActions, GlobalContext } from '../contexts/global';

export function LogoutButton() {
  const router = useRouter();
  const { globalDispatch } = useContext(GlobalContext);
  const handleLogout = (e: MouseEvent<HTMLButtonElement>) => {
    e.stopPropagation();
    localStorage.removeItem('token');
    Cookies.remove('token');
    globalDispatch(
      GlobalActions.globalShowMessage({
        messageText: '您已安全登出',
        severity: 'success',
      }),
    );
    router.push('/login');
  };
  return (
    <Button variant="outlined" color="primary" onClick={handleLogout}>
      退出账号
    </Button>
  );
}
