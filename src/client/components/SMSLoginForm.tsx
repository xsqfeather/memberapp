import { Button, Grid, makeStyles, TextField } from '@material-ui/core';
import { bind } from '@react-rxjs/core';
import { createSignal } from '@react-rxjs/utils';
import { useState } from 'react';
import { interval, Observable } from 'rxjs';
import { take } from 'rxjs/operators';

const useStyles = makeStyles(() => ({
  form: {
    width: '90%', // Fix IE 11 issue.
    minHeight: 300,
    padding: 10,
    textAlign: 'center',
    alignContent: 'center',
  },
  button: {
    margin: 20,
  },
  textInput: {
    width: '100%', // Fix IE 11 issue.
  },
}));

const [phoneChange$, setPhone]: [Observable<string>, (phone: string) => void] =
  createSignal<string>();

const [usePhone] = bind<string>(phoneChange$, '');

const [smsChange$, setSms]: [Observable<string>, (phone: string) => void] =
  createSignal<string>();

const [useSms] = bind<string>(smsChange$, '');

const countSub = interval(1000).pipe(take(60));
const [countChange$, setCount]: [Observable<number>, (count: number) => void] =
  createSignal<number>();
const [useCount] = bind<number>(countChange$, 60);

interface SMSLoginFormProps {
  onLoginSuccess?: () => void;
  sendSMS: (phone: string) => void;
  sendingSms?: boolean;
  doLogin: (phone: string, sms: string) => void;
  logining: boolean;
}

export default function SMSLoginForm(props: SMSLoginFormProps) {
  const { sendingSms, sendSMS, logining, doLogin } = props;
  const classes = useStyles();
  const count = useCount();
  const sms = useSms();
  const phone = usePhone();

  const [phoneError, setPhoneError] = useState(false);
  const [smsError, setSmsError] = useState(false);
  const [phoneHelperText, setPhoneHelperText] = useState('');
  const [smsHelperText, setSmsHelperText] = useState('');

  const validPhone = (phone: string) => {
    if (!phone || phone === '') {
      setPhoneError(true);
      setPhoneHelperText('手机号码不能为空');
      return false;
    }
    if (/^1[3-9]\d{9}$/.test(phone)) {
      setPhoneError(false);
      setPhoneHelperText('');
      return true;
    }
    setPhoneError(true);
    setPhoneHelperText('手机格式错误');
    return false;
  };

  const validSms = (sms: string) => {
    if (!sms || sms === '') {
      setSmsError(true);
      setSmsHelperText('验证码不得为空');
      return false;
    }
    return true;
  };

  const handleSubmit = (e: any) => {
    e.preventDefault();

    if (!validPhone(phone)) {
      return false;
    }
    if (!validSms(sms)) {
      return false;
    }
    doLogin(phone, sms);
  };

  return (
    <Grid
      container
      onSubmit={handleSubmit}
      component="form"
      direction="column"
      justify="space-between"
      alignItems="center"
      className={classes.form}
    >
      <TextField
        fullWidth
        disabled={logining}
        variant="outlined"
        error={phoneError}
        helperText={phoneHelperText}
        onChange={(e: React.ChangeEvent<HTMLInputElement>) => {
          setPhone(e.target.value);
        }}
        value={phone}
        label="手机号"
      />
      <TextField
        error={smsError}
        helperText={smsHelperText}
        disabled={logining}
        value={sms}
        onChange={(e: React.ChangeEvent<HTMLInputElement>) => {
          setSms(e.target.value);
        }}
        fullWidth
        variant="outlined"
        label="验证码"
        InputProps={{
          endAdornment: (
            <Button
              onClick={() => {
                if (!validPhone(phone)) {
                  return false;
                }
                sendSMS(phone);

                countSub.subscribe(async (c: number) => {
                  setCount(60 - c);

                  if (c === 59) {
                    setCount(60);
                  }
                });
              }}
              disabled={count !== 60 || sendingSms || logining}
              variant="contained"
              color="secondary"
              style={{
                width: 180,
              }}
            >
              {count !== 60 ? `${count}S重新获取` : '短信验证'}
            </Button>
          ),
        }}
      />
      <Button
        disabled={logining}
        className={classes.button}
        type="submit"
        fullWidth
        color="primary"
        variant="contained"
      >
        登录
      </Button>
    </Grid>
  );
}
