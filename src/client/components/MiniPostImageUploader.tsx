import React from 'react';
import { makeStyles, createStyles, Theme } from '@material-ui/core/styles';
import IconButton from '@material-ui/core/IconButton';
import Add from '@material-ui/icons/Add';
import { uploadToWoogemeSubject } from '../observers/upload';
import { useRouter } from 'next/router';

const useStyles = makeStyles((theme: Theme) =>
  createStyles({
    root: {
      display: 'flex',
      '& > *': {
        margin: theme.spacing(1),
      },
    },
    input: {
      display: 'none',
    },
  }),
);

export default function MiniPostImageUploader({
  getUrl,
}: {
  getUrl: (url: string) => void;
}) {
  const classes = useStyles();
  const router = useRouter();
  const handleChange = (e: any) => {
    const files = e.target.files;
    const formData = new FormData();
    formData.append('file', files[0]);
    try {
      uploadToWoogemeSubject(formData).subscribe({
        next: (v: any) => {
          getUrl(v.response.url);
        },
        error: (error) => {
          console.log(error);
          router.push('/login');
        },
        complete: () => {
          console.log('结束');
        },
      });
    } catch (error) {
      console.log(error);
    }
  };
  return (
    <div className={classes.root}>
      <input
        onChange={handleChange}
        accept="image/*"
        className={classes.input}
        id="icon-button-file"
        type="file"
      />
      <label htmlFor="icon-button-file">
        <IconButton
          color="primary"
          aria-label="upload picture"
          component="span"
        >
          <Add style={{fontSize:24}} />
        </IconButton>
      </label>
    </div>
  );
}
