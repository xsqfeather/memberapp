import React, { ReactNode, useContext, useState } from 'react';
import { makeStyles } from '@material-ui/core/styles';
import CssBaseline from '@material-ui/core/CssBaseline';
import Box from '@material-ui/core/Box';
import Container from '@material-ui/core/Container';
import { DashBoardPageStyles } from '../../styles/pages/dashboard';
import Copyright from '../Copyright';
import DashBoardTopBar from './DashBoardTopBar';
import Head from 'next/head';
import Cookies from 'js-cookie';
import { useRouter } from 'next/router';
import { GlobalActions, GlobalContext } from '../../contexts/global';
import PageLoader from '../PageLoader';

const useStyles = makeStyles(DashBoardPageStyles as any);

interface IDashboardProps {
  title: string;
  children: ReactNode;
}

export default function DashboardLayout(props: IDashboardProps) {
  const classes = useStyles(props);
  const { title, children } = props;
  const [drawerOpen, setDrawerOpen] = useState(false);
  const { globalDispatch } = useContext(GlobalContext);
  const router = useRouter();
  const handleExit = () => {
    localStorage.removeItem('token');
    Cookies.remove('token');
    globalDispatch(
      GlobalActions.globalShowMessage({
        messageText: '您已安全退出',
        severity: 'success',
      }),
    );
    router.push('/login');
  };
  return (
    <>
      <Head>
        <title>{title}</title>
      </Head>
      <div className={classes.root}>
        <CssBaseline />

        <DashBoardTopBar
          drawerOpen={drawerOpen}
          handleDrawerClose={() => setDrawerOpen(false)}
          handleDrawerOpen={() => setDrawerOpen(true)}
          title={title}
          handleExit={handleExit}
        />

        <main className={classes.content}>
          <div className={classes.appBarSpacer} />
          <Container maxWidth="lg" className={classes.container}>
            <div
              id="routerChangeLoader"
              style={{
                display: 'none',
                margin: 0,
                padding: 0,
                height: 5,
              }}
            >
              <PageLoader />
            </div>
            {children}
            <Box pt={4}>
              <Copyright />
            </Box>
          </Container>
        </main>
      </div>
    </>
  );
}
