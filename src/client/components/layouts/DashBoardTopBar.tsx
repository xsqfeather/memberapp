import React from 'react';
import clsx from 'clsx';
import { makeStyles } from '@material-ui/core/styles';
import AppBar from '@material-ui/core/AppBar';
import Toolbar from '@material-ui/core/Toolbar';
import IconButton from '@material-ui/core/IconButton';
import Badge from '@material-ui/core/Badge';
import MenuIcon from '@material-ui/icons/Menu';
import NotificationsIcon from '@material-ui/icons/Notifications';
import { ChevronLeftSharp, Dashboard } from '@material-ui/icons';
import { Button, Divider, Drawer, Grid, List } from '@material-ui/core';
import { mainListItems, secondaryListItems } from '../listItems';
import Link from 'next/link';
import { LogoutButton } from '../LogoutButton';
import PageLoader from '../PageLoader';

const drawerWidth = 240;

const useStyles = makeStyles((theme) => ({
  toolbar: {
    paddingRight: 24, // keep right padding when drawer closed
  },
  toolbarIcon: {
    display: 'flex',
    alignItems: 'center',
    justifyContent: 'flex-end',
    padding: '0 8px',
    ...theme.mixins.toolbar,
  },
  appBar: {
    zIndex: theme.zIndex.drawer + 1,
    transition: theme.transitions.create(['width', 'margin'], {
      easing: theme.transitions.easing.sharp,
      duration: theme.transitions.duration.leavingScreen,
    }),
  },
  appBarShift: {
    marginLeft: drawerWidth,
    width: `calc(100% - ${drawerWidth}px)`,
    transition: theme.transitions.create(['width', 'margin'], {
      easing: theme.transitions.easing.sharp,
      duration: theme.transitions.duration.enteringScreen,
    }),
  },
  menuButton: {
    marginRight: 36,
  },
  menuButtonHidden: {
    display: 'none',
  },
  title: {
    flexGrow: 1,
  },
  drawerPaper: {
    backgroundColor: theme.palette.background.default,
    position: 'relative',
    whiteSpace: 'nowrap',
    width: drawerWidth,
    transition: theme.transitions.create('width', {
      easing: theme.transitions.easing.sharp,
      duration: theme.transitions.duration.enteringScreen,
    }),
  },
  drawerPaperClose: {
    overflowX: 'hidden',
    transition: theme.transitions.create('width', {
      easing: theme.transitions.easing.sharp,
      duration: theme.transitions.duration.leavingScreen,
    }),
    width: theme.spacing(7),
    [theme.breakpoints.up('sm')]: {
      width: theme.spacing(9),
    },
  },
}));

interface ITopBarProps {
  handleDrawerOpen?: () => void;
  handleDrawerClose: () => void;
  drawerOpen: boolean;
  title: string;
  handleExit: () => void;
}

export default function DashBoardTopBar(props: ITopBarProps) {
  const classes = useStyles();

  const { handleDrawerOpen, drawerOpen, handleDrawerClose } = props;

  return (
    <>
      <AppBar
        color="secondary"
        position="fixed"
        className={clsx(classes.appBar, drawerOpen && classes.appBarShift)}
      >
        <Toolbar className={classes.toolbar}>
          <IconButton
            edge="start"
            color="inherit"
            aria-label="open drawer"
            onClick={handleDrawerOpen}
            className={clsx(
              classes.menuButton,
              drawerOpen && classes.menuButtonHidden,
            )}
          >
            <MenuIcon />
          </IconButton>
          <div className={classes.title}>
            <Link href="/dashboard" passHref>
              <Button color="inherit">
                <Dashboard />
                面板
              </Button>
            </Link>
          </div>

          <>
            <IconButton>
              <Badge badgeContent={4} color="primary">
                <NotificationsIcon />
              </Badge>
            </IconButton>
            <LogoutButton />
          </>
        </Toolbar>
        <div
          id="routerChangeLoader"
          style={{
            display: 'none',
            margin: 0,
            padding: 0,
            height: 5,
          }}
        >
          <PageLoader />
        </div>
      </AppBar>
      <Drawer
        variant="permanent"
        classes={{
          paper: clsx(
            classes.drawerPaper,
            !drawerOpen && classes.drawerPaperClose,
          ),
        }}
        open={drawerOpen}
      >
        <div className={classes.toolbarIcon}>
          {drawerOpen && (
            <Grid container direction="row" alignItems="center">
              <Grid item lg={9} xs={9} md={9}>
                个人中心
              </Grid>
              <Grid item lg={3} xs={3} md={3}>
                <IconButton onClick={handleDrawerClose}>
                  <ChevronLeftSharp />
                </IconButton>
              </Grid>
            </Grid>
          )}{' '}
        </div>
        <Divider />
        <List>{mainListItems}</List>
        <Divider />
        <List>{secondaryListItems}</List>
      </Drawer>
    </>
  );
}
