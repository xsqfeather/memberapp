import { AppBar, Avatar, Button, Toolbar, Typography } from '@material-ui/core';
import Link from 'next/link';

export default function TopBar(props: any) {
  const { username } = props;
  return (
    <AppBar position="fixed" variant="elevation">
      <Toolbar>
        <div
          style={{
            flexGrow: 1,
          }}
        >
          <Link href="/" passHref>
            <Button variant="outlined" color="primary">
              首页
            </Button>
          </Link>
          <Link href="/users" passHref>
            <Button variant="outlined" color="primary">
              认识朋友
            </Button>
          </Link>
          <Link href="/world/activities" passHref>
            <Button variant="outlined" color="primary">
              世界
            </Button>
          </Link>
        </div>
        <div>
          <Link href="/login" passHref>
            <Button variant="outlined" color="primary">
              登录
            </Button>
          </Link>
          <div
            style={{
              display: 'inline-block',
            }}
          >
            <Avatar component="button">{username}</Avatar>
            <Typography color="primary" component="span">
              {username}
            </Typography>
          </div>
        </div>
      </Toolbar>
    </AppBar>
  );
}
