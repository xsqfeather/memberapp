import RestdataProvider from '../dataProvider/rest';
import useScollList from '../hooks/useScrollList';
import UserInfoList from './UserInfoList';
const UserInfoListContainer = ({ filter, userId }: any) => {
  const fetchUsers = async (page: number) => {
    const token = localStorage.getItem('token');
    const dataProvider = new RestdataProvider(token);
    const rlt: any = await dataProvider.getList('users', {
      pagination: {
        page,
        perPage: 20,
      },
      sort: {
        field: 'createdAt',
        order: 'DESC',
      },
      filter,
    });
    if (userId) {
      console.log('开始搞关注');
    }
    return rlt.data as any;
  };

  const [users] = useScollList(fetchUsers);
  return (
    <>
      <UserInfoList list={users} userId={userId} />
    </>
  );
};

export default UserInfoListContainer;
