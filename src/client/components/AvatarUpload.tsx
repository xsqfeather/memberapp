import React from 'react';
import { makeStyles, createStyles, Theme } from '@material-ui/core/styles';
import Button from '@material-ui/core/Button';
import PhotoCamera from '@material-ui/icons/PhotoCamera';
import axios from 'axios';
import { REST_API } from '../constants/endpoints';
import { Avatar } from '@material-ui/core';

const useStyles = makeStyles((theme: Theme) =>
  createStyles({
    root: {
      '& > *': {
        margin: theme.spacing(1),
      },
    },
    input: {
      display: 'none',
    },
  }),
);

export default function AvatarUpload(props: any) {
  const classes = useStyles();
  const handleChange = async (e: any) => {
    const files: any = e.target.files;
    const formData = new FormData();
    formData.append('file', files[0]);
    const rlt = await axios.post(`${REST_API}/distributed-images`, formData);
    props.getImages(rlt.data);
  };
  return (
    <div className={classes.root}>
      <input
        accept="image/*"
        onChange={handleChange}
        className={classes.input}
        id="icon-button-file"
        type="file"
      />
      <label htmlFor="icon-button-file">
        <Button style={{
          width: 150,
          height: 150,
        }}
          color="primary"
          aria-label="upload picture"
          component="span"
        >
          <Avatar style={{
          width: 150,
          height: 150,
        }} sizes="large" src={props.src} />
        </Button>
      </label>
    </div>
  );
}
