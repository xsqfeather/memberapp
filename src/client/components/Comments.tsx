import React, { FC, Fragment, useCallback, useMemo, useState } from 'react';
import {
  makeStyles,
  createStyles,
  List,
  ListItem,
  Button,
  ListItemText,
  ListItemAvatar,
  Avatar,
  TextField,
} from '@material-ui/core';
import { Row } from '@mui-treasury/components/flex';
import { useSnackbar } from 'notistack';
import moment from 'moment';

const useStyles = makeStyles((theme) =>
  createStyles({
    listRoot: {
      width: '100%',
      backgroundColor: theme.palette.background.paper,
    },
    at: {
      display: 'inline',
      color: theme.palette.primary.light,
      marginRight: '4px',
    },
    commentWrap: {
      background: '#fff',
    },
    nickname: {
      fontWeight: 'bold',
      fontSize: '12px',
    },
    time: {
      fontSize: '12px',
    },
    info: {
      display: 'flex',
      alignItems: 'center',
      justifyContent: 'space-between',
    },
  }),
);

export namespace Comments {
  export interface Comment {
    id: number;
    user: {
      id: number;
      nickname: string;
      avatar?: string;
    };
    content: string;
    reply?: Comment;
    createdAt: string;
  }
  export const CommentBox: FC<{ onSend(content: string): Promise<void> }> = ({
    onSend,
  }) => {
    const styles = useStyles();
    const { enqueueSnackbar } = useSnackbar();
    const [content, setContent] = useState('');
    const send = useCallback(async () => {
      try {
        await onSend(content);
        enqueueSnackbar('发送成功', {
          variant: 'success',
          autoHideDuration: 1500,
        });
        setContent('');
      } catch (e) {
        enqueueSnackbar('发送失败', {
          variant: 'error',
          autoHideDuration: 1500,
        });
      }
    }, [onSend, content, enqueueSnackbar]);
    const disabled = useMemo(() => content.trim().length === 0, [content]);
    return (
      <Row className={styles.commentWrap} flexDirection="column">
        <TextField
          label="评论"
          style={{ margin: 8 }}
          fullWidth
          margin="normal"
          multiline
          rows={4}
          value={content}
          onChange={(ev) => setContent(ev.target.value)}
          variant="outlined"
        />
        <div style={{ textAlign: 'right' }}>
          <Button disabled={disabled} onClick={send}>
            发送
          </Button>
        </div>
      </Row>
    );
  };
  export const CommentsList: FC<{ data: Comment[] }> = ({ data }) => {
    const styles = useStyles();
    return (
      <List className={styles.listRoot}>
        {data.map((it) => (
          <Fragment key={it.id}>
            <ListItem alignItems="flex-start">
              <ListItemAvatar>
                <Avatar alt={it.user.nickname} src={it.user.avatar} />
              </ListItemAvatar>
              <ListItemText
                primary={
                  <Fragment>
                    <span className={styles.nickname}>{it.user.nickname}</span>
                    <span className={styles.time}>
                      {moment(it.createdAt).fromNow()}
                    </span>
                  </Fragment>
                }
                primaryTypographyProps={{
                  component: 'div',
                  className: styles.info,
                }}
                secondary={
                  <Fragment>
                    {/*<Typography*/}
                    {/*  component="span"*/}
                    {/*  variant="body2"*/}
                    {/*  className={styles.at}*/}
                    {/*  color="textPrimary"*/}
                    {/*>*/}
                    {/*  @张三*/}
                    {/*</Typography>*/}
                    <span>{it.content}</span>
                  </Fragment>
                }
                secondaryTypographyProps={{ style: { marginTop: '8px' } }}
              />
            </ListItem>
          </Fragment>
        ))}
      </List>
    );
  };
  export const useCommentBox = (postId: number) => {
    const [state, setState] = useState<{ id: number | null; visible: boolean }>(
      {
        id: null,
        visible: false,
      },
    );
    return {
      state,
      visible: state.visible,
      toggle: useCallback(() => {
        setState((prev) => {
          if (prev.visible) return { visible: false, id: null };
          return { visible: true, id: postId };
        });
      }, []),
    };
  };
}
