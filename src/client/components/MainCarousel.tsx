import { Button } from '@material-ui/core';
import { useTheme } from '@material-ui/core/styles';
import useMediaQuery from '@material-ui/core/useMediaQuery';
import 'react-responsive-carousel/lib/styles/carousel.min.css';
import { Carousel } from 'react-responsive-carousel';

import banner1 from '../statics/imgs/banner1.jpeg';
import banner2 from '../statics/imgs/banner2.jpeg';
import banner3 from '../statics/imgs/banner3.jpeg';

const ImageContainer = ({ imageUrl }: { imageUrl: string }) => {
  const theme = useTheme();
  const matches = useMediaQuery(theme.breakpoints.up('sm'));
  return (
    <Button
      style={{
        width: '100%',
        height: matches ? 600 : 300,
        backgroundImage: `url(${imageUrl})`,
        backgroundSize: '100%',
        backgroundPosition: 'center',
      }}
    ></Button>
  );
};

export default function MainCarousel() {
  return (
    <div
      style={{
        position: 'relative',
        top: -100,
      }}
    >
      <Carousel
        showStatus={false}
        showArrows={false}
        infiniteLoop={true}
        autoPlay={true}
        showThumbs={false}
      >
        <ImageContainer imageUrl={banner1} />
        <ImageContainer imageUrl={banner2} />
        <ImageContainer imageUrl={banner3} />
      </Carousel>
    </div>
  );
}
