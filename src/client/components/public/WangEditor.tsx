import { Typography } from '@material-ui/core';
import { useTranslate } from 'ra-core';
import { useEffect, useState } from 'react';
import Editor from 'wangeditor';

let editor: any = null;

function WangEditor(props: any) {
  const { resource, source } = props;
  const [preload, setPreload] = useState(false);
  const translate = useTranslate();
  useEffect(() => {
    editor = new Editor('#' + props.idName);
    editor.create();

    if (props.edit && !preload) {
      // editor.txt.html(value);
      setPreload(true);
    }

    return () => {
      editor.destroy();
    };
  }, []);

  return (
    <div
      style={{
        padding: 10,
      }}
    >
      <Typography variant="h5">
        {translate(`resources.${resource}.fields.${source}`)}: &nbsp;&nbsp;
      </Typography>
      <div id={props.idname}></div>
    </div>
  );
}

export default WangEditor;
