import React from 'react';
import Paper from '@material-ui/core/Paper';
import Tabs from '@material-ui/core/Tabs';
import Tab from '@material-ui/core/Tab';

export default function CustomTabs() {
  const [value, setValue] = React.useState(1);

  const handleChange = (event: React.ChangeEvent<any>, newValue: number) => {
    setValue(newValue);
  };

  return (
    <Paper square>
      <Tabs
        value={value}
        indicatorColor="primary"
        textColor="primary"
        onChange={handleChange}
        centered
        aria-label="disabled tabs example"
      >
        <Tab label="账号" />
        <Tab label="手机验证码" />
      </Tabs>
    </Paper>
  );
}
