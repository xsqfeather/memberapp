import React from 'react';
import { DropzoneArea } from 'material-ui-dropzone';

function DrapDropUpload() {
  const handleChange = (files: any) => {
    console.log(files);
  };
  return (
    <DropzoneArea dropzoneText="点击或者拖拽上传" onChange={handleChange} />
  );
}

export default DrapDropUpload;
