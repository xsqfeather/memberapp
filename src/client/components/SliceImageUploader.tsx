import React, { useCallback } from 'react';
import {
  IconButton,
  LinearProgress,
  Paper,
  Typography,
} from '@material-ui/core';
import { makeStyles, createStyles } from '@material-ui/core/styles';
import { Add } from '@material-ui/icons';
import { v4 as uuidv4 } from 'uuid';
import { bind } from '@react-rxjs/core';
import { createSignal } from '@react-rxjs/utils';
import { REST_API } from '../constants/endpoints';
import axios from 'axios';

//上传成功后的URL
const [URLChange$, setURL] = createSignal<string>();
const [useURL] = bind<string>(URLChange$, '');
//是否处于上传
const [uploadingChange$, setUploading] = createSignal<boolean>();
const [useUploading] = bind<boolean>(uploadingChange$, false);
//是否上传成功
const [uploadedChange$, setUploaded] = createSignal<boolean>();
const [useUploaded] = bind<boolean>(uploadedChange$, false);
//上传进度
const [progressChange$] = createSignal<number>();
bind<number>(progressChange$, 0);

//css-in-js
const useStyles = makeStyles(() =>
  createStyles({
    root: {
      marigin: 20,
      display: 'flex',
      flexDirection: 'column',
      textAlign: 'center',
      justifyContent: 'center',
      alignContent: 'center',
      height: 150,
      width: 200,
      overflow: 'hidden',
    },
  }),
);

export interface ImageUploaderProp {
  getURL?: (url: string) => void;
  url?: string;
}
/* export function SliceImageUploader(props: ImageUploaderProp) {
  const { getURL, url } = props;
  const classes = useStyles();
  const URL = useURL();
  const uploading = useUploading();
  const uploaded = useUploaded();
  const progress = useProgress();

  const handleChange = useCallback((e: React.ChangeEvent<HTMLInputElement>) => {
    if (e.target.files) {
      setUploading(true);
      uploadSliceFiles(
        `${REST_API}/slice-upload`,
        e.target.files,
        {},
        function onFulfilled(fileURL: string) {
          setUploading(false);
          setUploaded(true);
          setURL(`${fileURL}`);
          if (getURL) {
            getURL(fileURL);
          }
        },
        function onRejected(error) {
          console.log(error);
          alert('上传失败');
        },
        function onProgress(ev) {
          //保留两位,不做其他运算
          setProgress(Math.floor((ev.loaded / ev.total) * 10000) / 100);
        },
      );
    }
  }, []);
  return (
    <Paper className={classes.root}>
      {!uploaded ? (
        !uploading ? (
          <Add_ handleChange={handleChange} />
        ) : (
          <div>
            <LinearProgress
              variant="determinate"
              style={{
                width: '100%',
                height: 5,
              }}
              value={progress}
            />
            <Typography>上传中</Typography>
          </div>
        )
      ) : (
        <img src={URL} />
      )}
    </Paper>
  );
}
 */
interface ADDProps {
  handleChange: (event: React.ChangeEvent<HTMLInputElement>) => void;
}
function Add_(props: ADDProps) {
  const { handleChange } = props;
  const id = uuidv4();
  return (
    <div>
      <input
        id={id}
        type="file"
        accept="image/*"
        onChange={handleChange}
        style={{
          display: 'none',
        }}
      />
      <label htmlFor={id}>
        <IconButton
          color="primary"
          aria-label="upload picture"
          component="span"
        >
          <Add fontSize="large" />
        </IconButton>
      </label>
    </div>
  );
}

export function ImageUpload(props: ImageUploaderProp) {
  const { getURL } = props;
  const classes = useStyles();
  const URL = useURL();
  const uploading = useUploading();
  const uploaded = useUploaded();

  const handleChange = useCallback((e: React.ChangeEvent<HTMLInputElement>) => {
    if (e.target.files) {
      setUploading(true);
      const fd = new FormData();
      fd.append('file', e.target.files[0]);
      axios.post(`${REST_API}/images`, fd).then((value) => {
        setUploading(false);
        setUploaded(true);
        setURL(`${value.data[0].url}`);
        if (getURL) {
          getURL(value.data[0].url);
        }
      });
    }
  }, []);
  return (
    <Paper className={classes.root}>
      {!uploaded ? (
        !uploading ? (
          <Add_ handleChange={handleChange} />
        ) : (
          <div>
            <LinearProgress
              style={{
                width: '100%',
                height: 5,
              }}
            />
            <Typography>上传中</Typography>
          </div>
        )
      ) : (
        <img src={URL} />
      )}
    </Paper>
  );
}
