import React from 'react';
import cx from 'clsx';
import Avatar from '@material-ui/core/Avatar';
import { makeStyles } from '@material-ui/core/styles';
import { Row, Item } from '@mui-treasury/components/flex';
import { useDynamicAvatarStyles } from '@mui-treasury/styles/avatar/dynamic';
import { Card, CardContent, IconButton } from '@material-ui/core';
import { Chat } from '@material-ui/icons';
import { setChatOpen, setChattingUserId } from '../observers/messages';

const usePersonStyles = makeStyles(() => ({
  text: {
    fontFamily: 'Barlow, san-serif',
    whiteSpace: 'nowrap',
    textOverflow: 'ellipsis',
    overflow: 'hidden',
  },
  name: {
    fontWeight: 600,
    fontSize: '1rem',
    color: '#122740',
  },
  caption: {
    fontSize: '0.875rem',
    color: '#758392',
    marginTop: -4,
  },
  btn: {
    borderRadius: 20,
    padding: '0.125rem 0.75rem',
    borderColor: '#becddc',
    fontSize: '0.75rem',
  },
}));

const FriendCard = ({ src, name, username, id }) => {
  const avatarStyles = useDynamicAvatarStyles({ size: 56 });
  const styles = usePersonStyles();
  return (
    <Card
      style={{
        maxWidth: 550,
        width: '95%',
      }}
    >
      <CardContent>
        <Row gap={2} p={2.5}>
          <Item>
            <Avatar classes={avatarStyles} src={src} />
          </Item>
          <Row wrap grow gap={0.5} minWidth={0}>
            <Item grow minWidth={0}>
              <div className={cx(styles.name, styles.text)}>{name}</div>
              <div className={cx(styles.caption, styles.text)}>
                @{username}{' '}
              </div>
            </Item>
            <Item position={'middle'}>
              <IconButton
                onClick={() => {
                  setChattingUserId(id);
                  setChatOpen(true);
                }}
              >
                <Chat />
              </IconButton>
            </Item>
          </Row>
        </Row>
      </CardContent>
    </Card>
  );
};

export default FriendCard;
