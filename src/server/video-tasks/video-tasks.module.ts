import { Module } from '@nestjs/common';
import { VideoTasksService } from './video-tasks.service';

@Module({
  providers: [VideoTasksService],
})
export class VideoTasksModule {}
