import { Test, TestingModule } from '@nestjs/testing';
import { VideoTasksService } from './video-tasks.service';

describe('VideoTasksService', () => {
  let service: VideoTasksService;

  beforeEach(async () => {
    const module: TestingModule = await Test.createTestingModule({
      providers: [VideoTasksService],
    }).compile();

    service = module.get<VideoTasksService>(VideoTasksService);
  });

  it('should be defined', () => {
    expect(service).toBeDefined();
  });
});
