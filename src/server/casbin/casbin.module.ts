import { Module } from '@nestjs/common';
import { CasbinService } from './casbin.service';

@Module({
  
  providers: [CasbinService]
})
export class CasbinModule {}
