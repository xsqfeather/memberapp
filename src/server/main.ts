import { NestFactory } from '@nestjs/core';
import { AppModule } from './app.module';
import { SocketIoAdapter } from './common/socket-io.adapter';

import { SwaggerModule, DocumentBuilder } from '@nestjs/swagger';
import { INestApplication, ValidationPipe } from '@nestjs/common';
import { json } from 'body-parser';
import { firmFilter } from './common/firms.filter.middleware';

const dev = process.env.NODE_ENV !== 'production';

const createNextService = async (app: INestApplication) => {
  // 调试模式, 不开启next服务
  if (process.env['JB_INTERPRETER']) return void 0;
  const next = await import('next').then((m) => m.default);
  const server = next({ dev, dir: './src/client' });
  try {
    await server.prepare();
  } catch (error) {
    throw error;
  }
  const handle = server.getRequestHandler();
  const { NextRenderMiddleware } = await import(
    './common/next-render.middleware'
  );
  const render = new NextRenderMiddleware(handle, server);
  app.use(render.use);
};

async function bootstrap() {
  const app = await NestFactory.create(AppModule);
  app.enableCors({
    exposedHeaders: ['Content-Range', 'Authorization', 'X-APP-KEY'],
    allowedHeaders: [
      'Content-Type',
      'Authorization',
      'X-Requested-With',
      'X-APP-KEY',
      'Content-Range',
      'Credentials',
    ],
    origin: '*',
    methods: 'GET,HEAD,PUT,PATCH,POST,DELETE,OPTIONS',
    credentials: true,
    maxAge: 3600,
  });

  app.setGlobalPrefix('api');

  const config = new DocumentBuilder()
    .setTitle('所有Http API文档')
    .setDescription('提供测试和内容')
    .setVersion('2.0')
    .addBearerAuth({ type: 'http', scheme: 'bearer', bearerFormat: 'JWT' })
    .build();
  const document = SwaggerModule.createDocument(app, config);
  SwaggerModule.setup('/docs', app, document);
  app.useWebSocketAdapter(new SocketIoAdapter(app, ['*']));
  app.use(firmFilter);
  await createNextService(app);
  app.use(json({ limit: '500mb' }));
  app.useGlobalPipes(new ValidationPipe());
  console.log(
    `===================进程实例${process.env.NODE_APP_INSTANCE ?? 0}=========`,
  );

  await app.listen(3000 + parseInt(process.env.NODE_APP_INSTANCE || '0'));
}
bootstrap();
