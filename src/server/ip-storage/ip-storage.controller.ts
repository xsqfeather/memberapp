import { Controller, Get, Param, Res } from '@nestjs/common';
import { Response } from 'express';
import { IpStorageService } from './ip-storage.service';

@Controller('ip-storage')
export class IpStorageController {
  @Get(':cid')
  async getIpStorage(@Param('cid') cid: string, @Res() res: Response) {
    for await (const file of IpStorageService.node.get(cid)) {
      if (!file.content) continue;
      for await (const chunk of file.content) {
        if (chunk) {
          res.send(chunk);
          break;
        }
      }
    }
  }
}
