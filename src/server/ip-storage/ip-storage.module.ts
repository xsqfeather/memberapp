import { Module } from '@nestjs/common';
import { IpStorageService } from './ip-storage.service';
import { IpStorageController } from './ip-storage.controller';

@Module({
  controllers: [IpStorageController],
  providers: [IpStorageService],
})
export class IpStorageModule {}
