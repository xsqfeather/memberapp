import { Injectable } from '@nestjs/common';

@Injectable()
export class IpStorageService {
  public static node: any;
  constructor() {}
  async onApplicationBootstrap() {
    console.log('开始启动ipfs节点 ');
    await IpStorageService.initIpfsNode();
  }

  static async initIpfsNode() {
    const IPFS = require('ipfs');
    if (!this.node) {
      try {
        const ipfs = await IPFS.create({
          repo: `./data/ipfsnode${process.env.NODE_APP_INSTANCE?.toString()}`,
        });
        this.node = ipfs;
        const version = await this.node.version();
        console.log('Version:', version.version);
      } catch (error) {
        console.error(error);
      }
    }
    return this.node;
  }
}
