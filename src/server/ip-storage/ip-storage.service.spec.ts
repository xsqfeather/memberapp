import { Test, TestingModule } from '@nestjs/testing';
import { IpStorageService } from './ip-storage.service';

describe('IpStorageService', () => {
  let service: IpStorageService;

  beforeEach(async () => {
    const module: TestingModule = await Test.createTestingModule({
      providers: [IpStorageService],
    }).compile();

    service = module.get<IpStorageService>(IpStorageService);
  });

  it('should be defined', () => {
    expect(service).toBeDefined();
  });
});
