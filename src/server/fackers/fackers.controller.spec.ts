import { Test, TestingModule } from '@nestjs/testing';
import { FackersController } from './fackers.controller';

describe('FackersController', () => {
  let controller: FackersController;

  beforeEach(async () => {
    const module: TestingModule = await Test.createTestingModule({
      controllers: [FackersController],
    }).compile();

    controller = module.get<FackersController>(FackersController);
  });

  it('should be defined', () => {
    expect(controller).toBeDefined();
  });
});
