import { HttpService, Injectable } from '@nestjs/common';
import { generateRandomChineseName } from 'random-chinese-name-generator';

@Injectable()
export class FackersService {
  constructor(private httpService: HttpService) {}
  async fackerOneUser() {
    let userCard: any = null;
    var Faker = require('faker-zh-cn');
    userCard = Faker.Helpers.createCard();

    userCard.nickname = generateRandomChineseName();

    // const avatarApi =  this.httpService.get("http://api.btstu.cn/sjtx/api.php?lx=c1&format=json");
    // avatarApi.subscribe({
    //     next: () =>
    // })
    // userCard.avatar = avatarApi.data.imgurl;

    return userCard;
  }
}
