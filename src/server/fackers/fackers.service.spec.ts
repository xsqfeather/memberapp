import { Test, TestingModule } from '@nestjs/testing';
import { FackersService } from './fackers.service';

describe('FackersService', () => {
  let service: FackersService;

  beforeEach(async () => {
    const module: TestingModule = await Test.createTestingModule({
      providers: [FackersService],
    }).compile();

    service = module.get<FackersService>(FackersService);
  });

  it('should be defined', () => {
    expect(service).toBeDefined();
  });
});
