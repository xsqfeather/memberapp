import { HttpModule, Module } from '@nestjs/common';
import { FackersController } from './fackers.controller';
import { FackersService } from './fackers.service';

@Module({
  imports: [HttpModule],
  providers: [FackersService],
  controllers: [FackersController],
  exports: [FackersService],
})
export class FackersModule {}
