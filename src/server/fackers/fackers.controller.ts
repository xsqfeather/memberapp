import { Controller, Get } from '@nestjs/common';
import { FackersService } from './fackers.service';
import { generateRandomChineseName } from 'random-chinese-name-generator';
@Controller('fackers')
export class FackersController {
  constructor(private readonly fackersService: FackersService) {}

  @Get('one_user')
  findOne() {
    return this.fackersService.fackerOneUser();
  }
}
