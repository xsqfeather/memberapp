import { Module } from '@nestjs/common';
import { ExamRecordsService } from './exam-records.service';
import { ExamRecordsController } from './exam-records.controller';
import { TypeOrmModule } from '@nestjs/typeorm';
import { ExamRecord } from './entities/exam-record.entity';
import { Exam } from '../exams/entities/exam.entity';
import { LessonNode } from '../lesson-nodes/entities/lesson-node.entity';

@Module({
  imports: [TypeOrmModule.forFeature([ExamRecord, Exam, LessonNode])],
  controllers: [ExamRecordsController],
  providers: [ExamRecordsService],
  exports: [ExamRecordsService],
})
export class ExamRecordsModule {}
