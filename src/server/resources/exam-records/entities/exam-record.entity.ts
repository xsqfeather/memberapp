import {
  Column,
  Entity,
  JoinColumn,
  ManyToOne,
  PrimaryGeneratedColumn,
} from 'typeorm';
import { User } from '../../users/entities/user.entity';
import { Exam } from '../../exams/entities/exam.entity';
import { LessonNode } from '../../lesson-nodes/entities/lesson-node.entity';

@Entity()
export class ExamRecord {
  @PrimaryGeneratedColumn()
  id: number;

  @ManyToOne(() => User)
  @JoinColumn({
    name: 'userId',
  })
  user: User;

  @Column()
  userId: number;

  @ManyToOne(() => LessonNode)
  @JoinColumn({
    name: 'lessonNodeId',
  })
  lessonNode: LessonNode;

  @Column()
  lessonNodeId: number;

  @Column({ type: 'jsonb' })
  answers: any[];
}
