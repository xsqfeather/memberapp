import {
  Controller,
  Post,
  Body,
  Patch,
  Get,
  ParseIntPipe,
  Query,
  UseGuards,
} from '@nestjs/common';
import { ExamRecordsService } from './exam-records.service';
import { ExamRecordDto } from './dto/exam-record.dto';
import {
  ApiBearerAuth,
  ApiOperation,
  ApiQuery,
  ApiTags,
} from '@nestjs/swagger';
import { JwtAuthGuard } from '../../auth/jwt-auth.guard';
import { User } from '../../decorators/User';

@ApiTags('测验记录ExamRecords')
@Controller('exam-records')
@UseGuards(JwtAuthGuard)
@ApiBearerAuth()
export class ExamRecordsController {
  constructor(private readonly examRecordsService: ExamRecordsService) {}

  @ApiOperation({ summary: '创建测验记录' })
  @Post()
  create(@Body() examRecordDto: ExamRecordDto, @User('userId') userId: number) {
    return this.examRecordsService.create(userId, examRecordDto);
  }

  @ApiOperation({ summary: '更新测验记录' })
  @Patch()
  updateRecord(
    @Body() examRecordDto: ExamRecordDto,
    @User('userId') userId: number,
  ) {
    return this.examRecordsService.update(userId, examRecordDto);
  }

  @ApiOperation({ summary: '根据章节ID查询测验记录' })
  @Get('/find-by-lesson-node')
  @ApiQuery({ name: 'lessonNodeId', description: '章节ID' })
  findByLessonNode(
    @User('userId') userId: number,
    @Query('lessonNodeId', ParseIntPipe) lessonNodeId: number,
  ) {
    return this.examRecordsService.findByLessonNode(userId, lessonNodeId);
  }
  @ApiOperation({ summary: '根据星球ID查询测验记录' })
  @Get('/find-by-planet')
  @ApiQuery({ name: 'planet', description: '星球ID' })
  findByPlanet(
    @User('userId') userId: number,
    @Query('planet', ParseIntPipe) planet: number,
  ) {
    return this.examRecordsService.findByPlanet(userId, planet);
  }
}
