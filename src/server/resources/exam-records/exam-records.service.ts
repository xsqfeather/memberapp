import { Injectable } from '@nestjs/common';
import { ExamRecordDto } from './dto/exam-record.dto';
import { InjectRepository } from '@nestjs/typeorm';
import { ExamRecord } from './entities/exam-record.entity';
import { Repository, In } from 'typeorm';
import { Lesson } from '../lessons/entities/lesson.entity';
import { LessonNode } from '../lesson-nodes/entities/lesson-node.entity';
import { Exam } from '../exams/entities/exam.entity';

@Injectable()
export class ExamRecordsService {
  constructor(
    @InjectRepository(ExamRecord)
    private readonly recordRepository: Repository<ExamRecord>,
    @InjectRepository(Exam)
    private readonly examRepository: Repository<Exam>,
    @InjectRepository(LessonNode)
    private readonly lessonNodeRepository: Repository<LessonNode>,
  ) {}
  async create(userId: number, examRecordDto: ExamRecordDto) {
    const result = await this.recordRepository.create({
      ...examRecordDto,
      userId,
    });
    await this.recordRepository.save(result);
    return result.id;
  }

  async update(userId: number, examRecordDto: ExamRecordDto) {
    const result = await this.recordRepository.update(
      {
        userId,
        lessonNodeId: examRecordDto.lessonNodeId,
      },
      examRecordDto,
    );
    return { success: result.affected > 0 };
  }

  async findByLessonNode(userId: number, lessonNodeId: number) {
    return await this.recordRepository.findOne({
      userId,
      lessonNodeId,
    });
  }
  async findByPlanet(userId: number, planetId: number) {
    const qb = this.lessonNodeRepository.createQueryBuilder('lesson_node');
    const lessonNodes = await qb
      .where(
        `lesson_node.lessonId IN ${qb
          .subQuery()
          .select('lesson.id')
          .from(Lesson, 'lesson')
          .where(`lesson.planetId = :planetId`, { planetId })
          .getQuery()}`,
      )
      .getMany();
    const lessonsNodeIds = lessonNodes.map((it) => it.id);
    const records = await this.recordRepository.find({
      lessonNodeId: In(lessonsNodeIds),
    });
    const exams = await this.examRepository.find({
      lessonNodeId: In(lessonsNodeIds),
    });
    return { exams, records };
  }
  async calculateCredits(userId: number, planetId: number) {
    const { exams, records } = await this.findByPlanet(userId, planetId);
    return records.reduce((credits, it) => {
      const questions = exams
        .filter((v) => v.lessonNodeId === it.lessonNodeId)
        .sort((a, b) => a.order - b.order);
      let total = 0;
      for (const question of questions) {
        const answer = it.answers[questions.indexOf(question)];
        switch (question.type) {
          case 'filling-up':
          case 'multiple-choice':
            if (
              String(question.answer)
                .split(/[,，]/)
                .map((key) => key.trim())
                .every((key) => String(answer).includes(key))
            ) {
              total += question.score;
            }
            break;
          case 'judgment':
          case 'single-choice':
            if (question.answer === answer) total += question.score;
            break;
        }
      }
      return credits + total;
    }, 0);
  }
}
