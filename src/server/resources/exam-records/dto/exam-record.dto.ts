import { ApiProperty } from '@nestjs/swagger';

export class ExamRecordDto {
  @ApiProperty({ name: 'lessonNodeId', description: '测验章节ID' })
  lessonNodeId: number;

  @ApiProperty({ name: 'answers', description: '答案', type: Array })
  answers: Array<any>;
}
