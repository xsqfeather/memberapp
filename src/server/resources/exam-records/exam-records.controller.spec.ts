import { Test, TestingModule } from '@nestjs/testing';
import { ExamRecordsController } from './exam-records.controller';
import { ExamRecordsService } from './exam-records.service';

describe('ExamRecordsController', () => {
  let controller: ExamRecordsController;

  beforeEach(async () => {
    const module: TestingModule = await Test.createTestingModule({
      controllers: [ExamRecordsController],
      providers: [ExamRecordsService],
    }).compile();

    controller = module.get<ExamRecordsController>(ExamRecordsController);
  });

  it('should be defined', () => {
    expect(controller).toBeDefined();
  });
});
