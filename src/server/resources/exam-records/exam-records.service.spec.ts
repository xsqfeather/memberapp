import { Test, TestingModule } from '@nestjs/testing';
import { ExamRecordsService } from './exam-records.service';

describe('ExamRecordsService', () => {
  let service: ExamRecordsService;

  beforeEach(async () => {
    const module: TestingModule = await Test.createTestingModule({
      providers: [ExamRecordsService],
    }).compile();

    service = module.get<ExamRecordsService>(ExamRecordsService);
  });

  it('should be defined', () => {
    expect(service).toBeDefined();
  });
});
