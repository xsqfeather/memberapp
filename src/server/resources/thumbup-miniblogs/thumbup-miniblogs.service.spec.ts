import { Test, TestingModule } from '@nestjs/testing';
import { ThumbupMiniblogsService } from './thumbup-miniblogs.service';

describe('ThumbupMiniblogsService', () => {
  let service: ThumbupMiniblogsService;

  beforeEach(async () => {
    const module: TestingModule = await Test.createTestingModule({
      providers: [ThumbupMiniblogsService],
    }).compile();

    service = module.get<ThumbupMiniblogsService>(ThumbupMiniblogsService);
  });

  it('should be defined', () => {
    expect(service).toBeDefined();
  });
});
