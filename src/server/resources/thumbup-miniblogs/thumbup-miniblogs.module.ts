import { Module } from '@nestjs/common';
import { ThumbupMiniblogsService } from './thumbup-miniblogs.service';
import { ThumbupMiniblogsController } from './thumbup-miniblogs.controller';

@Module({
  controllers: [ThumbupMiniblogsController],
  providers: [ThumbupMiniblogsService],
})
export class ThumbupMiniblogsModule {}
