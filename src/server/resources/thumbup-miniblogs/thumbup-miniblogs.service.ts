import { Injectable } from '@nestjs/common';
import { CreateThumbupMiniblogDto } from './dto/create-thumbup-miniblog.dto';
import { UpdateThumbupMiniblogDto } from './dto/update-thumbup-miniblog.dto';

@Injectable()
export class ThumbupMiniblogsService {
  create(createThumbupMiniblogDto: CreateThumbupMiniblogDto) {
    return 'This action adds a new thumbupMiniblog';
  }

  findAll() {
    return `This action returns all thumbupMiniblogs`;
  }

  findOne(id: number) {
    return `This action returns a #${id} thumbupMiniblog`;
  }

  update(id: number, updateThumbupMiniblogDto: UpdateThumbupMiniblogDto) {
    return `This action updates a #${id} thumbupMiniblog`;
  }

  remove(id: number) {
    return `This action removes a #${id} thumbupMiniblog`;
  }
}
