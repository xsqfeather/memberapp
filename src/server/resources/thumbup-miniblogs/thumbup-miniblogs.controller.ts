import {
  Controller,
  Get,
  Post,
  Body,
  Patch,
  Param,
  Delete,
} from '@nestjs/common';
import { ThumbupMiniblogsService } from './thumbup-miniblogs.service';
import { CreateThumbupMiniblogDto } from './dto/create-thumbup-miniblog.dto';
import { UpdateThumbupMiniblogDto } from './dto/update-thumbup-miniblog.dto';

@Controller('thumbup-miniblogs')
export class ThumbupMiniblogsController {
  constructor(
    private readonly thumbupMiniblogsService: ThumbupMiniblogsService,
  ) {}

  @Post()
  create(@Body() createThumbupMiniblogDto: CreateThumbupMiniblogDto) {
    return this.thumbupMiniblogsService.create(createThumbupMiniblogDto);
  }

  @Get()
  findAll() {
    return this.thumbupMiniblogsService.findAll();
  }

  @Get(':id')
  findOne(@Param('id') id: string) {
    return this.thumbupMiniblogsService.findOne(+id);
  }

  @Patch(':id')
  update(
    @Param('id') id: string,
    @Body() updateThumbupMiniblogDto: UpdateThumbupMiniblogDto,
  ) {
    return this.thumbupMiniblogsService.update(+id, updateThumbupMiniblogDto);
  }

  @Delete(':id')
  remove(@Param('id') id: string) {
    return this.thumbupMiniblogsService.remove(+id);
  }
}
