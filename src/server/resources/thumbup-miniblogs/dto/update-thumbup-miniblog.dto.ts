import { PartialType } from '@nestjs/swagger';
import { CreateThumbupMiniblogDto } from './create-thumbup-miniblog.dto';

export class UpdateThumbupMiniblogDto extends PartialType(
  CreateThumbupMiniblogDto,
) {}
