import { Test, TestingModule } from '@nestjs/testing';
import { ThumbupMiniblogsController } from './thumbup-miniblogs.controller';
import { ThumbupMiniblogsService } from './thumbup-miniblogs.service';

describe('ThumbupMiniblogsController', () => {
  let controller: ThumbupMiniblogsController;

  beforeEach(async () => {
    const module: TestingModule = await Test.createTestingModule({
      controllers: [ThumbupMiniblogsController],
      providers: [ThumbupMiniblogsService],
    }).compile();

    controller = module.get<ThumbupMiniblogsController>(
      ThumbupMiniblogsController,
    );
  });

  it('should be defined', () => {
    expect(controller).toBeDefined();
  });
});
