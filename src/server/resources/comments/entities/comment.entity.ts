import {
  Column,
  CreateDateColumn,
  DeleteDateColumn,
  Entity,
  JoinColumn,
  ManyToOne,
  PrimaryGeneratedColumn,
  UpdateDateColumn,
} from 'typeorm';
import { User } from '../../users/entities/user.entity';
import { MiniBlog } from '../../mini-blog/entities/mini-blog.entity';

@Entity()
export class Comment {
  @PrimaryGeneratedColumn()
  id: number;

  @ManyToOne(() => User)
  @JoinColumn({ name: 'uid' })
  user: User;

  @Column({ comment: '所属用户ID' })
  uid: number;

  @ManyToOne(() => MiniBlog)
  @JoinColumn({ name: 'tid' })
  post: MiniBlog;

  @Column({ comment: '帖子ID' })
  tid: number;

  @ManyToOne(() => Comment)
  @JoinColumn({ name: 'rid' })
  reply: Comment[];

  @Column({ comment: '回复评论ID', nullable: true })
  rid?: number;

  @Column()
  content: string;

  @CreateDateColumn()
  createdAt: Date;

  @UpdateDateColumn()
  updatedAt: Date;

  @DeleteDateColumn()
  deletedAt: Date;
}
