import { Injectable } from '@nestjs/common';
import { CreateCommentDto } from './dto/create-comment.dto';
import { UpdateCommentDto } from './dto/update-comment.dto';
import { InjectRepository } from '@nestjs/typeorm';
import { Comment } from './entities/comment.entity';
import { Repository } from 'typeorm';
import { MiniBlog } from '../mini-blog/entities/mini-blog.entity';
import { EventEmitter2 } from '@nestjs/event-emitter';
import { REFRESH_COMMENTS } from '../../common/Emitters';

@Injectable()
export class CommentsService {
  constructor(
    private eventEmitter: EventEmitter2,
    @InjectRepository(Comment)
    private readonly commentRepository: Repository<Comment>,
    @InjectRepository(MiniBlog)
    private readonly postRepository: Repository<MiniBlog>,
  ) {}
  async create(userId: number, { postId, replyId, content }: CreateCommentDto) {
    const qr = this.postRepository
      .createQueryBuilder()
      .connection.createQueryRunner();
    await qr.connect();
    await qr.startTransaction();
    try {
      const exam = this.commentRepository.create({
        uid: userId,
        tid: postId,
        rid: replyId,
        content,
      });
      await this.commentRepository.save(exam);
      await qr.manager.increment(MiniBlog, { id: postId }, 'commentCount', 1);
      await qr.commitTransaction();
      this.eventEmitter.emit(REFRESH_COMMENTS, {
        postId: postId,
      });
      return { success: true, id: exam.id };
    } catch (e) {
      console.log('创建评论失败', e);
      await qr.rollbackTransaction();
      return { success: false, id: null };
    } finally {
      await qr.release();
    }
  }

  async findAll(postId: number) {
    return await this.commentRepository
      .findAndCount({
        where: {
          tid: postId,
        },
        order: {
          createdAt: 'ASC',
        },
        relations: ['user', 'post'],
      })
      .then(([comments, total]) => ({
        results: comments,
        total,
      }));
  }

  async findOne(id: number) {
    return await this.commentRepository.findOne({
      where: { id },
      relations: ['user', 'post'],
    });
  }

  async update(id: number, { content }: UpdateCommentDto) {
    const result = await this.commentRepository.update(
      {
        id,
      },
      {
        content,
      },
    );
    return { success: result.affected > 0 };
  }

  async remove(id: number) {
    const qr = this.postRepository
      .createQueryBuilder()
      .connection.createQueryRunner();
    await qr.connect();
    await qr.startTransaction();
    const record = await this.commentRepository.findOne({ id });
    try {
      const re = await this.commentRepository.softDelete({ id });
      if (re.affected === 0) {
        await qr.rollbackTransaction();
        return { success: false };
      }
      await qr.manager.decrement(
        MiniBlog,
        { id: record.tid },
        'commentCount',
        1,
      );
      await qr.commitTransaction();
      this.eventEmitter.emit(REFRESH_COMMENTS, {
        postId: id,
      });
      return { success: true };
    } catch (e) {
      await qr.rollbackTransaction();
      return { success: false };
    } finally {
      await qr.release();
    }
  }

  async count(postId: number) {
    return this.commentRepository.count({ where: { tid: postId } });
  }
}
