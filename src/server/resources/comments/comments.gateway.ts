import {
  ConnectedSocket,
  MessageBody,
  SubscribeMessage,
  WebSocketGateway,
  WebSocketServer,
} from '@nestjs/websockets';

import { Server, Socket } from 'socket.io';
import { OnEvent } from '@nestjs/event-emitter';

import { REFRESH_COMMENTS } from '../../common/Emitters';
import { UseGuards } from '@nestjs/common';
import { JwtSocketAuthGuard } from '../../auth/jwt-socket-auth.guard';

@WebSocketGateway({ namespace: '/comments' })
export class CommentsGateway {
  @WebSocketServer()
  server: Server;

  @UseGuards(JwtSocketAuthGuard)
  @SubscribeMessage('SOCKET_ASSIGN')
  alignSocket(@ConnectedSocket() socket: Socket, @MessageBody() id: string) {
    return JwtSocketAuthGuard.alignSocket(socket, id, 'mini-blog');
  }

  @OnEvent(REFRESH_COMMENTS)
  async notify({ postId }: { postId: number }) {
    this.server
      .to(`mini-blog:${postId}`)
      .emit(REFRESH_COMMENTS, { pid: postId });
  }
}
