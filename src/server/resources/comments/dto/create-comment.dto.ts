import { ApiProperty } from '@nestjs/swagger';
import { IsInt, IsOptional, IsString } from 'class-validator';

export class CreateCommentDto {
  @ApiProperty({ name: 'postId', description: '文章ID' })
  @IsInt()
  postId: number;

  @ApiProperty({ name: 'replyId', description: '回复评论ID', required: false })
  @IsOptional()
  @IsInt()
  replyId?: number;

  @ApiProperty({ name: 'content', description: '内容' })
  @IsString()
  content: string;
}
