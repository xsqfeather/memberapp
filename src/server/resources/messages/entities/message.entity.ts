import {
  Column,
  CreateDateColumn,
  DeleteDateColumn,
  Entity,
  JoinColumn,
  ManyToOne,
  PrimaryGeneratedColumn,
  UpdateDateColumn,
} from 'typeorm';
import { User } from '../../users/entities/user.entity';

@Entity()
export class Message {
  @PrimaryGeneratedColumn()
  id: number;

  @ManyToOne(() => User, (user) => user.messages)
  @JoinColumn({
    name: 'userId',
  })
  user: User;

  @Column({
    nullable: true,
  })
  userId: number;

  @Column({
    nullable: true,
  })
  title: string;

  @Column({
    nullable: true,
  })
  avatar: string;

  @Column({
    nullable: true,
  })
  fromUserId: number;

  @Column({
    default: false,
  })
  fromSystem: boolean;

  @Column()
  content: string;

  @Column()
  contentType: string;

  @Column({
    default: 0,
  })
  unreadCount: number;

  @CreateDateColumn()
  createdAt: Date;

  @Column({
    nullable: true,
  })
  msgType: string;

  @UpdateDateColumn()
  updatedAt: Date;

  @DeleteDateColumn()
  deletedAt: Date;
}
