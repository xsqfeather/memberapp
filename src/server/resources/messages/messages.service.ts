import { Injectable } from '@nestjs/common';
import { EventEmitter2, OnEvent } from '@nestjs/event-emitter';
import { InjectRepository } from '@nestjs/typeorm';
import { Repository } from 'typeorm';
import {
  REFRESH_MESSAGE_LIST,
  REFRESH_UNREAD_COUNT,
} from '../../common/Emitters';
import { UsersService } from '../users/users.service';
import { CreateMessageDto } from './dto/create-message.dto';
import { UpdateMessageDto } from './dto/update-message.dto';
import { Message } from './entities/message.entity';

@Injectable()
export class MessagesService {
  constructor(
    @InjectRepository(Message)
    private messagesRepository: Repository<Message>,
    private usersService: UsersService,
    private eventEmitter: EventEmitter2,
  ) {}
  create(createMessageDto: CreateMessageDto) {
    return 'This action adds a new message';
  }

  async findAll(userId: number) {
    const messages = await this.messagesRepository.find({
      where: {
        userId,
      },
      order: {
        updatedAt: 'DESC',
      },
    });
    return messages;
  }

  findOne(id: number) {
    return `This action returns a #${id} message`;
  }

  async update(id: number, updateMessageDto: UpdateMessageDto) {
    let message = await this.messagesRepository.findOne(id);

    if ((message && updateMessageDto.unreadCount !== undefined) || null) {
      message.unreadCount = updateMessageDto.unreadCount;
      await this.messagesRepository.save(message);
      this.eventEmitter.emit(REFRESH_MESSAGE_LIST, message.userId);
      this.eventEmitter.emit(REFRESH_UNREAD_COUNT, message.userId);
    }
    return message;
  }

  remove(id: number) {
    return `This action removes a #${id} message`;
  }

  async countUnread(userId: number) {
    const sum = await this.messagesRepository
      .createQueryBuilder('message')
      .select('SUM(message.unreadCount)', 'sum')
      .where('message.userId = :userId', { userId })
      .getRawOne();
    return sum;
  }

  async createOrUpdateChatMessage({
    fromUserId,
    content,
    userId,
    contentType,
  }) {
    const fromUser = await this.usersService.findOne(fromUserId);
    let message = await this.messagesRepository.findOne({
      where: {
        fromUserId,
        userId,
        contentType: 'fromUser',
      },
    });
    let toUserMessage = await this.messagesRepository.findOne({
      where: {
        fromUserId: userId,
        userId: fromUserId,
        contentType: 'fromUser',
      },
    });

    if (toUserMessage) {
      toUserMessage.content = content;
      toUserMessage.msgType = contentType;
      await this.messagesRepository.save(toUserMessage);
    }
    if (message) {
      message.content = content;
      message.unreadCount = message.unreadCount + 1;
      message.msgType = contentType;
      message.title = fromUser.nickname;
      message.avatar = fromUser.profile.avatar;
      //更新另外一面的内容
      await this.messagesRepository.save(message);
      this.eventEmitter.emit(REFRESH_MESSAGE_LIST, fromUserId);
      this.eventEmitter.emit(REFRESH_MESSAGE_LIST, userId);
      this.eventEmitter.emit(REFRESH_UNREAD_COUNT, message.userId);
      return message;
    }
    message = this.messagesRepository.create({
      fromUserId,
      content,
      userId,
      contentType: 'fromUser',
      msgType: contentType,
      unreadCount: 1,
      title: fromUser.nickname,
      avatar: fromUser.profile.avatar,
    });
    toUserMessage = this.messagesRepository.create({
      fromUserId: userId,
      content,
      userId: fromUserId,
      contentType: 'fromUser',
      unreadCount: 0,
      msgType: contentType,
      title: fromUser.nickname,
      avatar: fromUser.profile.avatar,
    });
    await this.messagesRepository.save(message);
    await this.messagesRepository.save(toUserMessage);
    this.eventEmitter.emit(REFRESH_MESSAGE_LIST, fromUserId);
    this.eventEmitter.emit(REFRESH_MESSAGE_LIST, userId);
    this.eventEmitter.emit(REFRESH_UNREAD_COUNT, message.userId);
    return message;
  }
}
