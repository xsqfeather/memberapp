import {
  WebSocketGateway,
  SubscribeMessage,
  MessageBody,
  ConnectedSocket,
  WebSocketServer,
} from '@nestjs/websockets';
import { MessagesService } from './messages.service';
import { CreateMessageDto } from './dto/create-message.dto';
import { UpdateMessageDto } from './dto/update-message.dto';
import { OnEvent } from '@nestjs/event-emitter';
import {
  REFRESH_MESSAGE_LIST,
  REFRESH_UNREAD_COUNT,
} from '../../common/Emitters';
import { Server } from 'socket.io';

@WebSocketGateway()
export class MessagesGateway {
  @WebSocketServer()
  server: Server;
  constructor(private readonly messagesService: MessagesService) {}

  @SubscribeMessage('unreadCount')
  async create(@MessageBody() unreadCount: { userId: number }) {
    const { sum } = await this.messagesService.countUnread(unreadCount.userId);
    return {
      count: sum,
    };
  }

  @SubscribeMessage('testMsg')
  testMsg(@MessageBody() createMessageDto: CreateMessageDto) {
    console.log({ createMessageDto });
    if (!createMessageDto.id) {
      return {
        code: 'id not found',
      };
    }
    const returned = {
      ...createMessageDto,
      returned: 1,
    };
    return returned;
  }

  @SubscribeMessage('findOneMessage')
  findOne(@MessageBody() id: number) {
    return this.messagesService.findOne(id);
  }

  @SubscribeMessage('updateMessage')
  update(@MessageBody() updateMessageDto: UpdateMessageDto) {
    return this.messagesService.update(updateMessageDto.id, updateMessageDto);
  }

  @OnEvent(REFRESH_UNREAD_COUNT)
  async refreshUnreadCount(userId: number) {
    console.log('触发统计');

    const { sum } = await this.messagesService.countUnread(userId);
    this.server.emit(`REFRESH_UNREAD_COUNT_${userId}`, { count: sum });
  }

  @OnEvent(REFRESH_MESSAGE_LIST)
  async refreshMessageList(userId: number) {
    this.server.emit(`REFRESH_MESSAGE_LIST_${userId}`);
  }
}
