import { Column, Entity, PrimaryGeneratedColumn } from 'typeorm';

@Entity()
export class TeamCondition {
  @PrimaryGeneratedColumn()
  id: number;

  @Column()
  gender: number;

  @Column()
  area: string;

  @Column()
  teamSize: number;
}

@Entity()
export class TeamRequest {
  
  @PrimaryGeneratedColumn()
  id: number;

  userId: string;
  conditionId: number;
  selfConditionId: number;
  //是否完成
  isMeet: boolean;
  teamSize: number;
  teamSizeRequired: number;
  selfRole: string;

  rolesNeeds: string[];
  teamUUID: string;
}
