import { Injectable } from '@nestjs/common';
import { EventEmitter2, OnEvent } from '@nestjs/event-emitter';
import { LokiService } from '../../loki/loki.service';
import { CreateTeamRequestDto } from './dto/create-team-request.dto';
import { UpdateTeamRequestDto } from './dto/update-team-request.dto';
import { TeamRequest } from './entities/team-request.entity';

@Injectable()
export class TeamRequestsService {
  private collection: any = null;
  constructor(private eventEmitter: EventEmitter2) {}

  onApplicationBootstrap() {
    const loki = new LokiService();
    this.collection = loki.collection('team-requests');
  }
  create(createTeamRequestDto: CreateTeamRequestDto) {
    //开始发起组队请求
    const myRequest = new TeamRequest();
    const selfConditon = new TeamRequest();
    //执行emitter
    this.eventEmitter.emit('BEGIN_MEET_TEAM_REQUEST', myRequest, selfConditon);
  }

  findAll() {
    return `This action returns all teamRequests`;
  }

  findOne(id: number) {
    return `This action returns a #${id} teamRequest`;
  }

  update(id: number, updateTeamRequestDto: UpdateTeamRequestDto) {
    return `This action updates a #${id} teamRequest`;
  }

  remove(id: number) {
    return `This action removes a #${id} teamRequest`;
  }

  async judgeRequestMeet(myR: TeamRequest) {
    const condition = {
      selfConditionId: myR.conditionId,
      conditionId: myR.selfConditionId,
      teamsize: {
        '$lt': myR.teamSizeRequired
      }
    }
    //找到请求，并且锁定
    let mayParter: TeamRequest = await this.collection.findOne({
      ...condition,
      isMeet: true,
    });
    if(mayParter){
      //假如被人匹配了就直接返回
      return mayParter;
    }
    if(!mayParter){
      mayParter = await this.collection.findOne({
        ...condition,
        isMeet: false,
      });
      // await this.collection.update(mayParter);
    }

    return mayParter || null;
  }

  @OnEvent('BEGIN_MEET_TEAM_REQUEST')
  keepJudge(myR: TeamRequest) {
    let loopCount = 0;
    let roleNeeded = myR.rolesNeeds;
    let team = [myR];
    const keeper = setInterval(async () => {
      loopCount++;
      if(loopCount === 100){
        //如果10s匹配不上就终止提醒用户重试,
        clearInterval(keeper);
        //删除涉及到的所有请求 todo
      }
      const partner: TeamRequest =  await this.judgeRequestMeet(myR);
      //满足但是没有锁定
      if (partner && partner.isMeet === false) {
        if(roleNeeded.length === 0){
          //所有角色匹配成功，能够接受任意角色了
          partner.isMeet = true;
          team.push(partner);
        }
        if(myR.rolesNeeds.includes(partner.selfRole)){
          roleNeeded = roleNeeded.filter(item => item!==partner.selfRole);
          team.push(partner);
        }
      }
      
      if (team.length === myR.teamSize) {
        //给出队伍uuid,
        await this.collection.insert(
          team.map(item => ({...item, teamUUID: "nouuid"}))
        ) 
        //通知用户匹配完成
        clearInterval(keeper);
      }

    }, 100);
  }
}
