import { Module } from '@nestjs/common';
import { TeamRequestsService } from './team-requests.service';
import { TeamRequestsController } from './team-requests.controller';
import { TypeOrmModule } from '@nestjs/typeorm';
import { TeamCondition, TeamRequest } from './entities/team-request.entity';

@Module({
  imports: [TypeOrmModule.forFeature([TeamRequest, TeamCondition])],
  controllers: [TeamRequestsController],
  providers: [TeamRequestsService]
})
export class TeamRequestsModule {}
