import { Controller, Get, Post, Body, Patch, Param, Delete } from '@nestjs/common';
import { TeamRequestsService } from './team-requests.service';
import { CreateTeamRequestDto } from './dto/create-team-request.dto';
import { UpdateTeamRequestDto } from './dto/update-team-request.dto';

@Controller('team-requests')
export class TeamRequestsController {
  constructor(private readonly teamRequestsService: TeamRequestsService) {}

  @Post()
  create(@Body() createTeamRequestDto: CreateTeamRequestDto) {
    return this.teamRequestsService.create(createTeamRequestDto);
  }

  @Get()
  findAll() {
    return this.teamRequestsService.findAll();
  }

  @Get(':id')
  findOne(@Param('id') id: string) {
    return this.teamRequestsService.findOne(+id);
  }

  @Patch(':id')
  update(@Param('id') id: string, @Body() updateTeamRequestDto: UpdateTeamRequestDto) {
    return this.teamRequestsService.update(+id, updateTeamRequestDto);
  }

  @Delete(':id')
  remove(@Param('id') id: string) {
    return this.teamRequestsService.remove(+id);
  }
}
