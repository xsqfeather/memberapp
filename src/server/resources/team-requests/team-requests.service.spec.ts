import { Test, TestingModule } from '@nestjs/testing';
import { TeamRequestsService } from './team-requests.service';

describe('TeamRequestsService', () => {
  let service: TeamRequestsService;

  beforeEach(async () => {
    const module: TestingModule = await Test.createTestingModule({
      providers: [TeamRequestsService],
    }).compile();

    service = module.get<TeamRequestsService>(TeamRequestsService);
  });

  it('should be defined', () => {
    expect(service).toBeDefined();
  });
});
