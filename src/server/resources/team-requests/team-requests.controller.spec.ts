import { Test, TestingModule } from '@nestjs/testing';
import { TeamRequestsController } from './team-requests.controller';
import { TeamRequestsService } from './team-requests.service';

describe('TeamRequestsController', () => {
  let controller: TeamRequestsController;

  beforeEach(async () => {
    const module: TestingModule = await Test.createTestingModule({
      controllers: [TeamRequestsController],
      providers: [TeamRequestsService],
    }).compile();

    controller = module.get<TeamRequestsController>(TeamRequestsController);
  });

  it('should be defined', () => {
    expect(controller).toBeDefined();
  });
});
