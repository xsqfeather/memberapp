import { RoleTag } from '../entities/role-tag.entity';
import { ApiProperty } from '@nestjs/swagger';
import { Exclude, Expose } from 'class-transformer';

@Exclude()
export class ResponseRoleDto implements Partial<RoleTag> {
  constructor(partial: Partial<ResponseRoleDto>) {
    Object.assign(this, partial);
  }
  @ApiProperty({ name: 'id', description: 'ID' })
  @Expose()
  id: number;

  @ApiProperty({ name: 'tagName', description: '标签名' })
  @Expose()
  tagName: string;

  @ApiProperty({ name: 'description', description: '描述' })
  @Expose()
  description: string;
}
