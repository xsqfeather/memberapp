import { CreateRoleTagDto } from './create-role-tag.dto';

export class UpdateRoleTagDto extends CreateRoleTagDto {}
