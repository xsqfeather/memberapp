import { Test, TestingModule } from '@nestjs/testing';
import { RoleTagsService } from './role-tags.service';

describe('RoleTagsService', () => {
  let service: RoleTagsService;

  beforeEach(async () => {
    const module: TestingModule = await Test.createTestingModule({
      providers: [RoleTagsService],
    }).compile();

    service = module.get<RoleTagsService>(RoleTagsService);
  });

  it('should be defined', () => {
    expect(service).toBeDefined();
  });
});
