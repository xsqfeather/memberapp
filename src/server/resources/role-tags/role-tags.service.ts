import { Injectable } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { In, Repository } from 'typeorm';
import { CreateRoleTagDto } from './dto/create-role-tag.dto';
import { UpdateRoleTagDto } from './dto/update-role-tag.dto';
import { RoleTag } from './entities/role-tag.entity';

@Injectable()
export class RoleTagsService {
  constructor(
    @InjectRepository(RoleTag)
    private readonly roleTagsRepository: Repository<RoleTag>,
  ) {}
  async create(createRoleTagDto: CreateRoleTagDto) {
    const { tagName, description } = createRoleTagDto;
    const roleTag = this.roleTagsRepository.create();
    roleTag.tagName = tagName;
    roleTag.description = description;
    await this.roleTagsRepository.save(roleTag);
    return roleTag;
  }

  findOne(id: number) {
    return this.roleTagsRepository.findOne(id);
  }

  findAll({ sort, range, where }) {
    const order = {};
    order[sort[0]] = sort[1];
    return this.roleTagsRepository.findAndCount({
      order,
      skip: range[0],
      take: range[1],
      where,
    });
  }

  async update(id: number, updateDto: UpdateRoleTagDto) {
    const result = await this.roleTagsRepository.update(id, {
      tagName: updateDto.tagName,
      description: updateDto.description,
    });
    return { id, success: result.affected > 0 };
  }
  findMany(ids: number[]) {
    return this.roleTagsRepository.find({
      where: {
        id: In(ids),
      },
    });
  }
  remove(id: number) {
    return this.roleTagsRepository.softDelete(id);
  }

  removeAll({ where }: { where: any }) {
    return this.roleTagsRepository.softDelete({ ...where });
  }
}
