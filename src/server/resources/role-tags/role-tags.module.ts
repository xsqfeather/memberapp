import { Module } from '@nestjs/common';
import { RoleTagsService } from './role-tags.service';
import { RoleTagsController } from './role-tags.controller';
import { TypeOrmModule } from '@nestjs/typeorm';
import { RoleTag } from './entities/role-tag.entity';

@Module({
  imports: [TypeOrmModule.forFeature([RoleTag])],
  controllers: [RoleTagsController],
  providers: [RoleTagsService],
  exports: [RoleTagsService],
})
export class RoleTagsModule {}
