import { Test, TestingModule } from '@nestjs/testing';
import { RoleTagsController } from './role-tags.controller';
import { RoleTagsService } from './role-tags.service';

describe('RoleTagsController', () => {
  let controller: RoleTagsController;

  beforeEach(async () => {
    const module: TestingModule = await Test.createTestingModule({
      controllers: [RoleTagsController],
      providers: [RoleTagsService],
    }).compile();

    controller = module.get<RoleTagsController>(RoleTagsController);
  });

  it('should be defined', () => {
    expect(controller).toBeDefined();
  });
});
