import {
  Controller,
  Get,
  Post,
  Body,
  Param,
  Delete,
  HttpException,
  HttpStatus,
  Query,
  Put,
  Res,
} from '@nestjs/common';
import { RoleTagsService } from './role-tags.service';
import { CreateRoleTagDto } from './dto/create-role-tag.dto';
import { UpdateRoleTagDto } from './dto/update-role-tag.dto';
import { Response } from 'express';
import { In, Like } from 'typeorm';
import { FindConditions } from 'typeorm/find-options/FindConditions';
import { RoleTag } from './entities/role-tag.entity';
import { ApiTags } from '@nestjs/swagger';

@ApiTags('角色RoleTags')
@Controller('role-tags')
export class RoleTagsController {
  constructor(private readonly roleTagsService: RoleTagsService) {}

  @Post()
  create(@Body() createRoleTagDto: CreateRoleTagDto) {
    return this.roleTagsService.create(createRoleTagDto);
  }

  @Get()
  async findAll(@Res() res: Response, @Query() query: any) {
    const { filter, range, sort } = query;

    let filterObj,
      rangeArr,
      sortArr = {};
    try {
      filterObj = JSON.parse(filter || '{}');
      rangeArr = JSON.parse(range || '[0,9]');
      sortArr = JSON.parse(sort || '["updatedAt", "DESC"]');
    } catch (error) {
      throw new HttpException(
        {
          status: HttpStatus.BAD_REQUEST,
          error: `json parse errors where query one of { filter, range, sort}`,
        },
        HttpStatus.BAD_REQUEST,
      );
    }
    let where: Array<FindConditions<RoleTag>> = [];
    if (filterObj.q) {
      where = [
        { tagName: Like(`%${filterObj.q}%`) },
        { description: Like(`%${filterObj.q}%`) },
      ];
    }
    if (filterObj.id) {
      where.push({
        id: In(filterObj.id.flat()),
      });
    }

    const [roleTags, count]: any = await this.roleTagsService.findAll({
      range: rangeArr,
      sort: sortArr,
      where,
    });
    res.setHeader(
      'Content-Range',
      `role-tags ${rangeArr[0]}-${rangeArr[1]}/${count}`,
    );
    res.send(roleTags);
  }

  @Get(':id')
  findOne(@Param('id') id: string) {
    return this.roleTagsService.findOne(+id);
  }

  @Put(':id')
  update(@Param('id') id: string, @Body() updateMatterDto: UpdateRoleTagDto) {
    return this.roleTagsService.update(+id, updateMatterDto);
  }

  @Delete(':id')
  remove(@Param('id') id: string) {
    return this.roleTagsService.remove(+id);
  }

  @Delete()
  async removeAll(@Query() query: any) {
    const { filter } = query;
    const filterObj = JSON.parse(filter);
    const where = {
      id: In(filterObj.id),
    };
    await this.roleTagsService.removeAll({ where });
    return [...JSON.parse(query.filter).id];
  }
}
