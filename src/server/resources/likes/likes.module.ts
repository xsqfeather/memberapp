import { Module } from '@nestjs/common';
import { LikesService } from './likes.service';
import { LikesController } from './likes.controller';
import { TypeOrmModule } from '@nestjs/typeorm';
import { Like } from './entities/like.entity';
import { UsersModule } from '../users/users.module';
import { MiniBlog } from '../mini-blog/entities/mini-blog.entity';

@Module({
  imports: [TypeOrmModule.forFeature([Like, MiniBlog]), UsersModule],
  controllers: [LikesController],
  providers: [LikesService],
})
export class LikesModule {}
