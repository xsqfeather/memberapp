import { Injectable } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { Repository } from 'typeorm';
import { Like } from './entities/like.entity';
import { MiniBlog } from '../mini-blog/entities/mini-blog.entity';
import { classToPlain } from 'class-transformer';
import { ResponseUserDto } from '../users/dto/response-user.dto';
import { ResponseLikeDto } from './dto/response-like.dto';

@Injectable()
export class LikesService {
  constructor(
    @InjectRepository(Like)
    private readonly likesRepository: Repository<Like>,

    @InjectRepository(MiniBlog)
    private readonly postRepository: Repository<MiniBlog>,
  ) {}
  async like(userId: number, postId: number) {
    const [{ exists }] = await this.likesRepository.manager.query(
      `SELECT EXISTS(${this.likesRepository
        .createQueryBuilder('like')
        .select('*')
        .where(`like.uid = ${userId} AND like.tid = ${postId}`)
        .getSql()})`,
    );
    if (exists) return { success: false };
    const qr = this.postRepository
      .createQueryBuilder()
      .connection.createQueryRunner();
    await qr.connect();
    await qr.startTransaction();
    try {
      const re = this.likesRepository.create({
        uid: userId,
        tid: postId,
      });
      await this.likesRepository.save(re);
      await qr.manager.increment(MiniBlog, { id: postId }, 'likeCount', 1);
      await qr.commitTransaction();
      return { success: true };
    } catch (e) {
      await qr.rollbackTransaction();
      return { success: false };
    } finally {
      await qr.release();
    }
  }
  async unlike(userId: number, postId: number) {
    const qr = this.postRepository
      .createQueryBuilder()
      .connection.createQueryRunner();
    await qr.connect();
    await qr.startTransaction();
    try {
      const re = await this.likesRepository.softDelete({
        uid: userId,
        tid: postId,
      });
      if (re.affected === 0) {
        await qr.rollbackTransaction();
        return { success: false };
      }
      await qr.manager.decrement(MiniBlog, { id: postId }, 'likeCount', 1);
      await qr.commitTransaction();
      return { success: true };
    } catch (e) {
      await qr.rollbackTransaction();
      return { success: false };
    } finally {
      await qr.release();
    }
  }
  async stat(postId: number, limit = 10, offset = 0) {
    const total = await this.likesRepository.count({
      where: {
        tid: postId,
      },
    });
    const results = await this.likesRepository
      .find({
        where: {
          tid: postId,
        },
        order: {
          createdAt: 'DESC',
        },
        relations: ['user'],
        take: limit,
        skip: offset,
      })
      .then((res) =>
        res.map((it) =>
          classToPlain(
            new ResponseLikeDto({
              ...it,
              user: classToPlain(
                new ResponseUserDto(it.user),
              ) as ResponseUserDto,
            }),
          ),
        ),
      );
    return { results, total };
  }
}
