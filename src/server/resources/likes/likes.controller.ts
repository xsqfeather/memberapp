import {
  Controller,
  Get,
  Put,
  Delete,
  UseGuards,
  Query,
  ParseIntPipe,
} from '@nestjs/common';
import { LikesService } from './likes.service';
import { JwtAuthGuard } from '../../auth/jwt-auth.guard';
import {
  ApiBearerAuth,
  ApiOperation,
  ApiQuery,
  ApiResponse,
  ApiTags,
} from '@nestjs/swagger';
import { User } from '../../decorators/User';
import { StatLikeDto } from './dto/stat-like.dto';

@ApiTags('点赞Likes')
@Controller('likes')
export class LikesController {
  constructor(private readonly likesService: LikesService) {}
  @Put()
  @ApiOperation({ summary: '点赞' })
  @UseGuards(JwtAuthGuard)
  @ApiQuery({ name: 'postId', description: '文章ID' })
  @ApiBearerAuth()
  like(
    @User('userId') userId: number,
    @Query('postId', ParseIntPipe) postId: number,
  ) {
    return this.likesService.like(userId, postId);
  }

  @Delete()
  @ApiOperation({ summary: '取消点赞' })
  @UseGuards(JwtAuthGuard)
  @ApiQuery({ name: 'postId', description: '文章ID' })
  @ApiBearerAuth()
  unlike(
    @User('userId') userId: number,
    @Query('postId', ParseIntPipe) postId: number,
  ) {
    return this.likesService.unlike(userId, postId);
  }

  @Get()
  @ApiOperation({ summary: '查询点赞' })
  @ApiQuery({ name: 'postId', description: '文章ID' })
  @ApiResponse({
    status: 200,
    description: '返回前十条点赞人员和点赞数量',
    type: StatLikeDto,
  })
  stat(@Query('postId') postId: number) {
    return this.likesService.stat(postId);
  }
}
