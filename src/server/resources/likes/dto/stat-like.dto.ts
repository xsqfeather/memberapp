import { ApiProperty } from '@nestjs/swagger';
import { ResponseLikeDto } from './response-like.dto';

export class StatLikeDto {
  @ApiProperty({
    name: 'results',
    description: '点赞详情',
    type: Array(ResponseLikeDto),
  })
  results: ResponseLikeDto[];

  @ApiProperty({ name: 'total', description: '总数' })
  total: number;
}
