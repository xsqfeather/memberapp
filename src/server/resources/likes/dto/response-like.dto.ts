import { Like } from '../entities/like.entity';
import { Exclude, Expose } from 'class-transformer';
import { ApiProperty } from '@nestjs/swagger';
import { ResponseUserDto } from '../../users/dto/response-user.dto';

@Exclude()
export class ResponseLikeDto implements Partial<Omit<Like, 'user'>> {
  constructor(partial: Partial<ResponseLikeDto>) {
    Object.assign(this, partial);
  }
  @ApiProperty({ name: 'id', description: '点赞ID' })
  @Expose()
  id: number;

  @ApiProperty({
    name: 'user',
    description: '点赞用户',
    type: ResponseUserDto,
  })
  @Expose()
  user: ResponseUserDto;
}
