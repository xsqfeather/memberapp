import {
  Column,
  CreateDateColumn,
  DeleteDateColumn,
  Entity,
  JoinColumn,
  ManyToOne,
  PrimaryGeneratedColumn,
  UpdateDateColumn,
} from 'typeorm';
import { User } from '../../users/entities/user.entity';
import { MiniBlog } from '../../mini-blog/entities/mini-blog.entity';
import { Exclude } from 'class-transformer';

@Entity()
export class Like {
  @PrimaryGeneratedColumn()
  id: number;

  @ManyToOne(() => MiniBlog)
  @JoinColumn({ name: 'tid' })
  @Column()
  tid: number;

  @ManyToOne(() => User)
  @JoinColumn({ name: 'uid' })
  user: User;
  @Column()
  uid: number;

  @Exclude()
  @CreateDateColumn()
  createdAt: Date;

  @Exclude()
  @UpdateDateColumn()
  updatedAt: Date;

  @Exclude()
  @DeleteDateColumn()
  deletedAt: Date;
}
