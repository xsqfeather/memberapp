import {
  Controller,
  Post,
  UseInterceptors,
  UploadedFiles,
  Headers,
  Body,
} from '@nestjs/common';
import { FilesInterceptor } from '@nestjs/platform-express';
import { ImagesService } from './images.service';
import { renameSync, unlinkSync } from 'fs';
import { Repository } from 'typeorm';
import * as md5 from 'md5';
import { join } from 'path';
import { InjectRepository } from '@nestjs/typeorm';
import { Image } from './entities/image.entity';
import { putImageToQiniuOss } from '../../upload/qiniuup';

interface _File {
  fieldname: 'file';
  originalname: string;
  encoding: string;
  mimetype: string;
  destination: string;
  filename: string;
  path: string;
  size: number;
}

@Controller('images')
export class ImagesController {
  constructor(
    @InjectRepository(Image)
    private imagesRepository: Repository<Image>,
    private readonly imagesService: ImagesService,
  ) {}

  @Post()
  @UseInterceptors(FilesInterceptor('file'))
  async create(
    @UploadedFiles() files: any[],
    //hfileName:flutter 上传添加的头
    @Headers('fileName') hfileName: string,
  ) {

    console.log("=========图片上传方法start==============");
    
    console.log("上传的图片", {files});
    
    const rlts = [] as { url: string; originalname: string }[];
    for (let i = 0; i < files.length; i++) {
      const file = files[i] as _File;

      let cut: string[];
      //newName,md5摘要,用于验证是否已上传
      let newName = '';
      if (hfileName) {
        cut = hfileName.split('.');
        newName = `${md5(`${file.size},${hfileName}`)}.${cut[1]}`;
      } else {
        cut = file.originalname.split('.');
        newName = `${md5(`${file.size},${file.originalname}`)}.${cut[1]}`;
      }

      const newPath = join(file.destination, newName);
      //搜索上传文件是否存在于数据库中
      const img = await this.imagesRepository.findOne({ uniqueName: newName });
      console.log("数据库",{img});
      
      if (img) {
        rlts.push({
          url: img.originUrl,
          originalname: img.originalname,
        });
        unlinkSync(file.path);
      } else {
        renameSync(file.path, newPath);
        try {
          const rlt = await putImageToQiniuOss(newPath);
          console.log("七牛云", {rlt});
          await this.imagesService.create({
            uniqueName: newName,
            originalname: hfileName ? hfileName : file.originalname,
            size: file.size,
            originUrl: rlt.url,
          });
          rlts.push({ url: rlt.url, originalname: file.originalname });
          unlinkSync(newPath);
        } catch (error) {
          console.log(error);
          throw error;
        }
      
       
      }
    }
    console.log("=========图片上传方法end==============");

    return rlts;
  }
}
