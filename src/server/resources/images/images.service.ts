import { Injectable } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { unlinkSync } from 'fs';
import { FindConditions, Repository } from 'typeorm';
import { CreateImageDto } from './dto/create-image.dto';
import { UpdateImageDto } from './dto/update-image.dto';
import { Image } from './entities/image.entity';
import * as OSS from 'ali-oss';

const ossConfig = {
  bucket: 'xindalu-dev',
  region: 'oss-cn-chengdu',
  accessKeyId: 'LTAIBYIHofssXbWt',
  accessKeySecret: 'RkNyIBM8hz6PpjRJ4MFyiRI75VbmPp',
};
const client = new OSS(ossConfig);

@Injectable()
export class ImagesService {
  constructor(
    @InjectRepository(Image)
    private imagesRepository: Repository<Image>,
  ) {}

  async create(createImageDto: CreateImageDto) {
    const img = this.imagesRepository.create(createImageDto);
    await this.imagesRepository.save(img);
    return img;
  }

  findAll({ sort, range, where }) {
    const order = {};
    order[sort[0]] = sort[1];
    return this.imagesRepository.findAndCount({
      order,
      skip: range[0],
      take: range[1] - range[0] + 1,
      where,
    });
  }

  async findOne(id: number | string) {
    return await this.imagesRepository.findOne(id);
  }

  update(id: number, updateImageDto: UpdateImageDto) {
    return `This action updates a #${id} image`;
  }

  async remove(uuid: string) {
    const img = await this.imagesRepository.findOne({
      uuid,
    });
    unlinkSync('./public' + img.originUrl);
    await this.imagesRepository.delete({
      uuid,
    });
    return img;
  }
}
