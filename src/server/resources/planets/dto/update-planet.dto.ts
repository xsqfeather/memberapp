import { OmitType } from '@nestjs/swagger';
import { CreatePlanetDto } from './create-planet.dto';

export class UpdatePlanetDto extends OmitType(CreatePlanetDto, [
  'level',
] as const) {}
