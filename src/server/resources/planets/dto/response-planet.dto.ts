import { Planet } from '../entities/planet.entity';
import { Exclude, Expose } from 'class-transformer';
import { ApiProperty } from '@nestjs/swagger';

@Exclude()
export class ResponsePlanetDto implements Partial<Planet> {
  constructor(partial: Partial<ResponsePlanetDto>) {
    Object.assign(this, partial);
  }

  @ApiProperty({ name: 'id', description: '星球ID' })
  @Expose()
  id: number;

  @ApiProperty({ name: 'level', description: '星球级别' })
  @Expose()
  level: number;

  @ApiProperty({ name: 'name', description: '星球名称' })
  @Expose()
  name: string;

  @ApiProperty({ name: 'limit', description: '升级所需分数' })
  @Expose()
  limit: number;
}
