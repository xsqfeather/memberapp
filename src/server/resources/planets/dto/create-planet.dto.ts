import { ApiProperty } from '@nestjs/swagger';
import { IsInt, IsString } from 'class-validator';

export class CreatePlanetDto {
  @IsString()
  @ApiProperty({ name: 'name', description: '星球名称' })
  name: string;

  @IsInt()
  @ApiProperty({ name: 'limit', description: '星球升级所需分数' })
  limit: number;

  @IsInt()
  @ApiProperty({ name: 'level', description: '星球级别' })
  level: number;
}
