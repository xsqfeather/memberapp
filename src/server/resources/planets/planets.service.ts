import { Injectable } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { Repository } from 'typeorm';
import { CreatePlanetDto } from './dto/create-planet.dto';
import { UpdatePlanetDto } from './dto/update-planet.dto';
import { Planet } from './entities/planet.entity';

@Injectable()
export class PlanetsService {
  constructor(
    @InjectRepository(Planet)
    private readonly planetsRepository: Repository<Planet>,
  ) {}

  async create(createPlanetDto: CreatePlanetDto) {
    const planet = this.planetsRepository.create(createPlanetDto);
    await this.planetsRepository.save(planet);
    return planet;
  }

  findAll({ sort, range, where }) {
    const order = {};
    order[sort[0]] = sort[1];
    return this.planetsRepository.findAndCount({
      order,
      skip: range[0],
      take: range[1] - range[0] + 1,
      where,
    });
  }

  findOne(id: number) {
    return this.planetsRepository.findOne(id);
  }

  findByRoleTagId(roleTagId: number) {
    return this.planetsRepository.find({
      where: {
        roleTagId,
      },
    });
  }

  async update(id: number, updatePlanetDto: UpdatePlanetDto) {
    Reflect.deleteProperty(updatePlanetDto, 'id');
    Reflect.deleteProperty(updatePlanetDto, 'level');
    Reflect.deleteProperty(updatePlanetDto, 'createdAt');
    Reflect.deleteProperty(updatePlanetDto, 'updatedAt');
    Reflect.deleteProperty(updatePlanetDto, 'deletedAt');
    await this.planetsRepository.update({ id }, updatePlanetDto);
    return { success: true };
  }

  remove(id: number) {
    return this.planetsRepository.softDelete(id);
  }

  removeAll({ where }: { where: any }) {
    return this.planetsRepository.softDelete({ ...where });
  }
}
