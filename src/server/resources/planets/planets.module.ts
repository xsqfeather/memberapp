import { Module } from '@nestjs/common';
import { PlanetsService } from './planets.service';
import { PlanetsController } from './planets.controller';
import { TypeOrmModule } from '@nestjs/typeorm';
import { Planet } from './entities/planet.entity';
import { WatchRecordsModule } from '../watch-records/watch-records.module';
import { ExamRecordsModule } from '../exam-records/exam-records.module';

@Module({
  imports: [
    TypeOrmModule.forFeature([Planet]),
    WatchRecordsModule,
    ExamRecordsModule,
  ],
  controllers: [PlanetsController],
  providers: [PlanetsService],
  exports: [PlanetsService],
})
export class PlanetsModule {}
