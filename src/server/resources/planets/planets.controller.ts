import {
  Controller,
  Get,
  Post,
  Body,
  Param,
  Delete,
  Query,
  Res,
  HttpException,
  HttpStatus,
  Put,
  ParseIntPipe,
  UseGuards,
} from '@nestjs/common';
import { PlanetsService } from './planets.service';
import { CreatePlanetDto } from './dto/create-planet.dto';
import { UpdatePlanetDto } from './dto/update-planet.dto';
import { Like } from 'typeorm';
import { Response } from 'express';
import {
  ApiBearerAuth,
  ApiOperation,
  ApiQuery,
  ApiTags,
} from '@nestjs/swagger';
import { WatchRecordsService } from '../watch-records/watch-records.service';
import { ExamRecordsService } from '../exam-records/exam-records.service';
import { JwtAuthGuard } from '../../auth/jwt-auth.guard';
import { User } from '../../decorators/User';

@ApiTags('星球Planets')
@Controller('planets')
export class PlanetsController {
  constructor(
    private readonly planetsService: PlanetsService,
    private readonly watchRecordService: WatchRecordsService,
    private readonly examRecordService: ExamRecordsService,
  ) {}

  @ApiOperation({ summary: '根据用户和星球获取学分' })
  @Get('/credits')
  @UseGuards(JwtAuthGuard)
  @ApiBearerAuth()
  @ApiQuery({ name: 'planetId', description: '星球ID' })
  async getCredits(
    @User('userId') userId: number,
    @Query('planetId', ParseIntPipe) planetId: number,
  ) {
    return (
      (await this.examRecordService.calculateCredits(userId, planetId)) +
      (await this.watchRecordService.calculateCredits(userId, planetId))
    );
  }
  @ApiOperation({ summary: '创建星球' })
  @Post()
  create(@Body() createPlanetDto: CreatePlanetDto) {
    return this.planetsService.create(createPlanetDto);
  }

  @ApiOperation({ summary: '获取所有星球' })
  @Get()
  async findAll(@Res() res: Response, @Query() query: any) {
    const { filter, range, sort } = query;
    let filterObj,
      rangeArr,
      sortArr = {};
    try {
      filterObj = JSON.parse(filter || '{}');
      rangeArr = JSON.parse(range || '[0,9]');
      sortArr = JSON.parse(sort || '["updatedAt", "DESC"]');
    } catch (error) {
      throw new HttpException(
        {
          status: HttpStatus.BAD_REQUEST,
          error: `json parse errors where query one of { filter, range, sort}`,
        },
        HttpStatus.BAD_REQUEST,
      );
    }
    let where: any = {};
    if (filterObj.roleTagId) {
      where.roleTagId = filterObj.roleTagId;
    }
    if (filterObj.q) {
      where = [{ name: Like(`%${filterObj.q}%`) }];
    }
    const [planets, count]: any = await this.planetsService.findAll({
      range: rangeArr,
      sort: sortArr,
      where,
    });
    res.setHeader(
      'Content-Range',
      `planets ${rangeArr[0]}-${rangeArr[1]}/${count}`,
    );
    res.send(planets);
  }

  @ApiOperation({ summary: '更新星球' })
  @Put(':id')
  update(@Param('id') id: number, @Body() updatePlanetDto: UpdatePlanetDto) {
    return this.planetsService.update(+id, updatePlanetDto);
  }

  @ApiOperation({ summary: '删除星球' })
  @Delete(':id')
  remove(@Param('id') id: string) {
    return this.planetsService.remove(+id);
  }
}
