import { Injectable } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { Repository } from 'typeorm';
import { CreateFirmInput } from './dto/create-firm.input';
import { UpdateFirmInput } from './dto/update-firm.input';
import { Firm } from './entities/firm.entity';

import { v4 as uuidv4 } from 'uuid';
import { Contains } from 'class-validator';
import { LevelCacheService } from '../../level-cache/level-cache.service';

@Injectable()
export class FirmsService {
  constructor(
    @InjectRepository(Firm)
    private firmsRepository: Repository<Firm>,
    private levelCacheService: LevelCacheService,
  ) {}
  async create(createFirmInput: CreateFirmInput) {
    const { title, name, description, appModules, slogan } = createFirmInput;

    const firm = this.firmsRepository.create({
      name,
      title,
      description,
      slogan,
      appKey: uuidv4(),
      appModules,
    });
    await this.firmsRepository.save(firm);
    console.log('数据库1次');

    return firm;
  }

  findAll({ sort, range, where }) {
    const order = {};
    order[sort[0]] = sort[1];
    console.log('数据库1次');

    return this.firmsRepository.findAndCount({
      order,
      skip: range[0],
      take: range[1] - range[0] + 1,
      where,
    });
  }

  findOne(id: number) {
    console.log('数据库1次');

    return this.firmsRepository.findOne(id);
  }

  findOneByAppKey(appKey: string) {
    console.log('数据库1次', 'appkey');

    return this.firmsRepository.findOne({
      where: {
        appKey,
      },
    });
  }

  findOneByHost(host: string) {
    console.log('数据库1次', 'host');

    return this.firmsRepository.findOne({
      where: {
        host: Contains(host),
      },
    });
  }

  async findFirmIdByHhost(host: string) {
    const key = host + '-firm-id';
    const firmCacheId = await this.levelCacheService.getItem(key);
    if (firmCacheId) {
      return parseInt(firmCacheId);
    }
    if (!firmCacheId) {
      const firm = await this.findOneByHost(host);
      if (!firm) {
        await this.levelCacheService.setItem(key, '0');
        return 0;
      }
      await this.levelCacheService.setItem(key, firm.id.toString());
      return firm.id;
    }
    return parseInt(firmCacheId);
  }

  update(id: number, updateFirmInput: UpdateFirmInput) {
    return `This action updates a #${id} firm`;
  }

  remove(id: number) {
    return `This action removes a #${id} firm`;
  }

  async findOrCreateDefaultFirm() {
    let firm = await this.firmsRepository.findOne({
      isDefault: true,
    });
    if (!firm) {
      firm = this.firmsRepository.create();
    }
    firm.name = process.env.DEFAULT_FIRM_NAME;
    firm.appKey = process.env.DEFAULT_APP_KEY;
    firm.isDefault = true;

    await this.firmsRepository.save(firm);

    return firm;
  }

  async getDefaultFirmId() {
    const firmCacheId = await this.levelCacheService.getItem('defaultFirmId');

    if (firmCacheId) {
      return parseInt(firmCacheId);
    }
    if (!firmCacheId) {
      const firm = await this.findOrCreateDefaultFirm();
      await this.levelCacheService.setItem('defaultFirmId', firm.id.toString());
      return firm.id;
    }
    console.log({ firmCacheId });

    return parseInt(firmCacheId);
  }

  onModuleInit() {
    this.findOrCreateDefaultFirm();
  }
}
