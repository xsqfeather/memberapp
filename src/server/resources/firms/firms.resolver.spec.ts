import { Test, TestingModule } from '@nestjs/testing';
import { FirmsResolver } from './firms.resolver';
import { FirmsService } from './firms.service';

describe('FirmsResolver', () => {
  let resolver: FirmsResolver;

  beforeEach(async () => {
    const module: TestingModule = await Test.createTestingModule({
      providers: [FirmsResolver, FirmsService],
    }).compile();

    resolver = module.get<FirmsResolver>(FirmsResolver);
  });

  it('should be defined', () => {
    expect(resolver).toBeDefined();
  });
});
