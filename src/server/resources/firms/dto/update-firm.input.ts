import { PartialType } from '@nestjs/swagger';
import { CreateUsersFirmDto } from '../../users-firms/dto/create-users-firm.dto';

export class UpdateFirmInput extends PartialType(CreateUsersFirmDto) {}
