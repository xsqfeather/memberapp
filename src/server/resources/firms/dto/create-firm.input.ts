export class CreateFirmInput {
  name: string;
  title?: string;
  description?: string;
  slogan?: string;
  appModules?: string[];
  homePageType?: string;
}
