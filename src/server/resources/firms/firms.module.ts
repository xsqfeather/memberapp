import { Module } from '@nestjs/common';
import { FirmsService } from './firms.service';
import { FirmsController } from './firms.controller';
import { TypeOrmModule } from '@nestjs/typeorm';
import { Firm } from './entities/firm.entity';
import { LevelCacheModule } from '../../level-cache/level-cache.module';

@Module({
  imports: [TypeOrmModule.forFeature([Firm]), LevelCacheModule],
  providers: [FirmsService],
  controllers: [FirmsController],
  exports: [FirmsService],
})
export class FirmsModule {}
