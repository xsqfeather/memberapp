import { isArray } from 'class-validator';
import { type } from 'os';
import {
  Column,
  Entity,
  Index,
  JoinColumn,
  ManyToOne,
  OneToMany,
  PrimaryGeneratedColumn,
} from 'typeorm';
import { UsersFirm } from '../../users-firms/entities/users-firm.entity';

@Entity()
export class Firm {
  @PrimaryGeneratedColumn()
  id: number;

  @Index()
  @Column()
  name: string;

  @Index()
  @Column({ nullable: true })
  title: string;

  @Index()
  @Column({ nullable: true })
  slogan: string;

  @Index()
  @Column({
    default: false,
    type: 'bool',
  })
  isDefault: boolean;
  //默认资源不可删除

  @Index()
  @Column({})
  appKey: string;

  @Column({
    type: 'jsonb',
    nullable: true,
    default: ['blog'],
  })
  appModules: string[];

  @Column({ default: 'blog', nullable: true })
  homePageType: string;

  @Column('simple-array', {
    nullable: true,
  })
  host: string[];

  @Column({
    type: 'jsonb',
    nullable: true,
  })
  appUrls: {
    android: string;
    ios: string;
  };

  @ManyToOne(() => Firm, (firm) => firm.firm)
  @JoinColumn({
    name: 'firmId',
  })
  firm: Firm;

  @Column({
    nullable: true,
  })
  firmId: number;

  @OneToMany(() => UsersFirm, (firmUsers) => firmUsers.firm)
  firmUsers: UsersFirm[];

  @Column({ nullable: true })
  description: string;
}
