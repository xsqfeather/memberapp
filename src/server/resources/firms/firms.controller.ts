import {
  Body,
  Controller,
  Get,
  HttpException,
  HttpStatus,
  Param,
  Post,
  Query,
  Res,
} from '@nestjs/common';
import { ApiTags } from '@nestjs/swagger';
import { Response } from 'express';
import { Like } from 'typeorm';
import { CreateFirmInput } from './dto/create-firm.input';
import { FirmsService } from './firms.service';

@ApiTags('firms')
@Controller('firms')
export class FirmsController {
  constructor(private firmsService: FirmsService) {}

  @Get(':id')
  findOne(@Param(':id') id: number) {
    return this.firmsService.findOne(id);
  }

  @Post()
  create(@Body() body: CreateFirmInput) {
    return this.firmsService.create(body);
  }

  @Get()
  async findAll(@Res() res: Response, @Query() query: any) {
    const { filter, range, sort } = query;
    let filterObj,
      rangeArr,
      sortArr = {};
    try {
      filterObj = JSON.parse(filter || '{}');
      rangeArr = JSON.parse(range || '[0,9]');
      sortArr = JSON.parse(sort || '["updatedAt", "DESC"]');
    } catch (error) {
      throw new HttpException(
        {
          status: HttpStatus.BAD_REQUEST,
          error: `json parse errors where query one of { filter, range, sort}`,
        },
        HttpStatus.BAD_REQUEST,
      );
    }
    let where: any = {};

    if (filterObj.roleTagId) {
      where.roleTagId = filterObj.roleTagId;
    }

    if (filterObj.q) {
      where = [
        { tagName: Like(`%${filterObj.q}%`) },
        { description: Like(`%${filterObj.q}%`) },
      ];
    }
    const [firms, count]: any = await this.firmsService.findAll({
      range: rangeArr,
      sort: sortArr,
      where,
    });
    res.setHeader(
      'Content-Range',
      `firms ${rangeArr[0]}-${rangeArr[1]}/${count}`,
    );
    res.send(firms);
  }
}
