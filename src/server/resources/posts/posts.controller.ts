import {
  Controller,
  Get,
  Post,
  Body,
  Patch,
  Param,
  Delete,
  Res,
  Query,
  HttpException,
  HttpStatus,
} from '@nestjs/common';
import { PostsService } from './posts.service';
import { CreatePostDto } from './dto/create-post.dto';
import { UpdatePostDto } from './dto/update-post.dto';
import { ApiBody, ApiTags } from '@nestjs/swagger';
import { Response } from 'express';
import { Like } from 'typeorm';

@ApiTags('posts')
@Controller('posts')
export class PostsController {
  constructor(private readonly postsService: PostsService) {}

  @ApiBody({
    description: '创建一个用户',
    type: CreatePostDto,
  })
  @Post()
  create(@Body() createPostDto: CreatePostDto) {
    return this.postsService.create(createPostDto);
  }

  @Get()
  async findAll(@Res() res: Response, @Query() query: any) {
    const { filter, range, sort } = query;
    /* console.log({ query }); */

    let filterObj,
      rangeArr,
      sortArr = {};
    try {
      filterObj = JSON.parse(filter || '{}');
      rangeArr = JSON.parse(range || '[0,9]');
      sortArr = JSON.parse(sort || '["updatedAt", "DESC"]');
    } catch (error) {
      throw new HttpException(
        {
          status: HttpStatus.BAD_REQUEST,
          error: `json parse errors where query one of { filter, range, sort}`,
        },
        HttpStatus.BAD_REQUEST,
      );
    }
    let where: any = {};

    if (filterObj.q) {
      where = [
        { tagName: Like(`%${filterObj.q}%`) },
        { description: Like(`%${filterObj.q}%`) },
      ];
    }

    const [roleTags, count]: any = await this.postsService.findAll({
      range: rangeArr,
      sort: sortArr,
      where,
    });
    res.setHeader(
      'Content-Range',
      `role-tags ${rangeArr[0]}-${rangeArr[1]}/${count}`,
    );
    res.send(roleTags);
  }

  @Get(':id')
  findOne(@Param('id') id: string) {
    return this.postsService.findOne(+id);
  }

  @Patch(':id')
  update(@Param('id') id: string, @Body() updatePostDto: UpdatePostDto) {
    return this.postsService.update(+id, updatePostDto);
  }

  @Delete(':id')
  remove(@Param('id') id: string) {
    return this.postsService.remove(+id);
  }
}
