import { OnEvent } from '@nestjs/event-emitter';
import { Server } from 'socket.io';

import {
  WebSocketGateway,
  SubscribeMessage,
  MessageBody,
  ConnectedSocket,
  WebSocketServer,
} from '@nestjs/websockets';
import { NEW_CHAT_MSGS_COMING } from '../../common/Emitters';
import { UsersService } from '../users/users.service';
import { ChatMsgsService } from './chat-msgs.service';
import { CreateChatMsgDto } from './dto/create-chat-msg.dto';
import { UpdateChatMsgDto } from './dto/update-chat-msg.dto';
import { ChatMsg } from './entities/chat-msg.entity';

@WebSocketGateway()
export class ChatMsgsGateway {
  @WebSocketServer()
  server: Server;
  constructor(
    private readonly chatMsgService: ChatMsgsService,
    private readonly usersService: UsersService,
  ) {}

  @SubscribeMessage('createChatMsg')
  async create(
    @MessageBody() createChatMsgDto: CreateChatMsgDto,
    @ConnectedSocket() client: any,
  ) {
    const { senderId, receiverId } = createChatMsgDto;
    const newMsg = await this.chatMsgService.create(createChatMsgDto);
    client.broadcast.emit(`${receiverId}_chat_channel`, newMsg);
    await this.usersService.update(receiverId, {
      unreadMsgCount: 1,
    });
    const user = await this.usersService.findOne(receiverId);
    const sender = await this.usersService.findOne(senderId);
    client.broadcast.emit(`${receiverId}_unread_count`, {
      unreadMsgCount: user.unreadMsgCount,
    });
    client.broadcast.emit(`${receiverId}_messages`, {
      type: 'fromUser',
      content: `来自${sender.nickname || sender.username}的消息`,
      from: sender.id,
      userId: receiverId,
      status: 'unread',
    });
    return newMsg;
  }

  @SubscribeMessage('updateChatMsg')
  async update(
    @MessageBody() updateChatMsgDto: UpdateChatMsgDto,
    @ConnectedSocket() client: any,
  ) {
    // const {  status } = UpdateChatMsgDto;
  }

  @OnEvent(NEW_CHAT_MSGS_COMING)
  async newChatMsgComing(chatMsg: ChatMsg) {
    this.server.emit(`NEW_CHAT_MSGS_COMING_${chatMsg.receiverId}`, chatMsg);
  }
}
