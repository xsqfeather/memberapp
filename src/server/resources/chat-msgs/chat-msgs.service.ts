import { Injectable } from '@nestjs/common';
import { EventEmitter2 } from '@nestjs/event-emitter';
import { InjectRepository } from '@nestjs/typeorm';
import { Repository } from 'typeorm';
import { NEW_CHAT_MSGS_COMING } from '../../common/Emitters';
import { MessagesService } from '../messages/messages.service';
import { CreateChatMsgDto } from './dto/create-chat-msg.dto';
import { UpdateChatMsgDto } from './dto/update-chat-msg.dto';
import { ChatMsg } from './entities/chat-msg.entity';

@Injectable()
export class ChatMsgsService {
  constructor(
    @InjectRepository(ChatMsg)
    private readonly chatMsgsRespository: Repository<ChatMsg>,
    private eventEmitter: EventEmitter2,
    private messagesService: MessagesService,
  ) {}

  async create(createChatMsgDto: CreateChatMsgDto) {
    const chatMsg = this.chatMsgsRespository.create(createChatMsgDto);
    console.log({ chatMsg });
    await this.chatMsgsRespository.save(chatMsg);

    const message = await this.messagesService.createOrUpdateChatMessage({
      fromUserId: chatMsg.senderId,
      userId: chatMsg.receiverId,
      content: chatMsg.content,
      contentType: chatMsg.contentType,
    });

    this.eventEmitter.emit(NEW_CHAT_MSGS_COMING, {
      ...chatMsg,
      messageId: message.id,
    });

    return chatMsg;
  }

  findAll({ sort, range, where }) {
    const order = {};
    order[sort[0]] = sort[1];
    return this.chatMsgsRespository.findAndCount({
      order,
      skip: range[0],
      take: range[1] - range[0] + 1,
      where,
    });
  }

  findOne(id: number) {
    return `This action returns a #${id} chatMsg`;
  }

  update(id: number, updateChatMsgDto: UpdateChatMsgDto) {
    return `This action updates a #${id} chatMsg`;
  }

  remove(id: number) {
    return `This action removes a #${id} chatMsg`;
  }
}
