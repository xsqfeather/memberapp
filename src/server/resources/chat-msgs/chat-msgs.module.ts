import { Module } from '@nestjs/common';
import { ChatMsgsService } from './chat-msgs.service';
import { ChatMsgsController } from './chat-msgs.controller';
import { TypeOrmModule } from '@nestjs/typeorm';
import { ChatMsg } from './entities/chat-msg.entity';
import { ChatMsgsGateway } from './chat-msgs.gateway';
import { UsersModule } from '../users/users.module';
import { MessagesModule } from '../messages/messages.module';

@Module({
  imports: [TypeOrmModule.forFeature([ChatMsg]), UsersModule, MessagesModule],
  controllers: [ChatMsgsController],
  providers: [ChatMsgsService, ChatMsgsGateway],
})
export class ChatMsgsModule {}
