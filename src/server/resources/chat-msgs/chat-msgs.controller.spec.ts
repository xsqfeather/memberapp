import { Test, TestingModule } from '@nestjs/testing';
import { ChatMsgsController } from './chat-msgs.controller';
import { ChatMsgsService } from './chat-msgs.service';

describe('ChatMsgsController', () => {
  let controller: ChatMsgsController;

  beforeEach(async () => {
    const module: TestingModule = await Test.createTestingModule({
      controllers: [ChatMsgsController],
      providers: [ChatMsgsService],
    }).compile();

    controller = module.get<ChatMsgsController>(ChatMsgsController);
  });

  it('should be defined', () => {
    expect(controller).toBeDefined();
  });
});
