import { PartialType } from '@nestjs/swagger';
import { CreateChatMsgDto } from './create-chat-msg.dto';

export class UpdateChatMsgDto extends PartialType(CreateChatMsgDto) {
  status?: 'sent' | 'read' | 'unread' | 'sending';
}
