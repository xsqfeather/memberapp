export class CreateChatMsgDto {
  receiverId: number;
  senderId: number;
  content: string;
  contentType: 'text' | 'image' | 'video' | 'audio';
}
