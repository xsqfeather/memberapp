import {
  Controller,
  Get,
  Post,
  Body,
  Patch,
  Param,
  Delete,
  HttpStatus,
  HttpException,
  Query,
  Res,
} from '@nestjs/common';
import { Response } from 'express';
import { Like } from 'typeorm';
import { ChatMsgsService } from './chat-msgs.service';
import { CreateChatMsgDto } from './dto/create-chat-msg.dto';
import { UpdateChatMsgDto } from './dto/update-chat-msg.dto';

@Controller('chat-msgs')
export class ChatMsgsController {
  constructor(private readonly chatMsgsService: ChatMsgsService) {}

  @Post()
  create(@Body() createChatMsgDto: CreateChatMsgDto) {
    console.log({ createChatMsgDto });

    return this.chatMsgsService.create(createChatMsgDto);
  }

  @Get()
  async findAll(@Res() res: Response, @Query() query: any) {
    const { filter, range, sort } = query;
    let filterObj,
      rangeArr,
      sortArr = {};
    try {
      filterObj = JSON.parse(filter || '{}');
      rangeArr = JSON.parse(range || '[0,9]');
      sortArr = JSON.parse(sort || '["updatedAt", "DESC"]');
    } catch (error) {
      throw new HttpException(
        {
          status: HttpStatus.BAD_REQUEST,
          error: `json parse errors where query one of { filter, range, sort}`,
        },
        HttpStatus.BAD_REQUEST,
      );
    }
    let where: any = {};

    if (filterObj.chattingUserId && filterObj.userId) {
      where = [
        {
          senderId: filterObj.chattingUserId,
          receiverId: filterObj.userId,
        },
        {
          senderId: filterObj.userId,
          receiverId: filterObj.chattingUserId,
        },
      ];
    }
    const [chatMsgs, count]: any = await this.chatMsgsService.findAll({
      range: rangeArr,
      sort: sortArr,
      where,
    });
    res.setHeader(
      'Content-Range',
      `courses ${rangeArr[0]}-${rangeArr[1]}/${count}`,
    );
    res.send(chatMsgs);
  }

  @Get(':id')
  findOne(@Param('id') id: string) {
    return this.chatMsgsService.findOne(+id);
  }

  @Patch(':id')
  update(@Param('id') id: string, @Body() updateChatMsgDto: UpdateChatMsgDto) {
    return this.chatMsgsService.update(+id, updateChatMsgDto);
  }

  @Delete(':id')
  remove(@Param('id') id: string) {
    return this.chatMsgsService.remove(+id);
  }
}
