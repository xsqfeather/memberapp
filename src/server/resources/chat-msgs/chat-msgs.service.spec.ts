import { Test, TestingModule } from '@nestjs/testing';
import { ChatMsgsService } from './chat-msgs.service';

describe('ChatMsgsService', () => {
  let service: ChatMsgsService;

  beforeEach(async () => {
    const module: TestingModule = await Test.createTestingModule({
      providers: [ChatMsgsService],
    }).compile();

    service = module.get<ChatMsgsService>(ChatMsgsService);
  });

  it('should be defined', () => {
    expect(service).toBeDefined();
  });
});
