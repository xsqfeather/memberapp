import {
  Column,
  CreateDateColumn,
  DeleteDateColumn,
  Entity,
  JoinColumn,
  ManyToOne,
  PrimaryGeneratedColumn,
  UpdateDateColumn,
} from 'typeorm';
import { User } from '../../users/entities/user.entity';

@Entity()
export class ChatMsg {
  @PrimaryGeneratedColumn()
  id: number;

  @ManyToOne(() => User, (user) => user.sentChatMsgs)
  @JoinColumn({
    name: 'senderId',
  })
  sender: User;

  @Column({
    nullable: true,
  })
  senderId: number;

  @ManyToOne(() => User, (user) => user.receiveChatMsgs)
  @JoinColumn({
    name: 'receiverId',
  })
  receiver: User;

  @Column({
    nullable: true,
  })
  receiverId: number;

  @Column()
  content: string;

  @Column({
    default: 'unread',
    nullable: true,
  })
  status: string;

  @Column()
  contentType: string;

  @CreateDateColumn()
  createdAt: Date;

  @UpdateDateColumn()
  updatedAt: Date;

  @DeleteDateColumn()
  deletedAt: Date;
}
