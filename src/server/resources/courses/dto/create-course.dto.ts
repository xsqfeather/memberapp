import { ApiProperty } from '@nestjs/swagger';
import { IsOptional, IsString, IsUrl } from 'class-validator';

export class CreateCourseDto {
  @IsString()
  @ApiProperty({ name: 'title', description: '课程系列标题' })
  title: string;

  @IsString()
  @ApiProperty({ name: 'description', description: '课程系列描述' })
  description: string;

  @IsOptional()
  @IsUrl()
  @ApiProperty({ name: 'coverUrl', description: '封面图地址', required: false })
  coverUrl: string;
}
