import {
  Controller,
  Get,
  Post,
  Body,
  Param,
  Delete,
  Put,
  Res,
  Query,
  HttpException,
  HttpStatus,
} from '@nestjs/common';
import { Response } from 'express';
import { In, Like } from 'typeorm';
import { CoursesService } from './courses.service';
import { CreateCourseDto } from './dto/create-course.dto';
import { UpdateCourseDto } from './dto/update-course.dto';
import { ApiOperation, ApiTags } from '@nestjs/swagger';

@ApiTags('课程系列Courses')
@Controller('courses')
export class CoursesController {
  constructor(private readonly coursesService: CoursesService) {}

  @ApiOperation({ summary: '创建课程系列' })
  @Post()
  create(@Body() createCourseDto: CreateCourseDto) {
    return this.coursesService.create(createCourseDto);
  }

  @ApiOperation({ summary: '查找所有课程系列' })
  @Get()
  async findAll(@Res() res: Response, @Query() query: any) {
    const { filter, range, sort } = query;
    let filterObj,
      rangeArr,
      sortArr = {};
    try {
      filterObj = JSON.parse(filter || '{}');
      rangeArr = JSON.parse(range || '[0,9]');
      sortArr = JSON.parse(sort || '["updatedAt", "DESC"]');
    } catch (error) {
      throw new HttpException(
        {
          status: HttpStatus.BAD_REQUEST,
          error: `json parse errors where query one of { filter, range, sort}`,
        },
        HttpStatus.BAD_REQUEST,
      );
    }
    let where: any = {};
    if (filterObj.roleTagId) {
      where.roleTagId = filterObj.roleTagId;
    }
    if (filterObj.q) {
      where = [
        { title: Like(`%${filterObj.q}%`) },
        { description: Like(`%${filterObj.q}%`) },
      ];
    }
    const [planets, count]: any = await this.coursesService.findAll({
      range: rangeArr,
      sort: sortArr,
      where,
    });
    res.setHeader(
      'Content-Range',
      `courses ${rangeArr[0]}-${rangeArr[1]}/${count}`,
    );
    res.send(planets);
  }

  @Get(':id')
  findOne(@Param('id') id: string) {
    return this.coursesService.findOne(+id);
  }

  @Put(':id')
  update(@Param('id') id: string, @Body() updateCourseDto: UpdateCourseDto) {
    return this.coursesService.update(+id, updateCourseDto);
  }

  @Delete(':id')
  remove(@Param('id') id: string) {
    return this.coursesService.remove(+id);
  }

  @Delete()
  async removeAll(@Query() query: any) {
    const { filter } = query;
    const filterObj = JSON.parse(filter);
    const where = {
      id: In(filterObj.id),
    };
    await this.coursesService.removeAll({ where });
    return [...JSON.parse(query.filter).id];
  }
}
