import { Injectable } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { Repository } from 'typeorm';
import { CreateCourseDto } from './dto/create-course.dto';
import { UpdateCourseDto } from './dto/update-course.dto';
import { Course } from './entities/course.entity';

@Injectable()
export class CoursesService {
  constructor(
    @InjectRepository(Course)
    private readonly coursesRepository: Repository<Course>,
  ) {}
  async create(createCourseDto: CreateCourseDto) {
    const course = this.coursesRepository.create(createCourseDto);
    await this.coursesRepository.save(course);
    return course;
  }

  findAll({ sort, range, where }) {
    const order = {};
    order[sort[0]] = sort[1];
    return this.coursesRepository.findAndCount({
      order,
      skip: range[0],
      take: range[1] - range[0] + 1,
      where,
    });
  }

  findOne(id: number) {
    return this.coursesRepository.findOne(id);
  }

  async update(id: number, updateCourseDto: UpdateCourseDto) {
    const course = this.coursesRepository.findOne(id);
    await this.coursesRepository.update(id, {
      ...updateCourseDto,
    });
    return course;
  }

  remove(id: number) {
    return this.coursesRepository.softDelete(id);
  }

  async removeAll({ where }: { where: any }) {
    return await this.coursesRepository.softDelete({ ...where });
  }
}
