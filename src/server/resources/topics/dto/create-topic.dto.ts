import { ApiProperty } from '@nestjs/swagger';

export class CreateTopicDto {
  @ApiProperty({ name: 'title', description: '标题', required: true })
  title: string;
  @ApiProperty({ name: 'des', description: '话题描述', required: false })
  des: string;
  @ApiProperty({
    name: 'cover',
    description: '话题封面图片',
    required: false,
  })
  cover: string;
  @ApiProperty({
    name: 'publisherId',
    description: '上传用户id',
    required: true,
  })
  publisherId: number;
  @ApiProperty({ name: 'status', description: '审核状态' })
  status: 'NotReviewed'|'Pass'|'NoPass';
}
