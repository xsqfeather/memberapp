import { Injectable } from '@nestjs/common';
import { OnEvent } from '@nestjs/event-emitter';
import { InjectRepository } from '@nestjs/typeorm';
import { Repository } from 'typeorm';
import { MINI_BLOG_ON_CREATE } from '../../common/Emitters';
import { CreateTopicDto } from './dto/create-topic.dto';
import { UpdateTopicDto } from './dto/update-topic.dto';
import { Topic } from './entities/topic.entity';
import { UsersService } from '../users/users.service';

@Injectable()
export class TopicService {
  constructor(
    private usersService: UsersService,
    @InjectRepository(Topic)
    private topicRepository: Repository<Topic>,
  ) {}

  async create(createTopicDto: CreateTopicDto) {
    const topic = this.topicRepository.create(createTopicDto);
    await this.topicRepository.save(topic);
    return topic;
  }

  async findAll(userId: number, { sort, range, where }) {
    const order = {};
    order[sort[0]] = sort[1];
    const isAdmin = await this.usersService.isAdmin(userId);
    if (isAdmin) {
      return this.topicRepository.findAndCount({
        order,
        skip: range[0],
        take: range[1] - range[0] + 1,
        where,
      });
    } else {
      if (Array.isArray(where)) {
        where = where.map((item) => {
          return {
            ...item,
            status: 'Pass',
          };
        });
      } else {
        where.status = 'Pass';
      }
      return this.topicRepository.findAndCount({
        order,
        skip: range[0],
        take: range[1] - range[0] + 1,
        where,
      });
    }
  }

  findOne(id: number) {
    return this.topicRepository.findOne(id);
  }

  async remove(id: number) {
    const topic = await this.topicRepository.findOne(id);
    return this.topicRepository.softRemove(topic);
  }

  async update(id: number, updateTopicDto: UpdateTopicDto) {
    const topic = await this.topicRepository.findOne(id);
    topic.cover = updateTopicDto.cover;
    topic.des = updateTopicDto.des;
    topic.publisherId = updateTopicDto.publisherId;
    topic.title = updateTopicDto.title;
    topic.status = updateTopicDto.status
    return this.topicRepository.save(topic);
  }

  @OnEvent(MINI_BLOG_ON_CREATE)
  IncrementBlogCountOnCreate({ topicIds }: { topicIds: number[] }) {
    if (topicIds && topicIds.length != 0) {
      this.topicRepository
        .createQueryBuilder()
        .update()
        .set({
          miniBlogCount: () => `"miniBlogCount" + 1`,
        })
        .where('id IN(:...ids)', { ids: topicIds })
        .execute()
        .then((value) => {
          console.log(`[${__filename}]:IncrementBlogCountOnCreate执行完成
          value:${value}`);
        });
    }
  }
}
