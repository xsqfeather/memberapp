import {
  Column,
  CreateDateColumn,
  DeleteDateColumn,
  Entity,
  Index,
  ManyToMany,
  ManyToOne,
  OneToMany,
  PrimaryGeneratedColumn,
  UpdateDateColumn,
} from 'typeorm';
import { User } from '../../users/entities/user.entity';
import { MidTopicMiniBlog } from '../../mid-topic-miniblog/entities/mid-topic-miniblog.entity';

@Entity()
export class Topic {
  @PrimaryGeneratedColumn()
  id: number;

  @Column()
  @Index({ unique: true })
  title: string;

  @Column({ nullable: true })
  des: string;

  @Column({
    nullable: true,
  })
  cover: string;

  @Column({
    nullable: true,
  })
  publisherId: number;

  @Column({
    default: 0,
  })
  miniBlogCount: number;

  @OneToMany(
    (type) => MidTopicMiniBlog,
    (midTopicMiniBlog) => midTopicMiniBlog.topic,
  )
  midTopicMiniBlog: MidTopicMiniBlog[];

  @Column({
    default:'NotReviewed'
  })
  status:'NotReviewed'|'Pass'|'NoPass'

  //单向
  @ManyToOne(() => User)
  publisher: User;

  @CreateDateColumn()
  createdAt: Date;

  @UpdateDateColumn()
  updatedAt: Date;

  @DeleteDateColumn()
  deletedAt: Date;
}
