import {
  Controller,
  Get,
  Post,
  Body,
  Patch,
  Param,
  Delete,
  HttpException,
  HttpStatus,
  Query,
  Res,
  Put,
  UseGuards,
} from '@nestjs/common';
import { TopicService } from './topic.service';
import { CreateTopicDto } from './dto/create-topic.dto';
import { UpdateTopicDto } from './dto/update-topic.dto';
import { IsNull, Like, Not } from 'typeorm';
import { Response } from 'express';
import { ApiOperation, ApiQuery, ApiTags } from '@nestjs/swagger';
import { JwtAuthGuard } from '../../auth/jwt-auth.guard';
import { User } from '../../decorators/User';

@ApiTags('话题topics')
@Controller('topics')
export class TopicController {
  constructor(private readonly topicService: TopicService) {}

  @Post()
  @UseGuards(JwtAuthGuard)
  @ApiOperation({ summary: '创建话题' })
  create(@Body() createTopicDto: CreateTopicDto) {
    return this.topicService.create(createTopicDto);
  }

  @Get()
  @UseGuards(JwtAuthGuard)
  @ApiOperation({ summary: '获取所有话题,默认获取最新的10条' })
  @ApiQuery({
    name: 'filter',
    description: '过滤, {field:value}',
    required: false,
  })
  @ApiQuery({
    name: 'range',
    description: '分页, [start: number, end: number]',
    required: false,
  })
  @ApiQuery({
    name: 'sort',
    description: '排序, [field: string, order: "DESC" | "ASC"]',
    required: false,
  })
  async findAll(
    @User('userId') userId: number,
    @Res() res: Response,
    @Query() query: any,
  ) {
    const { filter, range, sort } = query;
    let filterObj,
      rangeArr,
      sortArr = {};
    try {
      filterObj = JSON.parse(filter || '{}');
      rangeArr = JSON.parse(range || '[0,9]');
      sortArr = JSON.parse(sort || '["updatedAt", "DESC"]');
    } catch (error) {
      throw new HttpException(
        {
          status: HttpStatus.BAD_REQUEST,
          error: `json parse errors where query one of { filter, range, sort}`,
        },
        HttpStatus.BAD_REQUEST,
      );
    }
    let where: any = {};

    if (filterObj && filterObj.q) {
      where = [{ title: Like(`%${filterObj.q}%`) }];
    }
    /* if (filterObj && filterObj.publisherId) {
      where.publisherId = filterObj.publisherId;
    } */
    //内容筛选
    if (filterObj && filterObj.content) {
      switch (filterObj.content) {
        case 'nocontent':
          where = [{ des: null }, { cover: null }];
          break;
        case 'hascontent':
          where = [{ des: Not(IsNull()), cover: Not(IsNull()) }];
          break;
      }
    }
    //状态筛选
    if (filterObj && filterObj.status) {
      switch (filterObj.status) {
        case 'NotReviewed':
          where = [{ status: 'NotReviewed' }];
          break;
        case 'Pass':
          where = [{ status: 'Pass' }];
          break;
        case 'NoPass':
          where = [{ status: 'NoPass' }];
          break;
      }
    }
    const [miniBlogs, count]: any = await this.topicService.findAll(userId, {
      range: rangeArr,
      sort: sortArr,
      where,
    });
    res.setHeader(
      'Content-Range',
      `topics ${rangeArr[0]}-${rangeArr[1]}/${count}`,
    );
    res.send(miniBlogs);
  }

  @Get(':id')
  @ApiOperation({ summary: '取单个话题' })
  findOne(@Param('id') id: string) {
    return this.topicService.findOne(+id);
  }

  @Put(':id')
  @ApiOperation({ summary: '更新话题内容' })
  update(@Param('id') id: string, @Body() updateTopicDto: UpdateTopicDto) {
    console.log(updateTopicDto);
    return this.topicService.update(+id, updateTopicDto);
  }

  @Delete(':id')
  @ApiOperation({ summary: '删除话题' })
  remove(@Param('id') id: string) {
    return this.topicService.remove(+id);
  }
}
