import {
  Column,
  CreateDateColumn,
  DeleteDateColumn,
  Entity,
  Index,
  JoinColumn,
  ManyToOne,
  OneToMany,
  PrimaryGeneratedColumn,
  UpdateDateColumn,
} from 'typeorm';
import { ChatMsg } from '../../chat-msgs/entities/chat-msg.entity';
import { MiniBlog } from '../../mini-blog/entities/mini-blog.entity';
import { Session } from '../../sessions/entities/session.entity';
import { UsersFirm } from '../../users-firms/entities/users-firm.entity';
import { Planet } from '../../planets/entities/planet.entity';
import { Message } from '../../messages/entities/message.entity';

interface IProfile {
  birthday?: string;
  avatar?: string;
  gender?: number; //1 女 2 男 3 未知
  city?: string;
  role?: string;
}

@Entity()
export class User {
  @PrimaryGeneratedColumn()
  id: number;

  @Index()
  @Column({
    nullable: true,
  })
  username: string;

  @Index()
  @Column({
    nullable: true,
  })
  email: string;

  @Index()
  @Column({
    nullable: true,
  })
  phone: string;

  @Index()
  @Column({
    nullable: true,
  })
  isRobot: boolean;

  @Index()
  @Column({
    nullable: true,
    unique: true,
  })
  nickname: string;

  @Column({ default: 0 })
  gender: number;

  @Column({ type: 'jsonb', default: [] })
  tagIds: number[];

  @Column({ type: 'jsonb', default: [] })
  roleIds: number[];

  @ManyToOne(() => Planet)
  @JoinColumn({ name: 'planetId' })
  planet: Planet;

  @Column({ nullable: true })
  planetId: number;

  @Index()
  @Column({
    nullable: true,
  })
  postFreq: number; //如果是机器人，发帖频率,单位小时

  @Index()
  @Column({
    nullable: true,
  })
  followFreq: number; //如果是机器人，粉人频率, 单位小时

  @Column({
    type: 'jsonb',
    nullable: true,
  })
  profile: IProfile;

  @Index()
  @Column({
    default: false,
  })
  isDefault: boolean;
  //默认资源不可删除

  @CreateDateColumn()
  createdAt: Date;

  @UpdateDateColumn()
  updatedAt: Date;

  @DeleteDateColumn()
  deletedAt: Date;

  @Column({ nullable: true })
  //注册的公司ID
  registerFirmId: number;

  @OneToMany(() => UsersFirm, (userFirm) => userFirm.user)
  userFirms: UsersFirm[];

  @OneToMany(() => Session, (sessions) => sessions.user)
  sessions: Session[];

  @OneToMany(() => MiniBlog, (minis) => minis.publisher)
  miniBlogs: MiniBlog[];

  @OneToMany(() => ChatMsg, (chatMsg) => chatMsg.sender)
  sentChatMsgs: ChatMsg[];

  @OneToMany(() => ChatMsg, (chatMsg) => chatMsg.receiver)
  receiveChatMsgs: ChatMsg[];

  @Column({
    nullable: true,
  })
  unreadMsgCount: number;

  @OneToMany(() => Message, (message) => message.user)
  messages: Message[];
}
