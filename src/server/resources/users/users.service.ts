import { HttpException, HttpStatus, Injectable } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { In, Repository } from 'typeorm';
import { CreateUserDto } from './dto/create-user.dto';
import { UpdateUserDto } from './dto/update-user.dto';
import { User } from './entities/user.entity';
import { FirmsService } from '../firms/firms.service';
import { UsersFirmsService } from '../users-firms/users-firms.service';
import * as randomString from 'randomstring';
import { OnEvent } from '@nestjs/event-emitter';
import { UpdateProfileDto } from './dto/update-profile.dto';
import { Planet } from '../planets/entities/planet.entity';
import { MINI_BLOG_ON_CREATE } from '../../common/Emitters';
import { RoleTagsService } from '../role-tags/role-tags.service';
import { PerferTagsService } from '../perfer-tags/perfer-tags.service';
import { classToPlain } from 'class-transformer';
import { ResponseIdentityDto } from './dto/response-identity.dto';
import { ResponsePlanetDto } from '../planets/dto/response-planet.dto';
import { ResponseRoleDto } from '../role-tags/dto/response-role.dto';
import { ResponseTagDto } from '../perfer-tags/dto/response-tag.dto';
import { ResponseProfileDto } from './dto/response-profile.dto';
import { ResponseUserDto } from './dto/response-user.dto';

@Injectable()
export class UsersService {
  constructor(
    @InjectRepository(User)
    private usersRepository: Repository<User>,
    @InjectRepository(Planet)
    private readonly planetsRepository: Repository<Planet>,
    private readonly firmsService: FirmsService,
    private readonly usersFirmsService: UsersFirmsService,
    private readonly perferTagsService: PerferTagsService,
    private readonly roleTagsService: RoleTagsService,
  ) {}
  async create(createUserDto: CreateUserDto) {
    const { username, email, phone, registerFirmId, isRobot } = createUserDto;
    //判断用户名
    let checkedUsername = username;
    if (!checkedUsername) {
      checkedUsername = randomString.generate(9);
    }

    let user = await this.findOneByUsername(checkedUsername);
    if (!registerFirmId) {
      throw new HttpException(
        {
          status: HttpStatus.BAD_REQUEST,
          error: `firmId_not_found`,
          code: `user:create:fail:firmId_not_found`,
        },
        HttpStatus.BAD_REQUEST,
      );
    }
    let firm = await this.firmsService.findOne(registerFirmId);

    if (!firm) {
      throw new HttpException(
        {
          status: HttpStatus.BAD_REQUEST,
          error: `firmId_not_found`,
          code: `user:create:fail:firmId_not_found`,
        },
        HttpStatus.BAD_REQUEST,
      );
    }

    if (user) {
      throw new HttpException(
        {
          status: HttpStatus.BAD_REQUEST,
          error: `username ${checkedUsername} already exist`,
          code: `user:create:fail:${checkedUsername}_aleady_exist`,
        },
        HttpStatus.BAD_REQUEST,
      );
    }
    user = await this.findOneByEmail(email);
    if (user) {
      throw new HttpException(
        {
          status: HttpStatus.BAD_REQUEST,
          error: `email ${email} already exist`,
          code: `user:create:fail:${email}_aleady_exist`,
          message: `邮箱已经存在`,
        },
        HttpStatus.BAD_REQUEST,
      );
    }
    user = await this.findOneByPhone(phone, registerFirmId);
    if (user) {
      throw new HttpException(
        {
          status: HttpStatus.BAD_REQUEST,
          error: `phone ${phone} already exist`,
          message: `电话已经存在`,
          code: `user:create:fail:${phone}_aleady_exist`,
        },
        HttpStatus.BAD_REQUEST,
      );
    }
    user = this.usersRepository.create({
      ...createUserDto,
      username: checkedUsername,
      email: checkedUsername + '@noemail.locoal',
      phone: phone || '130000000000',
      registerFirmId,
      isRobot,
    });
    await this.usersRepository.save(user);

    await this.usersFirmsService.confirmUserBelongFirm(user.id, firm.id);
    return user;
  }

  findAll({ sort, range, where }) {
    const order = {};
    order[sort[0]] = sort[1];
    return this.usersRepository.findAndCount({
      order,
      skip: range[0],
      take: range[1] - range[0] + 1,
      where,
    });
  }

  findByIds(ids: number[], condition: any = {}) {
    const where: any = {
      id: In(ids),
    };
    if (condition.username) {
      where.username = condition.username;
    }
    if (condition.nikename) {
      where.nikename = condition.nikename;
    }
    if (condition.phone) {
      where.phone = condition.phone;
    }
    return this.usersRepository.findAndCount({
      where: where,
    });
  }

  findOneByUsername(username: string): Promise<User> {
    return this.usersRepository.findOne({
      where: {
        username,
      },
      relations: ['userFirms'],
    });
  }

  findOneByEmail(email: string): Promise<User> {
    return this.usersRepository.findOne({
      where: {
        email,
      },
      relations: ['userFirms'],
    });
  }

  findOneByPhone(phone: string, firmId: number): Promise<User> {
    if (!firmId) {
      throw new HttpException(
        {
          status: HttpStatus.BAD_REQUEST,
          error: `user#findOneByPhone:firmId_required`,
        },
        HttpStatus.BAD_REQUEST,
      );
    }
    return this.usersRepository.findOne({
      where: {
        phone,
        registerFirmId: firmId,
      },
      relations: ['userFirms'],
    });
  }

  count(): Promise<Number> {
    return this.usersRepository.count();
  }

  findOne(id: number) {
    return this.usersRepository.findOne({
      where: {
        id,
      },
      select: ['id', 'username', 'email', 'phone','nickname', 'profile'],
    });
  }

  async isAdmin(id: number) {
    const value = await this.usersRepository.findOne({
      where: {
        id,
      },
      select: ['id', 'isDefault'],
    });
    return !!value.isDefault;
  }
  
  async isRobot(userId:number){
    const user = await this.findOne(userId);
    return user.isRobot;
  }

  async getProfile(id: number) {
    return classToPlain(
      new ResponseUserDto(
        await this.usersRepository.findOne({
          where: {
            id,
          },
        }),
      ),
    );
  }

  async setTags(id: number, tagIds: number[]) {
    const result = await this.usersRepository.update(
      {
        id,
      },
      {
        tagIds,
      },
    );
    return { success: result.affected > 0 };
  }
  async setRoles(id: number, roleIds: number[]) {
    const result = await this.usersRepository.update(
      {
        id,
      },
      {
        roleIds,
      },
    );
    return { success: result.affected > 0 };
  }
  async updateProfile(userId: number, dto: UpdateProfileDto) {
    const user = await this.usersRepository.findOne(userId);
    const { birthday, avatar, WeChatOpenId, ...directWrite } = dto;
    const profile = {
      ...(user.profile ?? {}),
      ...{ birthday, avatar },
    };
    const result = await this.usersRepository.update(
      {
        id: userId,
      },
      Object.entries({ ...directWrite, profile })
        .filter(([_, it]) => it !== undefined)
        .reduce((obj, [k, v]) => {
          obj[k] = v;
          return obj;
        }, {}),
    );
    return { success: result.affected > 0 };
  }
  async update(id: number, updateUserDto: UpdateUserDto) {
    const user = await this.usersRepository.findOne(id);
    if (!user) {
      throw new HttpException(
        {
          status: HttpStatus.NOT_FOUND,
          error: `user:update:failed:user_not_found`,
          message: 'USER_NOT_FOUND',
        },
        HttpStatus.NOT_FOUND,
      );
    }
    const { unreadMsgCount, nickname, profile } = updateUserDto;
    if (unreadMsgCount) {
      user.unreadMsgCount += unreadMsgCount;
    }
    if(nickname){
      const nickUser = await this.usersRepository.findOne({where: { nickname }})
      if(nickUser && nickUser.id !== user.id){
        throw new HttpException(
          {
            status: HttpStatus.BAD_REQUEST,
            error: `user:update:failed:nickname_exist`,
            message: 'NICKNAME_EXIST',
          },
          HttpStatus.BAD_REQUEST,
        );
      }
      
      user.nickname = nickname;
    }
    if(profile){
      user.profile = profile;
    }
    await this.usersRepository.save(user);
    return user;
  }

  remove(id: number) {
    return this.usersRepository.softDelete(id);
  }

  async findOrCreateSuperAdmin() {
    let user = await this.usersRepository.findOne({
      where: { username: process.env.SUPER_ADMIN, isDefault: true },
    });
    if (!user) {
      user = this.usersRepository.create();
    }
    user.username = process.env.SUPER_ADMIN;
    user.isDefault = true;
    const firmId = await this.firmsService.getDefaultFirmId();
    await this.usersRepository.save(user);
    const userFirm = await this.usersFirmsService.confirmUserBelongFirm(
      user.id,
      firmId,
    );
    userFirm.password = process.env.SUPER_ADMIN_PASSWORD;
    userFirm.isOwner = true;
    await this.usersFirmsService.save(userFirm);
    await this.usersRepository.save(user);
    return user;
  }

  @OnEvent(MINI_BLOG_ON_CREATE)
  async updateUserLatestPostTime({ userId, postId }: any) {
    const user = await this.usersRepository.findOne(userId);
    await this.usersRepository.save(user);
    return user;
  }

  async removeAll({ where }: { where: any }) {
    return await this.usersRepository.softDelete({ ...where });
  }
  // 新加的接口
  // 获取用户信息
  async getProfile_v2(id: number) {
    return await this.usersRepository
      .findOne({
        where: {
          id,
        },
        select: [
          'id',
          'email',
          'phone',
          'tagIds',
          'roleIds',
          'planetId',
          'nickname',
          'gender',
          'profile',
        ],
      })
      .then((re) => classToPlain(new ResponseProfileDto(re)));
  }
  async getIdentity_v2(id: number) {
    const { planet, roleIds, tagIds } = await this.usersRepository.findOne({
      where: { id },
      select: ['id', 'planet', 'roleIds', 'tagIds'],
      relations: ['planet'],
    });
    // 查询角色
    const roles = await this.roleTagsService.findMany(roleIds);
    const tags = await this.perferTagsService.findMany(tagIds);
    // 副作用：清理不存在的标签
    if (roleIds.length !== roles.length || tagIds.length !== tags.length) {
      await this.usersRepository.update(
        { id },
        {
          tagIds: tags.map((it) => it.id),
          roleIds: roles.map((it) => it.id),
        },
      );
    }
    return classToPlain(
      new ResponseIdentityDto({
        planet: classToPlain(
          new ResponsePlanetDto(planet),
        ) as ResponsePlanetDto,
        roles: roles.map((role) =>
          classToPlain(new ResponseRoleDto(role)),
        ) as ResponseRoleDto[],
        tags: tags.map((tag) =>
          classToPlain(new ResponseTagDto(tag)),
        ) as ResponseTagDto[],
      }),
    );
  }
  async signup(phone: string, firmId?: number) {
    const planet = await this.planetsRepository.findOne({
      order: { level: 'ASC' },
    });
    const user = this.usersRepository.create({
      phone,
      registerFirmId: firmId,
      nickname: `用户${Math.random().toString(36).slice(-6)}`,
      planetId: planet.id,
      isRobot: false,
    });
    await this.usersRepository.save(user);
    return user;
  }

  onModuleInit() {
    this.findOrCreateSuperAdmin();
  }
}
