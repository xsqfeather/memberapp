import { User } from '../entities/user.entity';
import { Exclude, Expose } from 'class-transformer';
import { ApiProperty } from '@nestjs/swagger';

@Exclude()
export class ResponseUserDto implements Partial<User> {
  constructor(partial: Partial<ResponseUserDto>) {
    Object.assign(this, partial);
  }
  @ApiProperty({ name: 'id', description: '用户ID', type: Number })
  @Expose()
  id: number;

  @ApiProperty({ name: 'username', description: '用户名', type: String })
  @Expose()
  username: string;

  @ApiProperty({ name: 'nickname', description: '用户昵称', type: String })
  @Expose()
  nickname: string;

  @ApiProperty({ name: 'gender', description: '性别' })
  gender: User['gender'];

  profile: User['profile'];

  @ApiProperty({
    name: 'avatar',
    description: '用户头像',
    type: String,
    required: false,
  })
  @Expose()
  get avatar() {
    return this.profile?.avatar;
  }
}
