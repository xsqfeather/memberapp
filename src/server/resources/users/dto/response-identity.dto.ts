import { ApiProperty } from '@nestjs/swagger';
import { Exclude, Expose } from 'class-transformer';
import { ResponsePlanetDto } from '../../planets/dto/response-planet.dto';
import { ResponseRoleDto } from '../../role-tags/dto/response-role.dto';
import { ResponseTagDto } from '../../perfer-tags/dto/response-tag.dto';

@Exclude()
export class ResponseIdentityDto {
  constructor(partial: Partial<ResponseIdentityDto>) {
    Object.assign(this, partial);
  }
  @ApiProperty({ name: 'planet', description: '星球', type: ResponsePlanetDto })
  @Expose()
  planet: ResponsePlanetDto;

  @ApiProperty({
    name: 'roles',
    description: '角色',
    type: Array(ResponseRoleDto),
  })
  @Expose()
  roles: ResponseRoleDto[];

  @ApiProperty({
    name: 'tags',
    description: '标签',
    type: Array(ResponseTagDto),
  })
  @Expose()
  tags: ResponseTagDto[];
}
