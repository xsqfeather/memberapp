import { ApiProperty } from '@nestjs/swagger';
import {
  IsArray,
  IsEmail,
  IsNumber,
  IsOptional,
  IsString,
  IsUrl,
} from 'class-validator';
import { IsPhone, IsNickname } from '../../../validators';

export class UpdateProfileDto {
  @IsOptional()
  @IsArray()
  @ApiProperty({ name: 'tagsId', description: '用户标签ID', required: false })
  tagsId?: number[];

  @IsOptional()
  @IsArray()
  @ApiProperty({ name: 'rolesId', description: '用户角色ID', required: false })
  rolesId?: number;

  @IsOptional()
  @IsNickname()
  @ApiProperty({ name: 'nickname', description: '用户昵称', required: false })
  nickname?: string;

  @IsOptional()
  @IsPhone()
  @ApiProperty({ name: 'phone', description: '手机号', required: false })
  phone?: string;

  @IsOptional()
  @IsEmail()
  @ApiProperty({ name: 'email', description: '邮箱地址', required: false })
  email?: string;

  @IsOptional()
  @IsNumber()
  @ApiProperty({ name: 'gender', description: '性别', required: false })
  gender?: number;

  @IsOptional()
  @IsString()
  @ApiProperty({
    name: 'WeChatOpenId',
    description: '微信开放ID',
    required: false,
  })
  WeChatOpenId?: string;

  @IsOptional()
  @IsString()
  @ApiProperty({ name: 'birthday', description: '生日', required: false })
  birthday?: string;

  @IsOptional()
  @IsUrl()
  @ApiProperty({ name: 'avatar', description: '头像', required: false })
  avatar?: string;
}
