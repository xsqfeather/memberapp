import { ApiProperty } from '@nestjs/swagger';
import { IsEmail, IsString, IsOptional } from 'class-validator';
import { IsUsername, IsPhone } from '../../../validators';

export class CreateUserDto {
  @IsOptional()
  @IsUsername()
  @ApiProperty({
    description: '用户名',
    default: 'username',
    required: false,
  })
  username?: string;

  @IsOptional()
  @IsPhone()
  @ApiProperty({
    description: '手机号',
    default: '13800000000',
    required: false,
  })
  phone?: string;

  @IsOptional()
  @IsEmail()
  @ApiProperty({
    description: '邮箱',
    default: 'noemail@email.com',
    required: false,
  })
  email?: string = 'noemail@email.com';

  @IsOptional()
  @IsString()
  @ApiProperty({
    description: '密码',
    default: 'mypassword',
    required: false,
  })
  password?: string = '';

  @IsString()
  @ApiProperty({
    description: '微信平台ID',
    default: 'xxxxxxxxxxxxxxxxxxx',
    required: false,
  })
  weChatUniontId?: string = '';

  @IsOptional()
  @IsString()
  @ApiProperty({
    description: '微信公众号openid',
    default: 'xxxxxxxxxxxxxxxxxxx',
    required: false,
  })
  weChatWebOpenId?: string = '';

  @ApiProperty({
    description: '微信小程序openid',
    default: 'xxxxxxxxxxxxxxxxxxx',
    required: false,
  })
  weChatMiniOpenId?: string = '';

  @ApiProperty({
    description: '微信用户名，微信号',
    default: 'xxxxxxxxxxxxxxxxxxx',
    required: false,
  })
  weChat?: string = '';

  @ApiProperty({
    description: 'qq号',
    default: 'xxxxxxxxxxxxxxxxxxx',
    required: false,
  })
  qq?: string;

  @ApiProperty({
    description: '所注册的saas',
    default: 0,
    required: true,
  })
  registerFirmId: number;

  profile?: any;

  postFreq?: number;

  followFreq?: number;

  isRobot?: boolean;

  nickname?: string;

  avatar?: string;
}
