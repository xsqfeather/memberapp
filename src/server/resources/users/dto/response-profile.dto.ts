import { User } from '../entities/user.entity';
import { Exclude, Expose } from 'class-transformer';
import { ApiProperty } from '@nestjs/swagger';

@Exclude()
export class ResponseProfileDto implements Partial<User> {
  constructor(partial: Partial<ResponseProfileDto>) {
    Object.assign(this, partial);
  }
  @ApiProperty({ name: 'id', description: '用户ID' })
  @Expose()
  id: number;

  @ApiProperty({ name: 'email', description: '用户邮箱' })
  @Expose()
  email: string;

  @ApiProperty({ name: 'phone', description: '用户手机' })
  @Expose()
  phone: string;

  @Expose()
  @ApiProperty({ name: 'nickname', description: '用户昵称' })
  nickname: string;

  @Expose()
  @ApiProperty({ name: 'gender', description: '性别', type: 'number' })
  gender: User['gender'];

  profile: User['profile'];

  @Expose()
  @ApiProperty({
    name: 'avatar',
    description: '用户头像',
    type: String,
    required: false,
  })
  get avatar() {
    return this.profile?.avatar;
  }

  @Expose()
  @ApiProperty({
    name: 'birthday',
    description: '用户生日',
    type: String,
    required: false,
  })
  get birthday() {
    return this.profile?.birthday;
  }
}
