import {
  Controller,
  Get,
  Post,
  Body,
  Patch,
  Param,
  Delete,
  Query,
  Res,
  Req,
  HttpException,
  HttpStatus,
  UseGuards,
  ValidationPipe,
  Put,
} from '@nestjs/common';
import { UsersService } from './users.service';
import { CreateUserDto } from './dto/create-user.dto';
import { UpdateUserDto } from './dto/update-user.dto';
import { Request, Response } from 'express';
import {
  ApiBearerAuth,
  ApiBody,
  ApiHeader,
  ApiOperation,
  ApiResponse,
  ApiTags,
} from '@nestjs/swagger';
import { FirmsService } from '../firms/firms.service';
import { JwtAuthGuard } from '../../auth/jwt-auth.guard';
import { ILike, In } from 'typeorm';
import { User } from '../../decorators/User';
import { UpdateProfileDto } from './dto/update-profile.dto';
import { ResponseProfileDto } from './dto/response-profile.dto';
import { ResponseIdentityDto } from './dto/response-identity.dto';
import { ResponseUserDto } from './dto/response-user.dto';

@Controller('users')
@ApiTags('用户users')
export class UsersController {
  constructor(
    private readonly usersService: UsersService,
    private readonly firmsService: FirmsService,
  ) {}

  @ApiHeader({
    name: 'X-APP-KEY',
    description:
      'saas的appKey用以标识，用户注册在那个企业账号下，如果不写则为默认企业',
  })
  @Post()
  @ApiOperation({
    summary: '创建一个用户',
    description: `
    密码不是必须的，但用户名，邮箱，手机号，或者微信的openid任意一种必要
    `,
  })
  @ApiBody({
    description: '创建一个用户',
    type: CreateUserDto,
  })
  @ApiBearerAuth()
  create(@Body() createUserDto: CreateUserDto, @Req() req: Request) {
    const firmId = (req as any).firmId;
    return this.usersService.create({
      ...createUserDto,
      registerFirmId: firmId,
    });
  }

  @Get()
  async findAll(
    @Query() query: any,
    @Res() res: Response,
    @Req() req: Request,
  ) {
    const { filter, range, sort } = query;
    let filterObj,
      rangeArr,
      sortArr = {};
    try {
      filterObj = JSON.parse(filter || '{}');
      rangeArr = JSON.parse(range || '[0,9]');
      sortArr = JSON.parse(sort || '["updatedAt", "DESC"]');
    } catch (error) {
      throw new HttpException(
        {
          status: HttpStatus.BAD_REQUEST,
          error: `json parse errors where query one of { filter, range, sort}`,
        },
        HttpStatus.BAD_REQUEST,
      );
    }
    let where: any = {};
    if (filterObj && filterObj.q) {
      where = [
        { username: ILike(`%${filterObj.q}%`) },
        { email: ILike(`%${filterObj.q}%`) },
        { phone: ILike(`%${filterObj.q}%`) },
        { nickname: ILike(`%${filterObj.q}%`) },
      ];
    }
    if (filterObj && filterObj.id) {
      where.id = In(filterObj.id);
    }
    where.isDefault = false;
    const [users, count] = await this.usersService.findAll({
      range: rangeArr,
      sort: sortArr,
      where,
    });

    res.setHeader(
      'Content-Range',
      `users ${rangeArr[0]}-${rangeArr[1]}/${count}`,
    );
    res.send(users);
  }

  @ApiOperation({ summary: '获取当前用户详细信息' })
  @ApiBearerAuth()
  @ApiResponse({
    status: 200,
    description: 'success',
    type: ResponseProfileDto,
  })
  @UseGuards(JwtAuthGuard)
  @Get('/profile')
  async getProfile(@User('userId') userId: number) {
    return await this.usersService.getProfile_v2(userId);
  }

  @ApiOperation({ summary: '获取当前用户的身份数据' })
  @ApiBearerAuth()
  @ApiResponse({
    status: 200,
    description: 'success',
    type: ResponseIdentityDto,
  })
  @UseGuards(JwtAuthGuard)
  @Get('/identity')
  async getAccount(@User('userId') userId: number) {
    return await this.usersService.getIdentity_v2(userId);
  }

  @UseGuards(JwtAuthGuard)
  @Get(':id')
  findOne(@Param('id') id: string, @Req() req: Request) {
    return this.usersService.findOne(+id);
  }

  @ApiOperation({
    summary: '获取用户profile',
  })
  @ApiResponse({ status: 200, description: 'OK!', type: ResponseUserDto })
  @Get(':id/profile')
  findUserProfile(@Param('id') id: string) {
    return this.usersService.getProfile(+id);
  }

  @ApiOperation({
    summary: '设置用户标签',
  })
  @ApiBearerAuth()
  @Patch('/set-tags')
  @UseGuards(JwtAuthGuard)
  setTags(@User('userId') userId: number, @Body('tagsId') tagsId: number[]) {
    return this.usersService.setTags(userId, tagsId);
  }

  @ApiOperation({
    summary: '设置用户角色',
  })
  @ApiBearerAuth()
  @Patch('/set-roles')
  @UseGuards(JwtAuthGuard)
  setRoles(@User('userId') userId: number, @Body('rolesId') rolesId: number[]) {
    return this.usersService.setRoles(userId, rolesId);
  }

  @ApiOperation({ summary: '更新用户详细信息' })
  @ApiBearerAuth()
  @Patch('/update-profile')
  @UseGuards(JwtAuthGuard)
  async updateProfile(
    @Body(new ValidationPipe({ whitelist: true })) dto: UpdateProfileDto,
    @User('userId') userId: number,
  ) {
    return await this.usersService.updateProfile(userId, dto);
  }

  @Patch(':id')
  update(@Param('id') id: string, @Body() updateUserDto: UpdateUserDto) {
    return this.usersService.update(+id, updateUserDto);
  }

  @Put(":id")
  put(@Param('id') id: string, @Body() updateUserDto: UpdateUserDto) {
    return this.usersService.update(+id, updateUserDto);
  }

  @Delete(':id')
  remove(@Param('id') id: string) {
    return this.usersService.remove(+id);
  }
}
