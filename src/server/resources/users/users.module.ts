import { Module } from '@nestjs/common';
import { UsersService } from './users.service';
import { UsersController } from './users.controller';
import { TypeOrmModule } from '@nestjs/typeorm';
import { User } from './entities/user.entity';
import { FirmsModule } from '../firms/firms.module';
import { UsersFirmsModule } from '../users-firms/users-firms.module';
import { Planet } from '../planets/entities/planet.entity';
import { PerferTagsModule } from '../perfer-tags/perfer-tags.module';
import { RoleTagsModule } from '../role-tags/role-tags.module';

@Module({
  imports: [
    TypeOrmModule.forFeature([User, Planet]),
    FirmsModule,
    UsersFirmsModule,
    PerferTagsModule,
    RoleTagsModule,
  ],
  controllers: [UsersController],
  providers: [UsersService],
  exports: [UsersService],
})
export class UsersModule {}
