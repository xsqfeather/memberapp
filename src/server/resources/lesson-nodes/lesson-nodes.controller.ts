import {
  Controller,
  Get,
  Post,
  Body,
  Param,
  Delete,
  HttpException,
  HttpStatus,
  Res,
  Query,
  Put,
  ParseIntPipe,
  UseGuards,
} from '@nestjs/common';
import { LessonNodesService } from './lesson-nodes.service';
import { CreateLessonNodeDto } from './dto/create-lesson-node.dto';
import { UpdateLessonNodeDto } from './dto/update-lesson-node.dto';
import { In, Like } from 'typeorm';
import { Response } from 'express';
import {
  ApiBearerAuth,
  ApiOperation,
  ApiQuery,
  ApiResponse,
  ApiTags,
} from '@nestjs/swagger';
import { JwtAuthGuard } from '../../auth/jwt-auth.guard';
import { User } from '../../decorators/User';
import { ResponseCreditsDto } from './dto/response-credits.dto';

@ApiTags('课节LessonNodes')
@Controller('lesson-nodes')
export class LessonNodesController {
  constructor(private readonly lessonNodesService: LessonNodesService) {}

  @ApiOperation({ summary: '创建课节' })
  @Post()
  create(@Body() createLessonNodeDto: CreateLessonNodeDto) {
    return this.lessonNodesService.create(createLessonNodeDto);
  }

  @ApiOperation({ summary: '获取所有课节' })
  @Get()
  async findAll(@Res() res: Response, @Query() query: any) {
    const { filter, range, sort } = query;
    let filterObj,
      rangeArr,
      sortArr = {};
    try {
      filterObj = JSON.parse(filter || '{}');
      rangeArr = JSON.parse(range || '[0,9]');
      sortArr = JSON.parse(sort || '["updatedAt", "DESC"]');
    } catch (error) {
      throw new HttpException(
        {
          status: HttpStatus.BAD_REQUEST,
          error: `json parse errors where query one of { filter, range, sort}`,
        },
        HttpStatus.BAD_REQUEST,
      );
    }
    let where: any = {};
    if (filterObj.roleTagId) {
      where.roleTagId = filterObj.roleTagId;
    }
    if (filterObj.lessonId) {
      where.lessonId = filterObj.lessonId;
    }
    if (filterObj.q) {
      where = [
        { name: Like(`%${filterObj.q}%`) },
        { description: Like(`%${filterObj.q}%`) },
      ];
    }
    const [lessonNodes, count]: any = await this.lessonNodesService.findAll({
      range: rangeArr,
      sort: sortArr,
      where,
    });
    res.setHeader(
      'Content-Range',
      `lesson-nodes ${rangeArr[0]}-${rangeArr[1]}/${count}`,
    );
    res.send(lessonNodes);
  }

  @ApiOperation({ summary: '获取用户在该课节的得分' })
  @ApiResponse({
    status: 200,
    description: 'success',
    type: ResponseCreditsDto,
  })
  @ApiBearerAuth()
  @UseGuards(JwtAuthGuard)
  @Get('/credits')
  @ApiQuery({ name: 'lessonNodeId', description: '课节ID' })
  async getCredits(
    @User('userId') userId: number,
    @Query('lessonNodeId', ParseIntPipe) id: number,
  ) {
    return this.lessonNodesService.getCredits(id, userId);
  }

  @Get(':id')
  findOne(@Param('id') id: string) {
    return this.lessonNodesService.findOne(+id);
  }

  @ApiOperation({ summary: '对课件进行排序' })
  @Put('/sort')
  async sort(
    @Query('lessonId', ParseIntPipe) lessonId: number,
    @Query('targetId', ParseIntPipe) targetId: number,
    @Query('afterId') afterId?: string,
    @Query('beforeId') beforeId?: string,
  ) {
    await this.lessonNodesService.saveOrder({
      lessonId,
      targetId,
      afterId: afterId ? Number(afterId) : void 0,
      beforeId: beforeId ? Number(beforeId) : void 0,
    });
  }

  @Put(':id')
  update(
    @Param('id') id: string,
    @Body() updateLessonNodeDto: UpdateLessonNodeDto,
  ) {
    return this.lessonNodesService.update(+id, updateLessonNodeDto);
  }

  @Delete(':id')
  remove(@Param('id') id: string) {
    return this.lessonNodesService.remove(+id);
  }

  @Delete()
  async removeAll(@Query() query: any) {
    const { filter } = query;
    const filterObj = JSON.parse(filter);
    const where = {
      id: In(filterObj.id),
    };
    await this.lessonNodesService.removeAll({ where });
    return [...JSON.parse(query.filter).id];
  }
}
