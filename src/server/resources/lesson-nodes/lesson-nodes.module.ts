import { Module } from '@nestjs/common';
import { LessonNodesService } from './lesson-nodes.service';
import { LessonNodesController } from './lesson-nodes.controller';
import { TypeOrmModule } from '@nestjs/typeorm';
import { LessonNode } from './entities/lesson-node.entity';
import { WatchRecordsModule } from '../watch-records/watch-records.module';
import { ExamRecordsModule } from '../exam-records/exam-records.module';

@Module({
  imports: [
    TypeOrmModule.forFeature([LessonNode]),
    WatchRecordsModule,
    ExamRecordsModule,
  ],
  controllers: [LessonNodesController],
  providers: [LessonNodesService],
  exports: [LessonNodesService],
})
export class LessonNodesModule {}
