import {
  Column,
  CreateDateColumn,
  DeleteDateColumn,
  Entity,
  JoinColumn,
  ManyToOne,
  PrimaryGeneratedColumn,
  UpdateDateColumn,
} from 'typeorm';
import { Lesson } from '../../lessons/entities/lesson.entity';
import { Video } from '../../videos/entities/video.entity';

@Entity()
export class LessonNode {
  @PrimaryGeneratedColumn()
  id: number;

  @Column()
  title: string;

  @Column()
  body: string;

  @Column({
    default: false,
  })
  isPublished: boolean;

  @ManyToOne(() => Lesson, (lesson) => lesson.nodes)
  @JoinColumn({
    name: 'lessonId',
  })
  lesson: Lesson;

  @Column({ nullable: true })
  lessonId: number;

  @Column({
    nullable: true,
  })
  order: number;

  @ManyToOne(() => Video, (video) => video.lessonNodes, {
    cascade: true,
  })
  @JoinColumn({
    name: 'videoId',
  })
  video: Video;

  @Column({
    nullable: true,
  })
  videoId: number;

  @CreateDateColumn()
  createdAt: Date;

  @UpdateDateColumn()
  updatedAt: Date;

  @DeleteDateColumn()
  deletedAt: Date;
}
