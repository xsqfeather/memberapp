import { CreateLessonNodeDto } from './create-lesson-node.dto';

export class UpdateLessonNodeDto extends CreateLessonNodeDto {}
