import { ApiProperty } from '@nestjs/swagger';
import { IsInt, IsOptional, IsString } from 'class-validator';

export class CreateLessonNodeDto {
  @IsString()
  @ApiProperty({ name: 'title', description: '课节标题' })
  title: string;

  @IsString()
  @ApiProperty({ name: 'body', description: '课节内容' })
  body: string;

  @IsInt()
  @ApiProperty({ name: 'lessonId', description: '所属课程ID' })
  lessonId: number;

  @IsOptional()
  @IsInt()
  @ApiProperty({ name: 'videoId', description: '课节视频ID', required: false })
  videoId: number;
}
