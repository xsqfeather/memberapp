import { LessonNode } from '../entities/lesson-node.entity';
import { Exclude, Expose } from 'class-transformer';

@Expose()
export class ResponseLessonNodeDto implements Partial<LessonNode> {
  constructor(partial: Partial<ResponseLessonNodeDto>) {
    Object.assign(this, partial);
  }
  @Exclude()
  createdAt: Date;
  @Exclude()
  updatedAt: Date;
  @Exclude()
  deletedAt: Date;
}
