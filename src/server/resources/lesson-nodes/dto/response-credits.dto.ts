import { ApiProperty } from '@nestjs/swagger';
import { Exclude, Expose } from 'class-transformer';

@Exclude()
export class ResponseCreditsDto {
  constructor(partial: Partial<ResponseCreditsDto>) {
    Object.assign(this, partial);
  }
  @ApiProperty({ name: 'totalCredits', description: '总分' })
  @Expose()
  totalCredits: number;

  @ApiProperty({ name: 'videoTotalCredits', description: '视频总分' })
  @Expose()
  videoTotalCredits: number;

  @ApiProperty({ name: 'videoCredits', description: '视频分' })
  @Expose()
  videoCredits: number;

  @ApiProperty({ name: 'examTotalCredits', description: '练习总分' })
  @Expose()
  examTotalCredits: number;

  @ApiProperty({ name: 'examCredits', description: '练习分' })
  @Expose()
  examCredits: number;

  @ApiProperty({ name: 'credits', description: '得分', type: Number })
  @Expose()
  get credits() {
    return this.videoCredits + this.examCredits;
  }
}
