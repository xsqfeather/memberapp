import { Test, TestingModule } from '@nestjs/testing';
import { LessonNodesController } from './lesson-nodes.controller';
import { LessonNodesService } from './lesson-nodes.service';

describe('LessonNodesController', () => {
  let controller: LessonNodesController;

  beforeEach(async () => {
    const module: TestingModule = await Test.createTestingModule({
      controllers: [LessonNodesController],
      providers: [LessonNodesService],
    }).compile();

    controller = module.get<LessonNodesController>(LessonNodesController);
  });

  it('should be defined', () => {
    expect(controller).toBeDefined();
  });
});
