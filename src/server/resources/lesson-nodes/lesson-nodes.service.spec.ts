import { Test, TestingModule } from '@nestjs/testing';
import { LessonNodesService } from './lesson-nodes.service';

describe('LessonNodesService', () => {
  let service: LessonNodesService;

  beforeEach(async () => {
    const module: TestingModule = await Test.createTestingModule({
      providers: [LessonNodesService],
    }).compile();

    service = module.get<LessonNodesService>(LessonNodesService);
  });

  it('should be defined', () => {
    expect(service).toBeDefined();
  });
});
