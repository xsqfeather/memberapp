import { HttpException, HttpStatus, Injectable } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { Connection, In, Repository } from 'typeorm';
import { CreateLessonNodeDto } from './dto/create-lesson-node.dto';
import { UpdateLessonNodeDto } from './dto/update-lesson-node.dto';
import { LessonNode } from './entities/lesson-node.entity';
import { WatchRecordsService } from '../watch-records/watch-records.service';
import { ExamRecordsService } from '../exam-records/exam-records.service';
import { classToPlain } from 'class-transformer';
import { ResponseCreditsDto } from './dto/response-credits.dto';

const POSITION_STEP = 2 ** 16;

@Injectable()
export class LessonNodesService {
  constructor(
    @InjectRepository(LessonNode)
    private readonly lessonNodeRepository: Repository<LessonNode>,
    private readonly connection: Connection,
    private readonly watchRecordService: WatchRecordsService,
    private readonly examRecordService: ExamRecordsService,
  ) {}

  async create(createLessonNodeDto: CreateLessonNodeDto) {
    const prevLessNode = await this.lessonNodeRepository.findOne({
      where: {
        lessonId: createLessonNodeDto.lessonId,
      },
      order: {
        order: 'DESC',
      },
    });

    const lessonNode = this.lessonNodeRepository.create({
      ...createLessonNodeDto,
      order: (prevLessNode?.order ?? 0) + POSITION_STEP,
    });
    await this.lessonNodeRepository.save(lessonNode);
    return lessonNode;
  }

  findAll({ sort, range, where }) {
    const order = {};
    order[sort[0]] = sort[1];
    return this.lessonNodeRepository.findAndCount({
      order: {
        // ...order,
        order: 'ASC',
      },
      skip: range[0],
      take: range[1] - range[0] + 1,
      where,
      relations: ['video'],
    });
  }

  findOne(id: number) {
    return this.lessonNodeRepository.findOne({
      where: { id },
      relations: ['video'],
    });
  }

  async update(id: number, updateLessonNodeDto: UpdateLessonNodeDto) {
    const lessonNode = await this.lessonNodeRepository.findOne(id);
    lessonNode.lessonId = updateLessonNodeDto.lessonId;
    lessonNode.body = updateLessonNodeDto.body;
    lessonNode.title = updateLessonNodeDto.title;
    lessonNode.videoId = updateLessonNodeDto.videoId;
    await this.lessonNodeRepository.save(lessonNode);
    return lessonNode;
  }

  remove(id: number) {
    return this.lessonNodeRepository.softDelete(id);
  }

  removeAll({ where }: { where: any }) {
    return this.lessonNodeRepository.softDelete({ ...where });
  }

  async updateByVideoIds(videoIds: number[], updateParams: any) {
    await this.lessonNodeRepository.update(
      { videoId: In(videoIds) },
      {
        ...updateParams,
      },
    );
  }

  /**
   * 重新对数据库的数据进行建立顺序
   * @param lessonId
   * @private
   */
  private async reorderPosition(lessonId: number) {
    const queryRunner = this.connection.createQueryRunner();
    await queryRunner.connect();
    await queryRunner.startTransaction();
    try {
      const results = await queryRunner.manager.find(LessonNode, {
        where: {
          lessonId,
        },
        order: {
          order: 'ASC',
        },
      });
      for (let i = 0; i < results.length; i++) {
        const node = results[i];
        await queryRunner.manager.update(
          LessonNode,
          {
            id: node.id,
          },
          {
            order: (i + 1) * POSITION_STEP,
          },
        );
      }
      await queryRunner.commitTransaction();
    } catch (e) {
      await queryRunner.rollbackTransaction();
    } finally {
      await queryRunner.release();
    }
  }

  async saveOrder(params: {
    targetId: number;
    beforeId?: number;
    afterId?: number;
    lessonId: number;
  }): Promise<void>;
  async saveOrder({ targetId, beforeId, afterId, lessonId }) {
    const before = beforeId
      ? await this.lessonNodeRepository.findOne({ id: beforeId })
      : void 0;
    const after = afterId
      ? await this.lessonNodeRepository.findOne({ id: afterId })
      : void 0;
    if (!before && !after)
      throw new HttpException(
        {
          status: HttpStatus.BAD_REQUEST,
          error: 'missing before or after node',
          message: '意外错误',
        },
        HttpStatus.BAD_REQUEST,
      );
    let position: number;
    if (before && after) {
      position = Math.abs((before.order + after.order) / 2);
    } else if (before) {
      position = before.order + POSITION_STEP;
    } else if (after) {
      position = after.order - POSITION_STEP;
    }
    await this.lessonNodeRepository.update(
      {
        id: targetId,
      },
      {
        order: position,
      },
    );
    if (position < 2) {
      await this.reorderPosition(lessonId);
    }
  }

  async getCredits(id: number, userId: number) {
    const { lesson } = await this.lessonNodeRepository.findOne({
      where: { id },
      select: ['id', 'lesson'],
      relations: ['lesson'],
    });
    return classToPlain(
      new ResponseCreditsDto({
        totalCredits: 100,
        videoCredits: await this.watchRecordService.calculateCredits(
          userId,
          lesson.planetId,
        ),
        videoTotalCredits: 40,
        examCredits: await this.examRecordService.calculateCredits(
          userId,
          lesson.planetId,
        ),
        examTotalCredits: 60,
      }),
    );
  }
}
