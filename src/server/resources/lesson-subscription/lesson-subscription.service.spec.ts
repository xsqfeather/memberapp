import { Test, TestingModule } from '@nestjs/testing';
import { LessonSubscriptionService } from './lesson-subscription.service';

describe('LessonSubscriptionService', () => {
  let service: LessonSubscriptionService;

  beforeEach(async () => {
    const module: TestingModule = await Test.createTestingModule({
      providers: [LessonSubscriptionService],
    }).compile();

    service = module.get<LessonSubscriptionService>(LessonSubscriptionService);
  });

  it('should be defined', () => {
    expect(service).toBeDefined();
  });
});
