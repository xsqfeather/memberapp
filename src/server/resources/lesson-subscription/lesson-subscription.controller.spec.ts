import { Test, TestingModule } from '@nestjs/testing';
import { LessonSubscriptionController } from './lesson-subscription.controller';
import { LessonSubscriptionService } from './lesson-subscription.service';

describe('LessonSubscriptionController', () => {
  let controller: LessonSubscriptionController;

  beforeEach(async () => {
    const module: TestingModule = await Test.createTestingModule({
      controllers: [LessonSubscriptionController],
      providers: [LessonSubscriptionService],
    }).compile();

    controller = module.get<LessonSubscriptionController>(
      LessonSubscriptionController,
    );
  });

  it('should be defined', () => {
    expect(controller).toBeDefined();
  });
});
