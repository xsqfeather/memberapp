import {
  Controller,
  Delete,
  Get,
  ParseIntPipe,
  Post,
  Query,
  UseGuards,
} from '@nestjs/common';
import { LessonSubscriptionService } from './lesson-subscription.service';
import {
  ApiBearerAuth,
  ApiOperation,
  ApiQuery,
  ApiTags,
} from '@nestjs/swagger';
import { JwtAuthGuard } from '../../auth/jwt-auth.guard';
import { User } from '../../decorators/User';

@ApiTags('课程订阅LessonSubscription')
@UseGuards(JwtAuthGuard)
@ApiBearerAuth()
@Controller('lesson-subscription')
export class LessonSubscriptionController {
  constructor(
    private readonly lessonSubscriptionService: LessonSubscriptionService,
  ) {}

  @ApiOperation({ summary: '订阅课程' })
  @Post('/subscribe')
  @ApiQuery({ name: 'lessonId', description: '课程ID' })
  async subscribe(
    @Query('lessonId', ParseIntPipe) lessonId: number,
    @User('userId') userId: number,
  ) {
    return await this.lessonSubscriptionService.subscribe(lessonId, userId);
  }

  @ApiOperation({ summary: '取消订阅课程' })
  @Delete('/unsubscribe')
  @ApiQuery({ name: 'lessonId', description: '课程ID' })
  async unsubscribe(
    @Query('lessonId', ParseIntPipe) lessonId: number,
    @User('userId') userId: number,
  ) {
    return await this.lessonSubscriptionService.unsubscribe(lessonId, userId);
  }

  @ApiOperation({ summary: '查看我的订阅' })
  @Get()
  async findMine(@User('userId') userId: number) {
    return await this.lessonSubscriptionService.findByUser(userId);
  }
}
