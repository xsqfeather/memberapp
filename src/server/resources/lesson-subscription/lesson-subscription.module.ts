import { Module } from '@nestjs/common';
import { LessonSubscriptionService } from './lesson-subscription.service';
import { LessonSubscriptionController } from './lesson-subscription.controller';
import { TypeOrmModule } from '@nestjs/typeorm';
import { LessonSubscription } from './entities/lesson-subscription.entity';
import { User } from '../users/entities/user.entity';
import { Lesson } from '../lessons/entities/lesson.entity';

@Module({
  imports: [TypeOrmModule.forFeature([LessonSubscription, User, Lesson])],
  controllers: [LessonSubscriptionController],
  providers: [LessonSubscriptionService],
})
export class LessonSubscriptionModule {}
