import { HttpException, HttpStatus, Injectable } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { LessonSubscription } from './entities/lesson-subscription.entity';
import { Repository } from 'typeorm';
import { User } from '../users/entities/user.entity';
import { Lesson } from '../lessons/entities/lesson.entity';

@Injectable()
export class LessonSubscriptionService {
  constructor(
    @InjectRepository(LessonSubscription)
    private readonly subscriptionRepository: Repository<LessonSubscription>,
    @InjectRepository(User)
    private readonly userRepository: Repository<User>,
    @InjectRepository(Lesson)
    private readonly lessonRepository: Repository<Lesson>,
  ) {}
  static checkPlanet(user: User, lesson: Lesson) {
    if (user.planet.level < lesson.planet.level) {
      throw new HttpException(
        {
          status: HttpStatus.BAD_REQUEST,
          error: 'Level is not satisfied',
          message: '等级不满足',
        },
        HttpStatus.BAD_REQUEST,
      );
    }
  }
  async subscribe(userId: number, lessonId: number) {
    try {
      // 检查用户是否具备订阅的资格
      const user = await this.userRepository.findOne({
        where: {
          id: userId,
        },
        relations: ['planet'],
      });
      const lesson = await this.lessonRepository.findOne({
        where: {
          id: lessonId,
        },
        relations: ['planet'],
      });
      LessonSubscriptionService.checkPlanet(user, lesson);
      const result = this.subscriptionRepository.create({
        userId,
        lessonId,
      });
      await this.subscriptionRepository.save(result);
      return result.id;
    } catch (e) {
      if (e instanceof HttpException) throw e;
      throw new HttpException(
        {
          status: HttpStatus.BAD_REQUEST,
          error: 'Does not exist the lesson',
          message: '不存在该课程',
        },
        HttpStatus.BAD_REQUEST,
      );
    }
  }
  async unsubscribe(userId: number, lessonId: number) {
    const result = await this.subscriptionRepository.softDelete({
      userId,
      lessonId,
    });
    return { success: result.affected > 0 };
  }
  async findByUser(userId: number) {
    return this.subscriptionRepository.find({
      where: {
        userId,
      },
      relations: ['lesson'],
    });
  }
}
