import { PartialType } from '@nestjs/swagger';
import { CreateDistributedImageDto } from './create-distributed-image.dto';

export class UpdateDistributedImageDto extends PartialType(
  CreateDistributedImageDto,
) {}
