import { Module } from '@nestjs/common';
import { DistributedImagesService } from './distributed-images.service';
import { DistributedImagesController } from './distributed-images.controller';
import { MulterModule } from '@nestjs/platform-express';
import { TypeOrmModule } from '@nestjs/typeorm';
import { DistributedImage } from './entities/distributed-image.entity';

@Module({
  imports: [
    TypeOrmModule.forFeature([DistributedImage]),
    MulterModule.registerAsync({
      useFactory: () => ({
        dest: process.cwd() + '/public/temp',
      }),
    }),
  ],
  controllers: [DistributedImagesController],
  providers: [DistributedImagesService],
})
export class DistributedImagesModule {}
