import {
  Column,
  CreateDateColumn,
  DeleteDateColumn,
  Entity,
  PrimaryGeneratedColumn,
  UpdateDateColumn,
  VersionColumn,
} from 'typeorm';

@Entity()
export class DistributedImage {
  @PrimaryGeneratedColumn()
  id: number;

  @Column({ nullable: true })
  label: string;

  @Column({ nullable: true })
  mimetype: string;

  @Column({
    nullable: true,
  })
  uuid: string;

  @Column({
    nullable: true,
  })
  uniqueName: string;

  @Column({ nullable: true })
  size: number;

  @Column()
  originalname: string;

  @Column({
    nullable: true,
  })
  originUrl: string;

  @Column({ nullable: true })
  ipfsCid: string;

  @Column({ nullable: true })
  ipfsUrl: string;

  @Column({
    nullable: true,
  })
  cover: string;

  @Column({
    nullable: true,
  })
  thumbnail: string;

  @CreateDateColumn()
  createdAt: Date;

  @UpdateDateColumn()
  updatedAt: Date;

  @DeleteDateColumn()
  deletedAt: Date;

  @VersionColumn()
  versoin: number;
}
