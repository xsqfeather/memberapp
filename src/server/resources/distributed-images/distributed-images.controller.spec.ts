import { Test, TestingModule } from '@nestjs/testing';
import { DistributedImagesController } from './distributed-images.controller';
import { DistributedImagesService } from './distributed-images.service';

describe('DistributedImagesController', () => {
  let controller: DistributedImagesController;

  beforeEach(async () => {
    const module: TestingModule = await Test.createTestingModule({
      controllers: [DistributedImagesController],
      providers: [DistributedImagesService],
    }).compile();

    controller = module.get<DistributedImagesController>(
      DistributedImagesController,
    );
  });

  it('should be defined', () => {
    expect(controller).toBeDefined();
  });
});
