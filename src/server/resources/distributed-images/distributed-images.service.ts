import { Injectable } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { Repository } from 'typeorm';
import { CreateDistributedImageDto } from './dto/create-distributed-image.dto';
import { DistributedImage } from './entities/distributed-image.entity';

@Injectable()
export class DistributedImagesService {
  constructor(
    @InjectRepository(DistributedImage)
    private distributedImageRepository: Repository<DistributedImage>,
  ) {}
  async create(createDistributedImageDto: CreateDistributedImageDto) {
    const img = this.distributedImageRepository.create(
      createDistributedImageDto,
    );
    await this.distributedImageRepository.save(img);
    return img;
  }

  async findOne(id: number) {
    return await this.distributedImageRepository.findOne(id);
  }

  async findOneByFileHash(hash: string) {
    return await this.distributedImageRepository.findOne({ ipfsCid: hash });
  }
}
