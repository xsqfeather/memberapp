import { Test, TestingModule } from '@nestjs/testing';
import { DistributedImagesService } from './distributed-images.service';

describe('DistributedImagesService', () => {
  let service: DistributedImagesService;

  beforeEach(async () => {
    const module: TestingModule = await Test.createTestingModule({
      providers: [DistributedImagesService],
    }).compile();

    service = module.get<DistributedImagesService>(DistributedImagesService);
  });

  it('should be defined', () => {
    expect(service).toBeDefined();
  });
});
