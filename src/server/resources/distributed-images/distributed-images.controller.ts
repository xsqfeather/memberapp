import {
  Controller,
  Get,
  Post,
  Body,
  Patch,
  Param,
  Headers,
  Req,
  UseInterceptors,
  UploadedFile,
  Res,
  HttpException,
  HttpStatus,
} from '@nestjs/common';
import { FileInterceptor } from '@nestjs/platform-express';
import { Response } from 'express';
import { readFileSync } from 'fs';
import { IpStorageService } from '../../ip-storage/ip-storage.service';
import { DistributedImagesService } from './distributed-images.service';

const dev = process.env.NODE_ENV !== 'production';

@Controller('distributed-images')
export class DistributedImagesController {
  constructor(
    private readonly distributedImagesService: DistributedImagesService,
  ) {}

  @Post()
  @UseInterceptors(FileInterceptor('file'))
  async create(
    @UploadedFile() file, //hfileName:flutter 上传添加的头
    @Headers('fileName') hfileName: string,
  ) {
    console.log(file);
    
    const filename = hfileName || file.originalname;
    const ext = filename.slice(filename.lastIndexOf('.')+1) ;
    let fMime = 'image/*';
    switch (ext.toLocaleLowerCase()) {
      case 'png':
        fMime = 'image/png';
        break;
      case 'jpg':
      case 'jpeg':
        fMime = 'image/jpeg';
        break;
      case 'gif':
        fMime = 'image/gif';
    }
    const uploadFile = readFileSync(file.path);
    const rlt = await IpStorageService.node.add(uploadFile);

    const DI:any = await this.distributedImagesService.create({
      mimetype: hfileName ? fMime : file.mimetype,
      originalname: hfileName ? hfileName : file.originalname,
      ipfsCid: rlt.path,
      size: rlt.size,
    });
    return {
      url: dev
        ? `http://localhost:3000/api/distributed-images/${rlt.path}`
        : `http://hk.beingred.net/api/distributed-images/${rlt.path}`,
    };
  }

  @Get(':id')
  async findOne(@Param('id') id: string, @Res() res: Response) {
    const DI = await this.distributedImagesService.findOneByFileHash(id);
    if (!DI) {
      throw new HttpException('not found image!', HttpStatus.BAD_REQUEST);
    }
    const content = [];
    for await (const file of IpStorageService.node.get(id)) {
      if (!file.content) continue;
      for await (const chunk of file.content) {
        content.push(chunk);
      }
    }

    let length = 0;
    content.forEach((item) => {
      length += item.length;
    });
    let mergedArray = new Uint8Array(length);
    let offset = 0;
    content.forEach((item) => {
      mergedArray.set(item, offset);
      offset += item.length;
    });
    const b = Buffer.from(mergedArray);
    res.setHeader('Content-Type', DI.mimetype);
    res.setHeader('Content-Length', length);
    //客户端缓存7天
    /* res.setHeader('Cache-Control', `public, max-age=${60 * 60 * 24 * 7}`); */
    console.log({DI});
    
    res.setHeader('Content-Disposition', `inline;filename=${DI.ipfsCid.toString()}`);
    res.send(b);
  }
}
