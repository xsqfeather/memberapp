import { Test, TestingModule } from '@nestjs/testing';
import { MyFollowMiniBlogsService } from './my-follow-mini-blogs.service';

describe('MyFollowMiniBlogsService', () => {
  let service: MyFollowMiniBlogsService;

  beforeEach(async () => {
    const module: TestingModule = await Test.createTestingModule({
      providers: [MyFollowMiniBlogsService],
    }).compile();

    service = module.get<MyFollowMiniBlogsService>(MyFollowMiniBlogsService);
  });

  it('should be defined', () => {
    expect(service).toBeDefined();
  });
});
