import { Injectable } from '@nestjs/common';
import { CreateMyFollowMiniBlogDto } from './dto/create-my-follow-mini-blog.dto';
import { FindConditions, Repository } from 'typeorm';
import { InjectRepository } from '@nestjs/typeorm';
import { MyFollowMiniBlog } from './entities/my-follow-mini-blog.entity';
import { OnEvent } from '@nestjs/event-emitter';
import {
  MINI_BLOG_ON_CREATE,
  MINI_BLOG_ON_DELETE,
  USER_FOLLOW_USER,
  USER_UNFOLLOW_USER,
} from '../../common/Emitters';
import { FollowsService } from '../follows/follows.service';
import { MiniBlogService } from '../mini-blog/mini-blog.service';
import { UsersService } from '../users/users.service';

@Injectable()
export class MyFollowMiniBlogsService {
  constructor(
    private followsService: FollowsService,
    private miniBlogService: MiniBlogService,
    private usersService: UsersService,
    @InjectRepository(MyFollowMiniBlog)
    private readonly myFollowMiniBlogRepository: Repository<MyFollowMiniBlog>,
  ) {}

  create(createMyFollowMiniBlogDto: CreateMyFollowMiniBlogDto) {
    return this.myFollowMiniBlogRepository.save(
      this.myFollowMiniBlogRepository.create(createMyFollowMiniBlogDto),
    );
  }

  delete(where: FindConditions<MyFollowMiniBlog>) {
    //使用delete而不是remove,主要原因保证速度
    return this.myFollowMiniBlogRepository.delete(where);
  }

  feedUser(userId: number, { range, sort }: any) {
    return this.myFollowMiniBlogRepository
      .createQueryBuilder('mfmb')
      .where(`mfmb.userId = :userId`, { userId })
      .take(range[1] - range[0] + 1)
      .skip(range[0])
      .orderBy(`mfmb.${sort[0]}`, sort[1])
      .getManyAndCount();
  }

  //创建
  @OnEvent(MINI_BLOG_ON_CREATE)
  async pushMiniBlogToFollowMeUsers({ userId, updateAt, miniBlogId }) {
    //取出关注我的用户
    let skip = 0;
    let take = 3000;
    let next = false;
    do {
      let [follows, AllCount] = await this.followsService.getFansIds(
        userId,
        skip,
        take,
      );
      for (let i = 0, timeout = 0; i < follows.length; i++, timeout += 50) {
        const follow = follows[i];
        this.usersService.isRobot(userId).then((isRobot) => {
          if (!isRobot) {
            setTimeout(() => {
              this.create({
                miniBlogId,
                miniBlogCreatedAt: updateAt,
                userId: follow.fansId,
                followUserId: userId,
              }).then((value) => {
                /* console.log(`[插入]:${value}`); */
              });
            }, timeout);
          }
        });
      }
      //如果粉丝没有处理完
      if (AllCount > follows.length + skip) {
        skip += follows.length; //当前条件下等同于 +=take
        next = true;
      } else {
        next = false;
      }
    } while (next);
  }

  //删帖
  @OnEvent(MINI_BLOG_ON_DELETE)
  deletePushMiniBlogFromMyFans({ miniBlogId }) {
    this.myFollowMiniBlogRepository
      .createQueryBuilder()
      .delete()
      .where(`miniBlogId = :miniBlogId`, { miniBlogId })
      .execute();
  }

  //关注
  @OnEvent(USER_FOLLOW_USER)
  insertMiniBlogsToMyFans({ userId, followUserId }) {
    //todo transform stream
    this.usersService.isRobot(userId).then((IsRobot) => {
      if (!IsRobot) {
        this.miniBlogService
          .findUserAllMiniBlogIds(followUserId)
          .then((miniBlogs) => {
            const timer = setInterval(() => {
              if (miniBlogs[0] != undefined) {
                this.myFollowMiniBlogRepository.save(
                  this.myFollowMiniBlogRepository.create({
                    userId,
                    followUserId,
                    miniBlogId: miniBlogs[0].id,
                    miniBlogCreatedAt: miniBlogs[0].updatedAt,
                  }),
                );
                miniBlogs.shift();
              } else {
                clearInterval(timer);
              }
            }, 20); //1分钟50条录入
          });
      }
    });
  }

  //取关
  @OnEvent(USER_UNFOLLOW_USER)
  deletePushMiniBlogInUnfollow({ userId, followUserId }) {
    //userId被关注人,fansId关注人
    this.myFollowMiniBlogRepository
      .createQueryBuilder()
      .delete() //调用delete时,别名无效
      .where(`userId = :userId AND followUserId = :followUserId`, {
        userId,
        followUserId,
      })
      .execute();
  }
}


