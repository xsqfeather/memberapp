import {
  Column,
  CreateDateColumn,
  Entity,
} from 'typeorm';

@Entity()
export class MyFollowMiniBlog {
  @Column({
    primary: true,
  })
  userId: number;

  @Column()
  followUserId: number;

  @Column({
    primary: true,
  })
  miniBlogId: number;

  @CreateDateColumn()
  miniBlogCreatedAt: Date;
}
