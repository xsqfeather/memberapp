import { Module } from '@nestjs/common';
import { MyFollowMiniBlogsService } from './my-follow-mini-blogs.service';
import { MyFollowMiniBlogsController } from './my-follow-mini-blogs.controller';
import { MyFollowMiniBlog } from './entities/my-follow-mini-blog.entity';
import { FollowsModule } from '../follows/follows.module';
import { MiniBlogModule } from '../mini-blog/mini-blog.module';
import { TypeOrmModule } from '@nestjs/typeorm';
import { UsersModule } from '../users/users.module';

@Module({
  imports: [
    TypeOrmModule.forFeature([MyFollowMiniBlog]),
    FollowsModule,
    MiniBlogModule,
    UsersModule,
  ],
  controllers: [MyFollowMiniBlogsController],
  providers: [MyFollowMiniBlogsService],
})
export class MyFollowMiniBlogsModule {}
