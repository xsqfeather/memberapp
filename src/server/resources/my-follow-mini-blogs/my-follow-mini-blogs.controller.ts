import {
  Controller,
  Get,
  HttpException,
  HttpStatus,
  Query,
  UseGuards,
} from '@nestjs/common';
import { MyFollowMiniBlogsService } from './my-follow-mini-blogs.service';
import { JwtAuthGuard } from '../../auth/jwt-auth.guard';
import { User } from '../../decorators/User';
import { ApiOperation, ApiTags } from '@nestjs/swagger';

@ApiTags('关注推送')
@Controller('my-follow-mini-blogs')
export class MyFollowMiniBlogsController {
  constructor(
    private readonly myFollowMiniBlogsService: MyFollowMiniBlogsService,
  ) {}

  @UseGuards(JwtAuthGuard)
  @ApiOperation({ summary: '推送用户所关注的人的微博(朋友圈)' })
  @Get()
  findAll(@User('userId') userId: number, @Query() query: any) {
    const { range, sort } = query;
    let rangeArr,
      sortArr = {};
    try {
      rangeArr = JSON.parse((range as unknown as string) || '[0,9]');
      sortArr = JSON.parse(
        (sort as unknown as string) || '["miniBlogCreatedAt", "DESC"]',
      );
    } catch (error) {
      throw new HttpException(
        {
          status: HttpStatus.BAD_REQUEST,
          error: `json parse errors where query one of { filter, range, sort}`,
        },
        HttpStatus.BAD_REQUEST,
      );
    }
    return this.myFollowMiniBlogsService.feedUser(userId, {
      range: rangeArr,
      sort: sortArr,
    });
  }
}
