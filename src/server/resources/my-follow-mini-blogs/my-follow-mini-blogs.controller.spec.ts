import { Test, TestingModule } from '@nestjs/testing';
import { MyFollowMiniBlogsController } from './my-follow-mini-blogs.controller';
import { MyFollowMiniBlogsService } from './my-follow-mini-blogs.service';

describe('MyFollowMiniBlogsController', () => {
  let controller: MyFollowMiniBlogsController;

  beforeEach(async () => {
    const module: TestingModule = await Test.createTestingModule({
      controllers: [MyFollowMiniBlogsController],
      providers: [MyFollowMiniBlogsService],
    }).compile();

    controller = module.get<MyFollowMiniBlogsController>(
      MyFollowMiniBlogsController,
    );
  });

  it('should be defined', () => {
    expect(controller).toBeDefined();
  });
});
