import { PartialType } from '@nestjs/swagger';
import { CreateMyFollowMiniBlogDto } from './create-my-follow-mini-blog.dto';

export class UpdateMyFollowMiniBlogDto extends PartialType(
  CreateMyFollowMiniBlogDto,
) {}
