export class CreateMyFollowMiniBlogDto {
  userId: number;
  followUserId: number;
  miniBlogId: number;
  miniBlogCreatedAt: Date;
}
