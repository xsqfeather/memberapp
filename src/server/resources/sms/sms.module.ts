import { Module } from '@nestjs/common';
import { SmsService } from './sms.service';
import { SmsController } from './sms.controller';
import { LevelCacheModule } from '../../level-cache/level-cache.module';
import { LevelCacheService } from '../../level-cache/level-cache.service';

@Module({
  imports: [LevelCacheModule],
  controllers: [SmsController],
  providers: [SmsService, LevelCacheService],
})
export class SmsModule {}
