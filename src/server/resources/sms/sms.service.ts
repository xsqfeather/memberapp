import { HttpException, HttpStatus, Injectable } from '@nestjs/common';
import { LevelCacheService } from '../../level-cache/level-cache.service';
import { CreateSmDto } from './dto/create-sm.dto';
import { UpdateSmDto } from './dto/update-sm.dto';

import Dysmsapi20170525, * as $Dysmsapi20170525 from '@alicloud/dysmsapi20170525';
import * as $OpenApi from '@alicloud/openapi-client';

const accessKeyId = 'LTAIBYIHofssXbWt';
const accessKeySecret = 'RkNyIBM8hz6PpjRJ4MFyiRI75VbmPp';

@Injectable()
export class SmsService {
  static client: Dysmsapi20170525;
  constructor(private levelCacheService: LevelCacheService) {
    SmsService.client = SmsService.createAliSMSClient(
      accessKeyId,
      accessKeySecret,
    );
  }
  async create(createSmDto: CreateSmDto) {
    let { phone } = createSmDto;
    if (!phone) {
      throw new HttpException(
        {
          status: HttpStatus.BAD_REQUEST,
          error: `sms:create:fail:phone_required`,
          code: `PHONE_REQUIRED`,
        },
        HttpStatus.BAD_REQUEST,
      );
    }
    let sms = '';
    const randomNumber = Math.random() * 10000;
    const smsCode = Math.round(randomNumber);
    sms = smsCode.toString();
    if (smsCode < 10) {
      sms = '000' + sms;
    }
    if (smsCode < 100 && smsCode >= 10) {
      sms = '00' + sms;
    }
    if (smsCode >= 100 && smsCode < 1000) {
      sms = '0' + sms;
    }
    await this.levelCacheService.setItem(phone, sms);
    setTimeout(async () => {
      await this.levelCacheService.setItem(phone, null);
    }, 1000 * 60 * 5);
    if (sms) {
      console.log({ sms });
      await SmsService.sendCode(phone, sms);
      return {
        code: 'success',
      };
    }
    return {
      code: 'fail',
    };
  }

  async findOne(phone: string) {
    return this.levelCacheService.getItem(phone);
  }

  update(id: number, updateSmDto: UpdateSmDto) {
    return `This action updates a #${id} sm`;
  }

  remove(id: number) {
    return `This action removes a #${id} sm`;
  }

  static async sendCode(phone: string, sms: string) {
    let sendSmsRequest = new $Dysmsapi20170525.SendSmsRequest({
      phoneNumbers: phone,
      signName: '老猫商城',
      templateParam: `{valid: ${sms},  name: \"亲\" }`,
      templateCode: 'SMS_27665222',
    });
    await SmsService.client.sendSms(sendSmsRequest);
  }

  static createAliSMSClient(
    accessKeyId: string,
    accessKeySecret: string,
  ): Dysmsapi20170525 {
    const config = new $OpenApi.Config({
      accessKeyId: accessKeyId,
      accessKeySecret: accessKeySecret,
    });
    config.endpoint = 'dysmsapi.aliyuncs.com';
    return new Dysmsapi20170525(config);
  }
}
