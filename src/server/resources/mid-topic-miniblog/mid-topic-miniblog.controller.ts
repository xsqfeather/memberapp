import {
  Controller,
  Get,
  HttpException,
  HttpStatus,
  Param,
  Query,
  Response,
} from '@nestjs/common';
import { ApiOperation, ApiQuery, ApiTags } from '@nestjs/swagger';
import { MidTopicMiniblogService } from './mid-topic-miniblog.service';

@ApiTags('话题帖子相关')
@Controller('mid-topic-miniblog')
export class MidTopicMiniblogController {
  constructor(
    private readonly midTopicMiniblogService: MidTopicMiniblogService,
  ) {}

  @Get(':id')
  @ApiOperation({ summary: '获取话题下帖子' })
  @ApiQuery({
    name: 'range',
    description: '分页, [start: number, end: number]',
    required: false,
  })
  @ApiQuery({
    name: 'sort',
    description: '排序, [field: string, order: "DESC" | "ASC"]',
    required: false,
  })
  async findMiniBlogsInTopicId(
    @Param('id') id: string,
    @Query() query,
    @Response() res,
  ) {
    const { sort, range } = query;
    let rangeArr,
      sortArr = {};
    try {
      rangeArr = JSON.parse(range || '[0,9]');
      sortArr = JSON.parse(sort || '["updatedAt", "DESC"]');
    } catch (error) {
      throw new HttpException(
        {
          status: HttpStatus.BAD_REQUEST,
          error: `json parse errors where query one of { range, sort}`,
        },
        HttpStatus.BAD_REQUEST,
      );
    }
    const [mid, count] =
      await this.midTopicMiniblogService.findMiniBlogsInTopic(+id, {
        sort: sortArr,
        range: rangeArr,
      });
    const miniBlogs = mid.map((mid) => {
      return mid.miniBlog;
    });
    res.setHeader(
      'Content-Range',
      `mini-blogs ${rangeArr[0]}-${rangeArr[1]}/${count}`,
    );
    res.send(miniBlogs);
  }

  @Get('miniblog/:id')
  @ApiOperation({ summary: '获取话题下帖子' })
  findTopicsOfMiniBlog(@Param('id') id:number){
    return this.midTopicMiniblogService.findTopicsOfMiniBlog(+id)
  }
}
