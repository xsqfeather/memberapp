import { Test, TestingModule } from '@nestjs/testing';
import { MidTopicMiniblogService } from './mid-topic-miniblog.service';

describe('MidTopicMiniblogService', () => {
  let service: MidTopicMiniblogService;

  beforeEach(async () => {
    const module: TestingModule = await Test.createTestingModule({
      providers: [MidTopicMiniblogService],
    }).compile();

    service = module.get<MidTopicMiniblogService>(MidTopicMiniblogService);
  });

  it('should be defined', () => {
    expect(service).toBeDefined();
  });
});
