import { PartialType } from '@nestjs/swagger';
import { CreateMidTopicMiniblogDto } from './create-mid-topic-miniblog.dto';

export class UpdateMidTopicMiniblogDto extends PartialType(CreateMidTopicMiniblogDto) {}
