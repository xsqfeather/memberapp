import {
  Column,
  CreateDateColumn,
  DeleteDateColumn,
  Entity,
  JoinColumn,
  ManyToOne,
  PrimaryGeneratedColumn,
  UpdateDateColumn,
} from 'typeorm';
import { Topic } from '../../topics/entities/topic.entity';
import { MiniBlog } from '../../mini-blog/entities/mini-blog.entity';

@Entity()
export class MidTopicMiniBlog {
  @PrimaryGeneratedColumn()
  id: number;


  @ManyToOne((type) => Topic, (topic) => topic.midTopicMiniBlog)
  @JoinColumn({
    name: 'topicId',
  })
  topic: Topic;

  @Column({
    nullable: true,
  })
  topicId: number;


  @ManyToOne((type) => MiniBlog, (miniBlog) => miniBlog.midTopicMiniBlog)
  @JoinColumn()
  miniBlog: MiniBlog;

  @CreateDateColumn()
  createdAt: Date;

  @UpdateDateColumn()
  updatedAt: Date;

  @DeleteDateColumn()
  deletedAt: Date;
}
