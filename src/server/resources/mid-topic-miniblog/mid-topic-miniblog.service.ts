import { Injectable } from '@nestjs/common';
import { CreateMidTopicMiniblogDto } from './dto/create-mid-topic-miniblog.dto';
import { UpdateMidTopicMiniblogDto } from './dto/update-mid-topic-miniblog.dto';
import { MidTopicMiniBlog } from './entities/mid-topic-miniblog.entity';
import { InjectRepository } from '@nestjs/typeorm';
import { Repository } from 'typeorm';
@Injectable()
export class MidTopicMiniblogService {
  constructor(
    @InjectRepository(MidTopicMiniBlog)
    private midTopicMiniBlogRepository: Repository<MidTopicMiniBlog>,
  ) {}
  async create(createMidTopicMiniblogDto: CreateMidTopicMiniblogDto) {
    const { topicId } = createMidTopicMiniblogDto;
    const mid = this.midTopicMiniBlogRepository.create({ topicId });
    return await this.midTopicMiniBlogRepository.save(mid);
  }

  findMiniBlogsInTopic(topicId: number, { sort, range }: any) {
    const order = {};
    order[sort[0]] = sort[1];
    return this.midTopicMiniBlogRepository.findAndCount({
      order,
      where: { topicId: topicId },
      skip: range[0],
      take: range[1] - range[0] + 1,
      relations: ['miniBlog'],
    });
  }

  findTopicsOfMiniBlog(miniBlogId: number) {
    return this.midTopicMiniBlogRepository
            .query(`
              select t.*
              from mid_topic_mini_blog mtmb
              join topic t on t.id = mtmb ."topicId" 
              where mtmb ."miniBlogId" = ${miniBlogId}
            `)
  }

  findAll() {
    return `This action returns all midTopicMiniblog`;
  }

  findOne(id: number) {
    return `This action returns a #${id} midTopicMiniblog`;
  }

  update(id: number, updateMidTopicMiniblogDto: UpdateMidTopicMiniblogDto) {
    return `This action updates a #${id} midTopicMiniblog`;
  }

  remove(id: number) {
    return `This action removes a #${id} midTopicMiniblog`;
  }
}
