import { Module } from '@nestjs/common';
import { MidTopicMiniblogService } from './mid-topic-miniblog.service';
import { MidTopicMiniblogController } from './mid-topic-miniblog.controller';
import { MidTopicMiniBlog } from './entities/mid-topic-miniblog.entity';
import { TypeOrmModule } from '@nestjs/typeorm';

@Module({
  imports: [TypeOrmModule.forFeature([MidTopicMiniBlog])],
  controllers: [MidTopicMiniblogController],
  providers: [MidTopicMiniblogService],
  exports: [MidTopicMiniblogService],
})
export class MidTopicMiniblogModule {}
