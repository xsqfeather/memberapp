import { Test, TestingModule } from '@nestjs/testing';
import { MidTopicMiniblogController } from './mid-topic-miniblog.controller';
import { MidTopicMiniblogService } from './mid-topic-miniblog.service';

describe('MidTopicMiniblogController', () => {
  let controller: MidTopicMiniblogController;

  beforeEach(async () => {
    const module: TestingModule = await Test.createTestingModule({
      controllers: [MidTopicMiniblogController],
      providers: [MidTopicMiniblogService],
    }).compile();

    controller = module.get<MidTopicMiniblogController>(MidTopicMiniblogController);
  });

  it('should be defined', () => {
    expect(controller).toBeDefined();
  });
});
