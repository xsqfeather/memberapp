import {
  Controller,
  Get,
  Post,
  Body,
  Param,
  Delete,
  Res,
  Query,
  HttpException,
  HttpStatus,
  Put,
} from '@nestjs/common';
import { PerferTagsService } from './perfer-tags.service';
import { CreatePerferTagDto } from './dto/create-perfer-tag.dto';
import { UpdatePerferTagDto } from './dto/update-perfer-tag.dto';
import { In, Like } from 'typeorm';
import { Response } from 'express';
import { PerferTag } from './entities/perfer-tag.entity';
import { FindConditions } from 'typeorm/find-options/FindConditions';
import { ApiTags } from '@nestjs/swagger';

@ApiTags('标签PerferTags')
@Controller('perfer-tags')
export class PerferTagsController {
  constructor(private readonly perferTagsService: PerferTagsService) {}

  @Post()
  create(@Body() createPerferTagDto: CreatePerferTagDto) {
    return this.perferTagsService.create(createPerferTagDto);
  }

  @Get()
  async findAll(@Res() res: Response, @Query() query: any) {
    const { filter, range, sort } = query;
    let filterObj,
      rangeArr,
      sortArr = {};
    try {
      filterObj = JSON.parse(filter || '{}');
      rangeArr = JSON.parse(range || '[0,9]');
      sortArr = JSON.parse(sort || '["updatedAt", "DESC"]');
    } catch (error) {
      throw new HttpException(
        {
          status: HttpStatus.BAD_REQUEST,
          error: `json parse errors where query one of { filter, range, sort}`,
        },
        HttpStatus.BAD_REQUEST,
      );
    }
    let where: Array<FindConditions<PerferTag>> = [];

    if (filterObj.q) {
      where = [
        { tagName: Like(`%${filterObj.q}%`) },
        { description: Like(`%${filterObj.q}%`) },
      ];
    }
    if (filterObj.id) {
      where.push({
        id: In(filterObj.id.flat()),
      });
    }
    const [tags, count]: [PerferTag[], number] =
      await this.perferTagsService.findAll({
        range: rangeArr,
        sort: sortArr,
        where,
      });
    res.setHeader(
      'Content-Range',
      `perfer-tags ${rangeArr[0]}-${rangeArr[1]}/${count}`,
    );
    res.send(tags);
  }

  @Get(':id')
  findOne(@Param('id') id: string) {
    return this.perferTagsService.findOne(+id);
  }

  @Put(':id')
  update(
    @Param('id') id: string,
    @Body() updatePerferTagDto: UpdatePerferTagDto,
  ) {
    return this.perferTagsService.update(+id, updatePerferTagDto);
  }

  @Delete(':id')
  remove(@Param('id') id: string) {
    return this.perferTagsService.remove(+id);
  }

  @Delete()
  async removeAll(@Query() query: any) {
    const { filter } = query;
    const filterObj = JSON.parse(filter);
    const where = {
      id: In(filterObj.id),
    };
    await this.perferTagsService.removeAll({ where });
    return [...JSON.parse(query.filter).id];
  }
}
