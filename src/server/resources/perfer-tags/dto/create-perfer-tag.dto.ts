import { ApiProperty } from '@nestjs/swagger';

export class CreatePerferTagDto {
  @ApiProperty({ name: 'tagName', description: '标签名' })
  tagName: string;
  @ApiProperty({ name: 'description', description: '描述', required: false })
  description?: string;
}
