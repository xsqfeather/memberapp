import { PerferTag } from '../entities/perfer-tag.entity';
import { Exclude, Expose } from 'class-transformer';
import { ApiProperty } from '@nestjs/swagger';

@Exclude()
export class ResponseTagDto implements Partial<PerferTag> {
  constructor(partial: Partial<ResponseTagDto>) {
    Object.assign(this, partial);
  }
  @ApiProperty({ name: 'id', description: 'ID' })
  @Expose()
  id: number;

  @ApiProperty({ name: 'tagName', description: '标签名' })
  @Expose()
  tagName: string;

  @ApiProperty({ name: 'description', description: '描述' })
  @Expose()
  description: string;
}
