import { Test, TestingModule } from '@nestjs/testing';
import { PerferTagsService } from './perfer-tags.service';

describe('PerferTagsService', () => {
  let service: PerferTagsService;

  beforeEach(async () => {
    const module: TestingModule = await Test.createTestingModule({
      providers: [PerferTagsService],
    }).compile();

    service = module.get<PerferTagsService>(PerferTagsService);
  });

  it('should be defined', () => {
    expect(service).toBeDefined();
  });
});
