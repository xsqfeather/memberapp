import { HttpException, HttpStatus, Injectable } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { Repository, In } from 'typeorm';
import { CreatePerferTagDto } from './dto/create-perfer-tag.dto';
import { UpdatePerferTagDto } from './dto/update-perfer-tag.dto';
import { PerferTag } from './entities/perfer-tag.entity';

@Injectable()
export class PerferTagsService {
  constructor(
    @InjectRepository(PerferTag)
    private readonly perferTagsRepository: Repository<PerferTag>,
  ) {}
  async create(createPerferTagDto: CreatePerferTagDto) {
    const { tagName, description } = createPerferTagDto;
    let perferTag = await this.perferTagsRepository.findOne({
      tagName,
    });
    if (perferTag) {
      throw new HttpException(
        {
          status: HttpStatus.BAD_REQUEST,
          error: `perfer-tags:create:fail`,
          message: `该角色已经有了此标签`,
        },
        HttpStatus.BAD_REQUEST,
      );
    }
    perferTag = this.perferTagsRepository.create();
    if (tagName) {
      perferTag.tagName = tagName;
    }
    if (description) {
      perferTag.description = description;
    }

    await this.perferTagsRepository.save(perferTag);

    return perferTag;
  }

  findAll({ sort, range, where }) {
    const order = {};
    order[sort[0]] = sort[1];
    return this.perferTagsRepository.findAndCount({
      order,
      skip: range[0],
      take: range[1] - range[0] + 1,
      where,
    });
  }

  findOne(id: number) {
    if (id) {
      return this.perferTagsRepository.findOne({
        where: { id },
      });
    }
    return this.perferTagsRepository.create();
  }

  async update(id: number, updatePerferTagDto: UpdatePerferTagDto) {
    const { tagName, description } = updatePerferTagDto;
    const result = await this.perferTagsRepository.update(
      {
        id,
      },
      { tagName, description },
    );
    return { id, success: result.affected > 0 };
  }

  remove(id: number) {
    return this.perferTagsRepository.softDelete(id);
  }

  removeAll({ where }: { where: any }) {
    return this.perferTagsRepository.softDelete({ ...where });
  }

  findMany(ids: number[]) {
    return this.perferTagsRepository.find({
      where: {
        id: In(ids),
      },
    });
  }
}
