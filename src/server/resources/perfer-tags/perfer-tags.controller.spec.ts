import { Test, TestingModule } from '@nestjs/testing';
import { PerferTagsController } from './perfer-tags.controller';
import { PerferTagsService } from './perfer-tags.service';

describe('PerferTagsController', () => {
  let controller: PerferTagsController;

  beforeEach(async () => {
    const module: TestingModule = await Test.createTestingModule({
      controllers: [PerferTagsController],
      providers: [PerferTagsService],
    }).compile();

    controller = module.get<PerferTagsController>(PerferTagsController);
  });

  it('should be defined', () => {
    expect(controller).toBeDefined();
  });
});
