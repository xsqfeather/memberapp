import { Module } from '@nestjs/common';
import { PerferTagsService } from './perfer-tags.service';
import { PerferTagsController } from './perfer-tags.controller';
import { TypeOrmModule } from '@nestjs/typeorm';
import { PerferTag } from './entities/perfer-tag.entity';
import { LevelCacheModule } from '../../level-cache/level-cache.module';
import { TasksModule } from '../../tasks/tasks.module';

@Module({
  imports: [
    TypeOrmModule.forFeature([PerferTag]),
    LevelCacheModule,
    TasksModule,
  ],
  controllers: [PerferTagsController],
  providers: [PerferTagsService],
  exports: [PerferTagsService],
})
export class PerferTagsModule {}
