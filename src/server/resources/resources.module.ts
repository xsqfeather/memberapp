import { Module } from '@nestjs/common';
import { PostsModule } from './posts/posts.module';
import { FirmsModule } from './firms/firms.module';
import { MessagesModule } from './messages/messages.module';
import { DevicesModule } from './devices/devices.module';
import { UsersModule } from './users/users.module';
import { ConfigModule, ConfigService } from '@nestjs/config';
import { TypeOrmModule } from '@nestjs/typeorm';
import { UsersFirmsModule } from './users-firms/users-firms.module';
import { SessionsModule } from './sessions/sessions.module';
import { CoursesModule } from './courses/courses.module';
import { RoleTagsModule } from './role-tags/role-tags.module';
import { PlanetsModule } from './planets/planets.module';
import { PerferTagsModule } from './perfer-tags/perfer-tags.module';
import { FirmModulesModule } from './firm-modules/firm-modules.module';
import { VideosModule } from './videos/videos.module';
import { BlogsModule } from './blogs/blogs.module';
import { LessonsModule } from './lessons/lessons.module';
import { LessonNodesModule } from './lesson-nodes/lesson-nodes.module';
import { LessonNodeVideosModule } from './lesson-node-videos/lesson-node-videos.module';
import { ImagesModule } from './images/images.module';
import { MiniBlogModule } from './mini-blog/mini-blog.module';
import { MyFollowMiniBlogsModule } from './my-follow-mini-blogs/my-follow-mini-blogs.module';
import { ThumbupMiniblogsModule } from './thumbup-miniblogs/thumbup-miniblogs.module';
import { UploadsModule } from './uploads/uploads.module';
import { SmsModule } from './sms/sms.module';
import { RecommandUsersModule } from './recommand-users/recommand-users.module';
import { RecommandUsersMiniBlogsModule } from './recommand-users-mini-blogs/recommand-users-mini-blogs.module';
import { FakeUsersModule } from './fake-users/fake-users.module';
import { ChatMsgsModule } from './chat-msgs/chat-msgs.module';
import { CountersModule } from './counters/counters.module';
import { DistributedImagesModule } from './distributed-images/distributed-images.module';
import { ExamsModule } from './exams/exams.module';
import { WatchRecordsModule } from './watch-records/watch-records.module';
import { ExamRecordsModule } from './exam-records/exam-records.module';
import { LessonSubscriptionModule } from './lesson-subscription/lesson-subscription.module';
import { FollowsModule } from './follows/follows.module';
import { LikesModule } from './likes/likes.module';
import { TopicModule } from './topics/topic.module';
import { CommentsModule } from './comments/comments.module';
import { MidTopicMiniblogModule } from './mid-topic-miniblog/mid-topic-miniblog.module';
import { TeamRequestsModule } from './team-requests/team-requests.module';

@Module({
  imports: [
    TypeOrmModule.forRootAsync({
      imports: [ConfigModule],
      useFactory: async (configService: ConfigService) => ({
        type: 'postgres',
        host: configService.get('DATABASE_HOST'),
        port: configService.get('DATABASE_PORT'),
        database: configService.get('DATABASE_NAME'),
        username: configService.get('DATABASE_USERNAME'),
        password: configService.get('DATABASE_PASSWORD'),
        autoLoadEntities: true,
        entities: ['server/**/*.entity.ts'],
        synchronize: true,
        logging: false,
      }),
      inject: [ConfigService],
    }),
    PostsModule,
    FirmsModule,
    MessagesModule,
    DevicesModule,
    UsersModule,
    UsersFirmsModule,
    SessionsModule,
    CoursesModule,
    RoleTagsModule,
    PlanetsModule,
    PerferTagsModule,
    FirmModulesModule,
    VideosModule,
    BlogsModule,
    LessonsModule,
    LessonNodesModule,
    LessonNodeVideosModule,
    ExamsModule,
    ImagesModule,
    MiniBlogModule,
    MyFollowMiniBlogsModule,
    ThumbupMiniblogsModule,
    UploadsModule,
    SmsModule,
    RecommandUsersModule,
    RecommandUsersMiniBlogsModule,
    FakeUsersModule,
    ChatMsgsModule,
    CountersModule,
    DistributedImagesModule,
    WatchRecordsModule,
    ExamRecordsModule,
    LessonSubscriptionModule,
    FollowsModule,
    LikesModule,
    TopicModule,
    CommentsModule,
    MidTopicMiniblogModule,
    TeamRequestsModule,
  ],
})
export class ResourcesModule {}
