import {
  Controller,
  Get,
  Post,
  Body,
  Patch,
  Param,
  Delete,
  HttpException,
  HttpStatus,
  Res,
  Query,
  Put,
} from '@nestjs/common';
import { Response } from 'express';
import { In, Like } from 'typeorm';
import { BlogsService } from './blogs.service';
import { CreateBlogDto } from './dto/create-blog.dto';
import { UpdateBlogDto } from './dto/update-blog.dto';

@Controller('blogs')
export class BlogsController {
  constructor(private readonly blogsService: BlogsService) {}

  @Post()
  async create(@Body() createBlogDto: CreateBlogDto) {
    return this.blogsService.create(createBlogDto);
  }

  @Get()
  async findAll(@Res() res: Response, @Query() query: any) {
    console.log(query);
    const { filter, range, sort } = query;
    let filterObj,
      rangeArr,
      sortArr = {};
    try {
      filterObj = JSON.parse(filter || '{}');
      rangeArr = JSON.parse(range || '[0,9]');
      sortArr = JSON.parse(sort || '["updatedAt", "DESC"]');
    } catch (error) {
      throw new HttpException(
        {
          status: HttpStatus.BAD_REQUEST,
          error: `json parse errors where query one of { filter, range, sort}`,
        },
        HttpStatus.BAD_REQUEST,
      );
    }
    let where: any = {};

    if (filterObj && filterObj.q) {
      where = [
        { title: Like(`%${filterObj.q}%`) },
        { body: Like(`%${filterObj.q}%`) },
        { character: Like(`%${filterObj.q}%`) },
      ];
    }
    const [blogs, count]: any = await this.blogsService.findAll({
      range: rangeArr,
      sort: sortArr,
      where,
    });
    res.setHeader(
      'Content-Range',
      `blogs ${rangeArr[0]}-${rangeArr[1]}/${count}`,
    );
    res.send(blogs);
  }

  @Get(':id')
  async findOne(@Param('id') id: string) {
    return this.blogsService.findOne(+id);
  }

  @Put(':id')
  update(@Param('id') id: string, @Body() updateBlogDto: UpdateBlogDto) {
    return this.blogsService.update(+id, updateBlogDto);
  }

  @Delete(':id')
  remove(@Param('id') id: string) {
    console.log(`[id]${id}`);
    return this.blogsService.remove(+id);
  }

  @Delete()
  async removeAll(@Query() query: any) {
    const { filter } = query;
    const filterObj = JSON.parse(filter);
    const where = {
      id: In(filterObj.ids),
    };
    await this.blogsService.removeAll({ where });
    return [...JSON.parse(query.filter).ids];
  }
}
