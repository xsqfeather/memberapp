export class CreateBlogDto {
  coverImage: string;
  title: string;
  body: string;
  character: string;
  videoId: number;
}
