import { Injectable } from '@nestjs/common';
import { CreateBlogDto } from './dto/create-blog.dto';
import { UpdateBlogDto } from './dto/update-blog.dto';
import { InjectRepository } from '@nestjs/typeorm';
import { Repository } from 'typeorm';
import { Blog } from './entities/blog.entity';

@Injectable()
export class BlogsService {
  constructor(
    @InjectRepository(Blog)
    private readonly blogRepository: Repository<Blog>,
  ) {}

  async create(createBlogDto: CreateBlogDto) {
    const blog = this.blogRepository.create(createBlogDto);
    return await this.blogRepository.save(blog);
  }

  findAll({ sort, range, where }) {
    const order = {};
    order[sort[0]] = sort[1];
    return this.blogRepository.findAndCount({
      order,
      skip: range[0],
      take: range[1] - range[0] + 1,
      where,
    });
  }

  async findOne(id: number) {
    return await this.blogRepository.findOne(id);
  }

  async update(id: number, updateBlogDto: UpdateBlogDto) {
    const blog = await this.findOne(id);
    blog.body = updateBlogDto.body;
    blog.character = updateBlogDto.character;
    blog.title = updateBlogDto.title;
    blog.coverImage = updateBlogDto.coverImage;
    blog.videoId = updateBlogDto.videoId;
    return await this.blogRepository.save(blog);
  }

  async remove(id: number) {
    const rlt = await this.blogRepository.softDelete(id);
    console.log(id, rlt);
    return rlt;
  }

  async removeAll({ where }: { where: any }) {
    return this.blogRepository.softDelete({ ...where });
  }
}
