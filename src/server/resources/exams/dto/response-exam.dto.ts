import { Exam } from '../entities/exam.entity';
import { Exclude, Expose } from 'class-transformer';
import { ApiProperty } from '@nestjs/swagger';

@Exclude()
export class ResponseExamDto implements Partial<Exam> {
  constructor(partial: Partial<ResponseExamDto>) {
    Object.assign(this, partial);
  }
  @ApiProperty({ name: 'id', description: 'ID' })
  @Expose()
  id: number;

  @ApiProperty({ name: 'name', description: '测验名称' })
  @Expose()
  name: string;

  @ApiProperty({
    name: 'type',
    description: '测验类型',
    enum: ['single-choice', 'multiple-choice', 'judgment', 'filling-up'],
  })
  @Expose()
  type: Exam['type'];

  @ApiProperty({ name: 'score', description: '测验分' })
  @Expose()
  score: number;

  @ApiProperty({ name: 'lessonNodeId', description: '课节ID' })
  @Expose()
  lessonNodeId: number;

  @ApiProperty({ name: 'order', description: '测验顺序' })
  @Expose()
  order: number;

  @ApiProperty({
    name: 'options',
    description: '测验选项',
    required: false,
  })
  @Expose()
  options?: Exam['options'];

  @ApiProperty({ name: 'answer', description: '测验答案', required: false })
  @Expose()
  answer?: string;
}
