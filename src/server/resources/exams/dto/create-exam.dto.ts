import { ApiProperty } from '@nestjs/swagger';
import { IsArray, IsEnum, IsInt, IsOptional, IsString } from 'class-validator';
import { Transform } from 'class-transformer';

export class CreateExamDto {
  @IsString()
  @ApiProperty({ name: 'name', description: '题目' })
  name: string;

  @IsEnum(['single-choice', 'multiple-choice', 'judgment', 'filling-up'])
  @ApiProperty({
    name: 'type',
    description: '题目类型',
    enum: ['single-choice', 'multiple-choice', 'judgment', 'filling-up'],
  })
  type: 'single-choice' | 'multiple-choice' | 'judgment' | 'filling-up';

  @IsEnum([10, 20, 30])
  @ApiProperty({ name: 'score', description: '分数', enum: [10, 20, 30] })
  score: number;

  @IsInt()
  @ApiProperty({ name: 'lessonNodeId', description: '课节ID' })
  lessonNodeId: number;

  @IsInt()
  @ApiProperty({ name: 'order', description: '顺序' })
  order: number;

  @IsOptional()
  @IsArray()
  @ApiProperty({ name: 'options', description: '选项' })
  options: Array<{ optionName: string; optionValue: string }>;

  @Transform(({ value: answer }) =>
    typeof answer === 'boolean' ? (answer ? 'T' : 'F') : String(answer),
  )
  @ApiProperty({ name: 'answer', description: '答案' })
  answer: string;
}
