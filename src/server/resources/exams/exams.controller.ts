import {
  Controller,
  Get,
  Post,
  Body,
  Param,
  Delete,
  HttpException,
  HttpStatus,
  Res,
  Query,
  Put,
  ParseIntPipe,
  ValidationPipe,
} from '@nestjs/common';
import { ExamsService } from './exams.service';
import { CreateExamDto } from './dto/create-exam.dto';
import { UpdateExamDto } from './dto/update-exam.dto';
import { Response } from 'express';
import { In } from 'typeorm';
import { ApiOperation, ApiQuery, ApiResponse, ApiTags } from '@nestjs/swagger';
import { ResponseExamDto } from './dto/response-exam.dto';

@ApiTags('测验Exams')
@Controller('exams')
export class ExamsController {
  constructor(private readonly examsService: ExamsService) {}

  @Post()
  @ApiOperation({ summary: '创建测验题' })
  create(
    @Body(new ValidationPipe({ transform: true })) createExamDto: CreateExamDto,
  ) {
    return this.examsService.create(createExamDto);
  }

  @Get()
  @ApiOperation({ summary: '获取所有测验题' })
  @ApiQuery({
    name: 'filter',
    description: '过滤, Record<lessonId|lessonNodeId, any>',
    required: false,
  })
  @ApiQuery({
    name: 'range',
    description: '分页, [start: number, end: number]',
    required: false,
  })
  @ApiQuery({
    name: 'sort',
    description: '排序, [field: string, order: "DESC" | "ASC"]',
    required: false,
  })
  async findAll(@Res() res: Response, @Query() query: any) {
    const { filter, range, sort } = query;
    let filterObj,
      rangeArr,
      sortArr: any = {};
    try {
      filterObj = JSON.parse(filter || '{}');
      rangeArr = JSON.parse(range || '[0,9]');
      sortArr = JSON.parse(sort || '["updatedAt", "DESC"]');
    } catch (error) {
      throw new HttpException(
        {
          status: HttpStatus.BAD_REQUEST,
          error: `json parse errors where query one of { filter, range, sort}`,
        },
        HttpStatus.BAD_REQUEST,
      );
    }
    let where: any = {};
    ['lessonId', 'lessonNodeId'].forEach((key) => {
      if (Reflect.has(filterObj, key)) {
        where[key] = filterObj[key];
      }
    });
    const [results, count] = await this.examsService.findAll({
      range: rangeArr,
      sort: sortArr,
      where,
    });

    res.setHeader(
      'Content-Range',
      `exams ${rangeArr[0]}-${rangeArr[1]}/${count}`,
    );
    res.send(results);
  }

  @Get('/find-by-lesson-node')
  @ApiOperation({ summary: '根据课节ID查询测验题' })
  @ApiResponse({
    status: 200,
    description: '描述',
    type: [ResponseExamDto],
  })
  @ApiQuery({
    name: 'lessonNodeId',
    description: '课节ID',
  })
  async findByLessonNodeId(
    @Query('lessonNodeId', ParseIntPipe) lessonNodeId: number,
  ) {
    return this.examsService.findByLessonNodeId(lessonNodeId);
  }

  @Get('/score-info')
  @ApiOperation({ summary: '根据课节ID获取分数信息' })
  @ApiQuery({ name: 'lessonNodeId', description: '课节ID' })
  getScoreInfo(@Query('lessonNodeId', ParseIntPipe) id: number) {
    return this.examsService.getScoreInfo(id);
  }

  @Get(':id')
  @ApiOperation({ summary: '根据ID获取测验题' })
  findOne(@Param('id') id: string) {
    return this.examsService.findOne(+id);
  }

  @Put(':id')
  @ApiOperation({ summary: '更新测验题' })
  update(
    @Param('id') id: string,
    @Body(new ValidationPipe({ transform: true })) updateExamDto: UpdateExamDto,
  ) {
    return this.examsService.update(+id, updateExamDto);
  }

  @Delete(':id')
  @ApiOperation({ summary: '删除测验题' })
  remove(@Param('id') id: string) {
    return this.examsService.remove(+id);
  }

  @Delete()
  @ApiOperation({ summary: '删除一些测验题' })
  async removeMany(@Query() query: any) {
    const { filter } = query;
    const filterObj = JSON.parse(filter);
    await this.examsService.removeMany({
      id: In(filterObj.id),
    });
    return [...JSON.parse(query.filter).id];
  }
}
