import {
  Column,
  CreateDateColumn,
  DeleteDateColumn,
  Entity,
  JoinColumn,
  ManyToOne,
  PrimaryGeneratedColumn,
  UpdateDateColumn,
} from 'typeorm';
import { LessonNode } from '../../lesson-nodes/entities/lesson-node.entity';

@Entity()
export class Exam {
  @PrimaryGeneratedColumn()
  id: number;

  @Column()
  name: string;

  @Column()
  type: 'single-choice' | 'multiple-choice' | 'judgment' | 'filling-up';

  @Column()
  score: number;

  @ManyToOne(() => LessonNode)
  @JoinColumn({
    name: 'lessonNodeId',
  })
  lessonNode: LessonNode;

  @Column()
  lessonNodeId: number;

  @Column()
  order: number;

  @Column({ type: 'jsonb', nullable: true })
  options?: Array<{ optionName: string; optionValue: string }>;

  @Column({ type: 'jsonb', nullable: true })
  answer?: string;

  @CreateDateColumn()
  createdAt: Date;

  @UpdateDateColumn()
  updatedAt: Date;

  @DeleteDateColumn()
  deletedAt: Date;
}
