import { HttpException, HttpStatus, Injectable } from '@nestjs/common';
import { CreateExamDto } from './dto/create-exam.dto';
import { UpdateExamDto } from './dto/update-exam.dto';
import { InjectRepository } from '@nestjs/typeorm';
import { Exam } from './entities/exam.entity';
import { FindConditions, Repository, In } from 'typeorm';
import { LessonNode } from '../lesson-nodes/entities/lesson-node.entity';
import { classToPlain } from 'class-transformer';
import { ResponseExamDto } from './dto/response-exam.dto';

interface QueryParams {
  sort: [string, string];
  range: [number, number];
  where: any;
}

@Injectable()
export class ExamsService {
  constructor(
    @InjectRepository(Exam) private readonly examRepository: Repository<Exam>,
    @InjectRepository(LessonNode)
    private readonly lessonNodeRepository: Repository<LessonNode>,
  ) {}

  async create(createExamDto: CreateExamDto) {
    const { existing, total } = await this.getScoreInfo(
      createExamDto.lessonNodeId,
    );
    if (total <= existing + createExamDto.score)
      throw new HttpException(
        {
          status: HttpStatus.BAD_REQUEST,
          error: 'Exceeded total score',
          message: '超出总分分数',
        },
        HttpStatus.BAD_REQUEST,
      );
    Reflect.deleteProperty(createExamDto, 'lessonNode');
    const exam = this.examRepository.create(createExamDto);
    await this.examRepository.save(exam);
    return exam;
  }

  async findAll({ sort, range, where }: QueryParams) {
    const order = {};
    order[sort[0]] = sort[1];
    if (Reflect.has(where, 'lessonId')) {
      const lessonNodes = await this.lessonNodeRepository.find({
        lessonId: where['lessonId'],
      });
      const lessonNodesIds = lessonNodes.map((it) => it.id);
      Reflect.deleteProperty(where, 'lessonId');
      Reflect.set(where, 'lessonNodeId', In(lessonNodesIds));
    }
    return await this.examRepository
      .findAndCount({
        order,
        skip: range[0],
        take: range[1] - range[0] + 1,
        where,
        relations: ['lessonNode'],
      })
      .then(([exam, total]) => [
        exam.sort((a, b) => {
          if (a.lessonNode.order === b.lessonNode.order)
            return a.order - b.order;
          return a.lessonNode.order - b.lessonNode.order;
        }),
        total,
      ]);
  }
  async findByLessonNodeId(lessonNodeId: number) {
    return await this.examRepository
      .find({
        where: {
          lessonNodeId,
        },
        order: {
          order: 'ASC',
        },
      })
      .then((r) => r.map((it) => classToPlain(new ResponseExamDto(it))));
  }

  async findOne(id: number) {
    return this.examRepository.findOne({
      where: { id },
      relations: ['lessonNode'],
    });
  }

  async update(id: number, updateExamDto: UpdateExamDto) {
    Reflect.deleteProperty(updateExamDto, 'id');
    Reflect.deleteProperty(updateExamDto, 'createAt');
    Reflect.deleteProperty(updateExamDto, 'deleteAt');
    Reflect.deleteProperty(updateExamDto, 'updatedAt');
    Reflect.deleteProperty(updateExamDto, 'lessonNode');
    const result = await this.examRepository.update(
      {
        id,
      },
      updateExamDto,
    );
    return { success: result.affected > 0, id };
  }

  async getScoreInfo(lessonNodeId: number) {
    const exams = await this.examRepository.find({
      where: { lessonNodeId },
      select: ['id', 'score'],
    });
    return { total: 60, existing: exams.reduce((p, it) => p + it.score, 0) };
  }

  async remove(id: number) {
    return this.examRepository.softDelete(id);
  }
  async removeMany(conditions: FindConditions<Exam>) {
    return await this.examRepository.softDelete(conditions);
  }
}
