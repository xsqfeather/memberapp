import {
  Controller,
  Get,
  Post,
  Body,
  Patch,
  Param,
  Delete,
} from '@nestjs/common';
import { UsersFirmsService } from './users-firms.service';
import { CreateUsersFirmDto } from './dto/create-users-firm.dto';
import { UpdateUsersFirmDto } from './dto/update-users-firm.dto';

@Controller('users-firms')
export class UsersFirmsController {
  constructor(private readonly usersFirmsService: UsersFirmsService) {}

  @Post()
  create(@Body() createUsersFirmDto: CreateUsersFirmDto) {
    return this.usersFirmsService.create(createUsersFirmDto);
  }

  @Get()
  findAll() {
    return this.usersFirmsService.findAll();
  }

  @Get(':id')
  findOne(@Param('id') id: string) {
    return this.usersFirmsService.findOne(+id);
  }

  @Patch(':id')
  update(
    @Param('id') id: string,
    @Body() updateUsersFirmDto: UpdateUsersFirmDto,
  ) {
    return this.usersFirmsService.update(+id, updateUsersFirmDto);
  }

  @Delete(':id')
  remove(@Param('id') id: string) {
    return this.usersFirmsService.remove(+id);
  }
}
