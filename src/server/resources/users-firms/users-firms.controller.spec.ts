import { Test, TestingModule } from '@nestjs/testing';
import { UsersFirmsController } from './users-firms.controller';
import { UsersFirmsService } from './users-firms.service';

describe('UsersFirmsController', () => {
  let controller: UsersFirmsController;

  beforeEach(async () => {
    const module: TestingModule = await Test.createTestingModule({
      controllers: [UsersFirmsController],
      providers: [UsersFirmsService],
    }).compile();

    controller = module.get<UsersFirmsController>(UsersFirmsController);
  });

  it('should be defined', () => {
    expect(controller).toBeDefined();
  });
});
