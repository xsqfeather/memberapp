export class CreateUsersFirmDto {
  firmId: number;
  password: string;
  userId: number;
}
