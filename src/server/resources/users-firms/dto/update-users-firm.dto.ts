import { PartialType } from '@nestjs/swagger';
import { CreateUsersFirmDto } from './create-users-firm.dto';

export class UpdateUsersFirmDto extends PartialType(CreateUsersFirmDto) {}
