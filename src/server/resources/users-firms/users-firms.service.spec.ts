import { Test, TestingModule } from '@nestjs/testing';
import { UsersFirmsService } from './users-firms.service';

describe('UsersFirmsService', () => {
  let service: UsersFirmsService;

  beforeEach(async () => {
    const module: TestingModule = await Test.createTestingModule({
      providers: [UsersFirmsService],
    }).compile();

    service = module.get<UsersFirmsService>(UsersFirmsService);
  });

  it('should be defined', () => {
    expect(service).toBeDefined();
  });
});
