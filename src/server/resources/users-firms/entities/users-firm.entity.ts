import {
  Column,
  Entity,
  JoinColumn,
  ManyToOne,
  PrimaryGeneratedColumn,
} from 'typeorm';
import { Firm } from '../../firms/entities/firm.entity';
import { User } from '../../users/entities/user.entity';

@Entity()
export class UsersFirm {
  @PrimaryGeneratedColumn()
  id: number;

  @ManyToOne(() => User, (user) => user.userFirms)
  @JoinColumn({
    name: 'userId',
  })
  user: User;

  @Column()
  userId: number;

  @Column({
    type: 'bool',
    default: false,
  })
  isOwner: boolean;

  @Column({
    type: 'bool',
    default: false,
  })
  isDefault: boolean;
  //默认资源不可删除

  @ManyToOne(() => Firm, (firm) => firm.firmUsers)
  @JoinColumn({
    name: 'firmId',
  })
  firm: Firm;

  @Column({
    nullable: true,
  })
  firmId: number;

  @Column({
    nullable: true,
  })
  password: string;
}
