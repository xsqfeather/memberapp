import { Module } from '@nestjs/common';
import { UsersFirmsService } from './users-firms.service';
import { UsersFirmsController } from './users-firms.controller';
import { TypeOrmModule } from '@nestjs/typeorm';
import { UsersFirm } from './entities/users-firm.entity';

@Module({
  imports: [TypeOrmModule.forFeature([UsersFirm])],
  controllers: [UsersFirmsController],
  providers: [UsersFirmsService],
  exports: [UsersFirmsService],
})
export class UsersFirmsModule {}
