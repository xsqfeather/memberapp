import { Injectable } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { Repository } from 'typeorm';
import { CreateUsersFirmDto } from './dto/create-users-firm.dto';
import { UpdateUsersFirmDto } from './dto/update-users-firm.dto';
import { UsersFirm } from './entities/users-firm.entity';
import { Socket } from 'socket.io';

@Injectable()
export class UsersFirmsService {
  constructor(
    @InjectRepository(UsersFirm)
    private usersFirmsRepository: Repository<UsersFirm>,
  ) {}
  create(createUsersFirmDto: CreateUsersFirmDto) {
    return this.usersFirmsRepository.create({
      ...createUsersFirmDto,
    });
  }

  findAll() {
    return `This action returns all usersFirms`;
  }

  findOne(id: number) {
    return this.usersFirmsRepository.findOne(id);
  }

  async update(id: number, updateUsersFirmDto: UpdateUsersFirmDto) {
    const { firmId, userId, password } = updateUsersFirmDto;
    const belong = await this.findOne(id);
    if (!belong) {
      throw new Error('update userFirm id not found');
    }
    if (password) {
      belong.password = password;
    }
    if (firmId) {
      belong.firmId = firmId;
    }
    if (userId) {
      belong.userId = userId;
    }
    await this.usersFirmsRepository.save(belong);
    return belong;
  }

  async put() {}

  remove(id: number) {
    return `This action removes a #${id} usersFirm`;
  }

  async save(belong: UsersFirm) {
    await this.usersFirmsRepository.save(belong);
  }

  async confirmUserBelongFirm(userId: number, firmId: number) {
    let belong = await this.usersFirmsRepository.findOne({
      where: {
        userId,
        firmId,
      },
    });
    if (!belong) {
      belong = this.usersFirmsRepository.create();
    }
    belong.userId = userId;
    belong.firmId = firmId;
    await this.usersFirmsRepository.save(belong);
    return belong;
  }
}
