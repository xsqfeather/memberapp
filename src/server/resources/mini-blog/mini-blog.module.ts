import { HttpModule, Module } from '@nestjs/common';
import { MiniBlogService } from './mini-blog.service';
import { MiniBlogController } from './mini-blog.controller';
import { TypeOrmModule } from '@nestjs/typeorm';
import { MiniBlog } from './entities/mini-blog.entity';
import { Like } from '../likes/entities/like.entity';
import { UsersModule } from '../users/users.module';
import { MidTopicMiniblogModule } from '../mid-topic-miniblog/mid-topic-miniblog.module';

@Module({
  imports: [
    TypeOrmModule.forFeature([MiniBlog, Like]),
    HttpModule,
    UsersModule,
    MidTopicMiniblogModule
  ],
  controllers: [MiniBlogController],
  providers: [MiniBlogService],
  exports: [MiniBlogService],
})
export class MiniBlogModule {}
