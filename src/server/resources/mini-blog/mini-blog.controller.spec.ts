import { Test, TestingModule } from '@nestjs/testing';
import { MiniBlogController } from './mini-blog.controller';
import { MiniBlogService } from './mini-blog.service';

describe('MiniBlogController', () => {
  let controller: MiniBlogController;

  beforeEach(async () => {
    const module: TestingModule = await Test.createTestingModule({
      controllers: [MiniBlogController],
      providers: [MiniBlogService],
    }).compile();

    controller = module.get<MiniBlogController>(MiniBlogController);
  });

  it('should be defined', () => {
    expect(controller).toBeDefined();
  });
});
