import { BadRequestException, Injectable } from '@nestjs/common';
import { OnEvent } from '@nestjs/event-emitter';
import { InjectRepository } from '@nestjs/typeorm';
import axios from 'axios';
import { EventEmitter2 } from 'eventemitter2';
import { Repository, In, SelectQueryBuilder } from 'typeorm';
import { CreateMiniBlogDto } from './dto/create-mini-blog.dto';
import { UpdateMiniBlogDto } from './dto/update-mini-blog.dto';
import { MiniBlog } from './entities/mini-blog.entity';
import { v4 as uuidv4 } from 'uuid';
import { createWriteStream } from 'fs';
import { MINI_BLOG_FAKE_POST } from '../../common/Emitters';
import { Like } from '../likes/entities/like.entity';
import { MidTopicMiniblogService } from '../mid-topic-miniblog/mid-topic-miniblog.service';
import { MINI_BLOG_ON_CREATE } from '../../common/Emitters';
import { classToPlain } from 'class-transformer';
import { ResponseUserDto } from '../users/dto/response-user.dto';
import { ResponseMiniBlogDto } from './dto/response-mini-blog.dto';

@Injectable()
export class MiniBlogService {
  constructor(
    private eventEmitter: EventEmitter2,
    private midTopicMiniblogService: MidTopicMiniblogService,
    @InjectRepository(MiniBlog)
    private miniBlogsRepository: Repository<MiniBlog>,

    @InjectRepository(Like)
    private readonly likesRepository: Repository<Like>,
  ) {}

  @OnEvent(MINI_BLOG_FAKE_POST)
  async fakePost(userId: number) {
    console.log('============自动发帖=========');
    const miniBlog = this.miniBlogsRepository.create();
    try {
      const fImage = await axios({
        method: 'GET',
        url: 'https://source.unsplash.com/random',
        responseType: 'stream',
      });
      const uuid = uuidv4();
      const url = process.env.uploadUrl + '/' + uuid + '.png';
      const urlToSave = `/uploads/${uuid}.png`;
      const wirteFile = createWriteStream(url);
      fImage.data.pipe(wirteFile);
      console.log('图片下载成功', { urlToSave });
      miniBlog.images = [urlToSave];
      const saoHua = await axios.get('https://api.ghser.com/saohua?type=json');
      const { ishan } = saoHua.data;
      miniBlog.body = ishan;
      miniBlog.publisherId = userId;
      await this.miniBlogsRepository.save(miniBlog);
      return miniBlog;
    } catch (error) {
      console.log('error', error, '不用理会');
      return error;
    }
  }

  async create(userId: number, createMiniBlogDto: CreateMiniBlogDto) {
    const { topicIds } = createMiniBlogDto;
    //@ts-ignore
    createMiniBlogDto.publisherId = userId;
    const mini = this.miniBlogsRepository.create(createMiniBlogDto);
    const mids = [];
    if (topicIds && topicIds.length != 0) {
      for (let i = 0; i < topicIds.length; i++) {
        const topicId = topicIds[i];
        mids.push(await this.midTopicMiniblogService.create({ topicId }));
      }
    }
    if (mids.length > 0) {
      mini.midTopicMiniBlog = mids;
    }

    await this.miniBlogsRepository.save(mini);
    this.eventEmitter.emit(MINI_BLOG_ON_CREATE, {
      userId: userId,
      miniBlogId: mini.id,
      updateAt: mini.updatedAt,
      topicIds,
    });
    return mini;
  }

  async findAll(userId: number | undefined, { sort, range, where }: any) {
    const order = {};
    order[sort[0]] = sort[1];
    const [results, total] = await this.miniBlogsRepository.findAndCount({
      order,
      skip: range[0],
      take: range[1] - range[0] + 1,
      where,
      relations: ['publisher'],
    });
    const likes = userId
      ? await this.likesRepository
          .find({
            where: { tid: In(results.map((it) => it.id)), uid: userId },
          })
          .then((res) => res.map((it) => it.tid))
      : [];
    return [
      results.map((it) =>
        classToPlain(
          new ResponseMiniBlogDto({
            ...it,
            liked: likes.includes(it.id),
            publisher: classToPlain(
              new ResponseUserDto(it.publisher),
            ) as ResponseUserDto,
          }),
        ),
      ),
      total,
    ];
  }

  findUserAllMiniBlogIds(userId: number) {
    return this.miniBlogsRepository
      .createQueryBuilder('mb')
      .select('mb.id')
      .addSelect('mb.updatedAt')
      .where(`mb.publisherId = :userId`, { userId })
      .orderBy(`mb.id`, 'DESC') // 用id倒序和用时间是一样的效果且更快一点
      .getMany() as unknown as Promise<{ id: number; updatedAt: number }[]>;
  }

  async findOne(id: number) {
    return await this.miniBlogsRepository
      .findOne({
        where: { id },
        relations: ['publisher'],
      })
      .then((r) =>
        classToPlain(
          new ResponseMiniBlogDto({
            ...r,
            publisher: classToPlain(
              new ResponseUserDto(r.publisher),
            ) as ResponseUserDto,
          }),
        ),
      );
  }

  /*   update(id: number, updateMiniBlogDto: UpdateMiniBlogDto) {
    return `This action updates a #${id} miniBlog`;
  } */

  async remove(id: number,userId:number) {
    const miniBlog = await this.miniBlogsRepository.findOne(id);
    if(miniBlog.publisherId===userId){
      return this.miniBlogsRepository.softRemove(miniBlog);
    }
    throw new BadRequestException("Illegal operation");
  }
}
