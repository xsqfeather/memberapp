import {
  Column,
  CreateDateColumn,
  DeleteDateColumn,
  Entity,
  Index,
  ManyToOne,
  OneToMany,
  PrimaryGeneratedColumn,
  UpdateDateColumn,
} from 'typeorm';
import { User } from '../../users/entities/user.entity';
import { MidTopicMiniBlog } from '../../mid-topic-miniblog/entities/mid-topic-miniblog.entity';

@Entity()
export class MiniBlog {
  @PrimaryGeneratedColumn()
  id: number;

  @Column({
    type: 'text',
  })
  body: string;

  @Column({
    type: 'jsonb',
    nullable: true,
  })
  images: string[];

  @Column({
    nullable: true,
  })
  publisherId: number;

  @ManyToOne(() => User, (user) => user.miniBlogs)
  publisher: User;

  @OneToMany(
    (type) => MidTopicMiniBlog,
    (midTopicMiniBlog) => midTopicMiniBlog.miniBlog,
  )
  midTopicMiniBlog: MidTopicMiniBlog[];

  @CreateDateColumn()
  createdAt: Date;

  @UpdateDateColumn()
  updatedAt: Date;

  @DeleteDateColumn()
  deletedAt: Date;

  @Index()
  @Column({ default: 0 })
  likeCount: number;

  @Index()
  @Column({ default: 0 })
  commentCount: number;
}
