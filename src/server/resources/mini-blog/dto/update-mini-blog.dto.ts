import { PartialType } from '@nestjs/swagger';
import { CreateMiniBlogDto } from './create-mini-blog.dto';

export class UpdateMiniBlogDto extends PartialType(CreateMiniBlogDto) {}
