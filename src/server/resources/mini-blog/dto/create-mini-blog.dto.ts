import { ApiProperty } from '@nestjs/swagger';

export class CreateMiniBlogDto {
  @ApiProperty({ name: 'topicIds', description: '话题ID数组' })
  topicIds?: number[];
  @ApiProperty({ name: 'body', description: '正文' })
  body?: string;
  @ApiProperty({ name: 'images', description: '图片地址的数组' })
  images?: string[];
}
