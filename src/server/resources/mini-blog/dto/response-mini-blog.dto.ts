import { Exclude, Expose } from 'class-transformer';
import { MiniBlog } from '../entities/mini-blog.entity';
import { ApiProperty } from '@nestjs/swagger';
import { ResponseUserDto } from '../../users/dto/response-user.dto';

@Exclude()
export class ResponseMiniBlogDto
  implements Partial<Omit<MiniBlog, 'publisher'>>
{
  constructor(partial: Partial<ResponseMiniBlogDto>) {
    Object.assign(this, partial);
  }
  @ApiProperty({ name: 'id', description: '文章ID' })
  @Expose()
  id: number;

  @ApiProperty({ name: 'body', description: '文章内容' })
  @Expose()
  body: string;

  @ApiProperty({
    name: 'images',
    description: '文章图片',
    type: Array(String()),
  })
  @Expose()
  images: string[];

  @ApiProperty({
    name: 'publisher',
    description: '作者',
    type: ResponseUserDto,
  })
  @Expose()
  publisher: ResponseUserDto;

  @ApiProperty({ name: 'likeCount', description: '点赞数' })
  @Expose()
  likeCount: number;

  @ApiProperty({ name: 'liked', description: '当前用户是否点赞' })
  @Expose()
  liked: boolean;

  @ApiProperty({ name: 'commentCount', description: '评论数' })
  @Expose()
  commentCount: number;
}
