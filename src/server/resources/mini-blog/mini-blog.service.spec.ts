import { Test, TestingModule } from '@nestjs/testing';
import { MiniBlogService } from './mini-blog.service';

describe('MiniBlogService', () => {
  let service: MiniBlogService;

  beforeEach(async () => {
    const module: TestingModule = await Test.createTestingModule({
      providers: [MiniBlogService],
    }).compile();

    service = module.get<MiniBlogService>(MiniBlogService);
  });

  it('should be defined', () => {
    expect(service).toBeDefined();
  });
});
