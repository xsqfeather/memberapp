import {
  Controller,
  Get,
  Post,
  Body,
  Patch,
  Param,
  Delete,
  Query,
  Res,
  HttpException,
  HttpStatus,
  UseGuards,
  BadRequestException,
} from '@nestjs/common';
import { MiniBlogService } from './mini-blog.service';
import { CreateMiniBlogDto } from './dto/create-mini-blog.dto';
import { UpdateMiniBlogDto } from './dto/update-mini-blog.dto';
import { Like } from 'typeorm';
import { Response } from 'express';
import { JwtAuthGuard } from '../../auth/jwt-auth.guard';
import { ApiOperation, ApiQuery, ApiResponse, ApiTags } from '@nestjs/swagger';
import { User } from '../../decorators/User';
import { EventEmitter2 } from '@nestjs/event-emitter';
import { MINI_BLOG_ON_DELETE } from '../../common/Emitters';
import { JwtOptionalAuthGuard } from '../../auth/jwt-optional-auth.guard';
import { ResponseMiniBlogDto } from './dto/response-mini-blog.dto';

@ApiTags('博客MiniBlog')
@Controller('mini-blog')
export class MiniBlogController {
  constructor(
    private readonly miniBlogService: MiniBlogService,
    private eventEmitter: EventEmitter2,
  ) {}

  @UseGuards(JwtAuthGuard)
  @ApiOperation({ summary: '创建微博' })
  @Post()
  create(
    @Body() createMiniBlogDto: CreateMiniBlogDto,
    @User('userId') userId?: number,
  ) {
    return this.miniBlogService.create(userId, createMiniBlogDto);
  }

  @UseGuards(JwtOptionalAuthGuard)
  @Get()
  @ApiOperation({ summary: '获取所有微博' })
  @ApiQuery({
    name: 'filter',
    description: '过滤, {field:value}',
    required: false,
  })
  @ApiQuery({
    name: 'range',
    description: '分页, [start: number, end: number]',
    required: false,
  })
  @ApiQuery({
    name: 'sort',
    description: '排序, [field: string, order: "DESC" | "ASC"]',
    required: false,
  })
  @ApiResponse({ status: 200, type: [ResponseMiniBlogDto] })
  async findAll(
    @Res() res: Response,
    @Query() query: any,
    @User('userId') userId?: number,
  ) {
    const { filter, range, sort } = query;

    let filterObj,
      rangeArr,
      sortArr = {};
    try {
      filterObj = JSON.parse(filter || '{}');
      rangeArr = JSON.parse(range || '[0,9]');
      sortArr = JSON.parse(sort || '["updatedAt", "DESC"]');
    } catch (error) {
      throw new HttpException(
        {
          status: HttpStatus.BAD_REQUEST,
          error: `json parse errors where query one of { filter, range, sort}`,
        },
        HttpStatus.BAD_REQUEST,
      );
    }

    let where: any = {};

    if (filterObj && filterObj.q) {
      where = [
        { title: Like(`%${filterObj.q}%`) },
        { body: Like(`%${filterObj.q}%`) },
      ];
    }
    if (filterObj && filterObj.publisherId) {
      where.publisherId = filterObj.publisherId;
    }

    const [miniBlogs, count]: any = await this.miniBlogService.findAll(userId, {
      range: rangeArr,
      sort: sortArr,
      where,
    });
    res.setHeader(
      'Content-Range',
      `mini-blogs ${rangeArr[0]}-${rangeArr[1]}/${count}`,
    );
    res.send(miniBlogs);
  }

  @UseGuards(JwtAuthGuard)
  @Get(':id')
  @ApiOperation({ summary: 'id查帖子' })
  findOne(@Param('id') id: string) {
    return this.miniBlogService.findOne(+id);
  }

 /*  @UseGuards(JwtAuthGuard)
    @Patch(':id')
  @ApiOperation({ summary: '无此功能' })
  update(
    @Param('id') id: string,
    @Body() updateMiniBlogDto: UpdateMiniBlogDto,
  ) {
    return this.miniBlogService.update(+id, updateMiniBlogDto);
  } */

  @UseGuards(JwtAuthGuard)
  @ApiOperation({ summary: '删除指定帖子' })
  @Delete(':id')
  remove(@User('userId') userId: number, @Param('id') id: string) {
    this.eventEmitter.emit(MINI_BLOG_ON_DELETE, { userId, miniBlogId: id });
    return this.miniBlogService.remove(+id, userId);
  }
}
