import { Module } from '@nestjs/common';
import { SessionsService } from './sessions.service';
import { SessionsController } from './sessions.controller';
import { TypeOrmModule } from '@nestjs/typeorm';
import { Session } from './entities/session.entity';
import { AuthModule } from '../../auth/auth.module';
import { UsersModule } from '../users/users.module';
import { LevelCacheModule } from '../../level-cache/level-cache.module';

@Module({
  imports: [
    TypeOrmModule.forFeature([Session]),
    AuthModule,
    UsersModule,
    LevelCacheModule,
  ],
  controllers: [SessionsController],
  providers: [SessionsService],
})
export class SessionsModule {}
