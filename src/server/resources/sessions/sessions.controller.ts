import {
  Controller,
  Get,
  Post,
  Body,
  Patch,
  Param,
  Delete,
  Req,
  HttpException,
  HttpStatus,
} from '@nestjs/common';
import { SessionsService } from './sessions.service';
import { CreateSessionDto } from './dto/create-session.dto';
import { UpdateSessionDto } from './dto/update-session.dto';
import { ApiBody, ApiOperation, ApiTags } from '@nestjs/swagger';
import { AuthService } from '../../auth/auth.service';
import { Request } from 'express';
import { SignInUserDto } from './dto/sign-in-user.dto';
import { Firm } from '../../decorators/Firm';

@ApiTags('用户会话和登录Sessions')
@Controller('sessions')
export class SessionsController {
  constructor(
    private readonly sessionsService: SessionsService,
    private readonly authService: AuthService,
  ) {}

  @ApiOperation({ summary: '登录' })
  @Post('/signin')
  async signin(
    @Body() signInUserDto: SignInUserDto,
    @Firm('id') firmId: number,
  ) {
    return await this.authService.signin({
      ...signInUserDto,
      firmId: signInUserDto.firmId ?? firmId,
    });
  }
  @ApiOperation({
    summary: '登录并创建一个会话',
    description: `
    密码不是必须的，但用户名，邮箱，手机号，或者微信的openid任意一种必要, loginType必须
    `,
  })
  @ApiBody({
    description: '创建一个会话',
    type: CreateSessionDto,
  })
  @Post()
  async create(
    @Body() createSessionDto: CreateSessionDto,
    @Req() req: Request,
  ) {
    const { phone, password, sms, loginType, username } = createSessionDto;
    let token = null;

    switch (loginType) {
      case 'ANY_PASSWORD':
        return await this.authService.loginByAnyPassword({
          username,
          password,
        });
      case 'PHONE_SMS':
        if (!phone) {
          throw new HttpException(
            {
              status: HttpStatus.BAD_REQUEST,
              error: `sessions:create:fail:phone_missing`,
              code: `PHONE_MISSING`,
            },
            HttpStatus.BAD_REQUEST,
          );
        }
        if (!sms) {
          throw new HttpException(
            {
              status: HttpStatus.BAD_REQUEST,
              error: `sessions:create:fail:sms_missing`,
              code: `SMS_MISSING`,
            },
            HttpStatus.BAD_REQUEST,
          );
        }
        token = await this.authService.loginByPhoneSms(
          phone,
          sms,
          (req as any).firmId,
        );
        this.sessionsService.create(createSessionDto);

        return token;
      case 'USERNAME_PASSWORD':
        return this.authService.loginByUsername(username, password);
      default:
        throw new HttpException(
          {
            status: HttpStatus.BAD_REQUEST,
            error: `sessions:create:fail:loginType_missing`,
            code: `LOGINTYPE_MISSING`,
          },
          HttpStatus.BAD_REQUEST,
        );
    }
  }

  @Get()
  findAll() {
    return this.sessionsService.findAll();
  }

  @Get(':id')
  findOne(@Param('id') id: string) {
    return this.sessionsService.findOne();
  }

  @Patch(':id')
  update(@Param('id') id: string, @Body() updateSessionDto: UpdateSessionDto) {
    return this.sessionsService.update(+id, updateSessionDto);
  }

  @Delete(':id')
  remove(@Param('id') id: string) {
    return this.sessionsService.remove(+id);
  }
}
