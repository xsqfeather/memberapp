import { HttpException, HttpStatus, Injectable } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { Repository } from 'typeorm';
import { CreateSessionDto } from './dto/create-session.dto';
import { UpdateSessionDto } from './dto/update-session.dto';
import { Session } from './entities/session.entity';
import { EventEmitter2, OnEvent } from '@nestjs/event-emitter';
import { SignInUserDto } from './dto/sign-in-user.dto';

@Injectable()
export class SessionsService {
  constructor(
    @InjectRepository(Session)
    private sessionRepository: Repository<Session>,
    private eventEmitter: EventEmitter2,
  ) {}
  create(createSessionDto: CreateSessionDto) {
    this.eventEmitter.emit('sessions.created', 'sessions.created.test');

    return;
  }
  @OnEvent('sessions.created')
  handleOrderCreatedEvent(payload: string) {
    //试试
    console.log(payload);
  }

  findAll() {
    return `This action returns all sessions`;
  }

  findOne() {
    var Faker = require('faker-zh-cn');
    return Faker.Helpers.createCard();
  }

  update(id: number, updateSessionDto: UpdateSessionDto) {
    return `This action updates a #${id} session`;
  }

  remove(id: number) {
    return `This action removes a #${id} session`;
  }
}
