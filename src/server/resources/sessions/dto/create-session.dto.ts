import { ApiProperty } from '@nestjs/swagger';
import {
  IsEmail,
  IsEnum,
  IsOptional,
  IsString,
  MaxLength,
  MinLength,
} from 'class-validator';
import { IsPhone, IsUsername } from '../../../validators';

export class CreateSessionDto {
  @IsOptional()
  @IsUsername()
  @ApiProperty({
    description: '用户名',
    default: 'username',
    required: false,
  })
  username?: string;

  @IsOptional()
  @IsPhone()
  @ApiProperty({
    description: '手机号',
    default: '13800000000',
    required: false,
  })
  phone?: string;

  @IsOptional()
  @ApiProperty({
    description: '密码',
    default: '123666',
    required: false,
  })
  password?: string;

  @IsOptional()
  @IsEmail()
  @ApiProperty({
    description: '邮箱',
    required: false,
  })
  email?: string;

  @IsOptional()
  @IsString()
  @MaxLength(6)
  @MinLength(4)
  @ApiProperty({
    description: '验证码',
    required: false,
  })
  sms?: string;

  @IsEnum(['ANY_PASSWORD', 'USERNAME_PASSWORD', 'PHONE_SMS'])
  @ApiProperty({
    description: '登录类型：ANY_PASSWORD: 密码登录, PHONE_SMS: 手机号验证码',
    default: 'ANY_PASSWORD',
    required: false,
  })
  loginType: 'ANY_PASSWORD' | 'USERNAME_PASSWORD' | 'PHONE_SMS';
}
