import { ApiProperty } from '@nestjs/swagger';
import { IsBoolean, IsInt, IsOptional, Length } from 'class-validator';
import { IsPhone } from '../../../validators';

export class SignInUserDto {
  @IsPhone()
  @ApiProperty({ name: 'phone', description: '手机号' })
  phone: string;

  @Length(4, 4)
  @ApiProperty({ name: 'code', description: '验证码' })
  code: string;

  @IsOptional()
  @IsBoolean()
  @ApiProperty({
    name: 'autoRegistration',
    description: '自动注册新用户',
    default: false,
  })
  autoRegistration: boolean;

  @IsOptional()
  @IsInt()
  @ApiProperty({ name: 'firmId', description: '公司ID' })
  firmId: number;
}
