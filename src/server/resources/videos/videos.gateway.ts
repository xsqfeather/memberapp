import {
  WebSocketGateway,
  SubscribeMessage,
  ConnectedSocket,
} from '@nestjs/websockets';
import { VideosService } from './videos.service';
import { v4 as uuidv4 } from 'uuid';
import { Socket } from 'socket.io';
import * as ss from 'socket.io-stream';
import { createWriteStream } from 'fs';
import { TasksService } from '../../tasks/tasks.service';

@WebSocketGateway()
export class VideosGateway {
  constructor(
    private readonly videosService: VideosService,
    private readonly tasksService: TasksService,
  ) {}

  @SubscribeMessage('createVideoUUID')
  create(@ConnectedSocket() client: Socket) {
    const uuid = uuidv4();
    client.on('stop-upload-video-' + uuid, (data) => {
      console.log('终止了', uuid);
      const cmd = require('node-cmd');
      cmd.run(
        `rm ${process.env.uploadUrl}/${data.uuid}-video`,
        function (err, data, stderr) {
          console.log(err, data, stderr);
        },
      );
    });
    ss(client).on('upload-video-' + uuid, (stream: any, data: any) => {
      const { label } = data;
      stream.pipe(createWriteStream(`${process.env.uploadUrl}/${uuid}-video`));
      stream.on('end', async () => {
        client.emit(`video-${uuid}-uploaded`);
      });
    });
    return uuid;
  }
}
