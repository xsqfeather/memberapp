import { Injectable } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { In, Repository } from 'typeorm';
import { TasksService } from '../../tasks/tasks.service';
import { LessonNodesService } from '../lesson-nodes/lesson-nodes.service';
import { CreateVideoDto } from './dto/create-video.dto';
import { UpdateVideoDto } from './dto/update-video.dto';
import { Video } from './entities/video.entity';

@Injectable()
export class VideosService {
  constructor(
    @InjectRepository(Video)
    private readonly videosReposiry: Repository<Video>,
    private readonly tasksService: TasksService,
    private readonly lessonNodesService: LessonNodesService,
  ) {}

  async onApplicationBootstrap() {
    console.log('============将没有处理的视频重新加入处理地队列==============');
    const videosToDeal = await this.videosReposiry.find({
      isLive: false,
    });
    for (let index = 0; index < videosToDeal.length; index++) {
      const video = videosToDeal[index];
      this.dealVideoToLive(video);
    }

    console.log('============将没有上传完全的视频删除======================');
    const videosToDelete = await this.videosReposiry.find({
      isUploaded: false,
    });
    for (let index = 0; index < videosToDelete.length; index++) {
      const video = videosToDelete[index];
      this.deleteDownloadUrl(video.downloadUrl);
    }
    await this.videosReposiry.softRemove({
      isUploaded: false,
    });
  }

  async dealVideoToLive(video: Video) {
    const cmd = require('node-cmd');
    const { uuid, label } = video;
    try {
      cmd.run(
        `ffmpeg -i ${process.env.uploadUrl}/${uuid}-video -profile:v baseline -level 3.0 -start_number 0 -hls_time 5 -hls_list_size 0 -f hls ${process.env.liveUrl}/${uuid}.m3u8`,
        async (err: any, data: any, stderr: any) => {
          console.log(err, data, stderr);
          video.downloadUrl = `/uploads/${uuid}-video`;
          video.liveUrl = `/live/${uuid}.m3u8`;
          video.isLive = true;
          video.label = label;
          await this.save(video);
        },
      );
    } catch (error) {
      console.error(error);
    }
  }

  async create(createVideoDto: CreateVideoDto) {
    const video = this.videosReposiry.create({
      ...createVideoDto,
    });
    await this.videosReposiry.save(video);

    return video;
  }

  save(video: Video) {
    return this.videosReposiry.save(video);
  }

  async findAll({ sort, range, where }) {
    const order = {};
    order[sort[0]] = sort[1];
    const videos = await this.videosReposiry.findAndCount({
      order,
      skip: range[0],
      take: range[1] - range[0] + 1,
      where,
    });
    return videos;
  }

  findOne(id: number) {
    return this.videosReposiry.findOne();
  }

  async update(id: number, updateVideoDto: UpdateVideoDto) {
    const video = this.videosReposiry.findOne(id);
    await this.videosReposiry.update(1, {
      ...updateVideoDto,
    });
    return video;
  }

  deleteLiveUrl(uuid: string) {
    const cmd = require('node-cmd');
    cmd.run(
      `rm -rf  ${process.env.liveUrl}/${uuid}*`,
      async (err: any, data: any, stderr: any) => {
        console.log(err, data, stderr);
      },
    );
  }

  deleteDownloadUrl(uuid: string) {
    const cmd = require('node-cmd');
    cmd.run(
      `rm -rf  ${process.env.uploadUrl}/${uuid}*`,
      async (err: any, data: any, stderr: any) => {
        console.log(err, data, stderr);
      },
    );
  }

  async remove(id: number) {
    const video = await this.videosReposiry.findOne(id);
    this.deleteDownloadUrl(video.uuid);
    this.deleteLiveUrl(video.uuid);
    return await this.videosReposiry.delete(id);
  }

  async removeAll({ ids }: { ids: any }) {
    try {
      const videos = await this.videosReposiry.find({
        where: {
          id: In(ids),
        },
      });
      videos.forEach((video) => {
        this.deleteDownloadUrl(video.uuid);
        this.deleteLiveUrl(video.uuid);
      });

      await this.lessonNodesService.updateByVideoIds(ids, {
        videoId: null,
      });
      return await this.videosReposiry.remove(videos);
    } catch (error) {
      throw error;
    }
  }
}
