import { Module } from '@nestjs/common';
import { VideosService } from './videos.service';
import { VideosController } from './videos.controller';
import { TypeOrmModule } from '@nestjs/typeorm';
import { Video } from './entities/video.entity';
import { VideosGateway } from './videos.gateway';
import { TasksModule } from '../../tasks/tasks.module';
import { LessonNodesModule } from '../lesson-nodes/lesson-nodes.module';

@Module({
  imports: [TypeOrmModule.forFeature([Video]), TasksModule, LessonNodesModule],
  controllers: [VideosController],
  providers: [VideosService, VideosGateway],
})
export class VideosModule {}
