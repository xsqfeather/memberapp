import {
  Controller,
  Get,
  Post,
  Body,
  Patch,
  Param,
  Delete,
} from '@nestjs/common';
import { RecommandUsersMiniBlogsService } from './recommand-users-mini-blogs.service';
import { CreateRecommandUsersMiniBlogDto } from './dto/create-recommand-users-mini-blog.dto';
import { UpdateRecommandUsersMiniBlogDto } from './dto/update-recommand-users-mini-blog.dto';

@Controller('recommand-users-mini-blogs')
export class RecommandUsersMiniBlogsController {
  constructor(
    private readonly recommandUsersMiniBlogsService: RecommandUsersMiniBlogsService,
  ) {}

  @Post()
  create(
    @Body() createRecommandUsersMiniBlogDto: CreateRecommandUsersMiniBlogDto,
  ) {
    return this.recommandUsersMiniBlogsService.create(
      createRecommandUsersMiniBlogDto,
    );
  }

  @Get()
  findAll() {
    return this.recommandUsersMiniBlogsService.findAll();
  }

  @Get(':id')
  findOne(@Param('id') id: string) {
    return this.recommandUsersMiniBlogsService.findOne(+id);
  }

  @Patch(':id')
  update(
    @Param('id') id: string,
    @Body() updateRecommandUsersMiniBlogDto: UpdateRecommandUsersMiniBlogDto,
  ) {
    return this.recommandUsersMiniBlogsService.update(
      +id,
      updateRecommandUsersMiniBlogDto,
    );
  }

  @Delete(':id')
  remove(@Param('id') id: string) {
    return this.recommandUsersMiniBlogsService.remove(+id);
  }
}
