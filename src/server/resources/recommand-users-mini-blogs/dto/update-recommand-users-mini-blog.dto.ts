import { PartialType } from '@nestjs/swagger';
import { CreateRecommandUsersMiniBlogDto } from './create-recommand-users-mini-blog.dto';

export class UpdateRecommandUsersMiniBlogDto extends PartialType(
  CreateRecommandUsersMiniBlogDto,
) {}
