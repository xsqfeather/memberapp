import { Injectable } from '@nestjs/common';
import { CreateRecommandUsersMiniBlogDto } from './dto/create-recommand-users-mini-blog.dto';
import { UpdateRecommandUsersMiniBlogDto } from './dto/update-recommand-users-mini-blog.dto';

@Injectable()
export class RecommandUsersMiniBlogsService {
  create(createRecommandUsersMiniBlogDto: CreateRecommandUsersMiniBlogDto) {
    return 'This action adds a new recommandUsersMiniBlog';
  }

  findAll() {
    return `This action returns all recommandUsersMiniBlogs`;
  }

  findOne(id: number) {
    return `This action returns a #${id} recommandUsersMiniBlog`;
  }

  update(
    id: number,
    updateRecommandUsersMiniBlogDto: UpdateRecommandUsersMiniBlogDto,
  ) {
    return `This action updates a #${id} recommandUsersMiniBlog`;
  }

  remove(id: number) {
    return `This action removes a #${id} recommandUsersMiniBlog`;
  }
}
