import { Module } from '@nestjs/common';
import { RecommandUsersMiniBlogsService } from './recommand-users-mini-blogs.service';
import { RecommandUsersMiniBlogsController } from './recommand-users-mini-blogs.controller';

@Module({
  controllers: [RecommandUsersMiniBlogsController],
  providers: [RecommandUsersMiniBlogsService],
})
export class RecommandUsersMiniBlogsModule {}
