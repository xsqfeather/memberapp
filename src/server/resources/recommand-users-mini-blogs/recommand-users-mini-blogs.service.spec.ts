import { Test, TestingModule } from '@nestjs/testing';
import { RecommandUsersMiniBlogsService } from './recommand-users-mini-blogs.service';

describe('RecommandUsersMiniBlogsService', () => {
  let service: RecommandUsersMiniBlogsService;

  beforeEach(async () => {
    const module: TestingModule = await Test.createTestingModule({
      providers: [RecommandUsersMiniBlogsService],
    }).compile();

    service = module.get<RecommandUsersMiniBlogsService>(
      RecommandUsersMiniBlogsService,
    );
  });

  it('should be defined', () => {
    expect(service).toBeDefined();
  });
});
