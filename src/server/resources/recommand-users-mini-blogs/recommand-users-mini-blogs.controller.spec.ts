import { Test, TestingModule } from '@nestjs/testing';
import { RecommandUsersMiniBlogsController } from './recommand-users-mini-blogs.controller';
import { RecommandUsersMiniBlogsService } from './recommand-users-mini-blogs.service';

describe('RecommandUsersMiniBlogsController', () => {
  let controller: RecommandUsersMiniBlogsController;

  beforeEach(async () => {
    const module: TestingModule = await Test.createTestingModule({
      controllers: [RecommandUsersMiniBlogsController],
      providers: [RecommandUsersMiniBlogsService],
    }).compile();

    controller = module.get<RecommandUsersMiniBlogsController>(
      RecommandUsersMiniBlogsController,
    );
  });

  it('should be defined', () => {
    expect(controller).toBeDefined();
  });
});
