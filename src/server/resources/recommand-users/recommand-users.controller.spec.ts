import { Test, TestingModule } from '@nestjs/testing';
import { RecommandUsersController } from './recommand-users.controller';
import { RecommandUsersService } from './recommand-users.service';

describe('RecommandUsersController', () => {
  let controller: RecommandUsersController;

  beforeEach(async () => {
    const module: TestingModule = await Test.createTestingModule({
      controllers: [RecommandUsersController],
      providers: [RecommandUsersService],
    }).compile();

    controller = module.get<RecommandUsersController>(RecommandUsersController);
  });

  it('should be defined', () => {
    expect(controller).toBeDefined();
  });
});
