import { Test, TestingModule } from '@nestjs/testing';
import { RecommandUsersService } from './recommand-users.service';

describe('RecommandUsersService', () => {
  let service: RecommandUsersService;

  beforeEach(async () => {
    const module: TestingModule = await Test.createTestingModule({
      providers: [RecommandUsersService],
    }).compile();

    service = module.get<RecommandUsersService>(RecommandUsersService);
  });

  it('should be defined', () => {
    expect(service).toBeDefined();
  });
});
