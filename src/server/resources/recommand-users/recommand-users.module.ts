import { Module } from '@nestjs/common';
import { RecommandUsersService } from './recommand-users.service';
import { RecommandUsersController } from './recommand-users.controller';

@Module({
  controllers: [RecommandUsersController],
  providers: [RecommandUsersService],
})
export class RecommandUsersModule {}
