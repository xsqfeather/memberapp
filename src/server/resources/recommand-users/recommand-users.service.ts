import { Injectable } from '@nestjs/common';
import { CreateRecommandUserDto } from './dto/create-recommand-user.dto';
import { UpdateRecommandUserDto } from './dto/update-recommand-user.dto';

@Injectable()
export class RecommandUsersService {
  create(createRecommandUserDto: CreateRecommandUserDto) {
    return 'This action adds a new recommandUser';
  }

  findAll() {
    return `This action returns all recommandUsers`;
  }

  findOne(id: number) {
    return `This action returns a #${id} recommandUser`;
  }

  update(id: number, updateRecommandUserDto: UpdateRecommandUserDto) {
    return `This action updates a #${id} recommandUser`;
  }

  remove(id: number) {
    return `This action removes a #${id} recommandUser`;
  }
}
