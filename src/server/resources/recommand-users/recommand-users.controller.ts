import {
  Controller,
  Get,
  Post,
  Body,
  Patch,
  Param,
  Delete,
} from '@nestjs/common';
import { RecommandUsersService } from './recommand-users.service';
import { CreateRecommandUserDto } from './dto/create-recommand-user.dto';
import { UpdateRecommandUserDto } from './dto/update-recommand-user.dto';

@Controller('recommand-users')
export class RecommandUsersController {
  constructor(private readonly recommandUsersService: RecommandUsersService) {}

  @Post()
  create(@Body() createRecommandUserDto: CreateRecommandUserDto) {
    return this.recommandUsersService.create(createRecommandUserDto);
  }

  @Get()
  findAll() {
    return this.recommandUsersService.findAll();
  }

  @Get(':id')
  findOne(@Param('id') id: string) {
    return this.recommandUsersService.findOne(+id);
  }

  @Patch(':id')
  update(
    @Param('id') id: string,
    @Body() updateRecommandUserDto: UpdateRecommandUserDto,
  ) {
    return this.recommandUsersService.update(+id, updateRecommandUserDto);
  }

  @Delete(':id')
  remove(@Param('id') id: string) {
    return this.recommandUsersService.remove(+id);
  }
}
