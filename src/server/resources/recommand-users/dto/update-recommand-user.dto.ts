import { PartialType } from '@nestjs/swagger';
import { CreateRecommandUserDto } from './create-recommand-user.dto';

export class UpdateRecommandUserDto extends PartialType(
  CreateRecommandUserDto,
) {}
