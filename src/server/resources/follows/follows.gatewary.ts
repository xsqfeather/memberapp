import {
  WebSocketGateway,
  SubscribeMessage,
  MessageBody,
  WebSocketServer,
  ConnectedSocket,
} from '@nestjs/websockets';
import { FollowsService } from './follows.service';
import { Server } from 'socket.io';
import { OnEvent } from '@nestjs/event-emitter';
import {
  FANS_COUNT_CHANGE,
  FOLLOWINGS_COUNT_CHANGE,
  FRIENDS_COUNT_CHANGE,
} from '../../common/Emitters';

@WebSocketGateway()
export class FollowsGateway {
  @WebSocketServer()
  server: Server;
  constructor(private readonly followsService: FollowsService) {}

  @SubscribeMessage('followingsCount')
  async followersCount(@MessageBody() countCondition: { fansId: number }, @ConnectedSocket() client:any) {
    const rlt = await this.followsService.count(countCondition);
    return {
      count: rlt,
    };
  }

  @SubscribeMessage('friendsCount')
  async countFriends(@MessageBody() countCondition: { userId: number }) {
    const { userId } = countCondition;
    const rlt = await this.followsService.countFriends(userId);
    return {
      count: rlt,
    };
  }

  @SubscribeMessage('fansCount')
  async fansCount(@MessageBody() { userId }: { userId: number }) {
    const rlt = await this.followsService.count({ userId });
    return {
      count: rlt,
    };
  }

  @OnEvent(FANS_COUNT_CHANGE)
  async watchFansCountChange({ userId }) {
    const rlt = await this.followsService.count({ userId });
    this.server.emit(`${FANS_COUNT_CHANGE}_${userId}`, {
      count: rlt,
    });
  }

  @OnEvent(FOLLOWINGS_COUNT_CHANGE)
  async watchFollowingsChange(params:any) {
    console.log({params});
    
    const rlt = await this.followsService.count({ fansId: params.fansId });
    console.log("关注事件", rlt);
    
    this.server.emit(`${FOLLOWINGS_COUNT_CHANGE}_${params.fansId}`, {
      count: rlt,
    });
  }

  @OnEvent(FRIENDS_COUNT_CHANGE)
  async watchFriendsChange({ userId }) {
    const rlt = await this.followsService.count({ fansId: userId });
    this.server.emit(`${FRIENDS_COUNT_CHANGE}_${userId}`, {
      count: rlt,
    });
  }
}
