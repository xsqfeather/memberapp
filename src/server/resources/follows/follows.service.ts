import {
  HttpException,
  HttpStatus,
  Injectable,
  UseGuards,
} from '@nestjs/common';
import { EventEmitter2 } from '@nestjs/event-emitter';
import { InjectRepository } from '@nestjs/typeorm';
import { In, Repository } from 'typeorm';
import { JwtAuthGuard } from '../../auth/jwt-auth.guard';
import {
  FANS_COUNT_CHANGE,
  FOLLOWINGS_COUNT_CHANGE,
  USER_FOLLOW_USER,
  USER_UNFOLLOW_USER,
} from '../../common/Emitters';
import { User } from '../users/entities/user.entity';
import { UsersService } from '../users/users.service';
import { CreateFollowDto } from './dto/create-follow.dto';
import { Follow } from './entities/follow.entity';

@Injectable()
export class FollowsService {
  constructor(
    @InjectRepository(Follow)
    private followsRepository: Repository<Follow>,
    private usersService: UsersService,
    private eventEmitter: EventEmitter2,
  ) {}

  //关注
  async follow(createFollowDto: CreateFollowDto) {
    const follower = this.followsRepository.create(createFollowDto);
    await this.followsRepository.save(follower);
    
    this.eventEmitter.emit(FANS_COUNT_CHANGE, {
      userId: follower.userId,
    });
    this.eventEmitter.emit(FOLLOWINGS_COUNT_CHANGE, {
      fansId: follower.fansId,
    });
    this.eventEmitter.emit(USER_FOLLOW_USER, {
      followUserId: createFollowDto.userId,
      userId: createFollowDto.fansId,
    });
    return follower;
  }

  count(countCondition: any) {
    return this.followsRepository.count(countCondition);
  }

  //取消关注
  async unfollow({ where }) {
    const followers = await this.followsRepository.find({
      where: {
        ...where,
      },
    });
    await this.followsRepository.remove(followers);
    this.eventEmitter.emit(FANS_COUNT_CHANGE, { userId: where.userId });
    this.eventEmitter.emit(FOLLOWINGS_COUNT_CHANGE, { fansId: where.fansId });
    this.eventEmitter.emit(USER_UNFOLLOW_USER, {
      followUserId: where.userId, //被关注者
      userId: where.fansId, //关注者
    });
    return followers;
  }

  //获取搜索我的粉丝

  async getFans({ range, sort, where }) {
    const { userId, fansIds, username, phone, nickname } = where;

    const order = {};
    order[sort[0]] = sort[1];
    let filter: any = {
      userId,
    };
    if (fansIds && fansIds.length > 0) {
      filter.fansId = In(fansIds);
    }
    const follows = await this.followsRepository.find({
      order,
      skip: range[0],
      take: range[1] - range[0] + 1,
      where: {
        ...filter,
      },
    });
    const users = await this.usersService.findByIds(
      follows.map((f) => f.fansId),
      {
        username,
        phone,
        nickname,
      },
    );
    return users;
  }

  //获取搜索我的关注

  async getFollowings({ range, sort, where }) {
    const { userIds, fansId, username, phone, nickname, userId } = where;
    const order = {};
    order[sort[0]] = sort[1];
    const take = range[1] - range[0] + 1;
    const filter: any = {
      fansId: userId,
    };
    if (userIds) {
      filter.userId = In(userIds);
    }
    const follows = await this.followsRepository.find({
      order,
      skip: range[0],
      take: take,
      where: {
        fansId,
      },
    });
    const users = await this.usersService.findByIds(
      follows.map((f) => f.userId),
      {
        username,
        phone,
        nickname,
      },
    );
    console.log({ users });

    return users;
  }

  async getFriendsByUserId(userId: number, skip: number, take: number) {
    const friends = await this.followsRepository
      .createQueryBuilder('follow')
      .innerJoinAndSelect(
        Follow,
        'fan',
        'follow.userId = fan.fansId and follow.fansId = fan.userId',
      )
      .where(
        `
        follow.fansId = :userId 
        `,
        { userId },
      )
      .skip(skip)
      .take(take)
      .orderBy('follow.updatedAt', 'DESC')
      .getMany();

    return friends;
  }

  countFriends(userId: number) {
    return this.followsRepository
      .createQueryBuilder('follow')
      .innerJoinAndSelect(
        Follow,
        'fan',
        'follow.userId = fan.fansId and follow.fansId = fan.userId',
      )
      .where(
        `
        follow.fansId = :userId 
        `,
        { userId },
      )
      .getCount();
  }

  async getFriends({ range, sort, where }: any) {
    const order = {};
    order[sort[0]] = sort[1];
    const take = range[1] - range[0] + 1;
    const { userId, username, phone, nickname } = where;
    const friends = await this.getFriendsByUserId(userId, range[0], take);
    const [users, count] = await this.usersService.findByIds(
      friends.map((f: any) => f.userId),
      {
        username,
        phone,
        nickname,
      },
    );

    return [users, count];
  }

  // 获取我关注的， 最新发帖的人的用户列表
  recentPostUsers(fansId: number, skip: number, take: number) {
    return this.followsRepository
      .createQueryBuilder('follow')
      .innerJoinAndSelect(User, 'user', 'user.id = follow.userId')
      .where(
        `
        follow.fansId = :fansId 
        `,
        { fansId },
      )
      .orderBy('user.lastestPostDate', 'DESC')
      .skip(skip)
      .take(take)
      .getManyAndCount();
  }

  getFansIds(userId: number, skip: number, take: number) {
    return this.followsRepository
      .createQueryBuilder('f')
      .where(`f.userId = :userId`, { userId })
      .skip(skip)
      .take(take)
      .orderBy('f.id', 'DESC')
      .getManyAndCount();
  }
}
