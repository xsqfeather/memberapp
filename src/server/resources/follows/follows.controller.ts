import {
  Controller,
  Get,
  Post,
  Body,
  Delete,
  Res,
  Query,
  HttpException,
  HttpStatus,
  UseGuards,
  Req,
  Param,
} from '@nestjs/common';
import { FollowsService } from './follows.service';
import { CreateFollowDto } from './dto/create-follow.dto';
import { Request, Response } from 'express';
import { In } from 'typeorm';
import { JwtAuthGuard } from '../../auth/jwt-auth.guard';
import { User } from '../users/entities/user.entity';

@Controller('follows')
export class FollowsController {
  constructor(private readonly followsService: FollowsService) {}

  @Post('followings')
  follow(@Body() createFollowDto: CreateFollowDto) {
    return this.followsService.follow(createFollowDto);
  }

  @Delete('unfollow') //todo types
  async unfollow(@Query() query: any) {
    const { filter } = query;

    const filterObj = JSON.parse(filter);

    let where: any = {};
    if (filterObj.id) {
      where = {
        id: In(filterObj.id),
      };
    }
    if (filterObj.userId) {
      where.userId = filterObj.userId;
    }

    if (filterObj.fansId) {
      where.fansId = filterObj.fansId;
    }

    await this.followsService.unfollow({ where });

    if (filterObj.id) {
      return [...JSON.parse(query.filter).id];
    }
    return filterObj;
  }

  @UseGuards(JwtAuthGuard)
  @Get('friends') //todo types
  async getFriends(
    @Res() res: Response,
    @Req() req: Request,
    @Query() query: any,
  ) {
    const user: any = (req as any).user;
    const { userId } = user;
    const { filter, range, sort } = query;
    let filterObj,
      rangeArr,
      sortArr: any = {};
    try {
      filterObj = JSON.parse(filter || '{}');
      rangeArr = JSON.parse(range || '[0,9]');
      sortArr = JSON.parse(sort || '["updatedAt", "DESC"]');
    } catch (error) {
      throw new HttpException(
        {
          status: HttpStatus.BAD_REQUEST,
          error: `json parse errors where query one of { filter, range, sort}`,
        },
        HttpStatus.BAD_REQUEST,
      );
    }
    let where: any = { ...filterObj, userId };

    const [results, count] = await this.followsService.getFriends({
      range: rangeArr,
      sort: sortArr,
      where,
    });
    res.setHeader(
      'Content-Range',
      `exams ${rangeArr[0]}-${rangeArr[1]}/${count}`,
    );
    res.send(results);
  }

  @UseGuards(JwtAuthGuard)
  @Get('fans') //todo types
  async getFans(
    @Res() res: Response,
    @Req() req: Request,
    @Query() query: any,
  ) {
    const user: any = (req as any).user;
    const { userId } = user;

    const { filter, range, sort } = query;
    let filterObj,
      rangeArr,
      sortArr: any = {};
    try {
      filterObj = JSON.parse(filter || '{}');
      rangeArr = JSON.parse(range || '[0,9]');
      sortArr = JSON.parse(sort || '["updatedAt", "DESC"]');
    } catch (error) {
      throw new HttpException(
        {
          status: HttpStatus.BAD_REQUEST,
          error: `json parse errors where query one of { filter, range, sort}`,
        },
        HttpStatus.BAD_REQUEST,
      );
    }
    let where: any = { ...filterObj, userId };
    const [results, count] = await this.followsService.getFans({
      range: rangeArr,
      sort: sortArr,
      where,
    });
    res.setHeader(
      'Content-Range',
      `exams ${rangeArr[0]}-${rangeArr[1]}/${count}`,
    );
    res.send(results);
  }

  @UseGuards(JwtAuthGuard)
  @Get('followings')
  async getFollowings(
    @Res() res: Response,
    @Req() req: Request,
    @Query() query: any,
  ) {
    const user: any = (req as any).user;
    const { userId } = user;
    const { filter, range, sort } = query;
    let filterObj,
      rangeArr,
      sortArr: any = {};
    try {
      filterObj = JSON.parse(filter || '{}');
      rangeArr = JSON.parse(range || '[0,9]');
      sortArr = JSON.parse(sort || '["updatedAt", "DESC"]');
    } catch (error) {
      throw new HttpException(
        {
          status: HttpStatus.BAD_REQUEST,
          error: `json parse errors where query one of { filter, range, sort}`,
        },
        HttpStatus.BAD_REQUEST,
      );
    }
    let where: any = { ...filterObj, fansId: userId };

    const [results, count] = await this.followsService.getFollowings({
      range: rangeArr,
      sort: sortArr,
      where,
    });

    res.setHeader(
      'Content-Range',
      `exams ${rangeArr[0]}-${rangeArr[1]}/${count}`,
    );
    res.send(results);
  }

  @Get('test')
  async test(){
    return 'test'
  }
}
