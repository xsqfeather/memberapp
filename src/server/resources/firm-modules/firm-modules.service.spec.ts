import { Test, TestingModule } from '@nestjs/testing';
import { FirmModulesService } from './firm-modules.service';

describe('FirmModulesService', () => {
  let service: FirmModulesService;

  beforeEach(async () => {
    const module: TestingModule = await Test.createTestingModule({
      providers: [FirmModulesService],
    }).compile();

    service = module.get<FirmModulesService>(FirmModulesService);
  });

  it('should be defined', () => {
    expect(service).toBeDefined();
  });
});
