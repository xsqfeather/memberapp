import { Injectable } from '@nestjs/common';
import { CreateFirmModuleDto } from './dto/create-firm-module.dto';
import { UpdateFirmModuleDto } from './dto/update-firm-module.dto';

@Injectable()
export class FirmModulesService {
  create(createFirmModuleDto: CreateFirmModuleDto) {
    return 'This action adds a new firmModule';
  }

  findAll() {
    return `This action returns all firmModules`;
  }

  findOne(id: number) {
    return `This action returns a #${id} firmModule`;
  }

  update(id: number, updateFirmModuleDto: UpdateFirmModuleDto) {
    return `This action updates a #${id} firmModule`;
  }

  remove(id: number) {
    return `This action removes a #${id} firmModule`;
  }
}
