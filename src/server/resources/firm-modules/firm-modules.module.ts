import { Module } from '@nestjs/common';
import { FirmModulesService } from './firm-modules.service';
import { FirmModulesController } from './firm-modules.controller';

@Module({
  controllers: [FirmModulesController],
  providers: [FirmModulesService],
})
export class FirmModulesModule {}
