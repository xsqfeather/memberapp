import {
  Controller,
  Get,
  Post,
  Body,
  Patch,
  Param,
  Delete,
} from '@nestjs/common';
import { FirmModulesService } from './firm-modules.service';
import { CreateFirmModuleDto } from './dto/create-firm-module.dto';
import { UpdateFirmModuleDto } from './dto/update-firm-module.dto';

@Controller('firm-modules')
export class FirmModulesController {
  constructor(private readonly firmModulesService: FirmModulesService) {}

  @Post()
  create(@Body() createFirmModuleDto: CreateFirmModuleDto) {
    return this.firmModulesService.create(createFirmModuleDto);
  }

  @Get()
  findAll() {
    return this.firmModulesService.findAll();
  }

  @Get(':id')
  findOne(@Param('id') id: string) {
    return this.firmModulesService.findOne(+id);
  }

  @Patch(':id')
  update(
    @Param('id') id: string,
    @Body() updateFirmModuleDto: UpdateFirmModuleDto,
  ) {
    return this.firmModulesService.update(+id, updateFirmModuleDto);
  }

  @Delete(':id')
  remove(@Param('id') id: string) {
    return this.firmModulesService.remove(+id);
  }
}
