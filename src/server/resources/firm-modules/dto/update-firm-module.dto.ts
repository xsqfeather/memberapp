import { PartialType } from '@nestjs/swagger';
import { CreateFirmModuleDto } from './create-firm-module.dto';

export class UpdateFirmModuleDto extends PartialType(CreateFirmModuleDto) {}
