import { Test, TestingModule } from '@nestjs/testing';
import { FirmModulesController } from './firm-modules.controller';
import { FirmModulesService } from './firm-modules.service';

describe('FirmModulesController', () => {
  let controller: FirmModulesController;

  beforeEach(async () => {
    const module: TestingModule = await Test.createTestingModule({
      controllers: [FirmModulesController],
      providers: [FirmModulesService],
    }).compile();

    controller = module.get<FirmModulesController>(FirmModulesController);
  });

  it('should be defined', () => {
    expect(controller).toBeDefined();
  });
});
