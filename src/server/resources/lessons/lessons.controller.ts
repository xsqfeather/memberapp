import {
  Controller,
  Get,
  Post,
  Body,
  Param,
  Delete,
  Res,
  Query,
  HttpException,
  HttpStatus,
  Put,
  ParseArrayPipe,
  ParseIntPipe,
} from '@nestjs/common';
import { LessonsService } from './lessons.service';
import { CreateLessonDto } from './dto/create-lesson.dto';
import { UpdateLessonDto } from './dto/update-lesson.dto';
import { Response } from 'express';
import { In } from 'typeorm';
import { ApiOperation, ApiQuery, ApiTags } from '@nestjs/swagger';

@ApiTags('课程Lessons')
@Controller('lessons')
export class LessonsController {
  constructor(private readonly lessonsService: LessonsService) {}

  @Post()
  @ApiOperation({ summary: '创建课程' })
  create(@Body() createLessonDto: CreateLessonDto) {
    return this.lessonsService.create(createLessonDto);
  }

  @Get()
  @ApiOperation({ summary: '获取所有课程' })
  @ApiQuery({
    name: 'filter',
    description: '过滤, Record<roleTagId|courseId|q, any>',
    required: false,
  })
  @ApiQuery({
    name: 'range',
    description: '分页, [start: number, end: number]',
    required: false,
  })
  @ApiQuery({
    name: 'sort',
    description: '排序, [field: string, order: "DESC" | "ASC"]',
    required: false,
  })
  async findAll(@Res() res: Response, @Query() query: any) {
    const { filter, range, sort } = query;
    let filterObj,
      rangeArr,
      sortArr = {};
    try {
      filterObj = JSON.parse(filter || '{}');
      rangeArr = JSON.parse(range || '[0,9]');
      sortArr = JSON.parse(sort || '["updatedAt", "DESC"]');
    } catch (error) {
      throw new HttpException(
        {
          status: HttpStatus.BAD_REQUEST,
          error: `json parse errors where query one of { filter, range, sort}`,
        },
        HttpStatus.BAD_REQUEST,
      );
    }
    let where: any = {};
    ['roleTagId', 'courseId', 'q'].forEach((key) => {
      if (Reflect.has(filterObj, key)) {
        where[key] = filterObj[key];
      }
    });
    const [lesson, count] = await this.lessonsService.findAll({
      range: rangeArr,
      sort: sortArr,
      where,
    });
    res.setHeader(
      'Content-Range',
      `courses ${rangeArr[0]}-${rangeArr[1]}/${count}`,
    );
    res.send(lesson);
  }

  @Get('/find-by-tags')
  @ApiOperation({ summary: '根据角色或者标签获取课程' })
  @ApiQuery({
    name: 'roleIds',
    type: 'array<number>',
    description: '角色ID',
    required: false,
  })
  @ApiQuery({
    name: 'tagIds',
    type: 'array<number>',
    description: '标签ID',
    required: false,
  })
  async findByTagOrRole(
    @Query('roleIds', new ParseArrayPipe({ optional: true, items: Number }))
    roleIds?: number[],
    @Query('tagIds', new ParseArrayPipe({ optional: true, items: Number }))
    tagIds?: number[],
  ) {
    // if (roleIds) roleIds = roleIds.map(Number).filter(Boolean);
    // if (tagIds) tagIds = tagIds.map(Number).filter(Boolean);
    if (roleIds && tagIds) {
      return await this.lessonsService.findByTagsAndRoles(tagIds, roleIds);
    } else if (roleIds) {
      return await this.lessonsService.findByRoles(roleIds);
    } else if (tagIds) {
      return await this.lessonsService.findByTags(tagIds);
    }
    return [];
  }

  @Get('/find-by-course')
  @ApiOperation({ summary: '根据课程系列获取课程' })
  @ApiQuery({
    name: 'courseId',
    type: 'number',
    description: '课程系列ID',
  })
  async findByCourse(
    @Query('courseId', ParseIntPipe)
    courseId: number,
  ) {
    return this.lessonsService.findByCourse(courseId);
  }

  @Get(':id')
  @ApiOperation({ summary: '根据ID获取课程' })
  findOne(@Param('id') id: string) {
    return this.lessonsService.findOne(+id).then((res) => {
      res.nodes = res.nodes.sort((a, b) => a.order - b.order);
      return res;
    });
  }

  @Put(':id')
  @ApiOperation({ summary: '更新课程' })
  update(@Param('id') id: string, @Body() updateLessonDto: UpdateLessonDto) {
    return this.lessonsService.update(+id, updateLessonDto);
  }

  @Delete(':id')
  @ApiOperation({ summary: '删除课程' })
  remove(@Param('id') id: string) {
    return this.lessonsService.remove(+id);
  }

  @Delete()
  @ApiOperation({ summary: '删除一些课程' })
  async removeMany(@Query() query: any) {
    const { filter } = query;
    const filterObj = JSON.parse(filter);
    const where = {
      id: In(filterObj.id),
    };
    await this.lessonsService.removeAll({ where });
    return [...JSON.parse(query.filter).id];
  }
}
