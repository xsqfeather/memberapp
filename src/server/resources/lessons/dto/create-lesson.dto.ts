import { ApiProperty } from '@nestjs/swagger';
import { IsString, IsInt, IsArray, IsOptional, IsUrl } from 'class-validator';

export class CreateLessonDto {
  @IsString()
  @ApiProperty({ name: 'title', description: '课程标题' })
  title: string;

  @IsString()
  @ApiProperty({ name: 'body', description: '课程描述' })
  body: string;

  @IsInt()
  @ApiProperty({ name: 'courseId', description: '课程系列ID' })
  courseId: number;

  @IsInt()
  @ApiProperty({ name: 'planetId', description: '课程所属星球ID' })
  planetId: number;

  @IsArray()
  @ApiProperty({ name: 'tagIds', description: '课程标签' })
  tagIds: number[];

  @IsArray()
  @ApiProperty({ name: 'roleIds', description: '课程所属角色' })
  roleIds: number[];

  @IsOptional()
  @IsUrl()
  @ApiProperty({ name: 'coverUrl', description: '封面图地址', required: false })
  coverUrl: string;
}
