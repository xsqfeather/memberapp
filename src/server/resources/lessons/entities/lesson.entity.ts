import {
  Column,
  CreateDateColumn,
  DeleteDateColumn,
  Entity,
  JoinColumn,
  ManyToOne,
  OneToMany,
  PrimaryGeneratedColumn,
  UpdateDateColumn,
} from 'typeorm';
import { Course } from '../../courses/entities/course.entity';
import { LessonNode } from '../../lesson-nodes/entities/lesson-node.entity';
import { Planet } from '../../planets/entities/planet.entity';

@Entity()
export class Lesson {
  @PrimaryGeneratedColumn()
  id: number;

  @Column()
  title: string;

  @Column()
  body: string;

  @ManyToOne(() => Course, (course) => course.lessons)
  @JoinColumn({
    name: 'courseId',
  })
  course: Course;

  @Column({ nullable: true })
  courseId: number;

  @ManyToOne(() => Planet)
  @JoinColumn({
    name: 'planetId',
  })
  planet: Planet;

  @Column({ nullable: true })
  planetId: number;

  @Column({ type: 'jsonb', default: [] })
  tagIds: number[];

  @Column({ type: 'jsonb', default: [] })
  roleIds: number[];

  @OneToMany(() => LessonNode, (nodes) => nodes.lesson)
  nodes: LessonNode[];

  @CreateDateColumn()
  createdAt: Date;

  @UpdateDateColumn()
  updatedAt: Date;

  @DeleteDateColumn()
  deletedAt: Date;

  @Column({
    default: false,
  })
  isPublished: boolean;

  @Column({ nullable: true })
  coverUrl: string;
}
