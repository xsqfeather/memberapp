import { Injectable } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { Repository } from 'typeorm';
import { CreateLessonDto } from './dto/create-lesson.dto';
import { UpdateLessonDto } from './dto/update-lesson.dto';
import { Lesson } from './entities/lesson.entity';

@Injectable()
export class LessonsService {
  constructor(
    @InjectRepository(Lesson)
    private readonly lessonsRepository: Repository<Lesson>,
  ) {}

  async create(createLessonDto: CreateLessonDto) {
    const lesson = this.lessonsRepository.create(createLessonDto);
    await this.lessonsRepository.save(lesson);
    return lesson;
  }

  async findAll({ sort, range, where }) {
    const order = {};
    order[sort[0]] = sort[1];
    return this.lessonsRepository.findAndCount({
      order,
      skip: range[0],
      take: range[1] - range[0] + 1,
      relations: ['planet', 'course'],
      where,
    });
  }

  async findOne(id: number) {
    return this.lessonsRepository.findOne({
      where: { id },
      relations: ['planet', 'course', 'nodes', 'nodes.video'],
    });
  }

  async update(id: number, updateLessonDto: UpdateLessonDto) {
    const lesson = this.lessonsRepository.findOne(id);
    Reflect.deleteProperty(updateLessonDto, 'id');
    Reflect.deleteProperty(updateLessonDto, 'createdAt');
    Reflect.deleteProperty(updateLessonDto, 'deletedAt');
    Reflect.deleteProperty(updateLessonDto, 'updatedAt');
    Reflect.deleteProperty(updateLessonDto, 'nodes');
    Reflect.deleteProperty(updateLessonDto, 'course');
    Reflect.deleteProperty(updateLessonDto, 'planet');
    Reflect.deleteProperty(updateLessonDto, 'published');
    await this.lessonsRepository.update(id, {
      ...updateLessonDto,
    });
    return lesson;
  }

  async remove(id: number) {
    return this.lessonsRepository.softDelete(id);
  }

  async removeAll({ where }: { where: any }) {
    return await this.lessonsRepository.softDelete({ ...where });
  }

  async findByCourse(courseId: number) {
    return await this.lessonsRepository.find({
      where: { courseId },
      relations: ['course', 'planet'],
    });
  }

  async findByTags(tags: number[]) {
    return await this.lessonsRepository
      .createQueryBuilder('lesson')
      .leftJoinAndSelect('lesson.planet', 'planet')
      .leftJoinAndSelect('lesson.course', 'course')
      .where(tags.map((tag) => `"tagIds" ::jsonb @> '${tag}'`).join(' or '))
      .getMany();
  }
  async findByRoles(roles: number[]) {
    return await this.lessonsRepository
      .createQueryBuilder('lesson')
      .leftJoinAndSelect('lesson.planet', 'planet')
      .leftJoinAndSelect('lesson.course', 'course')
      .where(roles.map((tag) => `"roleIds" ::jsonb @> '${tag}'`).join(' or '))
      .getMany();
  }
  async findByTagsAndRoles(tags: number[], roles: number[]) {
    return await this.lessonsRepository
      .createQueryBuilder('lesson')
      .leftJoinAndSelect('lesson.planet', 'planet')
      .leftJoinAndSelect('lesson.course', 'course')
      .where(
        `(${roles
          .map((tag) => `"roleIds" ::jsonb @> '${tag}'`)
          .join(' or ')}) AND (${tags
          .map((tag) => `"tagIds" ::jsonb @> '${tag}'`)
          .join(' or ')})`,
      )
      .getMany();
  }
}
