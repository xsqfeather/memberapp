import { Module } from '@nestjs/common';
import { FakeUsersService } from './fake-users.service';
import { FakeUsersController } from './fake-users.controller';
import { UsersModule } from '../users/users.module';
import { LokiModule } from '../../loki/loki.module';

@Module({
  imports: [UsersModule, LokiModule],
  controllers: [FakeUsersController],
  providers: [FakeUsersService],
})
export class FakeUsersModule {}
