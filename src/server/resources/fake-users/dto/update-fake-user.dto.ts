import { PartialType } from '@nestjs/swagger';
import { CreateFakeUserDto } from './create-fake-user.dto';

export class UpdateFakeUserDto extends PartialType(CreateFakeUserDto) {}
