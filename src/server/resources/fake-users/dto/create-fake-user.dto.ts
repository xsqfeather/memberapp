export class CreateFakeUserDto {
  avatar: string;
  personalNote: string;
  followFreq: number;
  nickname: string;
  phone: string;
  postFreq: number;
  email: string;
  registerFirmId?: number;
  username?: string;
}
