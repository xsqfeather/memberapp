import {
  Controller,
  Get,
  Post,
  Body,
  Patch,
  Param,
  Delete,
  Query,
  Res,
  Req,
  HttpException,
  HttpStatus,
} from '@nestjs/common';
import { FakeUsersService } from './fake-users.service';
import { CreateFakeUserDto } from './dto/create-fake-user.dto';
import { UpdateFakeUserDto } from './dto/update-fake-user.dto';
import { Request, Response } from 'express';
import { In } from 'typeorm';

@Controller('fake-users')
export class FakeUsersController {
  constructor(private readonly fakeUsersService: FakeUsersService) {}

  @Post()
  create(@Body() createFakeUserDto: CreateFakeUserDto, @Req() req: Request) {
    const firmId = (req as any).firmId;
    return this.fakeUsersService.create({
      ...createFakeUserDto,
      registerFirmId: firmId,
    });
  }

  @Get()
  async findAll(
    @Query() query: any,
    @Res() res: Response,
    @Req() req: Request,
  ) {
    const { filter, range, sort } = query;
    let filterObj,
      rangeArr,
      sortArr = {};
    try {
      filterObj = JSON.parse(filter || '{}');
      rangeArr = JSON.parse(range || '[0,9]');
      sortArr = JSON.parse(sort || '["updatedAt", "DESC"]');
    } catch (error) {
      throw new HttpException(
        {
          status: HttpStatus.BAD_REQUEST,
          error: `json parse errors where query one of { filter, range, sort}`,
        },
        HttpStatus.BAD_REQUEST,
      );
    }
    const where = {
      isRobot: true,
    };
    const [users, count] = await this.fakeUsersService.findAll({
      range: rangeArr,
      sort: sortArr,
      where,
    });
    res.setHeader(
      'Content-Range',
      `fake-users ${rangeArr[0]}-${rangeArr[1]}/${count}`,
    );
    res.send(users);
  }

  @Get(':id')
  findOne(@Param('id') id: string) {
    return this.fakeUsersService.findOne(+id);
  }

  @Patch(':id')
  update(
    @Param('id') id: string,
    @Body() updateFakeUserDto: UpdateFakeUserDto,
  ) {
    return this.fakeUsersService.update(+id, updateFakeUserDto);
  }

  @Delete(':id')
  remove(@Param('id') id: string) {
    return this.fakeUsersService.remove(+id);
  }

  @Delete()
  async removeAll(@Query() query: any) {
    const { filter } = query;
    const filterObj = JSON.parse(filter);
    const where = {
      id: In(filterObj.id),
    };
    await this.fakeUsersService.removeAll({ where });
    return [...JSON.parse(query.filter).id];
  }
}
