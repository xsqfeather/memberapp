import { Injectable } from '@nestjs/common';
import { EventEmitter2 } from 'eventemitter2';
import { LokiService } from '../../loki/loki.service';
import { UsersService } from '../users/users.service';
import { CreateFakeUserDto } from './dto/create-fake-user.dto';
import { UpdateFakeUserDto } from './dto/update-fake-user.dto';

@Injectable()
export class FakeUsersService {
  constructor(
    private readonly usersService: UsersService,
    private readonly lokiService: LokiService,
  ) {}
  async create(createFakeUserDto: CreateFakeUserDto) {
    const {
      avatar,
      nickname,
      postFreq,
      username,
      personalNote,
      phone,
      email,
      registerFirmId,
      followFreq,
    } = createFakeUserDto;
    const user = await this.usersService.create({
      postFreq,
      phone,
      email,
      registerFirmId,
      followFreq,
      isRobot: true,
      nickname,
      username,
      profile: {
        nickname,
        avatar,
        personalNote,
      },
    });

    const cacheUserCollection = this.lokiService.collection('fake_users');
    await cacheUserCollection.insert({
      ...user,
    });

    return user;
  }

  findAll({ range: rangeArr, sort: sortArr, where }) {
    return this.usersService.findAll({
      range: rangeArr,
      sort: sortArr,
      where,
    });
  }

  findOne(id: number) {
    return `This action returns a #${id} fakeUser`;
  }

  update(id: number, updateFakeUserDto: UpdateFakeUserDto) {
    return `This action updates a #${id} fakeUser`;
  }

  remove(id: number) {
    return `This action removes a #${id} fakeUser`;
  }

  findEveryOneHourPostFakeUser() {
    return this.usersService.findOne(1);
  }

  async removeAll({ where }: { where: any }) {
    return await this.usersService.removeAll({ ...where });
  }
}
