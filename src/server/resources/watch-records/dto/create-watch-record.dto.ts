import { ApiProperty } from '@nestjs/swagger';

export class CreateWatchRecordDto {
  @ApiProperty({ name: 'lessonNodeId', description: '观看的课程' })
  lessonNodeId: number;
}
