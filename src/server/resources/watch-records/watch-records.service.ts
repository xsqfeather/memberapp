import { Injectable } from '@nestjs/common';
import { CreateWatchRecordDto } from './dto/create-watch-record.dto';
import { InjectRepository } from '@nestjs/typeorm';
import { WatchRecord } from './entities/watch-record.entity';
import { Repository, In } from 'typeorm';
import { Lesson } from '../lessons/entities/lesson.entity';

@Injectable()
export class WatchRecordsService {
  constructor(
    @InjectRepository(WatchRecord)
    private readonly recordRepository: Repository<WatchRecord>,
    @InjectRepository(Lesson)
    private readonly lessonRepository: Repository<Lesson>,
  ) {}
  async create(userId: number, createWatchRecordDto: CreateWatchRecordDto) {
    const result = await this.recordRepository.create({
      ...createWatchRecordDto,
      userId,
    });
    await this.recordRepository.save(result);
    return result.id;
  }
  async findByLesson(userId: number, lessonId: number) {
    const result = await this.lessonRepository.findOne(
      { id: lessonId },
      { relations: ['nodes'] },
    );
    if (!result) return [];
    return this.recordRepository.find({
      lessonNodeId: In(result.nodes.map((it) => it.id)),
      userId: userId,
    });
  }
  async findByPlanet(userId: number, planetId: number) {
    const result = await this.lessonRepository.find({
      where: { planetId },
      relations: ['nodes'],
    });
    return this.recordRepository.find({
      lessonNodeId: In(
        result.map(({ nodes }) => nodes.map((it) => it.id)).flat(),
      ),
      userId: userId,
    });
  }
  async calculateCredits(userId: number, planetId: number) {
    const result = await this.findByPlanet(userId, planetId);
    return result.length * 40;
  }
}
