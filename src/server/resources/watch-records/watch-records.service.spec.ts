import { Test, TestingModule } from '@nestjs/testing';
import { WatchRecordsService } from './watch-records.service';

describe('WatchRecordsService', () => {
  let service: WatchRecordsService;

  beforeEach(async () => {
    const module: TestingModule = await Test.createTestingModule({
      providers: [WatchRecordsService],
    }).compile();

    service = module.get<WatchRecordsService>(WatchRecordsService);
  });

  it('should be defined', () => {
    expect(service).toBeDefined();
  });
});
