import { Module } from '@nestjs/common';
import { WatchRecordsService } from './watch-records.service';
import { WatchRecordsController } from './watch-records.controller';
import { TypeOrmModule } from '@nestjs/typeorm';
import { WatchRecord } from './entities/watch-record.entity';
import { Lesson } from '../lessons/entities/lesson.entity';

@Module({
  imports: [TypeOrmModule.forFeature([WatchRecord, Lesson])],
  controllers: [WatchRecordsController],
  providers: [WatchRecordsService],
  exports: [WatchRecordsService],
})
export class WatchRecordsModule {}
