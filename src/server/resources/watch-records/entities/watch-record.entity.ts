import {
  Column,
  CreateDateColumn,
  DeleteDateColumn,
  Entity,
  JoinColumn,
  ManyToOne,
  PrimaryGeneratedColumn,
  UpdateDateColumn,
} from 'typeorm';
import { LessonNode } from '../../lesson-nodes/entities/lesson-node.entity';
import { User } from '../../users/entities/user.entity';

@Entity()
export class WatchRecord {
  @PrimaryGeneratedColumn()
  id: number;

  @ManyToOne(() => User)
  @JoinColumn({
    name: 'userId',
  })
  user: User;

  @Column()
  userId: number;

  @ManyToOne(() => LessonNode)
  @JoinColumn({
    name: 'lessonNodeId',
  })
  lessonNode: LessonNode;
  @Column()
  lessonNodeId: number;

  @CreateDateColumn()
  createdAt: Date;

  @UpdateDateColumn()
  updatedAt: Date;

  @DeleteDateColumn()
  deletedAt: Date;
}
