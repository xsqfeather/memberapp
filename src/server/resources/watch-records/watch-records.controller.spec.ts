import { Test, TestingModule } from '@nestjs/testing';
import { WatchRecordsController } from './watch-records.controller';
import { WatchRecordsService } from './watch-records.service';

describe('WatchRecordsController', () => {
  let controller: WatchRecordsController;

  beforeEach(async () => {
    const module: TestingModule = await Test.createTestingModule({
      controllers: [WatchRecordsController],
      providers: [WatchRecordsService],
    }).compile();

    controller = module.get<WatchRecordsController>(WatchRecordsController);
  });

  it('should be defined', () => {
    expect(controller).toBeDefined();
  });
});
