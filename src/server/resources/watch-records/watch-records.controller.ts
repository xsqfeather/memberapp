import {
  Controller,
  Get,
  Post,
  Body,
  Query,
  ParseIntPipe,
  UseGuards,
} from '@nestjs/common';
import { WatchRecordsService } from './watch-records.service';
import { CreateWatchRecordDto } from './dto/create-watch-record.dto';
import {
  ApiBearerAuth,
  ApiOperation,
  ApiQuery,
  ApiTags,
} from '@nestjs/swagger';
import { JwtAuthGuard } from '../../auth/jwt-auth.guard';
import { User } from '../../decorators/User';

@ApiTags('观看记录WatchRecords')
@UseGuards(JwtAuthGuard)
@ApiBearerAuth()
@Controller('watch-records')
export class WatchRecordsController {
  constructor(private readonly watchRecordsService: WatchRecordsService) {}

  @Post()
  @ApiOperation({ summary: '创建观看记录' })
  create(
    @Body() createWatchRecordDto: CreateWatchRecordDto,
    @User('userId') userId: number,
  ) {
    return this.watchRecordsService.create(userId, createWatchRecordDto);
  }

  @Get('/find-by-lesson')
  @ApiOperation({ summary: '根据课程查询观看记录' })
  @ApiQuery({ name: 'lessonId', description: '课程ID' })
  findByLesson(
    @User('userId') userId: number,
    @Query('lessonId', ParseIntPipe) lessonId: number,
  ) {
    return this.watchRecordsService.findByLesson(userId, lessonId);
  }

  @Get('/find-by-planet')
  @ApiOperation({ summary: '根据星球查询观看记录' })
  @ApiQuery({ name: 'planetId', description: '星球ID' })
  findByPlanet(
    @User('userId') userId: number,
    @Query('planetId', ParseIntPipe) planetId: number,
  ) {
    return this.watchRecordsService.findByPlanet(userId, planetId);
  }
}
