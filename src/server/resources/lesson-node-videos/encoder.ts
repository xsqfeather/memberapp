import { spawn } from 'child_process';
import { accessSync, constants as FSConstants, mkdirSync } from 'fs';

/**
 *
 * @param src 欲编码流媒体的文件
 * @param outDir 输出路径
 * @param outName m3u8文件名
 * @returns 成功resolve路径,失败reject错误信息
 */
export function encodeToHLS(
  src: string,
  outDir: string,
  outName: string,
): Promise<string> {
  //#region 路径创建,ffmpeg不会自动创建输出路径
  try {
    accessSync(outDir, FSConstants.F_OK);
  } catch (error) {
    mkdirSync(outDir, { recursive: true });
  }
  //#endregion
  /*
    ffmpeg -i ${src} -profile:v baseline -level 3.0 -start_number 0 -hls_time 5 -hls_list_size 0 -f hls ${outDir}/${outName}.m3u8 
  */
  return new Promise((resolve, reject) => {
    const ffmpegProcess = spawn(`ffmpeg`, [
      '-i',
      `${src}`,
      '-profile:v',
      'baseline',
      '-level',
      '3.0',
      '-start_number',
      '0',
      '-hls_time',
      '5',
      '-hls_list_size',
      '0',
      '-f',
      'hls',
      `${outDir}/${outName}.m3u8`,
    ]);

    let errorInfo: string[] = [];
    //经过测试,ffmpeg调用下 stderr才是输出 stdout无用
    ffmpegProcess.stderr.on('data', (chunk: Buffer) => {
      let _data = chunk.toString();
      if (_data.includes('Error')) {
        errorInfo.push(_data);
      }
      console.log(_data);
    });

    //为0表示正常退出,非0表示有错误
    ffmpegProcess.on('close', (code) => {
      if (code === 0) {
        resolve(`${outDir}/${outName}.m3u8`);
      } else {
        reject(errorInfo.join('\r\n'));
      }
    });
  });
}
