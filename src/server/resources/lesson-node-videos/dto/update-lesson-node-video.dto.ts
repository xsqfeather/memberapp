import { PartialType } from '@nestjs/swagger';
import { CreateLessonNodeVideoDto } from './create-lesson-node-video.dto';

export class UpdateLessonNodeVideoDto extends PartialType(
  CreateLessonNodeVideoDto,
) {}
