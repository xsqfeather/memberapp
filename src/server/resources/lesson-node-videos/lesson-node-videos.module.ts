import { Module } from '@nestjs/common';
import { LessonNodeVideosService } from './lesson-node-videos.service';
import { LessonNodeVideosController } from './lesson-node-videos.controller';
import { TypeOrmModule } from '@nestjs/typeorm';
import { Video } from './entities/video.entity';

@Module({
  imports: [TypeOrmModule.forFeature([Video])],
  controllers: [LessonNodeVideosController],
  providers: [LessonNodeVideosService],
})
export class LessonNodeVideosModule {}
