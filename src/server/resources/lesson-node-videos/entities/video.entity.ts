import {
  Column,
  CreateDateColumn,
  DeleteDateColumn,
  Entity,
  OneToMany,
  PrimaryGeneratedColumn,
  UpdateDateColumn,
} from 'typeorm';
import { LessonNode } from '../../lesson-nodes/entities/lesson-node.entity';

@Entity()
export class Video {
  @PrimaryGeneratedColumn()
  id: number;

  @Column()
  uuid: string;

  @Column({
    nullable: true,
  })
  originFilename: string;

  @Column({
    nullable: true,
  })
  cover: string;

  @Column({
    nullable: true,
  })
  gifPreview: string;

  @Column()
  label: string;

  @Column({
    default: false,
  })
  isLive: boolean;

  @Column({
    default: false,
  })
  isUploaded: boolean;

  @Column({ nullable: true })
  liveUrl: string;

  @Column({
    nullable: true,
  })
  originalname: string;

  @Column({
    nullable: true,
  })
  size: number;

  @Column({ nullable: true })
  downloadUrl: string;

  @Column({ nullable: true })
  magnetURI: string;

  @Column({ nullable: true })
  ipfsId: string;

  @Column({ nullable: true })
  poster: string;

  @Column({ nullable: true })
  previewGif: string;

  @CreateDateColumn()
  createdAt: Date;

  @UpdateDateColumn()
  updatedAt: Date;

  @DeleteDateColumn()
  deletedAt: Date;

  @OneToMany(() => LessonNode, (lessonNode) => lessonNode.video)
  lessonNodes: LessonNode[];
}
