import { HttpException, Injectable } from '@nestjs/common';
import { In, Repository } from 'typeorm';
import { Video } from './entities/video.entity';
import { UpLoadDTO } from '../../upload/dto/upload.dto';
import { join } from 'path';
import { mkdir, readdir, writeFile, access, rename, rm } from 'fs/promises';
import { streamMerge } from '../../upload/streamMerge';
import { constants as FSConstants } from 'fs';
import * as md5 from 'md5';
import * as OSS from 'ali-oss';
import { InjectRepository } from '@nestjs/typeorm';
import { encodeToHLS } from './encoder';
import { v4 as uuidv4 } from 'uuid';

//视频上传
@Injectable()
export class LessonNodeVideosService {
  constructor(
    @InjectRepository(Video)
    private readonly videosReposiry: Repository<Video>,
  ) {}

  async sliceUploadBefore(body: UpLoadDTO) {
    const sliceTempPath = process.env.tempUrl;
    const publicPath = process.env.uploadUrl;
    const dirName = join(
      sliceTempPath,
      `/[${body.chunkTotal}]${body.fileMd5}/`,
    );
    const fileName = `${body.fileMd5}.${body.fileName.split('.')[1]}`;
    /* console.log(`[sliceTempPath]`, sliceTempPath);
    console.log(`[dir]`, dirName); */

    const video = await this.videosReposiry.findOne({
      originFilename: fileName,
    });
    if (video) {
      return video;
    }

    try {
      await mkdir(dirName, { recursive: true });
    } catch (error) {
      console.log(error);
      throw new HttpException(error, 500);
    }
    return 'ready';
  }
  async sliceUploadChunk(body: UpLoadDTO, file: Express.Multer.File) {
    const sliceTempPath = process.env.tempUrl;

    const dirName = join(
      sliceTempPath,
      `/[${body.chunkTotal}]${body.fileMd5}/`,
    );
    const chunkPath = join(dirName, `[${body.position}]${body.chunkMd5}`);
    //缓存是否存在,存在则直接返回ok，md5是否正确,正确才写入缓存
    try {
      await access(chunkPath, FSConstants.F_OK);
      return 'ok';
    } catch (error) {
      if (body.chunkMd5 === md5(file.buffer)) {
        writeFile(chunkPath, file.buffer)
          .then((value) => {
            return 'ok';
          })
          .catch((e) => {
            throw new HttpException(error, 500);
          });
      }
    }
  }
  async sliceUploadEnd(body: UpLoadDTO) {
    const publicPath = process.env.uploadUrl;
    const livePath = process.env.liveUrl;
    const sliceTempPath = process.env.tempUrl;

    const dirName = join(
      sliceTempPath,
      `/[${body.chunkTotal}]${body.fileMd5}/`,
    );
    const fileName = `${body.fileMd5}.${body.fileName.split('.')[1]}`;
    const filePath = join(dirName, fileName);
    const destPath = join(publicPath, fileName);
    try {
      //讲chunk排序放入数组
      const chunkFiles = await this.readChunks(dirName);
      await this.mergeChunks(chunkFiles, filePath);
      await rename(filePath, destPath);
      await rm(dirName, { recursive: true });
    } catch (error) {
      throw new HttpException(error, 500);
    }
    let video = this.createVideo(body);

    const hlsOutDir = join(livePath, video.uuid);
    try {
      await this.save(video);
      encodeToHLS(destPath, hlsOutDir, video.uuid).then(async (path) => {
        video.liveUrl = `/live/${video.uuid}/${video.uuid}.m3u8`;
        video.isLive = true;
        await this.save(video);
      });
    } catch (error) {
      console.log('lesson-node-video save失败', error);
      throw new HttpException(error, 500);
    }

    return video;
  }

  createVideo(body: UpLoadDTO) {
    const uuid = uuidv4();
    const video = this.videosReposiry.create({
      //切片上传用了md5命名保存, originalname被用作文件原名
      //originFilename被用作保存文件的名称
      originFilename: `${body.fileMd5}.${body.fileName.split('.')[1]}`,
      originalname: body.fileName,
      label: body.fileName,
      size: Number(body.fileSize),
      downloadUrl: `/uploads/${body.fileMd5}.${body.fileName.split('.')[1]}`,
      uuid,
      isUploaded: true,
    });
    return video;
  }

  async save(video: Video) {
    return await this.videosReposiry.save(video);
  }

  async findAll({ sort, range, where }) {
    const order = {};
    order[sort[0]] = sort[1];
    const videos = await this.videosReposiry.findAndCount({
      order,
      skip: range[0],
      take: range[1] - range[0] + 1,
      where,
    });
    return videos;
  }

  deleteLiveUrl(uuid: string) {
    rm(`${process.env.liveUrl}/${uuid}`).catch((err) => {
      console.log(`${process.env.liveUrl}/${uuid} remove failed`);
    });
  }

  deleteDownloadUrl(originFileName: string) {
    rm(`${process.env.uploadUrl}/${originFileName}`).catch((err) => {
      console.log(`${process.env.uploadUrl}/${originFileName} remove failed`);
    });
  }

  async remove(id: number) {
    const video = await this.videosReposiry.findOne(id);
    const result = await this.videosReposiry.remove(video);
    //删除成功返回对象 Video{} 失败 undefined
    if (result) {
      this.deleteDownloadUrl(video.downloadUrl);
      this.deleteLiveUrl(video.uuid);
    }
    return result;
  }

  async findOne(id: number) {
    return await this.videosReposiry.findOne(id);
  }

  async readChunks(path: string) {
    const files = await readdir(path);
    const chunks: string[] = [];
    //确定切片顺序
    for (const file of files) {
      if (file[0] === '[') {
        const idx = file.match(/\[([0-9]+)\]/)[1];
        chunks[idx] = join(path, file);
      }
    }
    return chunks;
  }
  async mergeChunks(chunks: string[], targetFile: string) {
    try {
      return await streamMerge(chunks, targetFile);
    } catch (error) {
      console.log(error);
      throw new HttpException('MergeStream error', 500);
    }
  }
}
