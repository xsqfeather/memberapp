import { Test, TestingModule } from '@nestjs/testing';
import { LessonNodeVideosController } from './lesson-node-videos.controller';
import { LessonNodeVideosService } from './lesson-node-videos.service';

describe('LessonNodeVideosController', () => {
  let controller: LessonNodeVideosController;

  beforeEach(async () => {
    const module: TestingModule = await Test.createTestingModule({
      controllers: [LessonNodeVideosController],
      providers: [LessonNodeVideosService],
    }).compile();

    controller = module.get<LessonNodeVideosController>(
      LessonNodeVideosController,
    );
  });

  it('should be defined', () => {
    expect(controller).toBeDefined();
  });
});
