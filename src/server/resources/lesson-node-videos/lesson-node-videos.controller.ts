import {
  Body,
  Controller,
  Post,
  Req,
  Res,
  UseInterceptors,
  UploadedFile,
  Get,
  Param,
  HttpException,
  HttpStatus,
  Query,
  Delete,
} from '@nestjs/common';
import { UpLoadDTO } from '../../upload/dto/upload.dto';
import { LessonNodeVideosService } from './lesson-node-videos.service';
import { FileInterceptor } from '@nestjs/platform-express';
import { Response } from 'express';

@Controller('lesson-node-videos')
export class LessonNodeVideosController {
  constructor(private lessonNodeVideosService: LessonNodeVideosService) {}

  @Post()
  @UseInterceptors(FileInterceptor('file'))
  async create(
    @UploadedFile() file: Express.Multer.File,
    @Body() body: UpLoadDTO,
  ) {
    switch (body.message) {
      case 'sliceStart':
        return this.lessonNodeVideosService.sliceUploadBefore(body);
      case 'sliceChunk':
        return this.lessonNodeVideosService.sliceUploadChunk(body, file);
      case 'sliceEnd':
        return this.lessonNodeVideosService.sliceUploadEnd(body);
    }
    /* return 201 */
  }

  @Get(':id')
  findOne(@Param('id') id: string) {
    return this.lessonNodeVideosService.findOne(+id);
  }

  @Get()
  async findAll(@Res() res: Response, @Query() query: any) {
    const { filter, range, sort } = query;
    let filterObj,
      rangeArr,
      sortArr = {};
    try {
      filterObj = JSON.parse(filter || '{}');
      rangeArr = JSON.parse(range || '[0,9]');
      sortArr = JSON.parse(sort || '["updatedAt", "DESC"]');
    } catch (error) {
      throw new HttpException(
        {
          status: HttpStatus.BAD_REQUEST,
          error: `json parse errors where query one of { filter, range, sort}`,
        },
        HttpStatus.BAD_REQUEST,
      );
    }
    let where: any = {};

    const [videos, count]: any = await this.lessonNodeVideosService.findAll({
      range: rangeArr,
      sort: sortArr,
      where,
    });
    res.setHeader(
      'Content-Range',
      `videos ${rangeArr[0]}-${rangeArr[1]}/${count}`,
    );
    res.send(videos);
  }

  @Delete(':id')
  remove(@Param('id') id: string) {
    return this.lessonNodeVideosService.remove(+id);
  }
}
