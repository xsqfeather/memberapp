import { Test, TestingModule } from '@nestjs/testing';
import { LessonNodeVideosService } from './lesson-node-videos.service';

describe('LessonNodeVideosService', () => {
  let service: LessonNodeVideosService;

  beforeEach(async () => {
    const module: TestingModule = await Test.createTestingModule({
      providers: [LessonNodeVideosService],
    }).compile();

    service = module.get<LessonNodeVideosService>(LessonNodeVideosService);
  });

  it('should be defined', () => {
    expect(service).toBeDefined();
  });
});
