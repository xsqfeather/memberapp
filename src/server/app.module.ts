import { Module, Global } from '@nestjs/common';
import { ConfigModule, ConfigService } from '@nestjs/config';
import { LokiModule } from './loki/loki.module';
import { AppService } from './app.service';
import { ResourcesModule } from './resources/resources.module';
import { AuthModule } from './auth/auth.module';
import { AppController } from './app/app.controller';
import { ServeStaticModule } from '@nestjs/serve-static';
import { IpStorageModule } from './ip-storage/ip-storage.module';
import { TasksModule } from './tasks/tasks.module';
import { ScheduleModule } from '@nestjs/schedule';
import { LevelCacheModule } from './level-cache/level-cache.module';
import { VideoTasksModule } from './video-tasks/video-tasks.module';
import { UsersModule } from './resources/users/users.module';
import { FackersModule } from './fackers/fackers.module';
import { EventEmitterModule } from '@nestjs/event-emitter';
import { UploadModule } from './upload/upload.module';
import { CasbinModule } from './casbin/casbin.module';

@Global()
@Module({
  imports: [AuthModule],
  exports: [AuthModule],
})
class GlobalModule {}

@Module({
  imports: [
    ScheduleModule.forRoot(),
    EventEmitterModule.forRoot({
      maxListeners: 100,
    }),
    ConfigModule.forRoot({
      envFilePath: `env/${process.env.NODE_ENV || 'development'}.env`,
      expandVariables: true,
    }),

    ServeStaticModule.forRoot({
      rootPath: process.cwd() + '/public',
    }),
    LokiModule,
    ResourcesModule,
    AuthModule,
    IpStorageModule,
    TasksModule,
    LevelCacheModule,
    VideoTasksModule,
    UsersModule,
    FackersModule,
    UploadModule,
    CasbinModule,
    GlobalModule,
  ],
  providers: [AppService],
  controllers: [AppController],
})
export class AppModule {}
