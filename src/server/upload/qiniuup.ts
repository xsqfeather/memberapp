import { conf, zone, form_up, auth, rs } from 'qiniu';
import { v4 as uuidV4 } from 'uuid';
const accessKey = 'sVnAV5k1Wyu3pUfY6ydsq5guSRqbHVfCZ4mh4oMD';
const secretKey = 'JeoeswhZ63cMuaTa_eS7bjWmEhbQlJAtHnD3rzd2';
const mac = new auth.digest.Mac(accessKey, secretKey);
const url = 'http://qvn9xwlrv.hn-bkt.clouddn.com/';

const putImagePolicyOption: rs.PutPolicyOptions = {
  scope: `wwog`,
};
const putImgPolicy = new rs.PutPolicy(putImagePolicyOption);
const uploadToken = putImgPolicy.uploadToken(mac);

const config = new conf.Config({
  zone: zone.Zone_z2,
});

export function putImageToQiniuOss(filepath: string): Promise<{ url: string }> {
  return new Promise((resolve, reject) => {
    let formUploader = new form_up.FormUploader(config);
    let putExtra = new form_up.PutExtra();
    let ext = filepath.split('.');
    formUploader.putFile(
      uploadToken,
      'imgs/' + uuidV4() + '.' + ext[1],
      filepath,
      putExtra,
      (resErr, resBody, resInfo) => {
        if (resErr) {
          reject(resErr);
        }
        resolve({ url: `${url}${resBody.key}` });
      },
    );
  });
}
