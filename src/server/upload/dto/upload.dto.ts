export class UpLoadDTO {
  //常规key
  message: 'sliceStart' | 'sliceChunk' | 'sliceEnd';
  fileMd5: string;
  fileName: string;
  fileSize: string;
  fileType: string;
  //slice key
  chunkTotal?: string;
  chunkMd5?: string;
  position?: string;
}
