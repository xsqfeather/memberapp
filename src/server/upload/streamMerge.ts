import { createReadStream, createWriteStream, WriteStream } from 'fs';

/**
 * 合并文件
 * @param files 文件数组,数组内文件带完整路径
 * @param targetFile 目标文件
 * @returns 创建成功返回targetFile
 */
export function streamMerge(
  files: string[],
  targetFile: string,
): Promise<string | Error> {
  return new Promise((resolve, reject) => {
    const writeStream = createWriteStream(targetFile);
    streamMergeHandle(files, writeStream, reject);
    writeStream.on('finish', () => {
      resolve(targetFile);
    });
    writeStream.on('error', (e) => {
      reject(e);
    });
  });
}

function streamMergeHandle(
  files: string[],
  fileWriteStream: WriteStream,
  reject: (reason?: any) => void,
) {
  if (!files.length) {
    return fileWriteStream.end();
  }
  const currentFile = files.shift() as string;
  const currentReadStream = createReadStream(currentFile);

  currentReadStream.pipe(fileWriteStream, { end: false });
  currentReadStream.on('end', () => {
    streamMergeHandle(files, fileWriteStream, reject);
  });

  currentReadStream.on('error', (e) => {
    fileWriteStream.close();
    reject(e);
  });
}
