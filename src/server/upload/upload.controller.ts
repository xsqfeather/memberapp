import {
  Body,
  Controller,
  Post,
  Req,
  Res,
  UseInterceptors,
  UploadedFile,
  HttpException,
  HttpStatus,
} from '@nestjs/common';
import { UpLoadDTO } from './dto/upload.dto';
import { UploadService } from './upload.service';
import { FileInterceptor } from '@nestjs/platform-express';
//test uploadModule
@Controller('slice-upload')
export class UploadController {
  constructor(private uploadService: UploadService) {}

  @Post()
  @UseInterceptors(FileInterceptor('file'))
  async create(
    @UploadedFile() file: Express.Multer.File,
    @Body() body: UpLoadDTO,
  ) {
    /* switch (body.message) {
      case 'sliceStart':
        return this.uploadService.sliceUploadBefore(body);
      case 'sliceChunk':
        return this.uploadService.sliceUploadChunk(body, file);
      case 'sliceEnd':
        return this.uploadService.sliceUploadEnd(body);
    }
 */
    throw new HttpException('接口已经停用', HttpStatus.BAD_REQUEST);
    /* return 201 */
  }
}
