import { HttpException, Injectable } from '@nestjs/common';
import { UpLoadDTO } from './dto/upload.dto';
import { join } from 'path';
import { mkdir, readdir, writeFile, access, rename, rm } from 'fs/promises';
import { streamMerge } from './streamMerge';
import { constants as FSConstants } from 'fs';
import * as md5 from 'md5';
import * as OSS from 'ali-oss';

const ossConfig = {
  bucket: 'xindalu-dev',
  region: 'oss-cn-chengdu',
  accessKeyId: 'LTAIBYIHofssXbWt',
  accessKeySecret: 'RkNyIBM8hz6PpjRJ4MFyiRI75VbmPp',
};
const client = new OSS(ossConfig);

@Injectable()
export class UploadService {
  async sliceUploadBefore(body: UpLoadDTO) {
    const sliceTempPath = process.env.tempUrl;
    const publicPath = process.env.uploadUrl;
    const dirName = join(
      sliceTempPath,
      `/[${body.chunkTotal}]${body.fileMd5}/`,
    );
    const fileName = `${body.fileMd5}.${body.fileName.split('.')[1]}`;
    console.log(`[sliceTempPath]`, sliceTempPath);
    console.log(`[dir]`, dirName);
    //先看看路径下是否有该数据,有就返回图片url
    //最终文件所在地
    const destPath = join(publicPath, fileName);
    try {
      await access(destPath, FSConstants.F_OK);
      const rlt = await this.putToAliyunOSS(fileName, destPath);
      console.log({ rlt });

      return rlt.url;
      // return `/uploads/${fileName}`
    } catch (error) {
      try {
        await mkdir(dirName, { recursive: true });
      } catch (error) {
        console.log(error);
        throw new HttpException(error, 500);
      }
    }
    return 'ready';
  }

  async sliceUploadChunk(body: UpLoadDTO, file: Express.Multer.File) {
    const sliceTempPath = process.env.tempUrl;

    const dirName = join(
      sliceTempPath,
      `/[${body.chunkTotal}]${body.fileMd5}/`,
    );
    const chunkPath = join(dirName, `[${body.position}]${body.chunkMd5}`);
    //缓存是否存在,存在则直接返回ok，md5是否正确,正确才写入缓存
    try {
      await access(chunkPath, FSConstants.F_OK);
      return 'ok';
    } catch (error) {
      if (body.chunkMd5 === md5(file.buffer)) {
        writeFile(chunkPath, file.buffer)
          .then((value) => {
            return 'ok';
          })
          .catch((e) => {
            throw new HttpException(error, 500);
          });
      }
    }
  }

  async sliceUploadEnd(body: UpLoadDTO) {
    const publicPath = process.env.uploadUrl;
    const sliceTempPath = process.env.tempUrl;

    const dirName = join(
      sliceTempPath,
      `/[${body.chunkTotal}]${body.fileMd5}/`,
    );
    const fileName = `${body.fileMd5}.${body.fileName.split('.')[1]}`;
    const filePath = join(dirName, fileName);
    //最终文件所在地
    const destPath = join(publicPath, fileName);
    try {
      //讲chunk排序放入数组
      const chunkFiles = await this.readChunks(dirName);
      await this.mergeChunks(chunkFiles, filePath);
      await rename(filePath, destPath);
      await rm(dirName, { recursive: true });
    } catch (error) {
      throw new HttpException(error, 500);
    }
    const rlt = await this.putToAliyunOSS(fileName, destPath);
    console.log({ rlt });

    return rlt.url;
    // return `/uploads/${fileName}`
  }
  /**
   * 读取所有chunks并按顺序写入数组
   * @param path
   */
  async readChunks(path: string) {
    const files = await readdir(path);
    const chunks: string[] = [];
    //确定切片顺序
    for (const file of files) {
      if (file[0] === '[') {
        const idx = file.match(/\[([0-9]+)\]/)[1];
        chunks[idx] = join(path, file);
      }
    }
    return chunks;
  }

  async mergeChunks(chunks: string[], targetFile: string) {
    try {
      return await streamMerge(chunks, targetFile);
    } catch (error) {
      console.log(error);
      throw new HttpException('MergeStream error', 500);
    }
  }

  async putToAliyunOSS(filename: string, filepath: string) {
    //文件是否存在已经上传,不去考虑md5文件名的极低碰撞概率可能发生的错误
    try {
      const head_rlt = await client.head(filename);
      if (head_rlt.status != 200) {
        try {
          const put_rlt = await client.put(filename, filepath);
          return put_rlt;
        } catch (error) {
          throw error;
        }
      }
      return {
        url: `http://${ossConfig.bucket}.${ossConfig.region}.aliyuncs.com/${filename}`,
        meta: head_rlt.meta,
        res: head_rlt.res,
      };
    } catch (error) {
      try {
        const put_rlt = await client.put(filename, filepath);
        return put_rlt;
      } catch (error) {
        throw error;
      }
    }
  }
}
