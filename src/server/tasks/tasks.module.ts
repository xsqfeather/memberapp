import { Module } from '@nestjs/common';
import { LevelCacheModule } from '../level-cache/level-cache.module';
import { LokiModule } from '../loki/loki.module';
import { TasksService } from './tasks.service';

@Module({
  imports: [LokiModule, LevelCacheModule],
  providers: [TasksService],
  exports: [TasksService],
})
export class TasksModule {}
