import { Injectable, Logger } from '@nestjs/common';
import { Cron, CronExpression } from '@nestjs/schedule';
import { EventEmitter2 } from 'eventemitter2';
import { MINI_BLOG_FAKE_POST } from '../common/Emitters';
import { LokiService } from '../loki/loki.service';

@Injectable()
export class TasksService {
  private readonly logger = new Logger(TasksService.name);
  constructor(
    private readonly lokiService: LokiService,
    private eventEmitter: EventEmitter2,
  ) {}

  @Cron(CronExpression.EVERY_10_HOURS)
  async handleFakeUserPostOneHour() {
    const userCollections = this.lokiService.collection('fake_users');
    await userCollections
      .find({
        isRobot: true,
        postFreq: 1,
      })
      .forEach((user: any) => {
        this.eventEmitter.emit(MINI_BLOG_FAKE_POST, user.id);
      });
  }

  @Cron(CronExpression.EVERY_3_HOURS)
  async handleFakeUserPostThreeHour() {
    const userCollections = this.lokiService.collection('fake_users');

    await userCollections
      .find({
        isRobot: true,
        postFreq: 3,
      })
      .forEach((user: any) => {
        this.eventEmitter.emit(MINI_BLOG_FAKE_POST, user.id);
      });
  }
}
