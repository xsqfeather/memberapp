import {
  registerDecorator,
  ValidationOptions,
  ValidationArguments,
} from 'class-validator';

export const isPhone = (validationOptions?: ValidationOptions) => {
  return (object: Object, propertyName: string) => {
    registerDecorator({
      name: 'isPhone',
      target: object.constructor,
      propertyName: propertyName,
      // constraints: [property],
      options: {
        message: 'Phone number is invalid',
        ...validationOptions,
      },
      validator: {
        validate(value: any, args: ValidationArguments) {
          return /^((13[0-9])|(14[5-9])|(15([0-3]|[5-9]))|(16[6-7])|(17[1-8])|(18[0-9])|(19[1|3])|(19[5|6])|(19[8|9]))\d{8}$/.test(
            value,
          );
        },
      },
    });
  };
};
