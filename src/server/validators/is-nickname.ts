import { registerDecorator, ValidationOptions } from 'class-validator';

export const isNickname = (validationOptions?: ValidationOptions) => {
  return (object: Object, propertyName: string) => {
    registerDecorator({
      name: 'isNickname',
      target: object.constructor,
      propertyName: propertyName,
      options: {
        message: 'Nickname is invalid',
        ...validationOptions,
      },
      validator: {
        validate(value: any) {
          return /^[0-9a-zA-Z\u4e00-\u9fa5]{2,12}$/.test(value);
        },
      },
    });
  };
};
