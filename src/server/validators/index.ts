export { isPhone as IsPhone } from './is-phone';
export { isUsername as IsUsername } from './is-username';
export { isNickname as IsNickname } from './is-nickname';
