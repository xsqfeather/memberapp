import {
  registerDecorator,
  ValidationArguments,
  ValidationOptions,
} from 'class-validator';

export const isUsername = (validationOptions?: ValidationOptions) => {
  return (object: Object, propertyName: string) => {
    registerDecorator({
      name: 'isUsername',
      target: object.constructor,
      propertyName: propertyName,
      options: {
        message: 'Username is invalid',
        ...validationOptions,
      },
      validator: {
        validate(value: any, args: ValidationArguments) {
          return /^[a-zA-Z]\w{5,17}$/.test(value);
        },
      },
    });
  };
};
