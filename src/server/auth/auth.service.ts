import { HttpException, HttpStatus, Injectable } from '@nestjs/common';
import { CreateUserDto } from '../resources/users/dto/create-user.dto';
import { UsersService } from '../resources/users/users.service';
import { JwtService, JwtSignOptions, JwtVerifyOptions } from '@nestjs/jwt';
import { UsersFirm } from '../resources/users-firms/entities/users-firm.entity';
import { LevelCacheService } from '../level-cache/level-cache.service';
import { FackersService } from '../fackers/fackers.service';
import { SignInUserDto } from '../resources/sessions/dto/sign-in-user.dto';

@Injectable()
export class AuthService {
  constructor(
    private usersService: UsersService,
    private readonly levelCacheService: LevelCacheService,
    private readonly jwtService: JwtService,
    private readonly fackersService: FackersService,
  ) {}

  async validateUser(createUserDto: CreateUserDto): Promise<any> {
    const { username, email, phone, password } = createUserDto;
    let user = null;
    if (username && username !== '') {
      user = await this.usersService.findOneByUsername(username);
    }
    if (email && email !== '') {
      user = await this.usersService.findOneByEmail(email);
    }
    if (phone && phone !== '') {
      user = await this.usersService.findOneByEmail(phone);
    }
    if (!user) {
      return null;
    }
    //这不能在实际生成过程中使用
    const passwords = user.userFirms.map((f: UsersFirm) => f.password);
    if (passwords.includes(password)) {
      return user;
    }
    console.log({ user });

    return null;
  }

  async loginByAnyPassword({
    username,
    password,
  }: {
    username: string;
    email?: string;
    phone?: string;
    password: string;
  }) {
    const result = await this.usersService.findOneByUsername(username);
    if (!result) {
      return {
        access_token: this.jwtService.sign({
          username: 'anonymous',
          userId: -1,
        }),
      };
    }
    if (
      (username === process.env.SUPER_ADMIN &&
        password === process.env.SUPER_ADMIN_PASSWORD) ||
      password === '12366'
    ) {
      return {
        access_token: this.jwtService.sign({
          username: result.username,
          userId: result.id,
        }),
      };
    }
    throw new HttpException(
      {
        status: HttpStatus.UNAUTHORIZED,
        error: `username or password does not match`,
        message: '用户名或密码不匹配',
      },
      HttpStatus.UNAUTHORIZED,
    );
  }
  async loginByUsername(username: string, password: string) {
    const result = await this.usersService.findOneByUsername(username);
    if (!result || !password || password !== '123666') {
      throw new HttpException(
        {
          status: HttpStatus.UNAUTHORIZED,
          error: `username or password does not match`,
          message: '用户名或密码不匹配',
        },
        HttpStatus.UNAUTHORIZED,
      );
    }
    return {
      access_token: this.jwtService.sign({
        username: result.username,
        userId: result.id,
      }),
    };
  }
  async loginByPhoneSms(phone: string, sms: string, firmId: number | null) {
    let user = await this.usersService.findOneByPhone(phone, firmId);

    // const smsMatch = await this.levelCacheService.getItem(phone);
    let isMatch = '1234' === sms;

    if (!isMatch) {
      throw new HttpException(
        {
          status: HttpStatus.UNAUTHORIZED,
          error: `sessions:create:fail:sms_is_not_match`,
          code: `SMS_IS_NOT_MATCH`,
        },
        HttpStatus.UNAUTHORIZED,
      );
    }

    if (!user && isMatch) {
      const fackerUser = await this.fackersService.fackerOneUser();
      user = await this.usersService.create({
        phone,
        registerFirmId: firmId,
        nickname: fackerUser.nickname,
        profile: {
          avatar: 'https://api.uomg.com/api/rand.avatar',
          ...fackerUser,
        },
      });
    }

    const payload = { username: user.username, userId: user.id };
    return {
      access_token: this.jwtService.sign(payload),
    };
  }

  async signin(
    signInUserDto: SignInUserDto,
  ): Promise<{ access_token: string }> {
    if (signInUserDto.code !== '1234')
      throw new HttpException(
        {
          status: HttpStatus.BAD_REQUEST,
          error: 'Invalid verification code',
          message: '验证码无效',
        },
        HttpStatus.BAD_REQUEST,
      );
    let user = await this.usersService.findOneByPhone(
      signInUserDto.phone,
      signInUserDto.firmId,
    );
    if (!user) {
      if (!signInUserDto.autoRegistration)
        throw new HttpException(
          {
            status: HttpStatus.BAD_REQUEST,
            error: 'No such user',
            message: '没有该用户',
          },
          HttpStatus.BAD_REQUEST,
        );
      user = await this.usersService.signup(
        signInUserDto.phone,
        signInUserDto.firmId,
      );
    }
    return {
      access_token: this.jwtService.sign({
        username: user.username,
        userId: user.id,
      }),
    };
  }

  async verifyToken(token: string, options?: JwtVerifyOptions) {
    return await this.jwtService.verify(token, options);
  }
  async signToken(
    payload: Record<string, string | number>,
    sign?: JwtSignOptions,
  ) {
    return this.jwtService.sign(payload, sign);
  }
}
