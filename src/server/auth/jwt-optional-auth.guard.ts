import {
  ExecutionContext,
  ForbiddenException,
  Injectable,
  UnauthorizedException,
} from '@nestjs/common';
import { AuthGuard } from '@nestjs/passport';

@Injectable()
export class JwtOptionalAuthGuard extends AuthGuard('jwt') {
  canActivate(context: ExecutionContext) {
    const req = context.switchToHttp().getRequest();

    if (!req.firmId) {
      throw new ForbiddenException();
    }
    return super.canActivate(context);
  }

  handleRequest(err: any, user: any, info: any) {
    return user;
  }
}
