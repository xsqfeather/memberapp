import {
  CanActivate,
  ExecutionContext,
  Inject,
  Injectable,
} from '@nestjs/common';
import { Socket } from 'socket.io';
import { AuthService } from './auth.service';

@Injectable()
export class JwtSocketAuthGuard implements CanActivate {
  constructor(
    @Inject('AuthService') private readonly authService: AuthService,
  ) {}
  async canActivate(context: ExecutionContext) {
    console.log('开始鉴权');
    const client = context.switchToWs().getClient() as Socket;
    if (client['user']) return true;
    const authorization = client.handshake.headers.authorization as
      | string
      | undefined;
    if (!authorization) {
      client.emit('WILL_DISCONNECT', { code: 403, error: 'Forbidden socket' });
      client.disconnect(true);
      return false;
    }
    const bearerToken = authorization.split(' ')[1];
    try {
      client['user'] = await this.authService.verifyToken(bearerToken);
      return true;
    } catch (e) {
      client.emit('WILL_DISCONNECT', { code: 403, error: 'Forbidden socket' });
      client.disconnect(true);
      return false;
    }
  }
  static alignSocket(socket: Socket, id: string, rooms: string | string[]) {
    rooms = Array.isArray(rooms) ? rooms : [rooms];
    const { join, from } = socket.handshake.query;
    socket['authority_timer'] && clearTimeout(socket['authority_timer']);
    if (socket.id !== id) {
      socket.emit('WILL_DISCONNECT', {
        code: 401,
        message: 'Invalid socket connection',
      });
      socket.disconnect(true);
      return void 0;
    }
    if (!join || typeof join !== 'string') {
      socket.emit('WILL_DISCONNECT', {
        code: 401,
        message: 'No room specified',
      });
      socket.disconnect(true);
      return void 0;
    }
    if (rooms.every((room) => !join.includes(room))) {
      socket.emit('WILL_DISCONNECT', {
        code: 401,
        message: 'No room exists',
      });
      socket.disconnect(true);
      return void 0;
    }
    socket.join(join);
    console.log(`来自${from}:${socket.id}的请求进入了: ${join}`);
    socket.on('disconnect', () => {
      console.log(`来自${from}:${socket.id}的请求离开了: ${join}`);
    });
    return { message: 'ok!' };
  }
}
