import { Module } from '@nestjs/common';
import { ConfigModule, ConfigService } from '@nestjs/config';
import { JwtModule } from '@nestjs/jwt';
import { PassportModule } from '@nestjs/passport';
import { FackersModule } from '../fackers/fackers.module';
import { LevelCacheModule } from '../level-cache/level-cache.module';
import { UsersModule } from '../resources/users/users.module';
import { AuthService } from './auth.service';
import { JwtStrategy } from './jwt.strategy';
import { LocalStrategy } from './local.strategy';

@Module({
  imports: [
    PassportModule.register({ defaultStrategy: 'jwt' }),
    JwtModule.registerAsync({
      imports: [ConfigModule],
      useFactory: async (configService: ConfigService) => ({
        secret: process.env.APP_SECRECT,
        signOptions: { expiresIn: '60d' },
      }),
      inject: [ConfigService],
    }),
    UsersModule,
    PassportModule,
    LevelCacheModule,
    FackersModule,
  ],
  providers: [AuthService, LocalStrategy, JwtStrategy],
  exports: [AuthService, JwtModule],
})
export class AuthModule {}
