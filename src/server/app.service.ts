import { Injectable } from '@nestjs/common';
import { existsSync, mkdirSync, writeFileSync } from 'fs';

@Injectable()
export class AppService {
  onApplicationBootstrap() {
    console.log('========系统正在初始化)_============');
    console.log('========检查平台设置=============');
    console.log('========正在初始化数据============');
    console.log(process.env.SUPER_ADMIN);
    const liveUrl = process.cwd() + '/public/live';
    const uploadUrl = process.cwd() + '/public/uploads';
    const tempUrl = process.cwd() + '/public/temp';

    if (!existsSync(liveUrl)) {
      mkdirSync(liveUrl, { recursive: true });
    }

    if (!existsSync(uploadUrl)) {
      mkdirSync(uploadUrl, { recursive: true });
    }

    if (!existsSync(tempUrl)) {
      mkdirSync(tempUrl, { recursive: true });
    }
    writeFileSync(process.cwd() + '/public/index.html', '<html></html>');
    process.env.liveUrl = liveUrl;
    process.env.uploadUrl = uploadUrl;
    process.env.tempUrl = tempUrl;
  }
}
