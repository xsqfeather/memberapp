import { Injectable, NestMiddleware } from '@nestjs/common';
import { Request, Response, NextFunction } from 'express';
@Injectable()
export class NextRenderMiddleware implements NestMiddleware {
  constructor(private handle: any, private app: any) {
    this.handle = handle;
    this.app = app;
  }
  use = async (req: Request, res: Response, next: NextFunction) => {
    if (req.url.includes('/api')) {
      return next();
    }

    if (req.url.includes('/uploads')) {
      return next();
    }
    if (req.url.includes('/arc-sw.js')) {
      return next();
    }

    if (req.url.includes('/live')) {
      return next();
    }

    if (req.url.includes('/graphql')) {
      return next();
    }
    return await this.handle(req, res);
  };
}
