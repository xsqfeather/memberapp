export const ModuleKeys = {
  //1开头的基本系统
  101: 'posts', //文章管理
  102: 'banners', //首页banner管理
  103: 'videos', //视频管理
  104: 'video-live-houses', //直播房间管理
  105: 'friends', //好友管理
  106: 'users', //用户管理
  107: 'pictures', //图片管理
  108: 'video-pages', //视频页面管理
  109: 'carts', //购物车管理
  110: 'products', //商品管理
  111: 'product-orders', //商品订单管理
  112: 'appointments', //预约管理,主要用以o2o项目

  //在线教育模块
  113: 'courses',
  114: 'lessons',
  115: 'lesson-nodes', //依赖posts和videos
  116: 'lesson-exercises',
  117: 'lesson-exercises-answers',
  118: 'exam-papper', //试卷
  119: 'exam-papper-topics', //试卷题目
  120: 'exam-papper-answers',

  //众包兼职
  121: 'people-tasks',
  122: 'people-contracts', //众包合同

  //2开头的是复合模块
  202: 'E_COMMERCE', //电商模块：包含商品管理和商品订单管理，首页banner
  203: 'VIDEO_PAGE', //视频点播模块, 包括视频管理，视频页面管理，首页banner管理，
  205: 'SCHOOL', //在线教育模块，课程管理，章节和习题，依赖视频点播模块
  206: 'BLOG', //博客系统,包括博客标签，博客分类

  //6开头的一般依赖复合模块
  601: 'BLOG_COMMIT', //博客评论
  602: 'PRODUCT_AGENCY', //电商分销
};

export const ModuleDepencies = {
  202: [109, 110, 111],
};

export const ModuleEndRole = {
  1000: 'SASS_ADMIN',
  //saas 大平台管理，用以管理各个企业, 只有pc端
  1001: 'FIRM_ADMIN',
  //企业或者个人pc后台全业务管理，用于启动B2C的简单业务，比如
  1003: 'BUSINESS_ADMIN',
  //业务支撑中台管理，这个终端依赖于FIRM_ADMIN，比如美团商户端，电商商家端
  1004: 'ORDER_ADMIN',
  //业务支撑个人端， 比如骑手，滴滴司机
  1005: 'CUSTOMER',
  //个人端
};

export const ModuleEndRoleDepencies = {
  1005: 1001,
  1004: 1001,
  1003: 1001,
};
