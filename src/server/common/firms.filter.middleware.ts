import { getRepository } from 'typeorm';
import { Firm } from '../resources/firms/entities/firm.entity';
import { FirmsService } from '../resources/firms/firms.service';
import { Request, Response, NextFunction } from 'express';
import { LevelCacheService } from '../level-cache/level-cache.service';

export async function firmFilter(
  req: Request,
  res: Response,
  next: NextFunction,
) {
  const firmsRepository = getRepository(Firm);
  const cacheService = new LevelCacheService();
  const firmService = new FirmsService(firmsRepository, cacheService);
  let firmId = 0;
  const appKey = req.headers['x-app-key'];
  const host = req.headers.host;

  //优先通过appKey判断firm
  if (appKey) {
    const firm = await firmService.findOneByAppKey(appKey as any);
    firmId = firm.id;
  }
  if (host) {
    firmId = await firmService.findFirmIdByHhost(host);
  }

  if (!firmId) {
    firmId = await firmService.getDefaultFirmId();
  }

  (req as any).firmId = firmId;

  next();
}
