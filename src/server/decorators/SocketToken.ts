import { createParamDecorator, ExecutionContext } from '@nestjs/common';

export const SocketToken = createParamDecorator(
  (data: 'id', ctx: ExecutionContext) => {
    const client = ctx.switchToWs().getClient();
    return client.handshake.auth?.token;
  },
);
