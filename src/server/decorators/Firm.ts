import { createParamDecorator, ExecutionContext } from '@nestjs/common';

export const Firm = createParamDecorator(
  (data: 'id', ctx: ExecutionContext) => {
    const request = ctx.switchToHttp().getRequest();
    return data === 'id' ? request.firmId : { id: request.firmId };
  },
);
