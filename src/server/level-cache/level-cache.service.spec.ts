import { Test, TestingModule } from '@nestjs/testing';
import { LevelCacheService } from './level-cache.service';

describe('LevelCacheService', () => {
  let service: LevelCacheService;

  beforeEach(async () => {
    const module: TestingModule = await Test.createTestingModule({
      providers: [LevelCacheService],
    }).compile();

    service = module.get<LevelCacheService>(LevelCacheService);
  });

  it('should be defined', () => {
    expect(service).toBeDefined();
  });
});
