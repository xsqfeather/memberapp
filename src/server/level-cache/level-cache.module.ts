import { Module } from '@nestjs/common';
import { LevelCacheService } from './level-cache.service';

@Module({
  providers: [LevelCacheService],
  exports: [LevelCacheService],
})
export class LevelCacheModule {}
