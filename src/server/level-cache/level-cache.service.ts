import { Injectable } from '@nestjs/common';

@Injectable()
export class LevelCacheService {
  private static cache: any;
  async onModuleInit() {
    const level = require('level-party');
    LevelCacheService.cache = level('data/cache');
    console.log('=======level缓存初始化完毕=======');
  }
  setItem(keyString: string, value: string | null) {
    if (!value) {
      return false;
    }
    return new Promise((res: any, rej: any) => {
      LevelCacheService.cache.put(keyString, value, (err: any) => {
        if (err) {
          rej(err);
        } else {
          res(true);
        }
      });
    });
  }

  getItem(keyString: string): Promise<string | null> {
    return new Promise((res: any, rej: any) => {
      LevelCacheService.cache.get(keyString, function (err: any, value: any) {
        if (err) {
          res(null);
        } else {
          res(value);
        }
      });
    });
  }
}
