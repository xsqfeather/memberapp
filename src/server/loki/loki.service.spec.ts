import { Test, TestingModule } from '@nestjs/testing';
import { LokiService } from './loki.service';

describe('LokiService', () => {
  let service: LokiService;

  beforeEach(async () => {
    const module: TestingModule = await Test.createTestingModule({
      providers: [LokiService],
    }).compile();

    service = module.get<LokiService>(LokiService);
  });

  it('should be defined', () => {
    expect(service).toBeDefined();
  });
});
