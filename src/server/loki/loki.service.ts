import { Injectable } from '@nestjs/common';
import * as path from 'path';
import * as loki from 'lokijs';

@Injectable()
export class LokiService {
  private static db = null;
  onModuleInit() {
    const lfsa = require('lokijs/src/loki-fs-structured-adapter.js');
    var adapter = new lfsa();
    LokiService.db = new loki('data/db.json', {
      adapter: adapter,
      autoload: true,
      autoloadCallback: LokiService.databaseInitialize,
      autosave: true,
      verbose: true,
      autosaveInterval: 3000,
      autosaveCallback: () => {
        console.log('autosaved db');
      },
    });
  }

  static databaseInitialize = () => {
    let logs = LokiService.db.getCollection('logs');
    if (logs === null) {
      logs = LokiService.db.addCollection('logs');
    }
  };

  collection(collectonName: string) {
    let entries = LokiService.db.getCollection(collectonName);
    if (entries === null) {
      entries = LokiService.db.addCollection(collectonName);
    }
    return entries;
  }
}
