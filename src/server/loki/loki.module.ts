import { Module } from '@nestjs/common';
import { LokiService } from './loki.service';
//使用loki作为缓存

@Module({
  providers: [LokiService],
  exports: [LokiService],
})
export class LokiModule {}
